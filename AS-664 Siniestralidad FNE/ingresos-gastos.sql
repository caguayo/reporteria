



------------ EXTRACCION GASTOS POR SEGMENTO PERMANENCIA
----- TODA LA CARTERA CON MINIMA VIGENCIA
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.ca_gastos AS ( 
WITH vigencia AS (
SELECT DISTINCT 
"fld_cotrut" , 
"fld_ingreso" ,
MIN(FLD_VIGREALDESDE) AS minima_vigencia 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
"fld_cotrut",
"fld_ingreso"
ORDER BY "fld_cotrut"
)
---SELECT * FROM VIGENCIA
------------- CALCULANDO LA MINIMA VIGENCIA
, a_b AS ( 
SELECT DISTINCT 
a."fld_cotrut" , 
b."fld_ingreso",
last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig
FROM AFI.P_RDV_DST_LND_SYB.CNT a
LEFT JOIN vigencia b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
WHERE -- 202103 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA 
	  FLD_VIGREALDESDE <= 202012 AND 
	  FLD_VIGREALHASTA >= 201801
), ultima_vigencia AS ( 
SELECT 
	--*
	"fld_cotrut",
	--MINIMA_VIG,
	MAX(MINIMA_VIG) AS ultima_vigencia
FROM A_B 
--WHERE "fld_cotrut" = '009316771-6' 
	 --AND "fld_ingreso" IS NULL
GROUP BY
	"fld_cotrut"
    --MINIMA_VIG
--ORDER BY CANTIDAD DESC
),
gastos_segmento AS ( 
SELECT DISTINCT 
	SUBSTRING(dm.PERIODO,0,4) AS anio ,
	dm.RUT_TITULAR,
	(case 
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO))+1 between 0 and 12 then 1
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 13 and 24 then 2
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 25 and 36 then 3
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 37 and 48 then 4
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 49 and 60 then 5
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 61 and 72 then 6
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 73 and 84 then 7
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 85 and 96 then 8
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 97 and 108 then 9
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 109 and 120 then 10
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 121 and 240 then 11
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 241 and 360 then 12
            else 13
    end) AS ordper_gasto,
    (case 
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 0 and 12 then '00 a 12'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 13 and 24 then '13 a 24'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 25 and 36 then '25 a 36'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 37 and 48 then '37 a 48'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 49 and 60 then '49 a 60'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 61 and 72 then '61 a 72'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 73 and 84 then '73 a 84'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 85 and 96 then '85 a 96'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 97 and 108 then '97 a 108'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 109 and 120 then '109 a 120'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO))+1 between 121 and 240 then '121 a 240'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO) )+1 between 241 and 360 then '241 a 360'
            --when DATEDIFF(month, uv.minima_vig, EST.P_DDV_EST.PERIODO_FECHA(dm.PERIODO))+1 >= 361 then 13
            else '361 y m�s'
    end) AS rangoper_gasto,
    IFF(dm.ORIGEN = 'HOSPITALARIO',sum(BONIFICADO + monto_caec),0 ) AS HOSPITALARIO,
    IFF(dm.ORIGEN = 'AMBULATORIO',sum(BONIFICADO + monto_caec),0 ) AS AMBULATORIO,
    IFF(dm.ORIGEN = 'GES',sum(BONIFICADO),0 ) AS GES,
    sum(dm.MONTO_CAEC) AS CAEC,
    IFF(dm.ORIGEN = 'LICENCIA', sum(bonificado),0) AS licencia 
    --IFF(dm.ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
    --    SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
    --    SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS LICENCIA
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO dm
LEFT JOIN ultima_vigencia  uv ON (dm.RUT_TITULAR = uv."fld_cotrut" ) 
WHERE dm.PERIODO BETWEEN 201901 AND 201912
GROUP BY 
		anio,
		dm.RUT_TITULAR,
		dm.PERIODO, 
		dm.ORIGEN, 
		dm.TIPO_LICENCIA, 
		ultima_vigencia
ORDER BY 
		anio, 
		ordper_gasto
)
SELECT 
	anio,
	RUT_TITULAR,
	ordper_gasto,
	rangoper_gasto,
	sum(hospitalario)::int AS hospitalario,
	sum(ambulatorio)::int AS ambulatorio,
	sum(licencia)::int AS licencia,
	sum(ges)::int AS ges,
	sum(caec)::int AS caec
FROM gastos_segmento
WHERE RANGOPER_GASTO ='361 y m�s'--anio = 2020
GROUP BY 
	anio,
	RUT_TITULAR,
	ordper_gasto,
	rangoper_gasto
ORDER BY 
	anio,
	ordper_gasto
--LIMIT 10
)
------- UNION COM LOS GASTOS POR RUT Y FECHA DE INGRESO








                
                
----------------------------------------------------------------------------------------
------------ EXTRACCION ingresos POR SEGMENTO PERMANENCIA
----- TODA LA CARTERA CON MINIMA VIGENCIA
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.ca_ingresos AS ( 


WITH vigencia AS (
SELECT DISTINCT 
"fld_cotrut" , 
"fld_ingreso" ,
MIN(FLD_VIGREALDESDE) AS minima_vigencia
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
"fld_cotrut",
"fld_ingreso"
ORDER BY "fld_cotrut"
)
---SELECT * FROM VIGENCIA
------------- CALCULANDO LA MINIMA VIGENCIA
, a_b AS ( 
SELECT DISTINCT 
a."fld_cotrut" , 
b."fld_ingreso",
last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig
FROM AFI.P_RDV_DST_LND_SYB.CNT a
LEFT JOIN vigencia b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
WHERE -- 202103 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA 
	  FLD_VIGREALDESDE <= 202012 AND 
	  FLD_VIGREALHASTA >= 201801
)
--SELECT * FROM a_b LIMIT 10
, ultima_vigencia AS ( 
SELECT 
	--*
	"fld_cotrut",
	--MINIMA_VIG,
	MAX(MINIMA_VIG) AS ultima_vigencia
FROM A_B 
--WHERE "fld_cotrut" = '009316771-6' 
	 --AND "fld_ingreso" IS NULL
GROUP BY
	"fld_cotrut"
    --MINIMA_VIG
--ORDER BY CANTIDAD DESC
)
--SELECT * FROM ultima_vigencia LIMIT 10
, 
ingreso_segmento AS ( 
SELECT 
	con.PERIODO_VIGENCIA , 
	uv.ultima_vigencia,
	SUBSTRING(con.PERIODO_VIGENCIA ,0,4) AS anio ,
	con.RUT_TITULAR ,
	DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA)) AS MESES,
	(case 
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 0 and 12 then 1
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 13 and 24 then 2
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 25 and 36 then 3
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 37 and 48 then 4
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 49 and 60 then 5
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 61 and 72 then 6
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 73 and 84 then 7
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 85 and 96 then 8
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 97 and 108 then 9
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 109 and 120 then 10
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 121 and 240 then 11
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 241 and 360 then 12
            else 13
    end) AS ordper_gasto,
    (case 
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 0 and 12 then '00 a 12'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 13 and 24 then '13 a 24'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 25 and 36 then '25 a 36'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 37 and 48 then '37 a 48'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 49 and 60 then '49 a 60'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 61 and 72 then '61 a 72'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 73 and 84 then '73 a 84'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 85 and 96 then '85 a 96'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 97 and 108 then '97 a 108'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 109 and 120 then '109 a 120'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 121 and 240 then '121 a 240'
            when DATEDIFF(month, uv.ultima_vigencia, EST.P_DDV_EST.PERIODO_FECHA(con.PERIODO_VIGENCIA) )+1 between 241 and 360 then '241 a 360'
            else '361 y m�s'
    end) AS rangoper_gasto,
	sum(con.PAGADO_TOTAL_PESOS)::int AS pagado_total ,
	SUM(con.MONTO_EXCESO)::int AS excesos, 
	sum(con.MONTO_EXCEDENTE)::int AS excedentes,
	sum(t12.Valor_Iva)::int AS iva
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO con
LEFT JOIN ultima_vigencia  uv ON (con.RUT_TITULAR = uv."fld_cotrut" ) 
LEFT JOIN (select 
					perpag_esperado, 
					rut_cotizante, 
					sum( Valor_Iva) as  Valor_Iva
                 from "RCD"."P_DDV_RCD"."P_DDV_RCD_V_LIBRO_VENTAS"
                 where  Codigo_Pago = 0
                 group by 
                 	perpag_esperado, 
                 	rut_cotizante
                 ) t12 ON  (con.PERIODO_VIGENCIA = t12.perpag_esperado  and con.rut_titular = t12.rut_cotizante ) 
WHERE PERIODO_VIGENCIA BETWEEN  201901 AND 201912
GROUP BY 
	con.PERIODO_VIGENCIA , 
	con.RUT_TITULAR , 
	uv.ULTIMA_VIGENCIA ,
	MESES
)
SELECT 
	anio, 
	periodo_vigencia,
	ULTIMA_VIGENCIA,
	MESES,
	RUT_TITULAR,
	ordper_gasto, 
	rangoper_gasto, 
	--sum(pagado_total) , 
	--sum(excesos), 
	--sum(excedentes), 
	--sum(iva), 
	sum(pagado_total)-sum(excesos)-sum(excedentes)-sum(iva)::int AS ingreso  
FROM ingreso_segmento
WHERE RANGOPER_GASTO ='361 y m�s' AND meses >0
GROUP BY anio, ordper_gasto, rangoper_gasto, RUT_TITULAR,periodo_vigencia, ULTIMA_VIGENCIA , MESES
ORDER BY anio, ordper_gasto
LIMIT 100


)




WITH ingreso AS (
SELECT 
	*
FROM EST.P_DDV_EST.ca_ingresos
)
,
gasto AS ( 
SELECT 
	* 
FROM EST.P_DDV_EST.ca_gastos
)
, a_b AS ( 
SELECT
	COALESCE(a.RUT_TITULAR,b.RUT_TITULAR) AS titular,
	COALESCE(a.RANGOPER_GASTO,b.RANGOPER_GASTO) AS RANGO,
	INGRESO,
	HOSPITALARIO,
	AMBULATORIO,
	LICENCIA,
	GES,
	CAEC
FROM ingreso a
FULL JOIN gasto b ON (a.RUT_TITULAR = b.RUT_TITULAR)
ORDER BY titular
)
SELECT 
	--count(1),
	--count(DISTINCT titular),
	titular,
	rango,
	sum(ingreso),
	sum(hospitalario),
	sum(ambulatorio),
	sum(licencia),
	sum(ges),
	sum(caec)
FROM a_b
GROUP BY titular, rango
--LIMIT 10
--LIMIT 1000














SELECT * FROM llamada


SELECT 
	    --"fld_cotrut", 
	    SUBSTRING("fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	    "fld_carganum" AS cant_cargas,
		"fld_concostototal"::float AS pactado,
		CASE 
			WHEN "fld_concostototal" > 99.0 THEN 'Pesos'
			ELSE 'Uf'
		END AS unidad
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE FLD_VIGREALDESDE <= 202109 AND 
	  FLD_VIGREALHASTA >= 202109 --AND "fld_concostototal" > 40.0
--ORDER BY "fld_concostototal" 
--LIMIT 500

	 	
SELECT * FROM uf

SELECT * FROM ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA 
WHERE gls_mon IN ('UF 2021/09')
LIMIT 100
	 	





----------- NO LO USE PARA FNE 
------------ EXTRACCION INGRESOS POR SEGMENTO PERMANENCIA
----- TODA LA CARTERA CON MINIMA VIGENCIA
WITH vigencia AS (
SELECT DISTINCT 
"fld_cotrut" , 
"fld_ingreso" ,
MIN(FLD_VIGREALDESDE) AS minima_vigencia 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
"fld_cotrut",
"fld_ingreso"
ORDER BY "fld_cotrut"
)
------------- CALCULANDO LA MINIMA VIGENCIA
, a_b AS ( 
SELECT DISTINCT 
a."fld_cotrut" , 
b."fld_ingreso",
last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig
FROM AFI.P_RDV_DST_LND_SYB.CNT a
LEFT JOIN vigencia b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
WHERE -- 202103 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA 
	  FLD_VIGREALDESDE <= 202012 AND 
	  FLD_VIGREALHASTA >= 201801
), 
------- UNION COM LOS INGRESOS POR RUT Y FECHA DE INGRESO
ingresos_segmento AS ( 
SELECT DISTINCT 
	DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 AS permanencia,
	SUBSTRING(cb.PERIODO,0,4) AS anio ,
	    (case 
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 0 and 12 then 1
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 13 and 24 then 2
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 25 and 36 then 3
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 37 and 48 then 4
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 49 and 60 then 5
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 61 and 72 then 6
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 73 and 84 then 7
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 85 and 96 then 8
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 97 and 108 then 9
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 109 and 120 then 10
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 121 and 240 then 11
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 241 and 360 then 12
            --when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 >= 361 then 13
            else 13
    end) AS ordper_ingreso,
    (case 
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 0 and 12 then '00 a 12'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 13 and 24 then '13 a 24'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 25 and 36 then '25 a 36'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 37 and 48 then '37 a 48'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 49 and 60 then '49 a 60'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 61 and 72 then '61 a 72'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 73 and 84 then '73 a 84'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 85 and 96 then '85 a 96'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 97 and 108 then '97 a 108'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 109 and 120 then '109 a 120'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 121 and 240 then '121 a 240'
            when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1 between 241 and 360 then '241 a 360'
            --when DATEDIFF(month, ab.minima_vig, cb.PERIODO_FECHA )+1  361 then '361 y m�s'
            else '361 y m�s'
    end) AS rangoper_ingreso, 
	--cb.ANTIGUEDAD ,
	cb.RUT_TITULAR,
	--cb.FECHA_INGRESO ,
	--ab."fld_ingreso",
	SUM(cb.INGRESOS) AS ingresos
	--COUNT(cb.RUT_TITULAR) AS cantidad 
	--cb.* 
FROM EST.P_STG_EST.CARTERA_BEN cb
LEFT JOIN a_b  ab ON (cb.RUT_TITULAR =ab."fld_cotrut" AND cb.FECHA_INGRESO=ab."fld_ingreso") 
WHERE cb.PERIODO BETWEEN 201801 AND 202012 AND cb.REGISTRO_ID = 1--AND  RUT_TITULAR ='000859901-7' 
GROUP BY 
	anio, 
	permanencia,
	ordper_ingreso,
	rangoper_ingreso,
	cb.RUT_TITULAR
ORDER BY  
	--cb.RUT_TITULAR , 
	ANIO , 
	ordper_ingreso
)
SELECT 
	*
FROM ingresos_segmento LIMIT 10



SELECT 
	anio,
	--RUT_TITULAR,
	ordper_ingreso,
	rangoper_ingreso,
	sum(ingresos)
FROM ingresos_segmento
--WHERE RANGOPER_GASTO ='361 y m�s'
GROUP BY
		anio,
		ordper_ingreso,
		rangoper_ingreso
ORDER BY 
	 	anio,
	 	ordper_ingreso
LIMIT 10

	 	

SELECT * FROM ingresos_segmento
ORDER BY ordper_ingreso desc LIMIT 100



SELECT 
	* 
FROM EST.P_STG_EST.CARTERA_BEN LIMIT 10
------------------------------ FIN INGRESOS POR SEGMENTO PERMANENCIA


	 	




