SELECT DISTINCT  top 10 
cnt."fld_cotrut" AS cotizante, 
cnt.FLD_VIGREALDESDE ,
cnt.FLD_VIGREALHASTA ,
cob."fld_emprut" AS empleador, 
pag."pag_rutemp" AS empleador_pago  
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt 
LEFT JOIN RCD.P_RDV_DST_LND_SYB.PAG_STOCK pag ON (cnt."fld_funfolio"= pag."pag_funfol" AND cnt."fld_funcorrel"=pag."pag_funcor")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.COB cob on (cnt."fld_funfolio"= cob."fld_funnotiffun" AND cnt."fld_funcorrel"=cob."fld_funcorrel")
WHERE EMPLEADOR != EMPLEADOR_PAGO 
AND cnt.FLD_VIGREALDESDE >=201701

SELECT top 3 * FROM ISA.P_RDV_DST_LND_SYB.EMP


SELECT top 10 *  FROM AFI.P_RDV_DST_LND_SYB.CNT


SELECT top 10 * FROM RCD.P_RDV_DST_LND_SYB.PAG_STOCK

SELECT top 10 * FROM AFI.P_RDV_DST_LND_SYB.COB

SELECT TOP 3 * FROM EST.P_DDV_EST.temp_emp
WHERE "fld_cotrut"='013698854-9'

----------EMPLEADORES UNICOS POR RUT
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.temp_emp AS ( 
SELECT DISTINCT 
"fld_cotrut" , --as rut_cotizante
"fld_emprut", --AS rut_empleador	
"fld_funfolio", --AS  contrato,
"fld_funcorrel" --AS correlativo,	
FROM AFI.P_RDV_DST_LND_SYB.LFC
order by "fld_cotrut" 
)

----- replicando empleadores
CREATE OR REPLACE TEMPORARY TABLE est.P_DDV_EST.ca_emp AS (
WITH empleadores AS (
SELECT
"fld_cotrut",
"fld_emprut",
"fld_funfolio",
"fld_funcorrel",
ROW_NUMBER () OVER (PARTITION BY "fld_funfolio","fld_funcorrel" ORDER BY "fld_funfolio","fld_funcorrel") AS n_veces
FROM EST.P_DDV_EST.temp_emp)
SELECT fld_cotrut,fld_funfolio,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4
FROM empleadores 
pivot(max("fld_emprut") FOR n_veces IN (1,2,3,4))
AS P (fld_cotrut,fld_funfolio,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4))


SELECT top 3 fld_cotrut, SUBSTRING(fld_cotrut,1,9)::int*5-3265112+52658  AS id_rut FROM est.P_DDV_EST.ca_emp

--------------------------------fin empleadores

SELECT DISTINCT PERIODO , COUNT(DISTINCT(ID_RUT_AFILIADO)) FROM est.P_DDV_EST.CA_AMBU
GROUP BY PERIODO 
ORDER BY PERiODO�

WITH ambu AS ( 
SELECT 
t1.IDN ,
t1.RUT_AFILIADO ,
t1.RUT_BENEFICIARIO ,
t2."fld_pscnombre" AS nombre_prestador,
t1.EMPLEADOR_1 ,
t1.EMPLEADOR_2 ,
t1.COBRADO ,
t1.BONIFICADO ,
t1.COBRADO - t1.BONIFICADO AS copago
FROM EST.P_DDV_EST.ca_ambu t1
INNER JOIN ISA.P_RDV_DST_LND_SYB.PSC t2 ON (t1.RUT_PRESTADOR = t2."fld_pscrut")
) 
SELECT 
COUNT(1) AS registros,
count(DISTINCT(IDN)) AS folios,
COUNT(DISTINCT (RUT_AFILIADO)) AS afiliados,
SUM(cobrado) AS cobrad,
sum(BONIFICADO) AS boni 
FROM ambu

SELECT * FROM EST.P_DDV_EST.ca_ambu

SELECT * FROM GTO.P_RDV_DST_LND_SYB.BON 
WHERE "fld_medrut" ='079576810-6'
LIMIT 1000


SELECT 
	*
FROM ISA.P_RDV_DST_LND_SYB.PRM_AGRUPA_PRESTADORES
WHERE "rut" ='096963910-6'


SELECT 
	"rut" ,
	count("rut") AS cant
FROM ISA.P_RDV_DST_LND_SYB.PRM_AGRUPA_PRESTADORES
--WHERE "rut" ='079576810-6'
GROUP BY "rut" 
ORDER BY cant desc
--LIMIT 10


SELECT 
	PERIODO ,
	COUNT(DISTINCT RUT_AFILIADO ) AS cant,
	SUM(COBRADO) AS cob,
	sum(BONIFICADO) AS BONI
FROM  est.P_DDV_EST.CA_AMBU 
GROUP BY PERIODO 
ORDER BY periodo  


SELECT 
	*
FROM EST.P_DDV_EST.CA_AMBU 
LIMIT 10


SELECT * FROM GTO.P_RDV_DST_LND_SYB.BON LIMIT 10

SELECT * FROM GTO.P_RDV_DST_LND_SYB.BON_CDT LIMIT 10

SELECT * FROM GTO.P_RDV_DST_LND_SYB.SRE  LIMIT 10

SELECT * FROM GTO.P_RDV_DST_LND_SYB.SRE_CDT LIMIT 10 

SELECT * FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE "fld_periodoprod" BETWEEN '2021-12-01 00:00:00' AND '2022-02-28 00:00:00'
LIMIT 1000

-------------PRESTACIONES 
/*AMBULATORIO*/
CREATE OR REPLACE TABLE est.P_DDV_EST.CA_AMBU AS (
------------BONOS
select 
	bon."fld_bonfolio" as idn, /*folio del bono*/
	bon."fld_cotrut"   as rut_afiliado, /*rut del titular*/ 
	SUBSTRING(bon."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	bon."fld_bencorrel" as bencorrel, /*c�digo beneficiario*/
	ben."fld_benrut" as rut_beneficiario,
	SUBSTRING(ben."fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	--bon."fld_funfolio" as contrato, /*contrato*/	
	--bon."fld_funcorrel" as correlativo, /*correlativo del contrato*/    	
	emp.empleador_1,
	emp.empleador_2,	
	bon."fld_medrut" as rut_prestador, /*rut del prestador*/
	prest."gls_prestador" AS nombre_prestador,
	bon."fld_bonemifec"::date as fecha, /*fecha prestaci�n*/
	EST.P_DDV_EST.FECHA_PERIODO(bon."fld_bonemifec") AS periodo,
	--year(datepart(bon."fld_bonemifec"))*100 + month(datepart(bon."fld_bonemifec")) as periodo, /*periodo*/ 
	case
    	when bon."cso_idn" > 0 and bon."fld_folioderivages" = 0 then 'CAEC'
		when bon."cso_idn" = 0 and bon."fld_folioderivages" > 0 then 'GES'
		when bon."fld_folioderivages" > 0 and bon."cso_idn" > 0 then 'GES-CAEC'
		else 'OTRO' 
	end  as activacion, /*GES, CAEC o plan complementario*/
	bon."fld_totprestaval"::int as cobrado, /*Valor cobrado por el prestador*/
	bon."fld_totbonific"::int as bonificado, /*Valor bonificado por la isapre*/
	cobrado-bonificado AS copago,
	'bon' as origen
from GTO.P_RDV_DST_LND_SYB.BON bon
LEFT JOIN ISA.P_RDV_DST_LND_SYB.PRM_AGRUPA_PRESTADORES prest ON (bon."fld_medrut"=prest."rut")
inner join AFI.P_RDV_DST_LND_SYB.BEN ben
	on bon."fld_funfolio" = ben."fld_funfolio"
	and bon."fld_funcorrel" = ben."fld_funcorrel"
	and bon."fld_bencorrel" = ben."fld_bencorrel"
	and bon."fld_bonestado" not in (3,4) 
	and bon."fld_bonemifec" between '2021-07-01 00:00:00' and '2021-12-31 23:59:59'  ----cambiar el periodo por le solicitado
left join est.P_DDV_EST.ca_emp emp
	on bon."fld_funfolio" = emp.fld_funfolio
	and bon."fld_funcorrel" = emp.fld_funcorrel	
----------------------- FIN BONOS
union 
----------------/**REEMBOLSOS**/
select 
	ree."fld_reefolio" as idn,
	ree."fld_cotrut" as rut_afiliado,
	SUBSTRING(ree."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	cdt."fld_bencorrel" as bencorrel, /*aca esta en la tabla de detalle*/
	cdt."fld_benrut" as rut_beneficiario,
	SUBSTRING(cdt."fld_benrut",1,9)::int*5-3265112+52658  AS ID_RUT_BENEFICIARIO,
	--ree."fld_funfolio" as contrato, /*contrato*/
	--ree."fld_funcorrel" as correlativo, /*correlativo del contrato*/  
	emp.empleador_1,
	emp.empleador_2, 
	cdt."fld_medrut" as rut_prestador,
	prest."gls_prestador" AS nombre_prestador,
	ree."fld_reepagfec"::date as fecha, /*fecha prestaci�n*/
	EST.P_DDV_EST.FECHA_PERIODO(ree."fld_reepagfec") AS periodo,
	--year(datepart(ree.fld_reepagfec))*100 + month(datepart(ree.fld_reepagfec)) as periodo, /*periodo*/
	case
	   when ree."cso_idn" > 0 and ree."fld_folioderivages" = 0 then 'CAEC'
	   when ree."cso_idn" = 0 and ree."fld_folioderivages" > 0 then 'GES'
	   when ree."fld_folioderivages" > 0 and ree."cso_idn" > 0 then 'GES-CAEC'
	   else 'OTRO' 
	end as activacion, /*GES, CAEC o plan complementario*/
	ree."fld_totprestaval"::int as cobrado,
	(ree."fld_totprestaval" - ree."fld_totbonific")::int as bonificado,
	cobrado-bonificado  AS copago,
	'ree' as origen
from GTO.P_RDV_DST_LND_SYB.SRE  ree
inner join GTO.P_RDV_DST_LND_SYB.SRE_CDT cdt
	on ree."fld_reefolio" = cdt."fld_reefolio"
	and cdt."fld_srecorrel" = 1
	and ree."fld_reeestado" not in (3,4) 
	and ree."fld_reepagfec" between '2021-07-01 00:00:00' and '2021-12-30 23:59:59'
LEFT JOIN ISA.P_RDV_DST_LND_SYB.PRM_AGRUPA_PRESTADORES prest ON (cdt."fld_medrut"=prest."rut")
left join est.P_DDV_EST.ca_emp emp
	on ree."fld_funfolio" = emp.fld_funfolio
	and ree."fld_funcorrel" = emp.fld_funcorrel
------------- FIN REEM
)
-------------FIN GASTOS AMBULATORIOS
	


SELECT  PERIODO, count(DISTINCT(ID_RUT_AFILIADO)) FROM EST.P_DDV_EST.CA_HOSPI
GROUP BY PERIODO 
ORDER BY PERIODO 

WITH hosp AS ( 
SELECT 
t1.IDN ,
t1.RUT_AFILIADO ,
t1.RUT_BENEFICIARIO ,
t2."fld_pscnombre" AS nombre_prestador,
t1.EMPLEADOR_1 ,
t1.EMPLEADOR_2 ,
t1.COBRADO ,
t1.BONIFICADO ,
t1.COBRADO - t1.BONIFICADO AS copago
FROM EST.P_DDV_EST.CA_HOSPI t1
INNER JOIN ISA.P_RDV_DST_LND_SYB.PSC t2 ON (t1.RUT_PRESTADOR = t2."fld_pscrut")
)
SELECT 
COUNT(1) AS registros,
count(DISTINCT(IDN)) AS folios,
COUNT(DISTINCT (RUT_AFILIADO)) AS afiliados,
SUM(cobrado) AS cobrad,
sum(BONIFICADO) AS boni 
FROM hosp 


SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_SOLTRA LIMIT 10


SELECT "fld_gescaec" , * FROM  HOS.P_RDV_DST_LND_SYB.SH_EAH  LIMIT 10

SELECT * FROM EST.P_DDV_EST.CA_HOSPI

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_SOLTRA LIMIT 10

SELECT * FROM afi.P_DDV_AFI.P_DDV_AFI_V_EPL LIMIT 20

SELECT * FROM CME.P_RDV_DST_LND_SYB.CM_DDP LIMIT 10

SELECT * FROM afi.P_DDV_AFI.P_DDV_AFI_V_DPL LIMIT 20

SELECT * FROM isa.P_RDV_DST_LND_SYB.PSC LIMIT 20

----------- HOSPITALARIO
/*HOSPITALARIO*/
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_HOSPI AS (
select distinct    
	eah."eah_idn" as idn,/*idn prestacion*/  
	eah."eah_rutcot" as rut_afiliado,/*rut afiliado*/
	SUBSTRING(eah."eah_rutcot",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	eah."eah_codben" as bencorrel,/*codigo beneficiario*/
	ben."fld_benrut" as rut_beneficiario,
	SUBSTRING(ben."fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	--eah."eah_nrocon" as contrato, /*contrato*/
	--eah."eah_nrocor" as correlativo, /*correlativo*/
	emp.empleador_1,
	emp.empleador_2, 
	eah."eah_conefe"::date as fecha, /*fecha contable*/
	est.P_DDV_EST.FECHA_PERIODO(eah."eah_conefe") AS periodo,/*periodo_contable*/
	--year(datepart(eah.eah_conefe))*100 + month(datepart(eah.eah_conefe)) as periodo,/*periodo contable*/
	sol."sol_insrut" as rut_prestador, /*rut prestador*/
	case
	    when eah."der_idn" > 0 and eah."cso_idn" = 0 then 'GES'
		when eah."cso_idn" > 0 and eah."der_idn" = 0 then 'CAEC'
		when eah."der_idn" > 0 and eah."cso_idn" > 0 then 'GES-CAEC'
		else 'OTRO' 
	end as activacion, /*prestacion ges-caec, ges o caec*/	
	eah."eah_urgencia" as urgencia, /*es urgencia*/	
	eah."eah_cobtot"::int as cobrado, /*cobrado*/
	eah."eah_valbon"::int + eah."fld_gescaec"::int as bonificado /*bonificado*/
from HOS.P_RDV_DST_LND_SYB.SH_EAH  eah 
inner join AFI.P_RDV_DST_LND_SYB.BEN ben
	on eah."eah_nrocon" = ben."fld_funfolio"
	and eah."eah_nrocor" = ben."fld_funcorrel"
	and eah."eah_codben" = ben."fld_bencorrel"
	and eah."eah_conefe" BETWEEN '2021-01-01 00:00:00' and '2021-06-30 23:59:59'
	and eah."eah_estado" not in (3,4) 
left join HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol
	on eah."sol_nrosol" = sol."sol_nrosol" 
	and eah."eah_rutcot" = sol."sol_rutafi" 
	and eah."eah_codben" = sol."sol_codben"
left join est.P_DDV_EST.ca_emp emp
	on eah."eah_nrocon" = emp.fld_funfolio
	and eah."eah_nrocor" = emp.fld_funcorrel
union  
select distinct 
	/*idn prestacion*/ eam."eam_idn"  as idn,
	/*rut afiliado*/   eam."eam_rutcot"  as rut_afiliado,
	SUBSTRING(eam."eam_rutcot",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	/*codigo beneficiario*/ eam."eam_codben"  as bencorrel,
	ben."fld_benrut" as rut_beneficiario,
	SUBSTRING(ben."fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	/*contrato*/  eam."eam_nrocon" as contrato,
	/*correlativo*/ eam."eam_nrocor"  as correlativo,
	emp.empleador_1,
	emp.empleador_2,
	 /*fecha contable*/ eam."eam_conefe"::date as fecha,
	 /*periodo contable*/ --year(datepart(eam."eam_conefe"))*100 + month(datepart(eam."eam_conefe")) as periodo,
	est.P_DDV_EST.fecha_periodo(eam."eam_conefe") AS periodo,
	 /*rut prestador*/sol."sol_insrut"  as rut_prestador,
	 /*prestacion ges-caec, ges o caec*/
	case
	    when eam."der_idn">0 and eam."cso_idn" = 0 then 'GES'
		when eam."cso_idn">0 and eam."der_idn" = 0 then 'CAEC'
		when eam."der_idn">0 and eam."cso_idn">0   then 'GES-CAEC'
		else 'OTRO' 
	end  as activacion, 
	/*es urgencia*/	eam."eam_urgencia" as urgencia,
	/*cobrado*/eam."eam_valcob"::int  as cobrado,
 	/*bonificado*/ eam."eam_valbon"::int + eam."fld_gescaec"::int as bonificado
    from HOS.P_RDV_DST_LND_SYB.SH_EAM eam 
    inner join AFI.P_RDV_DST_LND_SYB.BEN ben
	on eam."eam_nrocon" = ben."fld_funfolio"
	and eam."eam_nrocor" = ben."fld_funcorrel"
	and eam."eam_codben" = ben."fld_bencorrel"
	and eam."eam_conefe" between '2021-01-01 00:00:00' and '2021-06-30 23:59:59'
    and eam."eam_estado" not in (3,4) 
    left join HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol
    on eam."sol_nrosol" = sol."sol_nrosol" 
    and eam."eam_rutcot" = sol."sol_rutafi" 
    and eam."eam_codben" = sol."sol_codben"
	left join est.P_DDV_EST.ca_emp emp
	on eam."eam_nrocon" = emp.fld_funfolio
	and eam."eam_nrocor" = emp.fld_funcorrel
)
----------FIN GASTOS HOSPITALARIOS
------------ FIN GASTOS



SELECT EST.P_DDV_EST.FECHA_PERIODO("FECHA_RECEPCION") AS PERIODO , COUNT(DISTINCT(RUT_AFILIADO)) FROM EST.P_DDV_EST.CA_LICENCIAS
GROUP BY PERIODO


------------LICENCIAS
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_LICENCIAS as (
	select 
		liq."liq_idn" as idn_liquidacion,
		lcc."lcc_idn" as idn_licencia,
		case
		   when ("lcc_comcod" = 1 or "lcc_comcod"=2) then 'Papel'
		   when ("lcc_comcod" = 3 and "lcc_sucrec" NOT IN (16)) then 'Mixta'
		   when ("lcc_comcod" = 3 and "lcc_sucrec"=16) then 'Electronica'
		   else 'indefinido'
		end	as tipo,
		--lcc."lcc_cotrut" as rut_afiliado,
		SUBSTRING(lcc."lcc_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	    lcc."lcc_funfol" as contrato,
		lcc."lcc_funcor" as correlativo,
		lcc."lcc_emprut" as rut_empleador,
		cnt."fld_concostototal"::double as valor_plan,		
		lcc."lcc_estado" as estado,
		case 
		   when lcc."lcc_caltra" in (1,2) then 'Empleador'
		   when lcc."lcc_caltra" in (3,4) and lcc."lcc_tpocvn" = 1 then 'Convenio Empleador'
		   else 'Pago Directo'
		end as convenio,
		--"lcc_visres" as resolucion, /*ultima resolucion*/
		CASE 
			WHEN "lcc_visres" = 1 THEN 'autorice'
			WHEN "lcc_visres" = 2 THEN 'rechazo'
			WHEN "lcc_visres" = 3 THEN 'ampliaci�n'
			WHEN "lcc_visres" = 4 THEN 'reducci�n'
		END AS resolucion,
		lcc."lcc_fecrec"::date as fecha_recepcion,
		"lcc_visfec"::date as fecha_resolucion,/*ultima fecha de resolucion*/
		"lcc_recfec"::date as fecha_redictamen,
		"lcc_fecini"::date as fecha_inicio, /*Minimo del inicio(dia) del reposo*/
		lcc."lcc_viscon" as continua,		
		"lcc_recori" as resolucion_compin, /*resolucion compin*/
		--"liq_digfec"::date as fecha_liquidacion,
	    --liq."liq_mtoafp" as monto_pension,
	    --liq."liq_mtoinv" as monto_invalidez,
	    --liq."liq_mtocaj" as monto_caja,
	    --liq."liq_mtodes" as monto_deshaucio,
	    --liq."liq_mtosal" as monto_salud,
	   	--liq."liq_mtocot" as monto_cotizacion_pactada,
	    liq."liq_mtosub"::int as monto_subsidio
	    --liq."liq_mtoret" as monto_retencion,
	    --liq."liq_mtoseg" as monto_seguro_cesantia,
	    --liq."liq_mtopag" as monto_a_pagar
	from LCC.P_RDV_DST_LND_SYB.LCC lcc
	inner join LCC.P_RDV_DST_LND_SYB.LIQ liq
    on lcc."lcc_idn" = liq."lcc_idn"
	and liq."liq_digfec" between '2020-11-01 00:00:00' and '2020-12-31 23:59:59'
	and lcc."lcc_estado" not in (13, 15)
	inner join AFI.P_RDV_DST_LND_SYB.CNT  cnt
	on lcc."lcc_funfol" = cnt."fld_funfolio"
	and lcc."lcc_funcor" = cnt."fld_funcorrel")
------- FIN LICENCIAS

	
SELECT FECHA , COUNT(DISTINCT(RUT_AFILIADO)) FROM EST.P_DDV_EST.CA_DESAFILIACIONES
GROUP BY fecha


SELECT DISTINCT ("fld_funnotificacod") FROM AFI.P_RDV_DST_LND_SYB.LFC
ORDER BY "fld_funnotificacod"

SELECT top 3 * FROM AFI.P_RDV_DST_LND_SYB.CNT

SELECT top 3 * FROM EST.P_DDV_EST.CA_DESAFILIACIONES

----------DESAFILIACIONES
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_DESAFILIACIONES AS (
select 
    lfc."fld_cotrut" as rut_afiliado,
    SUBSTRING(lfc."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
    lfc."fld_funfolio" as contrato,
    lfc."fld_funcorrel" as correlativo,
    lfc."fld_periodoprod"	as fecha,
    case (lfc."fld_funnotificacod")
	    when 2 then 'Desafiliacion'
		when 333 then 'renuncia voluntaria'
    end as motivo, 
    cnt."fld_ingreso" as fecha_ingreso
from AFI.P_RDV_DST_LND_SYB.LFC lfc
	inner join AFI.P_RDV_DST_LND_SYB.CNT cnt
	on lfc."fld_funfolio" = cnt."fld_funfolio"
	and lfc."fld_funcorrel" = cnt."fld_funcorrel"
	and "fld_periodoprod" between '2020-11-01 00:00:00' and '2020-12-31 23:59:59'
	and ("fld_funnotificacod"=2 or "fld_funnotificacod"=333))
------------FIN DESAFILIACIONES	

	
	
	
SELECT EST.P_DDV_EST.FECHA_PERIODO(FECHA_PRODUCCION) AS periodo, count(DISTINCT (RUT_AFILIADO))  FROM EST.P_DDV_EST.CA_AFILIACIONES
GROUP BY periodo
	


---------AFILIACIONES
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_AFILIACIONES AS ( 
	select 
	    --lfc."fld_cotrut" as rut_afiliado,
	    SUBSTRING(lfc."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	    lfc."fld_fechafun" as fecha_fun,
	    isa.gls_isapre as isapre_origen,
	    cnt."fld_concostototal"::double as monto_pactado,
	    cnt."fld_auge"::double as monto_ges,
	    0 as monto_caec,
	    cnt."fld_benefadic" - cnt."fld_auge" as beneficio_adicional,
	    CASE 
	    	WHEN monto_carga IS null THEN 0
	    	ELSE monto_carga
	    END::double AS monto_carga,	
	    lfc."fld_funfolio" as contrato,
	    lfc."fld_funcorrel" as correlativo,
	    lfc."fld_folio" as folio,
		cnt."fld_glscateg" as plan,
		cnt."fld_cntcateg" as codigo_plan,
		lfc."fld_emprut" as rut_empleador,
		rut_agente,
		rut_supervisor,
	    lfc."fld_fectraspaso" as fecha_traspaso,
	    lfc."fld_fechareal" as fecha_real,
		lfc."fld_periodoprod" as fecha_produccion
	from AFI.P_RDV_DST_LND_SYB.LFC lfc
	inner join AFI.P_RDV_DST_LND_SYB.CNT cnt
	on  lfc."fld_funfolio" = cnt."fld_funfolio"
	and lfc."fld_funcorrel" = cnt."fld_funcorrel"
	and lfc."fld_periodoprod" between '2020-11-01 00:00:00' and '2020-12-31 23:59:59'
	and lfc."fld_funnotificacod"  = 1 
	and lfc."fld_tpotransac" IN('VN','CT')
	left join 
	( SELECT ben."fld_funfolio" , ben."fld_funcorrel" , sum(ben."fld_factor") AS monto_carga 
	  FROM AFI.P_RDV_DST_LND_SYB.BEN 
	  WHERE ben."fld_bencorrel" NOT IN (0)
	  GROUP BY ben."fld_funfolio" , ben."fld_funcorrel" ) ben
	on  lfc."fld_funfolio" = ben."fld_funfolio" 
	and  lfc."fld_funcorrel" = ben."fld_funcorrel"
	inner join 
	(	select 
			age.age_rut_agente as rut_agente,
			age.age_cod_agente as codigo_agente,
			age.age_rut_agencia as codigo_agencia,
			sup.rut_supervisor
		from IQ.P_DDV_IQ.PRM_AGENCIAS_NUEVO age
		inner join (
			select 
				age_rut_agente as rut_supervisor,
				age_rut_agencia		
			from IQ.P_DDV_IQ.PRM_AGENCIAS_NUEVO 
            where age_cod_agente = 0) sup
		on  age.age_rut_agencia = sup.age_rut_agencia) age
	on  lfc."fld_agente" = age.codigo_agente
	and lfc."fld_agencia" = age.codigo_agencia
	inner join ISA.P_RDV_DST_LND_SYB.ISAPREISAPRE isa
	on lfc."isapre_origen" = isa.cod_isapre)
-------------FIN AFILIACIONES

	
SELECT EST.P_DDV_EST.FECHA_PERIODO(FECHA_ACTIVACION_GES) AS PERIODO, count(DISTINCT(RUT_AFILIADO))  FROM EST.P_DDV_EST.CA_INCOPR_GES
GROUP BY periodo


-----------------INCORPORACIONES GES
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_INCOPR_GES AS (
	select 
		--"rutcot" as rut_afiliado,
		SUBSTRING("rutcot",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
		"fecha_solges"::date as fecha_activacion_ges,
		--"rutben" as rut_beneficiario,
		SUBSTRING("rutben",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
		"nombre_ben" as nombre_beneficiario,
		"descrip_is" as tipo_patologia
	from GES.P_RDV_DST_LND_SYB.GES_ESTUDIO  ges
	where "fecha_solges" between '2020-11-01 00:00:00' and '2020-12-31 23:59:59' 
and "estado_caso" not in ('ANULADO', 'NO CURSADO'))
-------------FIN INCORPORACIONES GES


----- CARTERA VIGENTE
CREATE OR REPLACE TABLE est.P_DDV_EST.CA_CARTERA_DIC AS (
SELECT "fld_cotrut" , "fld_ingreso" AS incorporacion FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE FLD_VIGREALDESDE <= 202012 AND FLD_VIGREALHASTA >= 202012)




	