
-----RESULTADO FINAL OK
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TEST_01 AS (
-----extraccion de todos los codigos tipos de documentos
WITH 
	pagos_full_rut AS (
		SELECT DISTINCT
			EST.P_DDV_EST.FECHA_PERIODO(A."pag_pagfec")::INT AS PER_PAGO,
			"pag_cotrut" AS rut_titular,
			sum(A."pag_pagval")::INT AS total_pagado
		FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG A
			LEFT JOIN isa.P_RDV_DST_LND_SYB.ISAPREMONEDA B ON (EST.P_DDV_EST.FECHA_PERIODO(A."pag_pagfec") = est.P_DDV_EST.fecha_periodo(B.FEC_VAL))
		WHERE EST.P_DDV_EST.FECHA_PERIODO(A."pag_pagfec")::INT BETWEEN 201901 AND 202203
			  AND  a."pag_tipdoc" IN (1,2,3,4,5,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26,28,31,33,35,36,37) 
		GROUP BY 
			"pag_cotrut" ,
			EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
		ORDER BY 
			EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
)
SELECT 
	COALESCE (a.PERIODO_VIGENCIA,B.PER_PAGO) AS PERIODO,
	COALESCE (a.rut_titular,b.rut_titular) AS TITULAR,
	a.CNT_CARFAM +1 AS beneficiarios,
	a.COSTO_FINAL_UF AS total_pactado,
	a.COSTO_GES_PESOS AS pactado_ges_pesos,
	a.COSTO_GES_UF AS pactado_ges_uf,
	a.MONTO_EXCESO AS exceso,
	a.MONTO_EXCEDENTE AS excedente,
	a.PAGADO_TOTAL_PESOS AS total_recaudado_con,
	a.pagado_auge_pesos AS total_recaudacion_ges_con,
	b.total_pagado AS pagado_pag_full,
	(pactado_ges_pesos/NULLIF(total_recaudado_con,0))::double AS TASA_GES,
	(pactado_ges_pesos/NULLIF(total_recaudado_con,0))*B.TOTAL_PAGADO::double AS PAGO_GES,
	B.TOTAL_PAGADO-PAGO_GES::INT AS PAGO_PLAN
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO a
	FULL JOIN pagos_full_rut b ON (a.RUT_TITULAR=b.rut_titular AND a.PERIODO_VIGENCIA=b.per_pago)
WHERE PERIODO BETWEEN 201901 AND 202203)
;
---------extraccion de los codigos de tipo de documentos con designacion a la tabla contrato
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TEST_02 AS (
WITH 
	pagos_filtro_rut AS (
		SELECT DISTINCT
			EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT AS per_pago,
			"pag_cotrut" AS rut_titular,
			sum(A."pag_pagval")::INT AS total_pagado
		FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG A
		WHERE EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT BETWEEN 201901 AND 202203
			  AND  a."pag_tipdoc" IN (1,4,9,11,12,13,14,15,16,17,18,19,20,21,22,23,26,33,35,36,37) 
		GROUP BY 
			"pag_cotrut" ,
			EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
			ORDER BY EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
)
SELECT 
	COALESCE (a.PERIODO_VIGENCIA,B.PER_PAGO) AS PERIODO,
	COALESCE (a.rut_titular,b.rut_titular) AS TITULAR,
	a.CNT_CARFAM +1 AS beneficiarios,
	a.COSTO_FINAL_UF AS total_pactado,
	a.COSTO_GES_PESOS AS pactado_ges_pesos,
	a.COSTO_GES_UF AS pactado_ges_uf,
	a.MONTO_EXCESO AS exceso,
	a.MONTO_EXCEDENTE AS excedente,
	a.PAGADO_TOTAL_PESOS AS total_recaudado_con,
	a.pagado_auge_pesos AS total_recaudacion_ges_con,
	b.total_pagado AS pagado_pag_filtro,
	(pactado_ges_pesos/NULLIF(total_recaudado_con,0))::double AS TASA_GES,
	(pactado_ges_pesos/NULLIF(total_recaudado_con,0))*B.TOTAL_PAGADO::double AS PAGO_GES,
	B.TOTAL_PAGADO-PAGO_GES::INT AS PAGO_PLAN
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO a
	FULL JOIN pagos_filtro_rut b ON (a.RUT_TITULAR=b.rut_titular AND a.PERIODO_VIGENCIA=b.per_pago)
WHERE PERIODO BETWEEN 201901 AND 202203)
;
----- se unen las bases con criterios de full codigos y filtros de codigos
WITH 
	UNION_ AS (
		SELECT
			COALESCE(a.PERIODO,b.PERIODO) AS per_periodo, 
			c.VAL_MON AS valor_uf,
			COALESCE(a.TITULAR,b.TITULAR) AS TITULAR,
			COALESCE(a.BENEFICIARIOS,b.BENEFICIARIOS) AS BENEFICIARIOS,
			COALESCE(a.total_pactado,b.TOTAL_PACTADO) AS total_pactado_uf,
			COALESCE(a.pactado_ges_uf,b.PACTADO_GES_UF) AS pactado_ges_uf_total,
			COALESCE(a.pactado_ges_pesos, b.PACTADO_GES_PESOS)::int AS pactado_ges_pesos_total ,
			COALESCE(a.pago_ges,b.PAGO_GES)::int AS pago_ges,
			COALESCE(a.total_recaudado_con,b.TOTAL_RECAUDADO_CON)::int AS total_recaudado_contrato,
			COALESCE(a.total_recaudacion_ges_con,b.total_recaudacion_ges_con) AS total_recaudado_ges_contrato,
			a.PAGADO_PAG_FULL ,
			b.pagado_pag_filtro,
				CASE
					WHEN pagado_pag_filtro IS null THEN PAGADO_PAG_FULL
			    	ELSE pagado_pag_filtro
				END AS pagado_unico,
			COALESCE(a.PAGO_PLAN,b.PAGO_PLAN) AS PAGO_PLAN,
			--COALESCE(a.TASA_GES,b.TASA_GES) AS TASA_GES,
			(pactado_ges_uf_total/NULLIF(total_pactado_uf,0)) AS uf_pactado,
			(PACTADO_GES_PESOS_total/NULLIF(total_recaudado_contrato,0)) AS tasa_ges_total,
			valor_uf*total_pactado_uf AS total_pactado_pesos, 
			--pagado_unico*tasa_ges_total AS pagado_ges_estimado,
			pagado_unico/NULLIF (total_pactado_pesos,0) AS n_veces_pagadas,
			n_veces_pagadas*pactado_ges_pesos_total  AS pago_ges_estimado,
				CASE 
					 WHEN PAGADO_UNICO =  total_recaudado_contrato
					 THEN pactado_ges_pesos_total
					 ELSE n_veces_pagadas*pactado_ges_pesos_total
				END AS pago_ges_estimado_2,
			pagado_unico/NULLIF (pactado_ges_pesos_total,0) AS factor, 
				CASE 
					WHEN n_veces_pagadas <  0.6 THEN 0
			 		WHEN n_veces_pagadas >= 0.6 AND n_veces_pagadas < 1   THEN n_veces_pagadas*pactado_ges_pesos_total
			 		WHEN n_veces_pagadas >= 1  AND n_veces_pagadas < 1.6 THEN pactado_ges_pesos_total
			 		ELSE pago_ges_estimado_2
			 	END AS nueva_variable
		FROM EST.P_DDV_EST.CA_TEST_01 a 
			FULL JOIN EST.P_DDV_EST.CA_TEST_02 b ON (a.TITULAR=B.TITULAR AND a.PERIODO=b.PERIODO)
			LEFT JOIN isa.P_RDV_DST_LND_SYB.ISAPREMONEDA c ON (a.PERIODO=est.P_DDV_EST.fecha_periodo(FEC_VAL) AND b.PERIODO=est.P_DDV_EST.fecha_periodo(FEC_VAL))
		WHERE per_periodo BETWEEN 201901 AND 202203
		ORDER BY PER_PERIODO 
)
--SELECT top 10 *  FROM union_ 
------se agrupa la sumatoria de la union (los null filtrados corresponde a gente no vigente en la base contrato)
SELECT 
	PER_PERIODO,
	valor_uf,
	--TITULAR ,
	--BENEFICIARIOS ,
	SUM(total_pactado_uf)::double AS total_pactado_uf,
	sum(PACTADO_GES_UF_total)::double AS pactado_ges_uf,
	sum(pactado_ges_pesos_total) AS pactado_ges_pesos_total,
	sum(PAGO_GES) AS pago_ges,
	sum(total_recaudado_contrato) AS total_recaudado_con,
	sum(total_recaudado_ges_contrato) AS total_recaudado_ges_con,
	sum(pagado_pag_full) AS pagado_full,
	sum(PAGADO_PAG_FILTRO) AS pagado_gestion,
	sum(pagado_unico) AS pagado_unico,
	sum(total_pactado_pesos),
	sum(tasa_ges_total),
	sum(n_veces_pagadas),
	sum(pago_ges_estimado),
	sum(pago_ges_estimado_2),
	sum(nueva_variable)
FROM union_ 
WHERE valor_uf IS NOT NULL 
GROUP BY 
	PER_PERIODO , VALOR_UF --, titular,beneficiarios
ORDER BY 
	PER_PERIODO 



	
	
SELECT * FROM EST.P_DDV_EST.ca_test_01
WHERE periodo IN (201905)




SELECT top 1 * FROM EST.P_DDV_EST.CA_TEST_01

SELECT top 1 * FROM EST.P_DDV_EST.CA_TEST_02

SELECT top 100 * FROM isa.P_RDV_DST_LND_SYB.ISAPREMONEDA 

SELECT top 10 * FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO


SELECT top 10 * FROM P_DDV_AFI.P_DDV_AFI_CONTRATO




SELECT 
	PERIODO_VIGENCIA,
	sum(COSTO_GES_PESOS) AS total_costo_pesos,
	sum(PAGADO_TOTAL_PESOS) AS total_pagado_total_pesos
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE PERIODO_VIGENCIA BETWEEN 201901 AND 202105
GROUP BY PERIODO_VIGENCIA
ORDER BY PERIODO_VIGENCIA 





------------ REVISION
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TEST_01_rev AS (
WITH pagos_full_rut AS (
SELECT DISTINCT
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT AS per_pago,
"pag_cotrut" AS rut_titular,
sum(A."pag_pagval")::INT AS total_pagado
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG A
WHERE EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT BETWEEN 201901 AND 202012
	  AND  a."pag_tipdoc" IN (1,2,3,4,5,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26,28,31,33,35,36,37) 
GROUP BY 
"pag_cotrut" ,
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
ORDER BY EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
)
SELECT 
COALESCE (a.PERIODO_VIGENCIA,B.PER_PAGO) AS PERIODO,
COALESCE (a.rut_titular,b.rut_titular) AS TITULAR,
a.CNT_CARFAM +1 AS beneficiarios,
a.COSTO_FINAL_UF AS total_pactado,
a.COSTO_GES_PESOS AS pactado_ges_pesos,
a.COSTO_GES_UF AS pactado_ges_uf,
a.MONTO_EXCESO AS exceso,
a.MONTO_EXCEDENTE AS excedente,
a.pagado_auge_pesos AS total_recaudado_ges_con,
a.PAGADO_TOTAL_PESOS AS total_recaudado_con,
b.total_pagado AS pagado_pag_full,
(pactado_ges_pesos/NULLIF(total_recaudado_con,0))::double AS TASA_GES,
(pactado_ges_pesos/NULLIF(total_recaudado_con,0))*B.TOTAL_PAGADO::double AS PAGO_GES,
B.TOTAL_PAGADO-PAGO_GES::INT AS PAGO_PLAN
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO a
FULL JOIN pagos_full_rut b ON (a.RUT_TITULAR=b.rut_titular AND a.PERIODO_VIGENCIA=b.per_pago)
WHERE PERIODO BETWEEN 201901 AND 202012)
;
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TEST_02_rev AS (
WITH pagos_filtro_rut AS (
SELECT DISTINCT
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT AS per_pago,
"pag_cotrut" AS rut_titular,
sum(A."pag_pagval")::INT AS total_pagado
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG A
WHERE EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT BETWEEN 201901 AND 202012
	  AND  a."pag_tipdoc" IN (1,4,9,11,12,13,14,15,16,17,18,19,20,21,22,23,26,33,35,36,37) 
GROUP BY 
"pag_cotrut" ,
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
ORDER BY EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
)
SELECT 
COALESCE (a.PERIODO_VIGENCIA,B.PER_PAGO) AS PERIODO,
COALESCE (a.rut_titular,b.rut_titular) AS TITULAR,
a.CNT_CARFAM +1 AS beneficiarios,
a.COSTO_FINAL_UF AS total_pactado,
a.COSTO_GES_PESOS AS pactado_ges_pesos,
a.COSTO_GES_UF AS pactado_ges_uf,
a.MONTO_EXCESO AS exceso,
a.MONTO_EXCEDENTE AS excedente,
a.pagado_auge_pesos AS total_recaudado_ges_con,
a.PAGADO_TOTAL_PESOS AS total_recaudado_con,
b.total_pagado AS pagado_pag_filtro,
(pactado_ges_pesos/NULLIF(total_recaudado_con,0))::double AS TASA_GES,
(pactado_ges_pesos/NULLIF(total_recaudado_con,0))*B.TOTAL_PAGADO::double AS PAGO_GES,
B.TOTAL_PAGADO-PAGO_GES::INT AS PAGO_PLAN
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO a
FULL JOIN pagos_filtro_rut b ON (a.RUT_TITULAR=b.rut_titular AND a.PERIODO_VIGENCIA=b.per_pago)
WHERE PERIODO BETWEEN 201901 AND 202012)


WITH UNION_ AS (
SELECT 
COALESCE(a.PERIODO,b.PERIODO)::int AS per_periodo,
COALESCE(a.TITULAR,b.TITULAR) AS TITULAR,
COALESCE(a.BENEFICIARIOS,b.BENEFICIARIOS) AS BENEFICIARIOS,
COALESCE(a.total_pactado,b.TOTAL_PACTADO)::double AS total_pactado_uf,
COALESCE(a.pactado_ges_uf,b.PACTADO_GES_UF)::double AS pactado_ges_uf_total,
COALESCE(a.pactado_ges_pesos, b.PACTADO_GES_PESOS)::int AS pactado_ges_pesos_total ,
COALESCE(a.pago_ges,b.PAGO_GES)::int AS pago_ges,
COALESCE (a.total_recaudado_ges_con,b.total_recaudado_ges_con) AS total_recaudado_ges_contrato,
COALESCE(a.total_recaudado_con,b.TOTAL_RECAUDADO_CON)::int AS total_recaudado_contrato,
a.PAGADO_PAG_FULL ,
b.pagado_pag_filtro,
CASE
		WHEN pagado_pag_filtro IS null THEN PAGADO_PAG_FULL
    	ELSE pagado_pag_filtro
	END AS pagado_unico
FROM EST.P_DDV_EST.CA_TEST_01_rev a 
FULL JOIN EST.P_DDV_EST.CA_TEST_02_rev b ON (a.TITULAR=B.TITULAR AND a.PERIODO=b.PERIODO)
WHERE per_periodo in(202001)
ORDER BY PER_PERIODO 
)
SELECT * from UNION_