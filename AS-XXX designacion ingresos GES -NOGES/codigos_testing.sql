SELECT top 10	 
PERIODO_VIGENCIA,
--b.per_pago,
--SUBSTRING(NUM_CONTRATO,0,9)::int AS contrato,
--NUM_CORRELATIVO ,
COUNT(DISTINCT (RUT_TITULAR)) AS cant_afiliados,
sum(CNT_CARFAM)::int AS cant_cargas,
sum(MONTO_EXCEDENTE)::int AS excedente,
sum(MONTO_EXCESO)::int AS exceso,
sum(COSTO_GES_PESOS)::int AS COSTO_GES_PESOS,
sum(COSTO_GES_UF)::double  AS COSTO_GES_UF, 
sum(COSTO_FINAL_PESOS)::int AS COSTO_FINAL_PESOS,
sum(COSTO_FINAL_UF)::double AS COSTO_FINAL_UF, 
sum(PAGADO_TOTAL_PESOS)::int AS PAGADO_TOTAL_PESOS,
sum(PAGADO_TOTAL_UF)::double AS PAGADO_TOTAL_UF,
sum(PAGADO_AUGE_PESOS)::int AS PAGADO_AUGE_PESOS, 
sum(PAGADO_AUGE_UF)::int AS PAGADO_AUGE_UF
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE  PERIODO_VIGENCIA BETWEEN 201901 AND 202012
GROUP BY PERIODO_VIGENCIA
ORDER BY PERIODO_VIGENCIA



SELECT top 1 * FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG


----- monto pagado por per_pago
SELECT 
--CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING AS per_cotizacion,
	--EST.P_DDV_EST.FECHA_PERIODO(DATEADD('month', '1', EST.P_DDV_EST.PERIODO_FECHA(CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING)))::STRING  AS per_vig,
	EST.P_DDV_EST.FECHA_PERIODO("pag_pagfec")::STRING AS per_pago, 
	"pag_tipdoc",
	CASE
		WHEN "pag_tipdoc" =1 THEN 	'DP'
		WHEN "pag_tipdoc" =2 THEN 	'DNP'
		WHEN "pag_tipdoc" =3 THEN 	'P-DNP'
		WHEN "pag_tipdoc" =4 THEN 	'GRATIFICACION'
		WHEN "pag_tipdoc" =5 THEN 	'NONATOS'
		WHEN "pag_tipdoc" =8 THEN 	'DEVOLUCION'
		WHEN "pag_tipdoc" =9 THEN 	'SUBSIDIOS'
		WHEN "pag_tipdoc" =11 THEN 	'CARGO-CTACTE'
		WHEN "pag_tipdoc" =12 THEN 	'DP-APORTE EMPRESA'
		WHEN "pag_tipdoc" =13 THEN 	'DP-Seg. por muerte'
		WHEN "pag_tipdoc" =14 THEN 	'DP-Excesos/Compensados'
		WHEN "pag_tipdoc" =15 THEN 	'DP-Cargo TCR'
		WHEN "pag_tipdoc" =16 THEN 	'DP-PAGO CON CME'
		WHEN "pag_tipdoc" =17 THEN 	'DP-Excedentes'
		WHEN "pag_tipdoc" =18 THEN 	'DP-TRASPASO DESDE OTRAS ISAPRES'
		WHEN "pag_tipdoc" =19 THEN 	'DP-EXCESOS'
		WHEN "pag_tipdoc" =20 THEN 	'DP-CESANTIA'
		WHEN "pag_tipdoc" =21 THEN 	'EMPLEADORES COMPENSACIÓN EXCESOS'
		WHEN "pag_tipdoc" =22 THEN 	'EMPLEADORES COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =23 THEN 	'AFILIADOS COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =24 THEN 	'Dp-Tpso.Fonasa'
		WHEN "pag_tipdoc" =26 THEN 	'DP-FONASA/CCAF'
		WHEN "pag_tipdoc" =28 THEN 	'DP-CESANTIA _COLMENA'
		WHEN "pag_tipdoc" =31 THEN 	'PDNP-DNP incobrable'
		WHEN "pag_tipdoc" =33 THEN 	'RETROACTIVO'
		WHEN "pag_tipdoc" =35 THEN 	'DP-Retención SII'
		WHEN "pag_tipdoc" =36 THEN 	'DP-PRIMER PAGO COTIZACION AFILIADO INDEPENDIENTE VOLUNTARIO'
		WHEN "pag_tipdoc" =37 THEN 	'DP-Reajuste TGR'
		WHEN "pag_tipdoc" =38 THEN 	'DP-Reajuste CME'
		WHEN "pag_tipdoc" =61 THEN 	'Reg. por atraso notif. FUN suscripcion'
		WHEN "pag_tipdoc" =62 THEN 	'Reg. por diferencia de cotizacion'
		WHEN "pag_tipdoc" =63 THEN 	'Reg. por morosidad (finiquito de trabajo'
		WHEN "pag_tipdoc" =64 THEN 	'Reg. por condonacion'
		WHEN "pag_tipdoc" =69 THEN 	'Reg. por condonacion masiva'
		END AS gls_tipdoc,
	--"pag_funfol" , 
	--"pag_funcor" ,
	sum("pag_pagval")::int AS total_pagado
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG
WHERE "pag_tipdoc"  IN (1,2,3,4,5,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26,28,31,33,35,36,37) AND "pag_pagfec" BETWEEN '2019-01-01 00:00:00' AND '2020-12-31 23:59:59'
GROUP BY PER_PAGO,
"pag_tipdoc",
CASE
		WHEN "pag_tipdoc" =1 THEN 	'DP'
		WHEN "pag_tipdoc" =2 THEN 	'DNP'
		WHEN "pag_tipdoc" =3 THEN 	'P-DNP'
		WHEN "pag_tipdoc" =4 THEN 	'GRATIFICACION'
		WHEN "pag_tipdoc" =5 THEN 	'NONATOS'
		WHEN "pag_tipdoc" =8 THEN 	'DEVOLUCION'
		WHEN "pag_tipdoc" =9 THEN 	'SUBSIDIOS'
		WHEN "pag_tipdoc" =11 THEN 	'CARGO-CTACTE'
		WHEN "pag_tipdoc" =12 THEN 	'DP-APORTE EMPRESA'
		WHEN "pag_tipdoc" =13 THEN 	'DP-Seg. por muerte'
		WHEN "pag_tipdoc" =14 THEN 	'DP-Excesos/Compensados'
		WHEN "pag_tipdoc" =15 THEN 	'DP-Cargo TCR'
		WHEN "pag_tipdoc" =16 THEN 	'DP-PAGO CON CME'
		WHEN "pag_tipdoc" =17 THEN 	'DP-Excedentes'
		WHEN "pag_tipdoc" =18 THEN 	'DP-TRASPASO DESDE OTRAS ISAPRES'
		WHEN "pag_tipdoc" =19 THEN 	'DP-EXCESOS'
		WHEN "pag_tipdoc" =20 THEN 	'DP-CESANTIA'
		WHEN "pag_tipdoc" =21 THEN 	'EMPLEADORES COMPENSACIÓN EXCESOS'
		WHEN "pag_tipdoc" =22 THEN 	'EMPLEADORES COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =23 THEN 	'AFILIADOS COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =24 THEN 	'Dp-Tpso.Fonasa'
		WHEN "pag_tipdoc" =26 THEN 	'DP-FONASA/CCAF'
		WHEN "pag_tipdoc" =28 THEN 	'DP-CESANTIA COLMENA'
		WHEN "pag_tipdoc" =31 THEN 	'PDNP-DNP incobrable'
		WHEN "pag_tipdoc" =33 THEN 	'RETROACTIVO'
		WHEN "pag_tipdoc" =35 THEN 	'DP-Retención SII'
		WHEN "pag_tipdoc" =36 THEN 	'DP-PRIMER PAGO COTIZACION AFILIADO INDEPENDIENTE VOLUNTARIO'
		WHEN "pag_tipdoc" =37 THEN 	'DP-Reajuste TGR'
		WHEN "pag_tipdoc" =38 THEN 	'DP-Reajuste CME'
		WHEN "pag_tipdoc" =61 THEN 	'Reg. por atraso notif. FUN suscripcion'
		WHEN "pag_tipdoc" =62 THEN 	'Reg. por diferencia de cotizacion'
		WHEN "pag_tipdoc" =63 THEN 	'Reg. por morosidad (finiquito de trabajo'
		WHEN "pag_tipdoc" =64 THEN 	'Reg. por condonacion'
		WHEN "pag_tipdoc" =69 THEN 	'Reg. por condonacion masiva'
		END
ORDER BY PER_PAGO ,"pag_tipdoc"




------- pagos por periodo de vigencia
SELECT 
PERIODO_VIGENCIA,
count(DISTINCT(RUT_TITULAR)) AS cantidad,
sum(PAGADO_TOTAL_PESOS) AS pagado_total,
sum(MONTO_EXCEDENTE) AS total_excedente,
sum(MONTO_EXCESO) AS total_exceso
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO_VIGENCIA BETWEEN 201901 AND 202012
GROUP BY PERIODO_VIGENCIA
ORDER BY PERIODO_VIGENCIA 




----- excesos
SELECT top 1 * FROM RCD.P_RDV_DST_LND_SYB.EXC_CTA  

SELECT top 1 * FROM MCC


------excedentes
SELECT top 1 * FROM RCD.P_RDV_DST_LND_SYB.EXS_CTA  







WITH pagado_auge AS (
SELECT  	 
PERIODO_VIGENCIA ,
RUT_TITULAR,
CNT_CARFAM ,
CASE 
	WHEN CNT_CARFAM NOT IN (0) THEN COSTO_GES_UF/(CNT_CARFAM +1)
	ELSE COSTO_GES_UF
	END AS costos_ges_carga,
COSTO_GES_PESOS ,
COSTO_GES_UF ,
COSTO_CAEC , 
COSTO_TOTAL_PESOS,
COSTO_TOTAL_UF ,
COSTO_FINAL_PESOS,
COSTO_FINAL_UF, 
PAGADO_TOTAL_PESOS ,
PAGADO_TOTAL_UF ,
PAGADO_CAEC_PESOS ,
PAGADO_CAEC_UF ,
PAGADO_AUGE_PESOS 
PAGADO_AUGE_UF
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE  PERIODO_VIGENCIA BETWEEN 201901 AND 202012
)SELECT * FROM pagado_auge 
WHERE  RUT_TITULAR ='006099633-4' 





WITH pagado_auge AS (
SELECT  	 
PERIODO_VIGENCIA ,
RUT_TITULAR,
CNT_CARFAM ,
CASE 
	WHEN CNT_CARFAM NOT IN (0) THEN COSTO_GES_UF/(CNT_CARFAM +1)
	ELSE COSTO_GES_UF
	END AS costos_ges_carga,
COSTO_GES_PESOS ,
COSTO_GES_UF ,
COSTO_CAEC , 
COSTO_TOTAL_PESOS,
COSTO_TOTAL_UF ,
COSTO_FINAL_PESOS,
COSTO_FINAL_UF, 
PAGADO_TOTAL_PESOS ,
PAGADO_TOTAL_UF ,
PAGADO_CAEC_PESOS ,
PAGADO_CAEC_UF ,
PAGADO_AUGE_PESOS 
PAGADO_AUGE_UF
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE  PERIODO_VIGENCIA BETWEEN 201801 AND 202101
)
SELECT DISTINCT top 200
	CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING AS per_cotizacion,
	EST.P_DDV_EST.FECHA_PERIODO(DATEADD('month', '1', EST.P_DDV_EST.PERIODO_FECHA(CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING)))::STRING  AS per_vig,
	EST.P_DDV_EST.FECHA_PERIODO("pag_pagfec")::STRING AS per_pago , 
	c."fld_cotrut" ,
	"pag_funfol" , 
	"pag_funcor" , 
	"pag_rutemp" ,
	b."fld_emprazsoc" ,
	--"pag_tipdoc" AS cod_ingreso,
	CASE
		WHEN "pag_tipdoc" IN (1,15) THEN 	'1 DP - 15 DP-Cargo TCR '
		WHEN "pag_tipdoc" =2 THEN 	'2 DNP'
		WHEN "pag_tipdoc" =3 THEN 	'3 P-DNP'
		WHEN "pag_tipdoc" =4 THEN 	'4 GRATIFICACION'
		WHEN "pag_tipdoc" =5 THEN 	'5 NONATOS'
		WHEN "pag_tipdoc" =8 THEN 	'8 DEVOLUCION'
		WHEN "pag_tipdoc" =9 THEN 	'9 SUBSIDIOS'
		WHEN "pag_tipdoc" =11 THEN 	'11 CARGO-CTACTE'
		WHEN "pag_tipdoc" =12 THEN 	'12 DP-APORTE EMPRESA'
		WHEN "pag_tipdoc" =13 THEN 	'13 DP-Seg. por muerte'
		WHEN "pag_tipdoc" =14 THEN 	'14 DP-Excesos/Compensados'
		--WHEN "pag_tipdoc" =15 THEN 	'15 DP-Cargo TCR'
		WHEN "pag_tipdoc" =16 THEN 	'16 DP-PAGO CON CME'
		WHEN "pag_tipdoc" =17 THEN 	'17 DP-Excedentes'
		WHEN "pag_tipdoc" =18 THEN 	'18 DP-TRASPASO DESDE OTRAS ISAPRES'
		WHEN "pag_tipdoc" =19 THEN 	'19 DP-EXCESOS'
		WHEN "pag_tipdoc" =20 THEN 	'20 DP-CESANTIA'
		WHEN "pag_tipdoc" =21 THEN 	'21 EMPLEADORES COMPENSACIÓN EXCESOS'
		WHEN "pag_tipdoc" =22 THEN 	'22 EMPLEADORES COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =23 THEN 	'23 AFILIADOS COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =24 THEN 	'24 Dp-Tpso.Fonasa'
		WHEN "pag_tipdoc" =26 THEN 	'26 DP-FONASA/CCAF'
		WHEN "pag_tipdoc" =28 THEN 	'28 DP-CESANTIA COLMENA'
		WHEN "pag_tipdoc" =31 THEN 	'31 PDNP-DNP incobrable'
		WHEN "pag_tipdoc" =33 THEN 	'33 RETROACTIVO'
		WHEN "pag_tipdoc" =35 THEN 	'35 DP-Retención SII'
		WHEN "pag_tipdoc" =36 THEN 	'36 DP-PRIMER PAGO COTIZACION AFILIADO INDEPENDIENTE VOLUNTARIO'
		WHEN "pag_tipdoc" =37 THEN 	'37 DP-Reajuste TGR'
		WHEN "pag_tipdoc" =38 THEN 	'38 DP-Reajuste CME'
		WHEN "pag_tipdoc" =61 THEN 	'61 Reg. por atraso notif. FUN suscripcion'
		WHEN "pag_tipdoc" =62 THEN 	'62 Reg. por diferencia de cotizacion'
		WHEN "pag_tipdoc" =63 THEN 	'63 Reg. por morosidad (finiquito de trabajo'
		WHEN "pag_tipdoc" =64 THEN 	'64 Reg. por condonacion'
		WHEN "pag_tipdoc" =69 THEN 	'69 Reg. por condonacion masiva'
		END AS gls_tipdoc,
		sum(a."pag_pagval") AS total_pagado,
	    sum(CASE
			WHEN "pag_tipdoc" IN (1) THEN e.PAGADO_AUGE_UF
			ELSE 0
		END) AS monto_auge,
	    TOTAL_PAGADO - MONTO_AUGE AS pagado_plan
		--sum(a."pag_pagval") AS total_pagado
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG a
	LEFT JOIN ISA.P_RDV_DST_LND_SYB.EMP b ON (a."pag_rutemp" = b."fld_emprut")
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT c ON (a."pag_funfol" = c."fld_funfolio" AND a."pag_funcor" = c."fld_funcorrel")
	LEFT JOIN PAGADO_AUGE e ON (c."fld_cotrut" = e.RUT_TITULAR AND EST.P_DDV_EST.FECHA_PERIODO(DATEADD('month', '1', EST.P_DDV_EST.PERIODO_FECHA(CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING)))::STRING = e.PERIODO_VIGENCIA)
WHERE per_pago IN ('202001','202002','202003','202004','202005','202006','202007') and
	  "pag_tipdoc"  IN (1,15) 
	  AND C."fld_cotrut" ='007018337-4'
GROUP BY 
		CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING,
	EST.P_DDV_EST.FECHA_PERIODO(DATEADD('month', '1', EST.P_DDV_EST.PERIODO_FECHA(CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING)))::STRING,
	EST.P_DDV_EST.FECHA_PERIODO("pag_pagfec")::STRING , 
	c."fld_cotrut" ,
	"pag_funfol" , 
	"pag_funcor" , 
	"pag_rutemp" ,
	b."fld_emprazsoc" ,
	CASE
		WHEN "pag_tipdoc" IN (1,15) THEN 	'1 DP - 15 DP-Cargo TCR '
		WHEN "pag_tipdoc" =2 THEN 	'2 DNP'
		WHEN "pag_tipdoc" =3 THEN 	'3 P-DNP'
		WHEN "pag_tipdoc" =4 THEN 	'4 GRATIFICACION'
		WHEN "pag_tipdoc" =5 THEN 	'5 NONATOS'
		WHEN "pag_tipdoc" =8 THEN 	'8 DEVOLUCION'
		WHEN "pag_tipdoc" =9 THEN 	'9 SUBSIDIOS'
		WHEN "pag_tipdoc" =11 THEN 	'11 CARGO-CTACTE'
		WHEN "pag_tipdoc" =12 THEN 	'12 DP-APORTE EMPRESA'
		WHEN "pag_tipdoc" =13 THEN 	'13 DP-Seg. por muerte'
		WHEN "pag_tipdoc" =14 THEN 	'14 DP-Excesos/Compensados'
		--WHEN "pag_tipdoc" =15 THEN 	'15 DP-Cargo TCR'
		WHEN "pag_tipdoc" =16 THEN 	'16 DP-PAGO CON CME'
		WHEN "pag_tipdoc" =17 THEN 	'17 DP-Excedentes'
		WHEN "pag_tipdoc" =18 THEN 	'18 DP-TRASPASO DESDE OTRAS ISAPRES'
		WHEN "pag_tipdoc" =19 THEN 	'19 DP-EXCESOS'
		WHEN "pag_tipdoc" =20 THEN 	'20 DP-CESANTIA'
		WHEN "pag_tipdoc" =21 THEN 	'21 EMPLEADORES COMPENSACIÓN EXCESOS'
		WHEN "pag_tipdoc" =22 THEN 	'22 EMPLEADORES COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =23 THEN 	'23 AFILIADOS COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =24 THEN 	'24 Dp-Tpso.Fonasa'
		WHEN "pag_tipdoc" =26 THEN 	'26 DP-FONASA/CCAF'
		WHEN "pag_tipdoc" =28 THEN 	'28 DP-CESANTIA COLMENA'
		WHEN "pag_tipdoc" =31 THEN 	'31 PDNP-DNP incobrable'
		WHEN "pag_tipdoc" =33 THEN 	'33 RETROACTIVO'
		WHEN "pag_tipdoc" =35 THEN 	'35 DP-Retención SII'
		WHEN "pag_tipdoc" =36 THEN 	'36 DP-PRIMER PAGO COTIZACION AFILIADO INDEPENDIENTE VOLUNTARIO'
		WHEN "pag_tipdoc" =37 THEN 	'37 DP-Reajuste TGR'
		WHEN "pag_tipdoc" =38 THEN 	'38 DP-Reajuste CME'
		WHEN "pag_tipdoc" =61 THEN 	'61 Reg. por atraso notif. FUN suscripcion'
		WHEN "pag_tipdoc" =62 THEN 	'62 Reg. por diferencia de cotizacion'
		WHEN "pag_tipdoc" =63 THEN 	'63 Reg. por morosidad (finiquito de trabajo'
		WHEN "pag_tipdoc" =64 THEN 	'64 Reg. por condonacion'
		WHEN "pag_tipdoc" =69 THEN 	'69 Reg. por condonacion masiva'
		END ,
	CASE 
		 when "pag_pagopo" = 3 THEN 'ADELANTADA'
		 when "pag_pagopo" = 2 THEN 'ATRASADA'
		 when "pag_pagopo" = 1 THEN 'NORMAL'
	END 
ORDER BY per_pago, PER_COTIZACION 






-------------------------respaldo
WITH pagado_auge AS (
SELECT  	 
PERIODO_VIGENCIA ,
RUT_TITULAR,
CNT_CARFAM ,
CASE 
	WHEN CNT_CARFAM NOT IN (0) THEN COSTO_GES_UF/(CNT_CARFAM +1)
	ELSE COSTO_GES_UF
	END AS costos_ges_carga,
COSTO_GES_PESOS ,
COSTO_GES_UF ,
COSTO_CAEC , 
COSTO_TOTAL_PESOS,
COSTO_TOTAL_UF ,
COSTO_FINAL_PESOS,
COSTO_FINAL_UF, 
PAGADO_TOTAL_PESOS ,
PAGADO_TOTAL_UF ,
PAGADO_CAEC_PESOS ,
PAGADO_CAEC_UF ,
PAGADO_AUGE_PESOS 
PAGADO_AUGE_UF
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE  PERIODO_VIGENCIA BETWEEN 201801 AND 202101
)
SELECT DISTINCT top 200
	CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING AS per_cotizacion,
	EST.P_DDV_EST.FECHA_PERIODO(DATEADD('month', '1', EST.P_DDV_EST.PERIODO_FECHA(CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING)))::STRING  AS per_vig,
	EST.P_DDV_EST.FECHA_PERIODO("pag_pagfec")::STRING AS per_pago , 
	c."fld_cotrut" ,
	"pag_funfol" , 
	"pag_funcor" , 
	"pag_rutemp" ,
	b."fld_emprazsoc" ,
	"pag_tipdoc" AS cod_ingreso,
	CASE
		WHEN "pag_tipdoc" =1 THEN 	'DP'
		WHEN "pag_tipdoc" =2 THEN 	'DNP'
		WHEN "pag_tipdoc" =3 THEN 	'P-DNP'
		WHEN "pag_tipdoc" =4 THEN 	'GRATIFICACION'
		WHEN "pag_tipdoc" =5 THEN 	'NONATOS'
		WHEN "pag_tipdoc" =8 THEN 	'DEVOLUCION'
		WHEN "pag_tipdoc" =9 THEN 	'SUBSIDIOS'
		WHEN "pag_tipdoc" =11 THEN 	'CARGO-CTACTE'
		WHEN "pag_tipdoc" =12 THEN 	'DP-APORTE EMPRESA'
		WHEN "pag_tipdoc" =13 THEN 	'DP-Seg. por muerte'
		WHEN "pag_tipdoc" =14 THEN 	'DP-Excesos/Compensados'
		WHEN "pag_tipdoc" =15 THEN 	'DP-Cargo TCR'
		WHEN "pag_tipdoc" =16 THEN 	'DP-PAGO CON CME'
		WHEN "pag_tipdoc" =17 THEN 	'DP-Excedentes'
		WHEN "pag_tipdoc" =18 THEN 	'DP-TRASPASO DESDE OTRAS ISAPRES'
		WHEN "pag_tipdoc" =19 THEN 	'DP-EXCESOS'
		WHEN "pag_tipdoc" =20 THEN 	'DP-CESANTIA'
		WHEN "pag_tipdoc" =21 THEN 	'EMPLEADORES COMPENSACIÓN EXCESOS'
		WHEN "pag_tipdoc" =22 THEN 	'EMPLEADORES COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =23 THEN 	'AFILIADOS COMPENSACIÓN CADUCADOS'
		WHEN "pag_tipdoc" =24 THEN 	'Dp-Tpso.Fonasa'
		WHEN "pag_tipdoc" =26 THEN 	'DP-FONASA/CCAF'
		WHEN "pag_tipdoc" =28 THEN 	'DP-CESANTIA COLMENA'
		WHEN "pag_tipdoc" =31 THEN 	'PDNP-DNP incobrable'
		WHEN "pag_tipdoc" =33 THEN 	'RETROACTIVO'
		WHEN "pag_tipdoc" =35 THEN 	'DP-Retención SII'
		WHEN "pag_tipdoc" =36 THEN 	'DP-PRIMER PAGO COTIZACION AFILIADO INDEPENDIENTE VOLUNTARIO'
		WHEN "pag_tipdoc" =37 THEN 	'DP-Reajuste TGR'
		WHEN "pag_tipdoc" =38 THEN 	'DP-Reajuste CME'
		WHEN "pag_tipdoc" =61 THEN 	'Reg. por atraso notif. FUN suscripcion'
		WHEN "pag_tipdoc" =62 THEN 	'Reg. por diferencia de cotizacion'
		WHEN "pag_tipdoc" =63 THEN 	'Reg. por morosidad (finiquito de trabajo'
		WHEN "pag_tipdoc" =64 THEN 	'Reg. por condonacion'
		WHEN "pag_tipdoc" =69 THEN 	'Reg. por condonacion masiva'
		END AS gls_tipdoc,
	CASE 
		 when "pag_pagopo" = 3 THEN 'ADELANTADA'
		 when "pag_pagopo" = 2 THEN 'ATRASADA'
		 when "pag_pagopo" = 1 THEN 'NORMAL'
	END AS oportunidad_pago,
	"pag_pagval" AS total_pagado,
	CASE 
		WHEN "pag_tipdoc" IN (1) THEN e.PAGADO_AUGE_UF
		ELSE 0
		END AS monto_auge,
	CASE 
		WHEN "pag_tipdoc" NOT IN (1) THEN "pag_pagval"
		ELSE TOTAL_PAGADO - MONTO_AUGE 
		END AS monto_plan	
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG a
	LEFT JOIN ISA.P_RDV_DST_LND_SYB.EMP b ON (a."pag_rutemp" = b."fld_emprut")
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT c ON (a."pag_funfol" = c."fld_funfolio" AND a."pag_funcor" = c."fld_funcorrel")
	LEFT JOIN PAGADO_AUGE e ON (c."fld_cotrut" = e.RUT_TITULAR AND EST.P_DDV_EST.FECHA_PERIODO(DATEADD('month', '1', EST.P_DDV_EST.PERIODO_FECHA(CONCAT(SUBSTRING("pag_percot",1,4),SUBSTRING("pag_percot",6,2))::STRING)))::STRING = e.PERIODO_VIGENCIA)
WHERE per_pago IN ('202001','202002','202003','202004','202005','202006','202007') and
	  "pag_tipdoc"  IN (1,4,9,11,12,13,14,15,16,17,18,19,20,21,22,23,26,33,35,36,37) 
	  AND C."fld_cotrut" ='007018337-4'
ORDER BY per_pago, PER_COTIZACION 





SELECT top 100	 
PERIODO_VIGENCIA ,
RUT_TITULAR,
CNT_CARFAM ,
CASE 
	WHEN CNT_CARFAM NOT IN (0) THEN COSTO_GES_UF/(CNT_CARFAM +1)
	ELSE COSTO_GES_UF
	END AS costos_ges_carga,
COSTO_GES_PESOS ,
COSTO_GES_UF ,
COSTO_CAEC , 
COSTO_TOTAL_PESOS,
COSTO_TOTAL_UF ,
COSTO_FINAL_PESOS,
COSTO_FINAL_UF, 
PAGADO_TOTAL_PESOS ,
PAGADO_TOTAL_UF ,
PAGADO_CAEC_PESOS ,
PAGADO_CAEC_UF ,
PAGADO_AUGE_PESOS 
PAGADO_AUGE_UF
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE  PERIODO_VIGENCIA BETWEEN 201901 AND 202012 AND RUT_TITULAR ='005043879-1' AND PERIODO_VIGENCIA BETWEEN 201810 AND 201903
ORDER BY PERIODO_VIGENCIA 



	  

	  

	  
	  
	  
	 
	  
SELECT 
	EST.P_DDV_EST.FECHA_PERIODO("pag_pagfec")::STRING AS per_pago,
	"pag_pagfec" ,
	"pag_funfol" , 
	"pag_funcor", 
	count("pag_pagfec" ) AS cant 
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG 	  
WHERE     "pag_tipdoc" IN  (1,15) 
	  AND "pag_pagfec" BETWEEN '2019-01-01 00:00:00' AND '2019-12-31 00:00:00'
GROUP BY "pag_pagfec", "pag_funfol", "pag_funcor" 	  
ORDER BY cant desc	  
	  




SELECT DISTINCT SELECT top 100
B."fld_cotrut",
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::int AS per_pago,
a."pag_percot", 
--count (DISTINCT(a."pag_percot")) AS cant_per_cot,
--A."pag_funfol" ,
--A."pag_funcor" , 
--A."pag_rutemp" , 
--a."pag_tipdoc" ,
--A."pag_pagopo" ,
sum(A."pag_pagval")::int AS total_pagado
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG A
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT B ON (A."pag_funfol"=B."fld_funfolio" AND A."pag_funcor"=B."fld_funcorrel")
WHERE EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::int in (201901,201902,201903,201904,201905,201906,201907,201908,201909,201910,201911,201912,202012,202001,202002,202003,202004,202005,202006,202007,202008,202009,202010,202011,202012)
	  AND  a."pag_tipdoc" IN  (1,2,3,4,5,9,11,12,13,14,15,16,17,18,19,20,21,22,23,26,27,28,33,35,36,37)
	--AND "pag_funfol" = 10224528 AND "pag_funcor" =13
	--AND b."fld_cotrut" IN ('004636798-7')
GROUP BY 
B."fld_cotrut",
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::int
ORDER BY EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::int






WITH pagos_rut AS (
SELECT DISTINCT
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::int AS per_pago,
"pag_cotrut" AS rut_titular,
sum(A."pag_pagval")::int AS total_pagado
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG A
WHERE EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::int in (201901,201902,201903,201904,201905,201906,201907,201908,201909,201910,201911,201912,202001,202002,202003,202004,202005,202006,202007,202008,202009,202010,202011,202012)
	  AND  a."pag_tipdoc" IN (1,2,3,4,5,9,11,12,13,14,15,16,17,18,19,20,21,22,23,26,27,28,33,35,36,37) 
GROUP BY 
"pag_cotrut" ,
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::int
ORDER BY EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::int
)
SELECT per_pago, sum(TOTAL_PAGADO) FROM pagos_rut
GROUP BY per_pago




CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TEST_01 AS (
WITH pagos_full_rut AS (
SELECT DISTINCT
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT AS per_pago,
"pag_cotrut" AS rut_titular,
sum(A."pag_pagval")::INT AS total_pagado
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG A
WHERE EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT BETWEEN 201901 AND 202012
	  AND  a."pag_tipdoc" IN (1,2,3,4,5,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26,28,31,33,35,36,37) 
GROUP BY 
"pag_cotrut" ,
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
ORDER BY EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
)
SELECT 
COALESCE (a.PERIODO_VIGENCIA,B.PER_PAGO) AS PERIODO,
COALESCE (a.rut_titular,b.rut_titular) AS TITULAR,
a.CNT_CARFAM +1 AS beneficiarios,
a.COSTO_FINAL_UF AS total_pactado,
a.COSTO_GES_PESOS AS pactado_ges_pesos,
a.COSTO_GES_UF AS pactado_ges_uf,
a.MONTO_EXCESO AS exceso,
a.MONTO_EXCEDENTE AS excedente,
a.pagado_ges_pesos AS pagado_ges_pesos, 
a.PAGADO_TOTAL_PESOS AS total_recaudado_con,
b.total_pagado AS pagado_pag_full,
(pactado_ges_pesos/NULLIF(total_recaudado_con,0))::double AS TASA_GES,
(pactado_ges_pesos/NULLIF(total_recaudado_con,0))*B.TOTAL_PAGADO::double AS PAGO_GES,
B.TOTAL_PAGADO-PAGO_GES::INT AS PAGO_PLAN
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO a
FULL JOIN pagos_full_rut b ON (a.RUT_TITULAR=b.rut_titular AND a.PERIODO_VIGENCIA=b.per_pago)
WHERE PERIODO BETWEEN 201901 AND 202012)
;
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TEST_02 AS (
WITH pagos_filtro_rut AS (
SELECT DISTINCT
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT AS per_pago,
"pag_cotrut" AS rut_titular,
sum(A."pag_pagval")::INT AS total_pagado
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG A
WHERE EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT BETWEEN 201901 AND 202012
	  AND  a."pag_tipdoc" IN (1,4,9,11,12,13,14,15,16,17,18,19,20,21,22,23,26,33,35,36,37) 
GROUP BY 
"pag_cotrut" ,
EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
ORDER BY EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagfec")::INT
)
SELECT 
COALESCE (a.PERIODO_VIGENCIA,B.PER_PAGO) AS PERIODO,
COALESCE (a.rut_titular,b.rut_titular) AS TITULAR,
a.CNT_CARFAM +1 AS beneficiarios,
a.COSTO_FINAL_UF AS total_pactado,
a.COSTO_GES_PESOS AS pactado_ges_pesos,
a.COSTO_GES_UF AS pactado_ges_uf,
a.MONTO_EXCESO AS exceso,
a.MONTO_EXCEDENTE AS excedente,
a.PAGADO_TOTAL_PESOS AS total_recaudado_con,
b.total_pagado AS pagado_pag_filtro,
(pactado_ges_pesos/NULLIF(total_recaudado_con,0))::double AS TASA_GES,
(pactado_ges_pesos/NULLIF(total_recaudado_con,0))*B.TOTAL_PAGADO::double AS PAGO_GES,
B.TOTAL_PAGADO-PAGO_GES::INT AS PAGO_PLAN
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO a
FULL JOIN pagos_filtro_rut b ON (a.RUT_TITULAR=b.rut_titular AND a.PERIODO_VIGENCIA=b.per_pago)
WHERE PERIODO BETWEEN 201901 AND 202012)






WITH UNION_ AS (
SELECT 
COALESCE(a.PERIODO,b.PERIODO) AS per_periodo,
COALESCE(a.TITULAR,b.TITULAR) AS TITULAR,
COALESCE(a.BENEFICIARIOS,b.BENEFICIARIOS) AS BENEFICIARIOS,
COALESCE(a.total_pactado,b.TOTAL_PACTADO) AS total_pactado_uf,
COALESCE(a.pactado_ges_uf,b.PACTADO_GES_UF) AS pactado_ges_uf,
COALESCE(a.pactado_ges_pesos, b.PACTADO_GES_PESOS)::int AS pactado_ges_pesos ,
COALESCE(a.pago_ges,b.PAGO_GES)::int AS pago_ges,
COALESCE(a.total_recaudado_con,b.TOTAL_RECAUDADO_CON)::int AS total_recaudado_contrato,
a.PAGADO_PAG_FULL ,
b.pagado_pag_filtro,
COALESCE(a.PAGO_PLAN,b.PAGO_PLAN) AS PAGO_PLAN,
COALESCE(a.TASA_GES,b.TASA_GES) AS TASA_GES
FROM EST.P_DDV_EST.CA_TEST_01 a 
FULL JOIN EST.P_DDV_EST.CA_TEST_02 b ON (a.TITULAR=B.TITULAR AND a.PERIODO=b.PERIODO)
WHERE per_periodo IN (201901)
ORDER BY PER_PERIODO 
)
SELECT 
PER_PERIODO,
--TITULAR ,
--BENEFICIARIOS ,
SUM(total_pactado_uf) AS total_pactado_uf,
sum(PACTADO_GES_UF) AS pactado_ges_uf,
sum(PAGO_GES) AS pago_ges,
sum(total_recaudado_contrato),
sum(pagado_pag_full) AS pagado_full,
sum(PAGADO_PAG_FILTRO) AS pagado_gestion,
sum(pactado_ges_uf/total_pactado_uf) AS uf_pactado,
sum(PACTADO_GES_PESOS/total_recaudado_contrato) AS real_,
sum(CASE
		WHEN pagado_gestion IS null THEN pagado_full
	    ELSE pagado_gestion
	END) AS pagado_unico,
pagado_unico *real_ AS DD
FROM union_ 
GROUP BY PER_PERIODO --, titular,beneficiarios




SELECT
COALESCE(vigencia,PER_PAGO) AS PERIODO,
sum(pactado_ges_pesos)::int,
sum(pago_ges)::int,
sum(total_recaudado_con)::int,
sum(pagado_pag)::int
FROM EST.P_DDV_EST.CA_TEST_01
WHERE PERIODO BETWEEN 201901 AND 201912
GROUP BY PERIODO
ORDER BY PERIODO


SELECT TOP 100 * FROM EST.P_DDV_EST.CA_TEST_01



SELECT top 1 * FROM  afi.P_RDV_DST_LND_SYB.CNT 
WHERE "fld_funfolio" = 10628076 AND "fld_funcorrel" = 10

SELECT
PER_REAL_PAGO,
PER_COT ,
RUT_COBRANZA, 
NOMBRE_RUT_COBRANZA,
RUT_PAGADOR ,
NOMBRE_RUT_PAGADOR ,
OPORTUNIDAD_PAGO ,	
COD_ING ,
INGRESO_GLS ,
MONTO_ING_PESOS 
--sum(MONTO_ING_PESOS)::int AS monto_pago_pesos
FROM RCD.P_DDV_RCD.P_DDV_RCD_INGRESOS
WHERE BEN_RUT_AFILIADO = '015677550-9' AND PER_PAGO IN(202003)
/*GROUP BY PER_PAGO ,
PER_COT ,
RUT_PAGADOR ,
NOMBRE_RUT_PAGADOR ,
OPORTUNIDAD_PAGO ,
COD_ING ,
INGRESO_GLS */
ORDER BY PER_REAL_PAGO , PER_COT 


WITH contador AS (
SELECT BEN_RUT_AFILIADO , PER_COT , COUNT(PER_COT) AS CANT
FROM RCD.P_DDV_RCD.P_DDV_RCD_INGRESOS
WHERE PER_COT BETWEEN 201901 AND 202012 AND OPORTUNIDAD_PAGO NOT IN (1)
GROUP BY BEN_RUT_AFILIADO , PER_COT
ORDER BY CANT DESC)
SELECT 
	per_cot, 
	count(DISTINCT(BEN_RUT_AFILIADO)) AS cantidad 
FROM contador 
WHERE CANT > 0  
GROUP BY per_cot


SELECT PER_PAGO , count(DISTINCT(BEN_RUT_AFILIADO)) AS CANT
FROM RCD.P_DDV_RCD.P_DDV_RCD_INGRESOS
WHERE PER_PAGO BETWEEN 201901 AND 202012 AND COD_ING NOT IN  (1,15,8) 
GROUP BY PER_PAGO 
ORDER BY PER_PAGO asc


SELECT BEN_RUT_AFILIADO , PER_PAGO, COUNT(PER_PAGO) AS CANT
FROM RCD.P_DDV_RCD.P_DDV_RCD_INGRESOS
WHERE PER_PAGO BETWEEN 201901 AND 202012 AND (COD_ING  IN  (1,15)) 
GROUP BY BEN_RUT_AFILIADO, PER_PAGO 
ORDER BY CANT desc







SELECT * FROM RCD.P_DDV_RCD.P_DDV_RCD_INGRESOS
WHERE BEN_RUT_AFILIADO = '010916999-4' AND PER_COT IN(201912) AND RUT_PAGADOR ='076240079-0'



SELECT *  FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG 
WHERE "pag_funfol" = 10644659 AND "pag_funcor" = 8 AND "pag_percot" = '2019/12' AND "pag_pagval" =58909.0000--AND "pag_tipdoc" NOT IN (8)
