
SELECT * FROM EST.P_DDV_EST.CA_INCOR_LEY_URGENCIA 
WHERE idn IN (	106653352,
				106391678,
				108151348,
				106574035,
				104675758,
				104782167,
				105943480,
				108541413,
				106643725,
				105564299,
				105570838,
				107784432,
				106694103
)
--LIMIT 10


--------- inicio extraccion
----------EMPLEADORES UNICOS POR RUT
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.temp_emp AS ( 
SELECT DISTINCT 
"fld_cotrut" , --as rut_cotizante
"fld_emprut", --AS rut_empleador	
"fld_funfolio", --AS  contrato,
"fld_funcorrel" --AS correlativo,	
FROM AFI.P_RDV_DST_LND_SYB.LFC
order by "fld_cotrut" 
)
;
----- replicando empleadores
CREATE OR REPLACE TEMPORARY TABLE est.P_DDV_EST.ca_emp AS (
WITH empleadores AS (
SELECT
"fld_cotrut",
"fld_emprut",
"fld_funfolio",
"fld_funcorrel",
ROW_NUMBER () OVER (PARTITION BY "fld_funfolio","fld_funcorrel" ORDER BY "fld_funfolio","fld_funcorrel") AS n_veces
FROM EST.P_DDV_EST.temp_emp)
SELECT fld_cotrut,fld_funfolio,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4
FROM empleadores 
pivot(max("fld_emprut") FOR n_veces IN (1,2,3,4))
AS P (fld_cotrut,fld_funfolio,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4))
;
----------- HOSPITALARIO
/*HOSPITALARIO*/
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_INCOR_LEY_URGENCIA_ID AS (
select distinct    
	eah."eah_idn" as idn,/*idn prestacion*/  
	--eah."eah_rutcot" as rut_afiliado,/*rut afiliado*/
	SUBSTRING(eah."eah_rutcot",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	eah."eah_codben" as bencorrel,/*codigo beneficiario*/
	--ben."fld_benrut" as rut_beneficiario,
	SUBSTRING(ben."fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	eah."eah_nrocon" as contrato, /*contrato*/
	eah."eah_nrocor" as correlativo, /*correlativo*/
	emp.empleador_1,
	emp.empleador_2, 
	eah."eah_conefe"::date as fecha, /*fecha contable*/
	est.P_DDV_EST.FECHA_PERIODO(eah."eah_conefe") AS periodo,/*periodo_contable*/
	--year(datepart(eah.eah_conefe))*100 + month(datepart(eah.eah_conefe)) as periodo,/*periodo contable*/
	sol."sol_insrut" as rut_prestador, /*rut prestador*/
	case
	    when eah."der_idn" > 0 and eah."cso_idn" = 0 then 'GES'
		when eah."cso_idn" > 0 and eah."der_idn" = 0 then 'CAEC'
		when eah."der_idn" > 0 and eah."cso_idn" > 0 then 'GES-CAEC'
		else 'OTRO' 
	end as activacion, /*prestacion ges-caec, ges o caec*/	
	eah."eah_urgencia" as urgencia, /*es urgencia*/	
	eah."eah_cobtot"::int as cobrado, /*cobrado*/
	eah."eah_valbon"::int + eah."fld_gescaec"::int as bonificado /*bonificado*/
from HOS.P_RDV_DST_LND_SYB.SH_EAH  eah 
inner join AFI.P_RDV_DST_LND_SYB.BEN ben
	on eah."eah_nrocon" = ben."fld_funfolio"
	and eah."eah_nrocor" = ben."fld_funcorrel"
	and eah."eah_codben" = ben."fld_bencorrel"
	and eah."eah_conefe" BETWEEN '2021-07-01 00:00:00' and '2021-12-31 23:59:59'
	and eah."eah_estado" not in (3,4) 
	AND eah."eah_urgencia" = 'S'
left join HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol
	on eah."sol_nrosol" = sol."sol_nrosol" 
	and eah."eah_rutcot" = sol."sol_rutafi" 
	and eah."eah_codben" = sol."sol_codben"
left join est.P_DDV_EST.ca_emp emp
	on eah."eah_nrocon" = emp.fld_funfolio
	and eah."eah_nrocor" = emp.fld_funcorrel
union  
select distinct 
	/*idn prestacion*/ eam."eam_idn"  as idn,
	--/*rut afiliado*/   eam."eam_rutcot"  as rut_afiliado,
	SUBSTRING(eam."eam_rutcot",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	/*codigo beneficiario*/ eam."eam_codben"  as bencorrel,
	--ben."fld_benrut" as rut_beneficiario,
	SUBSTRING(ben."fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	/*contrato*/  eam."eam_nrocon" as contrato,
	/*correlativo*/ eam."eam_nrocor"  as correlativo,
	emp.empleador_1,
	emp.empleador_2,
	 /*fecha contable*/ eam."eam_conefe"::date as fecha,
	 /*periodo contable*/ --year(datepart(eam."eam_conefe"))*100 + month(datepart(eam."eam_conefe")) as periodo,
	est.P_DDV_EST.fecha_periodo(eam."eam_conefe") AS periodo,
	 /*rut prestador*/sol."sol_insrut"  as rut_prestador,
	 /*prestacion ges-caec, ges o caec*/
	case
	    when eam."der_idn">0 and eam."cso_idn" = 0 then 'GES'
		when eam."cso_idn">0 and eam."der_idn" = 0 then 'CAEC'
		when eam."der_idn">0 and eam."cso_idn">0   then 'GES-CAEC'
		else 'OTRO' 
	end  as activacion, 
	/*es urgencia*/	eam."eam_urgencia" as urgencia,
	/*cobrado*/eam."eam_valcob"::int  as cobrado,
 	/*bonificado*/ eam."eam_valbon"::int + eam."fld_gescaec"::int as bonificado
    from HOS.P_RDV_DST_LND_SYB.SH_EAM eam 
    inner join AFI.P_RDV_DST_LND_SYB.BEN ben
	on eam."eam_nrocon" = ben."fld_funfolio"
	and eam."eam_nrocor" = ben."fld_funcorrel"
	and eam."eam_codben" = ben."fld_bencorrel"
	and eam."eam_conefe" between '2021-07-01 00:00:00' and '2021-12-31 23:59:59'
    and eam."eam_estado" not in (3,4)
    AND eam."eam_urgencia" ='S'
    left join HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol
    on eam."sol_nrosol" = sol."sol_nrosol" 
    and eam."eam_rutcot" = sol."sol_rutafi" 
    and eam."eam_codben" = sol."sol_codben"
	left join est.P_DDV_EST.ca_emp emp
	on eam."eam_nrocon" = emp.fld_funfolio
	and eam."eam_nrocor" = emp.fld_funcorrel
)
----------FIN GASTOS HOSPITALARIOS
------------ FIN GASTOS

----------------------------------- fin extraccion



SELECT * FROM EST.P_DDV_EST.CA_HOSPI  LIMIT 10

SELECT PERIODO ,count(1) FROM EST.P_DDV_EST.CA_HOSPI 
GROUP BY PERIODO 
ORDER BY PERIODO 
LIMIT 10


SELECT * FROM LCC.P_RDV_DST_LND_SYB.CARTAS_TODO_FACTOR LIMIT 10

SELECT * FROM est.P_DDV_EST.ca_emp LIMIT 10


SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_EAH
WHERE "eah_crecol" = 0 AND "eah_urgencia" IN ('S')
LIMIT 100






SELECT * FROM est.P_DDV_EST.ca_emp LIMIT 10

SELECT * FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO LIMIT 10

-------DATAMART
------urgencias
WITH urgencia AS ( 
SELECT 
		a.ORIGEN,
		a.ID,
		a.PERIODO ,
		--COD_CIE ,
		--GLOSA_CIE ,
		A.RUT_TITULAR ,
		a.CODATE_TRAZADORA,
		b."fld_agruitemdes" ,
		c.empleador_1,
		c.empleador_2,
		COUNT(DISTINCT (A.SOLICITUD_DE_TRATAMIENTO)) AS CANTIDAD_CONTADA,
		SUM(A.CANTIDAD) AS CANTIDAD_SUMADA,
		SUM(a.BONIFICADO)::INT AS TOTAL_BONIFICADO
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO A
INNER JOIN ISA.P_RDV_DST_LND_SYB.CDTCOMP B ON (A.CODATE_TRAZADORA = B."fld_prestacod")
LEFT JOIN est.P_DDV_EST.ca_emp  c ON (SUBSTRING(a.CONTRATO,0,9)::int = c.fld_funfolio AND a.correlativo = c.fld_funcorrel)
WHERE A.PERIODO BETWEEN 202011 AND 202104  
	  AND A.CODATE_TRAZADORA IN (30508,30509,30512,30513,101771,101772,101773,101774,101775,101776) 
GROUP BY 
		a.ORIGEN,
		a.ID,
		a.PERIODO ,
		--COD_CIE ,
		--GLOSA_CIE ,
		A.RUT_TITULAR ,
		a.CODATE_TRAZADORA,
		b."fld_agruitemdes",
		c.empleador_1,
		c.empleador_2
)
SELECT * FROM urgencia LIMIT 50		





----urgencia		
SELECT DISTINCT
	PERIODO ,
	RUT_TITULAR
FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (30508,30509,30512,30513,101771,101772,101773,101774,101775,101776)
	  AND PERIODO BETWEEN 202011 AND 202104;



	 
-------- urgencia
----urgencia
WITH urgen AS ( 	
SELECT DISTINCT
	PERIODO ,
	RUT_TITULAR
FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE  CODATE_DETALLE IN (30508,30509,30512,30513,101771,101772,101773,101774,101775,101776)
	  AND PERIODO BETWEEN 202009 AND 202108
)
SELECT 
	SUBSTRING(PERIODO,0,4) AS anio,
	RUT_TITULAR , 
	count(RUT_TITULAR) AS cantidad  
FROM urgen
GROUP BY anio , RUT_TITULAR
ORDER BY cantidad DESC , RUT_TITULAR 
;


------ activacion GES-CAEC

SELECT * FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
LIMIT 10


SELECT * FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO 
LIMIT 10


