SELECT DISTINCT 
	PERIODO ,
	ORIGEN ,
	count(1),
	SUM(BONIFICADO) 
FROM GTO.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM 
WHERE PERIODO >= 202101
GROUP BY PERIODO ,ORIGEN 
ORDER BY PERIODO  , ORIGEN 
--LIMIT 10


SELECT * FROM GTO.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM
WHERE CODIGO_ATENCION IN (1801006, 1891006)
LIMIT 10


SELECT
*
FROM GTO.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM GTPI
INNER JOIN ISA.P_RDV_DST_LND_SYB.CDTCOMP CDT ON(GTPI.CODIGO_ATENCION=CDT."fld_prestacod")
LIMIT 10


--AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
--HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)



----- 1  Cataract extraction and insert IOL
----  AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (1202064)
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
--------------------------------------




----- 2  Colonoscopy (+/- Biopsy / Polyp)
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (1801006)
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY PERIODO ,
		 RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------




SELECT DISTINCT 
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total,
	sum(CANTIDAD)
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (	1701031 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY PERIODO ,
		 RUT_TITULAR ,
		 RUT_BENEFICIARIO 


----- 3  Coronary Angioplasty
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (	1701031 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
--------





--- 5 al 34  CT Scan
--- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (		403001,
								403002,
								403003,
								403006,
								403007,
								403008,
								403012,
								403013,
								403014,
								403016,
								403017,
								403018,
								403019,
								403020,
								403021,
								403022,
								403023,
								403024,
								403025,
								403101,
								403102,
								403103,
								403104,
								403105,
								403106,
								403701,
								403702,
								403709,
								403719,
								404113)
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
----------------------------



--- 35 al 70  MRI Scan
--- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (		405001,
								405002,
								405003,
								405004,
								405005,
								405006,
								405007,
								405008,
								405009,
								405010,
								405011,
								405012,
								405013,
								405016,
								405017,
								405018,
								405019,
								405020,
								405021,
								405022,
								405023,
								405024,
								405025,
								405026,
								405027,
								405028,
								405029,
								405030,
								405031,
								405032,
								405033,
								405098,
								405726,
								431027,
								431141,
								441029)
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
----------------------------
--------------------------------------


----- 71 al 75  Total Hip Replacement (Uni and Bi)
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (		2104129,
								2104229,
								2104803,
								2104804,
								2301065 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------



----- 76 al 77  Total Knee Replacement (Uni and Bi)
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (		2104153,
								2301055 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------




----- 78  Hernia - Inguinal Repair
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (1802003)
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------





----- 79 al 82  Prostate Surgery (includes Prostate resection and prostatectomy)
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (		1902055,
								1902056,
								1902057,
								2501016	 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------




----- 83  Spinal Fusion
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (	1103069 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------




----- 84  Upper endoscopy
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (1801001)
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 85 al 169   Xray
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	401001,
							401002,
							401004,
							401005,
							401006,
							401008,
							401009,
							401010,
							401011,
							401012,
							401013,
							401014,
							401015,
							401016,
							401018,
							401019,
							401020,
							401021,
							401022,
							401023,
							401024,
							401026,
							401027,
							401028,
							401029,
							401030,
							401031,
							401032,
							401033,
							401034,
							401035,
							401040,
							401042,
							401043,
							401044,
							401045,
							401046,
							401047,
							401048,
							401049,
							401051,
							401052,
							401053,
							401054,
							401055,
							401056,
							401057,
							401058,
							401059,
							401060,
							401062,
							401063,
							401064,
							401070,
							401110,
							401124,
							401130,
							401151,
							401310,
							401351,
							401701,
							402005,
							402008,
							402009,
							402011,
							402012,
							402014,
							402015,
							402019,
							402020,
							402022,
							402023,
							402024,
							402025,
							402027,
							402029,
							402030,
							402031,
							402032,
							402033,
							402035,
							402038,
							402040,
							402041,
							402050)
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 170  Appendectomy
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (	1802053 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------




----- 171  Cholecystectomy
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (	1802081 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------




----- 172  C-section
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (	2004006 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------




----- 173  Heart bypass surgery
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (	1703020 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------




----- 174  Lab test - Full Blood Count
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (301045)
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------





----- 175  Normal delivery
----- HOSPITALARIO (CODATE_TRAZADORA) - (CUENTA ST DISTINTAS)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (	2004003 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-----------------------





----- 176  Physiotherapy
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	601024 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------



----- 177  Physiotherapy
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	601022 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 178  Physiotherapy
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	601015 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 179  Physiotherapy
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	601023 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 180 al 215  Virtual visits
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE 	PERIODO BETWEEN 201901 AND 201912 AND 
		--GRUPO =('CONSULTAS') AND 
		CODATE_DETALLE IN (	108200,
							108201,
							108202,
							108203,
							108204,
							108205,
							108206,
							108207,
							108208,
							108209,
							108210,
							108211,
							108212,
							108213,
							108301,
							108302,
							108303,
							108304,
							108305,
							108306,
							108307,
							108308,
							108309,
							108310,
							108311,
							108312,
							108319,
							108320,
							108321,
							108322,
							108323,
							108324,
							108325,
							108326,
							108327,
							108331)
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 216-217  Follow-up visit (specialist) in office
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE 	PERIODO BETWEEN 201901 AND 201912 AND 
		GRUPO =('CONSULTAS') AND 
		CODATE_DETALLE NOT IN (	108200,
								108201,
								108202,
								108203,
								108204,
								108205,
								108206,
								108207,
								108208,
								108209,
								108210,
								108211,
								108212,
								108213,
								108301,
								108302,
								108303,
								108304,
								108305,
								108306,
								108307,
								108308,
								108309,
								108310,
								108311,
								108312,
								108319,
								108320,
								108321,
								108322,
								108323,
								108324,
								108325,
								108326,
								108327,
								108331)
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----------------------------
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	COUNT(DISTINCT SOLICITUD_DE_TRATAMIENTO ) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	1602203,
							1602205 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 

----- 218-219 Excision Skin Lesion
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	1602203,
							1602205 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------








------------------------------------------
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	1602211,
							1602212 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 

----- 220-221 Moh's Surgery
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	count(DISTINCT SOLICITUD_DE_TRATAMIENTO )AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	1602211,
							1602212 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 222 ENBREL SURECLICK
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	9970825 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 223  HUMIRA
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN ( 9946983 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY PERIODO ,
		 RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 226  ATORVASTATIN (Lipitor)
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN (	9914801 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY PERIODO ,
		 RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
GROUP BY PERIODO 
LIMIT 10
-------------------------------






-----------------------------------------
SELECT DISTINCT 
				PERIODO ,
				RUT_TITULAR ,
				RUT_BENEFICIARIO ,
				COBRADO,
				CANTIDAD 
				--,*
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN ( 9988029 )
	  AND PERIODO BETWEEN 201901 AND 201912
ORDER BY PERIODO , RUT_TITULAR 

----- 227  HARVONI (LEDIPASIVIR/SOFOSBUVIR)
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN ( 9988029 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 228  HUMALOG KWIKPEN U-100
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN ( 9966986 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------



----- 229  INSULIN GLARGINE (Lantus)
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN ( 9939702 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY PERIODO ,
		 RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
GROUP BY PERIODO 
LIMIT 10
-------------------------------



----- 232  DESCOVY
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN ( 9988107 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY PERIODO ,
		 RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 234  SERTRALINE
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN ( 9938420 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY --PERIODO ,
		 --RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
--GROUP BY PERIODO 
LIMIT 10
-------------------------------




----- 234  ESCITALOPRAM
----- AMBULATORIA (CODATE_DETALLE) - (SUMA CANTIDAD)
WITH fila_1 AS ( 
SELECT DISTINCT 
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(COBRADO) AS cobrado_total,
	sum(CANTIDAD) AS cantidad_total
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_DETALLE IN ( 9960229 )
	  AND PERIODO BETWEEN 201901 AND 201912
GROUP BY PERIODO ,
		 RUT_TITULAR ,
		 RUT_BENEFICIARIO 
)
, fila_1_ordenada AS ( 
SELECT 
	* 
FROM fila_1
)
SELECT
	--PERIODO ,
	sum(cobrado_total)::int AS cobrado,
	sum(cantidad_total)::int AS cantidad,
	cobrado/cantidad::int AS promedio,
	'pesos' AS moneda,
	sum(cantidad_total)::int AS cantidad,
	count(DISTINCT RUT_BENEFICIARIO) AS cantidad_rut
FROM fila_1_ordenada
GROUP BY PERIODO 
LIMIT 10
-------------------------------







SELECT 
	PERIODO_VIGENCIA ,
	count(DISTINCT RUT_TITULAR),
	sum(CNT_CARFAM)
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO_VIGENCIA BETWEEN 201901 AND 201912
GROUP BY PERIODO_VIGENCIA 
LIMIT 10

