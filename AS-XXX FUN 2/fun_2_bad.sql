WITH fun_2 AS(
SELECT
	"fld_periodoprod", 
	"fld_cotrut",
	"fld_funfolio",
	"fld_funcorrel",
	"fld_funnotificacod",
	"fld_desafmotivo" 
FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE       "fld_funnotificacod" in (2) 
		AND "fld_periodoprod" >= '2015-01-01 00:00:00' 
		AND "fld_desafmotivo" IN (3,13)
), personas_bad AS (
SELECT 
	b."fld_cotrut",	
	b."fld_periodoprod",
	b."fld_funnotificacod",
	b."fld_desafmotivo",
	b."fld_funfolio",
	b."fld_funcorrel",
	a."bad_producto",
	c."bad_nombre",
	a."bad_fecsus",
	a."bad_vigdes",
	a."bad_vighas" 
FROM AFI.P_RDV_DST_LND_SYB.BAD a
	INNER JOIN fun_2 b ON (a."bad_funfolio"=b."fld_funfolio" AND a."bad_funcorrel"=b."fld_funcorrel"  AND a."bad_producto" IN(39,40,41,45,46,47,50))
	INNER JOIN ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL c ON (a."bad_producto"=c."bad_idn")
)
SELECT 
	* 
FROM personas_bad
ORDER BY "fld_periodoprod"


