------- extraccion por las otras prestaciones
------ fila 29
SELECT 
	PERIODO ,
	FUENTE_ORIGEN ,
	--SUCURSAL_EMISION ,
	sum (CASE 
			WHEN SUCURSAL_EMISION IN (16) THEN CANTIDAD
		 ELSE 0
	END) AS WEB,
	sum (CASE 
			WHEN SUCURSAL_EMISION NOT IN (16) THEN CANTIDAD
		 ELSE 0
	END) AS PRESENCIAL
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO BETWEEN 202103 AND 202108 AND ORIGEN NOT IN ('LICENCIA') AND FUENTE_ORIGEN IN ('BONO', 'REEMBOLSO')
GROUP BY PERIODO,
		 FUENTE_ORIGEN 
ORDER BY PERIODO  


------- extraccion por las otras prestaciones
------ fila 31
SELECT 
	PERIODO ,
	FUENTE_ORIGEN ,
	COUNT(DISTINCT 
					CASE 
						WHEN SUCURSAL_EMISION IN (16) THEN  RUT_BENEFICIARIO 
						END
		 ) AS WEB, 
		COUNT(DISTINCT 
					CASE 
						WHEN SUCURSAL_EMISION NOT IN  (16) THEN  RUT_BENEFICIARIO 
						END
		 ) AS PRESENCIAL
	--FUENTE_ORIGEN ,
	--ORIGEN ,
	--sum(CANTIDAD) AS cant 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO BETWEEN 202103 AND 202108 AND ORIGEN NOT IN ('LICENCIA') AND FUENTE_ORIGEN IN ('REEMBOLSO')
GROUP BY PERIODO,
		 FUENTE_ORIGEN 
ORDER BY PERIODO  




------- extraccion por las otras prestaciones
------ fila 29
SELECT 
	PERIODO ,
	--FUENTE_ORIGEN ,
	--SUCURSAL_EMISION ,
	sum (CASE 
			WHEN SUCURSAL_EMISION IN (16) THEN CANTIDAD
		 ELSE 0
	END) AS WEB,
	sum (CASE 
			WHEN SUCURSAL_EMISION NOT IN (16) THEN CANTIDAD
		 ELSE 0
	END) AS PRESENCIAL
	--FUENTE_ORIGEN ,
	--ORIGEN ,
	--sum(CANTIDAD) AS cant 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO BETWEEN 202103 AND 202108 AND ORIGEN NOT IN ('LICENCIA') AND PRESTACION =13
GROUP BY PERIODO
		-- FUENTE_ORIGEN 
		 --SUCURSAL_EMISION ,
		 --TIPO_CANAL
		 --FUENTE_ORIGEN ,
		 --ORIGEN 
ORDER BY PERIODO  



------- Activacion ges
------- 39 apertura ges
SELECT DISTINCT 
	--"rutcot" as rut_afiliado,
	--SUBSTRING("rutcot",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	--"rutben" as rut_beneficiario,
	--SUBSTRING("rutben",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	--"nombre_ben" as nombre_beneficiario,
	"periodo_solges" AS periodo_activacion_ges,
	--"fecha_solges"::date as fecha_activacion_ges,
	count(DISTINCT "codcie") AS cant                     ----- EL CONTADOR ES POR PROBLEMA DE SALUD
	--"descrip_is" as tipo_patologia
	--"estado_caso" 
FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO  
WHERE "fecha_solges" BETWEEN '2021-03-01 00:00:00' AND '2021-08-31 23:59:59' 
	  AND "estado_caso" not in ('ANULADO', 'NO CURSADO')
GROUP BY 
	--rut_afiliado,
	--rut_beneficiario,
	--nombre_beneficiario,
	periodo_activacion_ges
	--fecha_activacion_ges
ORDER BY
	periodo_activacion_ges
LIMIT 100



----- activaciones caec
------ 40 Apertura caec
SELECT DISTINCT 
	--"rutcot" AS rut_afiliado,
	--"rutben" AS rut_beneficiario,
	--"nombre_ben"  AS nombre_beneficiario,
	"periodo_red" AS fecha_activacion_caec,
	count(DISTINCT "codcie") AS cantidad          ----- la cantidad es por problema de salud
FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
WHERE "periodo_red" BETWEEN 202103 AND 202108 --AND "cod_sucursal" IN (0)
GROUP BY 
	--rut_afiliado,
	--rut_beneficiario,
	--nombre_beneficiario,
	fecha_activacion_caec
ORDER BY 
	fecha_activacion_caec
LIMIT 100



----- compra bono ges
----- 46 compra bono ges
SELECT 
	PERIODO ,
	ORIGEN ,
	--FUENTE_ORIGEN ,
	--TIPO_BONO ,
	--SUCURSAL_EMISION ,
	COUNT(DISTINCT 
					CASE 
						WHEN SUCURSAL_EMISION IN (16) THEN  FOLIO 
						END
		 ) AS WEB, 
		COUNT(DISTINCT 
					CASE 
						WHEN SUCURSAL_EMISION NOT IN  (16) THEN  FOLIO 
						END
		 ) AS PRESENCIAL
	--FUENTE_ORIGEN ,
	--ORIGEN ,
	--sum(CANTIDAD) AS cant 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO BETWEEN 202103 AND 202108 AND ORIGEN IN ('GES') AND TIPO_BONO IN (2,5)
GROUP BY PERIODO ,
		 ORIGEN 
		 --FUENTE_ORIGEN 
		 --TIPO_BONO 
		 --SUCURSAL_EMISION ,
		 --TIPO_CANAL
		 --FUENTE_ORIGEN ,
		 --ORIGEN 
ORDER BY PERIODO  





--------- extracto hospitalario
--------- 49 hospitalizaciones
SELECT 
	PERIODO ,
	ORIGEN ,
	COUNT(DISTINCT 
					CASE 
						WHEN SUCURSAL_EMISION IN (16) THEN  FOLIO 
						END
		 ) AS WEB, 
		COUNT(DISTINCT 
					CASE 
						WHEN SUCURSAL_EMISION NOT IN  (16) THEN  FOLIO 
						END
		 ) AS PRESENCIAL
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO BETWEEN 202103 AND 202108 AND ORIGEN IN ('HOSPITALARIO')
GROUP BY PERIODO , ORIGEN 
ORDER BY PERIODO  




---- licencias
---- 57 historial de licencias 
SELECT 
	PERIODO ,
	ORIGEN ,
	COUNT(DISTINCT 
					CASE 
						WHEN SUCURSAL_RECEPCION IN (16) THEN  LCC_COMCOR 
						END
		 ) AS WEB, 
		COUNT(DISTINCT 
					CASE 
						WHEN SUCURSAL_RECEPCION NOT IN  (16) THEN  LCC_COMCOR 
						END
		 ) AS PRESENCIAL
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO BETWEEN 202103 AND 202108 AND ORIGEN IN ('LICENCIA')
GROUP BY PERIODO , ORIGEN 
ORDER BY PERIODO  




------- licencias
------- 60 liquidaciones pagos
WITH licencias AS ( 
select
	CONCAT_WS('-',  LCC."lcc_comcod", LCC."lcc_comcor") AS FOLIO_LM_OP_COMPIN,
	LCC."lcc_comcod",
	LCC."lcc_comcor",
	est.P_DDV_EST.fecha_periodo(liq."liq_fecpag") AS periodo
FROM  "LCC"."P_RDV_DST_LND_SYB"."LIQ" liq
LEFT JOIN "LCC"."P_RDV_DST_LND_SYB"."LCC" lcc ON (LCC."lcc_idn"=LIQ."lcc_idn" )
where 1=1
and LIQ."liq_estado" <> 5
and "liq_fecpag" between '2021-03-01' and '2021-08-30'  --que pasa con las LM de 2016 que se pagaron en 2020, es real el dato?
group by 1,2,3,4
order by 1 nulls FIRST
)
SELECT 
	PERIODO,
	COUNT(1) AS CANTIDAD
FROM licencias 
GROUP BY 
	PERIODO
ORDER BY PERIODO
LIMIT 10






SELECT DISTINCT origen, fuente_origen FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO

------ para la tabla caec_estudio no hay codigos de sucursal  0 ni codigos 16
SELECT DISTINCT "periodo_red", "cod_sucursal" , "gls_sucursal" , count(DISTINCT "codcie" ) AS cant FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
WHERE "periodo_red" BETWEEN 202101 AND 202104
GROUP BY "periodo_red" , "cod_sucursal" , "gls_sucursal" 
ORDER BY "periodo_red" , "cod_sucursal" 
--LIMIT 100
------------------ fin activaciones CAEC




	  


------ para la tabla caec_estudio no hay codigos de sucursal  0 ni codigos 16
SELECT 
	"periodo_solges" ,
	"cod_sucursal" ,
	"gls_sucursal" ,
	COUNT(DISTINCT "codcie" ) cantidad 
FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO
WHERE "periodo_solges" BETWEEN 202101 AND 202103 AND "estado_caso" not in ('ANULADO', 'NO CURSADO') 
GROUP BY "periodo_solges" ,
		"cod_sucursal" ,
		"gls_sucursal" 
ORDER BY "periodo_solges" ,
		 "cod_sucursal" 
--LIMIT 10


	  
	  
	  
SELECT * FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
WHERE "rutcot" ='016965310-0' AND "periodo_red" IN (202102)
LIMIT 10



SELECT DISTINCT "cod_sucursal" , "gls_sucursal" FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO
ORDER BY "cod_sucursal" 
LIMIT 100

SELECT * FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO
WHERE "cod_sucursal" IN (16) AND "periodo_solges" >= 202101
LIMIT 100






-----------------LO ULTIMO QUE EXTRAJE POR LA CANTIDAD QUE NO CUADRABA EN EL DTM

WITH reem AS ( 
select 
	ree."fld_reefolio" as idn,
	ree."fld_succod" AS sucursal,
	ree."fld_cotrut" as rut_afiliado,
	SUBSTRING(ree."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	cdt."fld_bencorrel" as bencorrel, /*aca esta en la tabla de detalle*/
	cdt."fld_benrut" as rut_beneficiario,
	SUBSTRING(cdt."fld_benrut",1,9)::int*5-3265112+52658  AS ID_RUT_BENEFICIARIO,
	ree."fld_funfolio" as contrato, /*contrato*/
	ree."fld_funcorrel" as correlativo, /*correlativo del contrato*/  
	--emp.empleador_1,
	--emp.empleador_2, 
	cdt."fld_medrut" as rut_prestador,
	ree."fld_reepagfec"::date as fecha, /*fecha prestación*/
	EST.P_DDV_EST.FECHA_PERIODO(ree."fld_reepagfec") AS periodo,
	--year(datepart(ree.fld_reepagfec))*100 + month(datepart(ree.fld_reepagfec)) as periodo, /*periodo*/
	case
	   when ree."cso_idn" > 0 and ree."fld_folioderivages" = 0 then 'CAEC'
	   when ree."cso_idn" = 0 and ree."fld_folioderivages" > 0 then 'GES'
	   when ree."fld_folioderivages" > 0 and ree."cso_idn" > 0 then 'GES-CAEC'
	   else 'OTRO' 
	end as activacion, /*GES, CAEC o plan complementario*/
	cdt."fld_cant" AS cantidad,
	ree."fld_totprestaval"::int as cobrado,
	(ree."fld_totprestaval" - ree."fld_totbonific")::int as bonificado,
	'ree' as origen
from GTO.P_RDV_DST_LND_SYB.SRE  ree
inner join GTO.P_RDV_DST_LND_SYB.SRE_CDT cdt
	on ree."fld_reefolio" = cdt."fld_reefolio"
	--and cdt."fld_srecorrel" = 1
	and ree."fld_reeestado" not in (3,4) 
	and ree."fld_reepagfec" between '2021-03-01 00:00:00' and '2021-08-31 23:59:59'
--left join est.P_DDV_EST.ca_emp emp
	--on ree."fld_funfolio" = emp.fld_funfolio
	--and ree."fld_funcorrel" = emp.fld_funcorrel
------------- FIN REEM
)
SELECT
	periodo,
	--CASE 
	--	WHEN sucursal = 16 THEN 'web'
	--	ELSE 'presencial'
	--END canal,
	--count(DISTINCT idn),
	sum(cantidad)
FROM reem 
GROUP BY 
	periodo
	--canal
ORDER BY 
	periodo
	--canal
LIMIT 10


SELECT * FROM emisuc

SELECT "eah_pagsuc" ,"eah_conesu"  ,* FROM HOS.P_RDV_DST_LND_SYB.SH_EAH LIMIT 100
--WHERE 

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_SOLTRA LIMIT 10
WHERE "sol_sucdig" 

WITH  eah_eam AS ( 
SELECT DISTINCT 
	est.P_DDV_EST.fecha_periodo("eah_conefe") AS periodo, 
	"eah_rutcot" AS titular, 
	--eah."sol_benrut" ,
	--"eah_pagsuc" , 
	--"eah_conesu" , 
	CASE 
		WHEN "eah_pagsuc" = 16 THEN 'web'
		ELSE 'presencial'
	END AS canal,
	--"eah_idn" , 
	--eah."sol_nrosol" 
	 "eah_idn" AS idn
	--sol."sol_sucdig" 
FROM HOS.P_RDV_DST_LND_SYB.SH_EAH  
WHERE "eah_conefe" BETWEEN '2021-03-01 00:00:00' AND '2021-08-31 23:59:59' AND "eah_tipemi" = 'R' and "eah_estado" not in (3,4) 
UNION 
SELECT 
	est.P_DDV_EST.fecha_periodo("eam_conefe") AS periodo,
	"eam_rutcot" AS titular,
	CASE 
		WHEN "eam_pagsuc" = 16 THEN 'web'
		ELSE 'presencial'
	END AS canal,
	-"eam_idn"  AS idn
FROM HOS.P_RDV_DST_LND_SYB.SH_EAM 
WHERE "eam_conefe" BETWEEN '2021-03-01 00:00:00' AND '2021-08-31 23:59:59' AND "eam_estado" NOT IN (3,4) AND "eam_tipemi" ='R' 
)
SELECT  
	periodo,
	canal,
	count(DISTINCT idn) AS cant
	--count(DISTINCT titular) AS cantidad
FROM eah_eam
GROUP BY periodo, canal
ORDER BY periodo
LIMIT 10




SELECT * FROM gto.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS LIMIT 10


SELECT 
	PERIODO ,
	TIPO_EMISION ,
	CASE 
		WHEN CODSUCEMISION IN (16) THEN 'web' 
		ELSE 'presencial'
	END AS canal,
	count(DISTINCT FOLIO) AS cantidad
	--sum(CANTIDAD)
	--count(DISTINCT RUTAFI) AS cantidad
FROM gto.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS
WHERE PERIODO BETWEEN 202103 AND 202108 --AND TIPO_EMISION ='REEMBOLSO'
GROUP BY PERIODO , canal , TIPO_EMISION 
ORDER BY PERIODO  , TIPO_EMISION , canal
--LIMIT 10



SELECT * FROM 



SELECT * FROM hos.P_RDV_DST_LND_SYB.VIHOSP_HMQ LIMIT 10

SELECT PERIODO , FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202103 AND 202108 AND 
LIMIT 10


SELECT * FROM GTO.P_RDV_DST_LND_SYB.SRE 
LIMIT 10


SELECT * FROM 

SELECT 
	* 
FROM 







----- revision de codigo de sucursal
select
CONCAT_WS('-',  LCC."lcc_comcod", LCC."lcc_comcor") AS FOLIO_LM_OP_COMPIN,
 LCC."lcc_comcod",
 LCC."lcc_comcor",
liq."liq_fecpag"
from "LCC"."P_RDV_DST_LND_SYB"."LIQ" liq
LEFT JOIN "LCC"."P_RDV_DST_LND_SYB"."LCC" lcc ON (LCC."lcc_idn"=LIQ."lcc_idn" )
where 1=1
and LIQ."liq_estado" <> 5
and "liq_fecpag" between '2021-01-01' and '2021-08-30'  --que pasa con las LM de 2016 que se pagaron en 2020, es real el dato?
group by 1,2,3,4
order by 1 nulls FIRST
LIMIT 10

