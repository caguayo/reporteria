SELECT 
	* 
FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_periodoprod" 
LIMIT 10


SELECT DISTINCT 
	PERIODO_VN ,
	count(DISTINCT FLD_COTRUT_VN),
	PERIODO_REN ,
	count(DISTINCT FLD_COTRUT_REN),
	PERIODO_DESAF ,
	count(DISTINCT FLD_COTRUT_DESAF)
FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA 
WHERE PERIODO_VN >= 202201
GROUP BY PERIODO_VN , PERIODO_REN , PERIODO_DESAF 
ORDER BY PERIODO_VN 
--LIMIT 10









SELECT 
	periodo_vn,
	count(periodo_vn),
	count(periodo_ren),
	count(periodo_desaf)
FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA
WHERE periodo_vn >=202101
GROUP BY periodo_vn
ORDER BY periodo_vn
LIMIT 1000



SELECT
	*
FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA 
WHERE periodo_vn=202205 AND periodo_ren =202209
LIMIT 100


SELECT 
	*
FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA 
WHERE fld_cotrut_vn ='018396442-9'
ORDER BY periodo_vn

SELECT 
	*
FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_cotrut"='018396442-9'
LIMIT 10



SET periodo_ini ='2017-01-01 00:00:00'

SET periodo_fin = getdate()::timestamp

CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_PHARMAMAX_TASA AS ( 
----- ventas nuevas pharmamax
------------------ CODIGO EXTRACCION FINAL 
WITH vn_pharmamax AS ( 
---- pharmamax antiguos
SELECT DISTINCT 
	lfc."fld_cotrut" AS fld_cotrut_vn ,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN, 
	cnt."fld_concostototal" AS costototal_vn ,
	lfc."fld_succod" AS cod_sucursal_vn,
	lfc."fld_agente" AS agecod_vn,
	age1.FLD_AGERUT AS rut_age_vn ,
	age1.FLD_NOMBRE AS nombre_agente_vn,
	cnt."fld_agencianum" AS agencia_vn,
	dir1.JEFE_COMERCIAL AS jefe_comercial_vn,
	COALESCE (dir1.NOMBRE_DIRECTOR,dir2.AGENTE_GLS) AS director_vn
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )
LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age1 ON (lfc."fld_agente"=age1.FLD_AGECOD AND lfc."fld_agencia" =age1.FLD_AGENCIARUT)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age2 ON (lfc."fld_agenteresp"=age2.FLD_AGECOD AND lfc."fld_agenciaresp" =age2.FLD_AGENCIARUT)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_DIRECTOR  dir1 ON (lfc."fld_agencia"=dir1.AGENCIA) --- amarre por codigo de agencia
--LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (lfc."fld_agencia"=dir2.COD_AGENCIA)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (cnt."fld_codneg"=dir2.COD_NEGOCIO) --- amarre por el codigo de negocio
	WHERE  lfc."fld_funnotificacod" = 1  AND 
		   lfc."fld_periodoprod" BETWEEN $periodo_ini AND $periodo_fin  AND
		   cnt."fld_tipopharma" = 5
UNION 
---- pharmamax nuevos
SELECT DISTINCT 
	lfc."fld_cotrut" AS fld_cotrut_vn,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN,
	cnt."fld_concostototal"  AS costototal_vn,
	lfc."fld_succod" AS cod_sucursal_vn,
	lfc."fld_agente" AS agecod_vn,
	age1.FLD_AGERUT AS rut_age_vn,
	age1.FLD_NOMBRE AS nombre_agente_vn,
	cnt."fld_agencianum" AS agencia_vn,
	dir1.JEFE_COMERCIAL AS jefe_comercial_vn ,
	COALESCE (dir1.NOMBRE_DIRECTOR,dir2.AGENTE_GLS) AS director_vn
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc 
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel") 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (lfc."fld_funfolio"=bad."bad_funfolio" AND lfc."fld_funcorrel"=bad."bad_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age1 ON (lfc."fld_agente"=age1.FLD_AGECOD AND lfc."fld_agencia" =age1.FLD_AGENCIARUT)
--LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age2 ON (lfc."fld_agenteresp"=age2.FLD_AGECOD AND lfc."fld_agenciaresp" =age2.FLD_AGENCIARUT)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_DIRECTOR  dir1 ON (lfc."fld_agencia"=dir1.AGENCIA) --- amarre por codigo de agencia
--LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (lfc."fld_agencia"=dir2.COD_AGENCIA)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (cnt."fld_codneg"=dir2.COD_NEGOCIO) --- amarre por el codigo de negocio  cambiar por agancia y agente 
	WHERE  bad."bad_producto" IN (65) AND
		   lfc."fld_funnotificacod" = 1 AND 
		   lfc."fld_periodoprod" BETWEEN $periodo_ini AND $periodo_fin
)
, ordena_pharma AS ( 
SELECT 
		ROW_NUMBER () OVER (PARTITION BY fld_cotrut_vn  ORDER BY periodo_vn ) AS n_veces,
	* 
FROM vn_pharmamax 
)
,
----- renuncias pharmamax 
renuncia_pharmamax AS ( 
SELECT
	"fld_cotrut" AS fld_cotrut_ren,
	pd.period AS periodo_ren,
	cnt."fld_concostototal" AS costotoal_ren,
	cnt."fld_succod" AS cod_sucursal_ren,
	cnt."fld_agecod" AS agecod_ren,
	age1.FLD_NOMBRE AS nombre_agente_ren,
	cnt."fld_agencianum" AS agencia_ren,
	dir1.JEFE_COMERCIAL AS jefe_comercial_ren ,
	COALESCE (dir1.NOMBRE_DIRECTOR,dir2.AGENTE_GLS) AS director_ren,
	CASE
		WHEN cnt."fld_tipopharma" IN (5)   THEN 'pharma_max'
		WHEN bad."bad_producto"   IN (65)  THEN 'pharma_max'
		ELSE 'sin pharma_max'
	END AS transac_pharmamax,
	CASE 
		WHEN transac_pharmamax ='pharma_max' THEN 'mantiene pharmamax'
		ELSE 'renuncia'
	END AS motivo_ren
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
left JOIN EST.P_DDV_EST.periods pd ON (pd.period BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA)
left JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel" AND bad."bad_producto"=65)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age1 ON (cnt."fld_agecod"=age1.FLD_AGECOD AND cnt."fld_agencianum" =age1.FLD_AGENCIARUT)
--LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age2 ON (cnt.."fld_agenteresp"=age2.FLD_AGECOD AND lfc."fld_agenciaresp" =age2.FLD_AGENCIARUT)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_DIRECTOR  dir1 ON (cnt."fld_agencianum"=dir1.AGENCIA) --- amarre por codigo de agencia
--LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (lfc."fld_agencia"=dir2.COD_AGENCIA)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (cnt."fld_codneg"=dir2.COD_NEGOCIO) --- amarre por el codigo de negocio  cambiar por agancia y agente 
--INNER JOIN est.P_DDV_EST.CA_VN b ON (a."fld_cotrut"= b."fld_cotrut") 
	WHERE period >=201701
--a.FLD_VIGREALDESDE <= 201910 AND a.FLD_VIGREALHASTA >= 201910 
--AND "fld_tipopharma" = 5 --AND "fld_finpharma" > 0
ORDER BY  fld_cotrut_ren , periodo_ren
)
, 
ordena_renuncias AS (
SELECT DISTINCT 
	*,
	ROW_NUMBER () OVER (PARTITION BY fld_cotrut_ren ORDER BY periodo_ren) AS contador_ren
FROM renuncia_pharmamax 
WHERE motivo_ren ='renuncia'
ORDER BY fld_cotrut_ren , periodo_ren
)
,
--WITH 
----- desafiliaciones pharmamax
desafi_pharmamax AS ( 
SELECT DISTINCT 
	lfc."fld_cotrut" AS fld_cotrut_desaf ,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS periodo_desaf, 
	cnt."fld_concostototal" AS costototal_desaf ,
	cnt."fld_succod" AS cod_sucursal_desaf,
	cnt."fld_agecod" AS agecod_desaf,
	age1.FLD_NOMBRE AS nombre_agente_desaf,
	cnt."fld_agencianum" AS agencia_desaf,
	dir1.JEFE_COMERCIAL AS jefe_comercial_desaf ,
	COALESCE (dir1.NOMBRE_DIRECTOR,dir2.AGENTE_GLS) AS director_desaf,
	'sin pharma_max' AS fld_tipopharmamax,
	case ("fld_funnotificacod")
	    when 2 then 'Desafiliacion isapre'
		when 333 then 'desafiliacion voluntaria'
    end as motivo_desaf
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )
LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age1 ON (cnt."fld_agecod"=age1.FLD_AGECOD AND cnt."fld_agencianum" =age1.FLD_AGENCIARUT)
--LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age2 ON (cnt.."fld_agenteresp"=age2.FLD_AGECOD AND lfc."fld_agenciaresp" =age2.FLD_AGENCIARUT)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_DIRECTOR  dir1 ON (cnt."fld_agencianum"=dir1.AGENCIA) --- amarre por codigo de agencia
--LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (lfc."fld_agencia"=dir2.COD_AGENCIA)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (cnt."fld_codneg"=dir2.COD_NEGOCIO) --- amarre por el codigo de negocio  cambiar por agancia y agente 
	WHERE   ("fld_funnotificacod"=2 or "fld_funnotificacod"=333) AND ---- tipos de desafiliacion
		   lfc."fld_periodoprod" BETWEEN $periodo_ini AND $periodo_fin  AND  ----- periodo produccion
		   cnt."fld_tipopharma" = 5  ---- pertenece a pharmamax
UNION
SELECT DISTINCT 
	lfc."fld_cotrut" AS fld_cotrut_desaf ,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS periodo_desaf, 
	cnt."fld_concostototal" AS costototal_desaf ,
	cnt."fld_succod" AS cod_sucursal_desaf,
	cnt."fld_agecod" AS agecod_desaf,
	age1.FLD_NOMBRE AS nombre_agente_desaf,
	cnt."fld_agencianum" AS agencia_desaf,
	dir1.JEFE_COMERCIAL AS jefe_comercial_desaf ,
	COALESCE (dir1.NOMBRE_DIRECTOR,dir2.AGENTE_GLS) AS director_desaf,
	'sin pharma_max' AS fld_tipopharmamax,
	case ("fld_funnotificacod")
	    when 2 then 'Desafiliacion isapre'
		when 333 then 'desafiliacion voluntaria'
    end as motivo_desaf
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )
INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (lfc."fld_funfolio"=bad."bad_funfolio" AND lfc."fld_funcorrel"=bad."bad_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age1 ON (cnt."fld_agecod"=age1.FLD_AGECOD AND cnt."fld_agencianum" =age1.FLD_AGENCIARUT)
--LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE age2 ON (cnt.."fld_agenteresp"=age2.FLD_AGECOD AND lfc."fld_agenciaresp" =age2.FLD_AGENCIARUT)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_DIRECTOR  dir1 ON (cnt."fld_agencianum"=dir1.AGENCIA) --- amarre por codigo de agencia
--LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (lfc."fld_agencia"=dir2.COD_AGENCIA)
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO     dir2 ON (cnt."fld_codneg"=dir2.COD_NEGOCIO) --- amarre por el codigo de negocio  cambiar por agancia y agente 
	WHERE   ("fld_funnotificacod"=2 or "fld_funnotificacod"=333) AND ---- tipos de desafiliacion
		    lfc."fld_periodoprod" BETWEEN $periodo_ini AND $periodo_fin  AND  ----- periodo produccion
		    bad."bad_producto" IN (65)		    
)
SELECT 
	vn.N_VECES ,
	vn.FLD_COTRUT_VN ,
	vn.PERIODO_VN::INT AS PERIODO_VN ,
	vn.COSTOTOTAL_VN::DOUBLE AS COSTOTOTAL_VN ,
	vn.cod_sucursal_vn,
	vn.agecod_vn,
	vn.rut_age_vn,
	vn.nombre_agente_vn,
	vn.agencia_vn,
	vn.jefe_comercial_vn,
	vn.director_vn,
	ren.FLD_COTRUT_REN ,
	ren.PERIODO_REN::INT AS PERIODO_REN ,
	ren.COSTOTOAL_REN::DOUBLE AS COSTOTOTAL_REN ,
	ren.cod_sucursal_ren,
	ren.agecod_ren,
	ren.nombre_agente_ren,
	ren.agencia_ren,
	ren.jefe_comercial_ren,
	ren.director_ren,
	ren.TRANSAC_PHARMAMAX ,
	ren.MOTIVO_REN ,
	ren.CONTADOR_REN ,
	des.FLD_COTRUT_DESAF ,
	des.PERIODO_DESAF::INT AS PERIODO_DESAF ,
	des.COSTOTOTAL_DESAF::DOUBLE AS COSTOTOTAL_DESAF ,
	des.cod_sucursal_desaf,
	des.agecod_desaf,
	des.nombre_agente_desaf,
	des.agencia_desaf,
	des.jefe_comercial_desaf,
	des.director_desaf,
	des.FLD_TIPOPHARMAMAX ,
	des.MOTIVO_DESAF 
FROM ordena_pharma vn
LEFT  JOIN ordena_renuncias   ren ON (vn.fld_cotrut_vn = ren.fld_cotrut_ren AND periodo_ren >= periodo_vn AND motivo_ren='renuncia' AND contador_ren = 1)
LEFT  JOIN desafi_pharmamax   des ON (vn.fld_cotrut_vn = des.fld_cotrut_desaf AND periodo_desaf >= periodo_vn)
--WHERE fld_cotrut_desaf = '011044802-3'
ORDER BY periodo_vn , fld_cotrut_vn , periodo_ren
--LIMIT 2000
)




SELECT DISTINCT 
	PERIODO_VN ,
	count(1)
FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA
GROUP BY PERIODO_VN 
ORDER BY PERIODO_VN desc
LIMIT 10


SELECT * FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA LIMIT 1000



SELECT DISTINCT 
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN,
	*
	--count(DISTINCT lfc."fld_cotrut")
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
WHERE lfc."fld_funnotificacod" = 1 AND lfc."fld_periodoprod" BETWEEN '2021-08-01 00:00:00' AND '2021-08-31 23:59:59'
--GROUP BY periodo_vn
ORDER BY periodo_vn desc
LIMIT 1000



SELECT 
	*
FROM ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL



------- 1
SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_monedatipocod",
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cnt."fld_cotrut" ,
	ben."fld_benrut" ,
	ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel")
INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
WHERE  cnt.FLD_VIGREALHASTA = 202112 AND bad."bad_producto" IN (50,65,66) AND  cnt."fld_cotrut" IN ('008553063-1' , '009005128-8')-- NO VIGENTE
ORDER BY ben."fld_cotrut" , ben."fld_bencorrel" 
LIMIT 10000



-- 2

SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_monedatipocod",
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cnt."fld_cotrut" ,
	--ben."fld_benrut" ,
	ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel" AND cnt."fld_cotrut"=ben."fld_benrut")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
WHERE cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202201 AND TIPO_COTIZACION IN ('$ + UF','%','7% + UF' ) AND tipo_bad IN ('OTRO BAD' , 'SIN BAD')-- AND bad."bad_producto" IN (50,65,66)
ORDER BY ben."fld_cotrut" , ben."fld_bencorrel" 
LIMIT 1000


SELECT 
	
	*
FROM ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ 

SELECT
	cnt."fld_monedatipocod" AS mod_moneda,
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad,
	COUNT (DISTINCT "fld_cotrut")
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
LEFT  JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
WHERE  cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202201
GROUP BY mod_moneda , tipo_cotizacion ,tipo_bad



-- 3
SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_monedatipocod",
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cnt."fld_cotrut" ,
	--ben."fld_benrut" ,
	ben."fld_bennacfec" ,
	(datediff(month, ben."fld_bennacfec", getdate()) / 12)::int  as Edad,
	ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel" AND cnt."fld_cotrut"=ben."fld_benrut")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
WHERE cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202201 AND edad < 16 --AND ben."fld_bencorrel" =0 -- AND bad."bad_producto" IN (50,65,66)
ORDER BY ben."fld_cotrut" , ben."fld_bencorrel" 
LIMIT 50



---- 4
SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_monedatipocod",
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cnt."fld_cotrut" ,
	ben."fld_benrut" ,
	ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	--bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IN (0)		THEN 'SIN BAD'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel")-- AND cnt."fld_cotrut"=ben."fld_benrut")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
WHERE cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202201 AND ben."fld_benedad" >= 69 AND tipo_bad ='BUR'  --AND ben."fld_bencorrel" NOT IN (0) -- AND bad."bad_producto" IN (50,65,66)
ORDER BY ben."fld_cotrut" , ben."fld_bencorrel" 
LIMIT 100


---- 5
SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_monedatipocod",
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cnt."fld_cotrut" ,
	ben."fld_benrut" ,
	ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	--bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IN (0)		THEN 'SIN BAD'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
WHERE cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202201 AND ben."fld_benedad" <= 66 AND tipo_bad NOT IN ('BAC') AND ben."fld_bencorrel" NOT IN (0) -- AND bad."bad_producto" IN (50,65,66)
ORDER BY ben."fld_cotrut" , ben."fld_bencorrel" 
--LIMIT 100


SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_monedatipocod",
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cnt."fld_cotrut" ,
	ben."fld_benrut" ,
	ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	--bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IN (0)		THEN 'SIN BAD'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
WHERE cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202201 AND ben."fld_benedad" BETWEEN 67 AND 69 AND tipo_bad NOT IN ('BUR') AND ben."fld_bencorrel" NOT IN (0) -- AND bad."bad_producto" IN (50,65,66)
ORDER BY ben."fld_cotrut" , ben."fld_bencorrel" 
--LIMIT 100



---- 6
WITH deuda_vigente AS ( 
SELECT DISTINCT 
	--DTI_PER_COT ,
	DTI_RUT_COT , 
	DTI_FUNFOLIO ,
	DTI_FUNCORREL,
	COUNT(DISTINCT DTI_PER_COT) AS CANT_MESES,
	sum(DTI_MTO_DIF_CTO) AS total_deuda 
FROM ISA.P_RDV_DST_LND_SYB.DTI_NEW
	WHERE DTI_ESTADO IN (0,2) AND 
		  DTI_PER_COT BETWEEN 202101 AND 202112
GROUP BY 
	--DTI_PER_COT ,
	DTI_RUT_COT , 
	DTI_FUNFOLIO ,
	DTI_FUNCORREL
)
SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_monedatipocod",
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cnt."fld_cotrut" ,
	ben."fld_benrut" ,
	ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	--bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IN (0)		THEN 'SIN BAD'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad,
	CASE 
		WHEN dv.TOTAL_DEUDA > 0 THEN 'CON MORA'
		ELSE 'SIN MORA'
	END AS deuda_vigente,
	CANT_MESES
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel")
LEFT  JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
LEFT  JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
LEFT  JOIN deuda_vigente dv ON (cnt."fld_funfolio"=dv.DTI_FUNFOLIO AND cnt."fld_funcorrel"=DTI_FUNCORREL)
WHERE cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202201 AND tipo_bad ='OTRO BAD' AND deuda_vigente ='CON MORA'  AND ben."fld_bencorrel" =0 -- AND bad."bad_producto" IN (50,65,66)
ORDER BY CANT_MESES DESC 
LIMIT 1000




WITH deuda_vigente AS ( 
SELECT DISTINCT 
	--DTI_PER_COT ,
	DTI_RUT_COT , 
	DTI_FUNFOLIO ,
	DTI_FUNCORREL, 
	COUNT(DISTINCT DTI_PER_COT) AS CANT_MESES,
	sum(DTI_MTO_DIF_CTO) AS total_deuda 
FROM ISA.P_RDV_DST_LND_SYB.DTI_NEW
	WHERE DTI_ESTADO IN (0,2) AND 
		  DTI_PER_COT BETWEEN 202101 AND 202112
GROUP BY 
	--DTI_PER_COT ,
	DTI_RUT_COT , 
	DTI_FUNFOLIO ,
	DTI_FUNCORREL
)
SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_monedatipocod",
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cnt."fld_cotrut" ,
	ben."fld_benrut" ,
	ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	--bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IN (0)		THEN 'SIN BAD'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad,
	CASE 
		WHEN dv.TOTAL_DEUDA > 0 THEN 'CON MORA'
		ELSE 'SIN MORA'
	END AS deuda_vigente,
	CANT_MESES
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel")
LEFT  JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
LEFT  JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (cnt."fld_monedatipocod"= modc.COD_MODCOTZ)
LEFT  JOIN deuda_vigente dv ON (cnt."fld_funfolio"=dv.DTI_FUNFOLIO AND cnt."fld_funcorrel"=DTI_FUNCORREL)
WHERE cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202201 AND tipo_bad IN ('BUR','BAC') AND deuda_vigente ='CON MORA' AND ben."fld_bencorrel" =0 -- AND bad."bad_producto" IN (50,65,66)
ORDER BY CANT_MESES DESC
LIMIT 100




---- 7
SELECT DISTINCT 
	pd.period,
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	--cnt."fld_monedatipocod",
	cnt."fld_cotrut" ,
	--ben."fld_benrut" ,
	--ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	--bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IN (0)		THEN 'SIN BAD'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad,
	ROW_NUMBER () OVER (PARTITION BY cnt."fld_cotrut"  ORDER BY pd.period ) AS n_veces,
	CASE 
		WHEN pd.PERIOD < 202101 AND tipo_bad = 'BUR' THEN 'OK'
		ELSE 'NO'
	END chequeo_8
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
LEFT  JOIN EST.P_DDV_EST.periods pd ON (pd.period BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA)
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel" AND cnt."fld_cotrut"=ben."fld_benrut")
INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
	WHERE  cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202009 AND tipo_bad = 'BUR'--AND pd.period BETWEEN 202009 AND 202201--AND tipo_bad IN 'OTRO BAD' --  AND ben."fld_bencorrel" =0 -- AND bad."bad_producto" IN (50,65,66)
ORDER BY   pd.PERIOD , cnt."fld_cotrut" 
--LIMIT 1000



---- 8
SELECT DISTINCT 
	pd.period,
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	--cnt."fld_monedatipocod",
	cnt."fld_cotrut" ,
	--ben."fld_benrut" ,
	--ben."fld_bencorrel" ,
	ben."fld_benedad" ,
	--bad."bad_producto" ,
	CASE 
		WHEN bad."bad_producto" IN (50) 	THEN 'BAC'
		WHEN bad."bad_producto" IN (65,66) 	THEN 'BUR'
		WHEN bad."bad_producto" IN (0)		THEN 'SIN BAD'
		WHEN bad."bad_producto" IS NULL 	THEN 'SIN BAD'
		ELSE 'OTRO BAD'
	END AS tipo_bad,
	ROW_NUMBER () OVER (PARTITION BY cnt."fld_cotrut"  ORDER BY pd.period ) AS n_veces,
	CASE 
		WHEN pd.PERIOD > 202201  THEN 'VIGENTE'
		ELSE 'ANTERIOR'
	END chequeo_8
FROM  AFI.P_RDV_DST_LND_SYB.CNT cnt 
LEFT  JOIN EST.P_DDV_EST.periods pd ON (pd.period BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA)
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel" AND cnt."fld_cotrut"=ben."fld_benrut")
INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
	WHERE  cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202009 AND tipo_bad IN  ('BUR')--AND pd.period BETWEEN 202009 AND 202201--AND tipo_bad IN 'OTRO BAD' --  AND ben."fld_bencorrel" =0 -- AND bad."bad_producto" IN (50,65,66)
ORDER BY   cnt."fld_cotrut" ,   pd.PERIOD-- , tipo_bad
--LIMIT 1000









--t1.FLD_BENVIGREALDESDE <= 201912
--t1.FLD_BENVIGREALHASTA >= 201901
--
SELECT
	pd.PERIOD ,
	count(DISTINCT cnt."fld_cotrut")
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
LEFT JOIN EST.P_DDV_EST.periods pd ON (pd.period BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA)
WHERE 		cnt."fld_tipopharma" IN (5)  AND  cnt.FLD_VIGREALDESDE<= 202201 AND cnt.FLD_VIGREALHASTA >= 202101
GROUP BY pd.PERIOD 
ORDER BY pd.PERIOD 
--LIMIT 10


SELECT 
* 
FROM EST.P_DDV_EST.periods


SELECT
	lfc."fld_cotrut" AS fld_cotrut_vn,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN,
	cnt."fld_concostototal"  AS costototal_vn,
	cnt."fld_agecod" AS agecod_vn,
	cnt."fld_agencianum" AS agencia_vn
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc 
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel") 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (lfc."fld_funfolio"=bad."bad_funfolio" AND lfc."fld_funcorrel"=bad."bad_funcorrel")
	WHERE  bad."bad_producto" IN (65) 
	AND lfc."fld_periodoprod" BETWEEN '2020-01-01 00:00:00' AND '2022-01-31 23:59:59'
--GROUP BY periodo_vn
ORDER BY periodo_vn
LIMIT 10


SELECT 
	* 
FROM AFI.P_RDV_DST_LND_SYB.BAD 
WHERE "bad_producto" IN(65)
LIMIT 10




SELECT * FROM ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL-- LIMIT 10


WITH vn_pharmamax AS ( 
SELECT DISTINCT 
	lfc."fld_cotrut" AS fld_cotrut_vn ,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN, 
	cnt."fld_concostototal" AS costototal_vn ,
	cnt."fld_agecod" AS agecod_vn,
	cnt."fld_agencianum" AS agencia_vn
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )

WHERE  lfc."fld_funnotificacod" = 1  AND cnt."fld_tipopharma" IN (5)   AND 
	   lfc."fld_periodoprod" BETWEEN '2020-01-01 00:00:00' AND '2022-01-31 23:59:59'
UNION
SELECT
	lfc."fld_cotrut" AS fld_cotrut_vn,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN,
	cnt."fld_concostototal"  AS costototal_vn,
	cnt."fld_agecod" AS agecod_vn,
	cnt."fld_agencianum" AS agencia_vn
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc 
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel") 
INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (lfc."fld_funfolio"=bad."bad_funfolio" AND lfc."fld_funcorrel"=bad."bad_funcorrel")
	WHERE  bad."bad_producto" IN (65) 
	AND lfc."fld_periodoprod" BETWEEN '2020-01-01 00:00:00' AND '2022-01-31 23:59:59'
)
, ordena_pharma AS ( 
SELECT 
		ROW_NUMBER () OVER (PARTITION BY fld_cotrut_vn  ORDER BY periodo_vn ) AS n_veces,
	* 
FROM vn_pharmamax 
)
SELECT DISTINCT 
	periodo_vn,
	count(DISTINCT fld_cotrut_vn )
FROM ordena_pharma
GROUP BY periodo_vn
ORDER BY periodo_vn



SELECT 
	*
FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA 
WHERE 
	PERIODO_REN =202108--  AND 
	--FLD_COTRUT_VN ='011843411-0'
LIMIT 1000

SELECT * FROM ISA.P_RDV_DST_LND_SYB.AGE 
	--WHERE --FLD_AGECOD = 27 AND 
    --CONTAINS (FLD_NOMBRE,'MARIE GA')--AND FLD_AGENCIARUT ='087-6'
LIMIT 100

SELECT 
	*
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE "fld_cotrut" ='011843411-0'
LIMIT 100

SELECT * FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA LIMIT 1000

SELECT * FROM 

SELECT
	"fld_agencia" ,
	* 
FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_cotrut" ='016793163-4'
LIMIT 10

SELECT "fld_agencianum" ,* FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE "fld_funfolio" =11523978 AND "fld_funcorrel" = 1



SELECT * FROM ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_DIRECTOR LIMIT 100

SELECT * FROM ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO LIMIT 100




create or replace view EST.P_DDV_EST.periods as (
with nums(i) as (
				select 0 union all select i + 1 from nums where i <=80  ---ajustar para el fin de los meses
			    )    
	select 
		est.P_DDV_EST.fecha_periodo(dateadd(month, i, '2017-01-01 00:00:00.000'::timestamp)) as period 
	from nums
	    )


SELECT 
min(PERIOD),
MAX(PERIOD) 
FROM EST.P_DDV_EST.periods LIMIT 10


SELECT * FROM ISA.P_RDV_DST_LND_SYB.AGE  LIMIT 10


SELECT
	cnt."fld_tipopharma" ,
	bad."bad_producto" 
	FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
	left JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (cnt."fld_funfolio"=bad."bad_funfolio" AND cnt."fld_funcorrel"=bad."bad_funcorrel")
WHERE "fld_cotrut" ='006681204-9'


SELECT * FROM  EST.P_DDV_EST.CA_PHARMAMAX_TASA 
WHERE PERIODO_VN =202112 AND MOTIVO_REN ='renuncia'
LIMIT 100


SELECT 
	*,
	FLD_AGERUT 
FROM ISA.P_RDV_DST_LND_SYB.AGE LIMIT 10









SELECT 
	*
FROM EST.P_DDV_EST.CA_PHARMAMAX_TASA 
WHERE 
	PERIODO_REN =202108--  AND 
	--FLD_COTRUT_VN ='011843411-0'
LIMIT 1000



SELECT * FROM EST.P_DDV_EST.CA_MIX_MES_MOVIMIENTO  LIMIT 10



SELECT * FROM ISA.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO LIMIT 10  --- amarre por el codigo de negocio


SELECT * FROM "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_DIRECTOR" LIMIT 10  --- amarre por codigo de agencia



LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE B ON (A."fld_agente"=B.FLD_AGECOD AND A."fld_agencia" =B.FLD_AGENCIARUT)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.AGE C ON (A."fld_agenteresp"=C.FLD_AGECOD AND A."fld_agenciaresp" =C.FLD_AGENCIARUT)


SELECT * FROM ISA.P_RDV_DST_LND_SYB.AGE LIMIT 10

SELECT * FROM AFI.P_RDV_DST_LND_SYB.LFC LIMIT 10

---------- DESAFILIACIONES
WITH desafiliaciones AS ( 
SELECT
	EST.P_DDV_EST.FECHA_PERIODO("fld_periodoprod") as PERIOD,
    "fld_cotrut" AS fld_cotrut_mensual,
    "fld_funfolio" ,
    "fld_funcorrel" ,
    --"fld_succod" ,
    case ("fld_funnotificacod")
	    when 2 then 'Desafiliacion isapre'
		when 333 then 'desafiliacion voluntaria'
    end as motivo,
    'renuncia pharma_max' AS fld_tipopharma 
from AFI.P_RDV_DST_LND_SYB.LFC 
	WHERE  "fld_periodoprod" >= '2017-01-01 00:00:00'
	and ("fld_funnotificacod"=2 or "fld_funnotificacod"=333)
)
SELECT 
	*
FROM desafiliaciones
LIMIT 10


SELECT  
	period,
	fld_cotrut_mensual,
	ROW_NUMBER () OVER (PARTITION BY fld_cotrut_mensual ORDER BY period) AS n_veces,
	fld_tpotransac ,
	fld_tipopharma 
	FROM desafiliaciones
WHERE fld_tpotransac NOT IN ('pharma_max') 
	  --"fld_cotrut" ='015485668-4' 
ORDER BY period , fld_cotrut_mensual 
