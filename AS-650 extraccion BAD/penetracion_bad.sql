

SELECT TOp 10 * FROM 



------cheq idn bad
SELECT LFC_PERIODO ,"bad_idn" , COUNT(DISTINCT("bad_cotrut")) FROM EST.P_DDV_EST.CA_BADPENETRACION
WHERE "bad_idn" = 50
GROUP BY LFC_PERIODO , "bad_idn" 

SELECT TOP 10 * FROM EST.P_DDV_EST.CA_BAD

SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.LFC 
----- fin check




----- bad penetracion 
CREATE OR REPLACE TABLE est.P_DDV_EST.ca_badpenetracion AS (
WITH BASE_BAD  AS (
SELECT
		T2."fld_cotrut",
		T1."bad_cotrut", 
		T2."fld_funfolio",
		T2."fld_funcorrel",
		T1."bad_funfolio",
		T1."bad_funcorrel",
		T2."fld_tpotransac",
		T2."fld_funnotificacod",
		T2."fld_funnotificacod"::int AS funcod,
		T2."fld_periodo",
		T1."bad_vigdes",
		T1."bad_vighas",
		t1."bad_producto",
		T3."bad_idn",
		T3."bad_nombre",
		T3."bad_costo",
		T1."bad_costot",
		T1."bad_cosfin",
		EST.P_DDV_EST.FECHA_PERIODO(T2."fld_fechafun") as lfc_periodo,
		T1."bad_descto"
FROM afi.P_RDV_DST_LND_SYB.BAD t1
     INNER JOIN afi.P_RDV_DST_LND_SYB.LFC              t2 ON (t2."fld_funfolio" = t1."bad_funfolio" AND t2."fld_funcorrel" = t1."bad_funcorrel")
     INNER JOIN isa.P_RDV_DST_LND_SYB.CPW_BENADICIONAL  t3 ON (t1."bad_producto" = t3."bad_idn")
WHERE  T2."fld_fechafun" BETWEEN '2020-01-01 00:00:00' AND '2020-12-31 00:00:00'-- AND    t2."fld_fechafun" = t1."bad_fecsus")
,
revision_bad AS (
SELECT
		t1.lfc_periodo,
		t1."fld_cotrut",
		t1."fld_funfolio",
		t1."fld_funcorrel",
		t1."fld_tpotransac",
		t1."fld_funnotificacod",
		t1."bad_idn",
		t1."bad_nombre",
		t1."bad_costo",
		t1."bad_costot",
		t1."bad_cosfin",
		t1."bad_descto",
case when t1."fld_tpotransac" in('VN','CT') then 'VN' else 'MOD' end as tipo_transac
from BASE_BAD t1
where t1."bad_idn" IN (28,39,40,41,45,46,47,50,55,56) AND
      (t1."fld_funnotificacod" = 1 or (t1.funcod LIKE '%9%')))
, 
BEN AS (
   SELECT *
      FROM AFI.P_RDV_DST_LND_SYB.BEN  t1
      WHERE t1.FLD_BENVIGREALDESDE <= 202012 AND t1.FLD_BENVIGREALHASTA >= 202001
)
SELECT DISTINCT 
	  /* anno */
	  --(input(substr(cat(t1.lfc_periodo),1,4),4.)) AS anno, 
	  t1.lfc_periodo AS Periodo_Mensual, 
	  /* id_rut */
	  --(INPUT(SUBSTR(t1.fld_cotrut,1,9),15.)*5-3265112+52658) AS id_rut, 
	  t1."fld_funfolio" AS Codigo_Fun, 
	  t1."fld_funcorrel" AS Correlativo_Fun, 
	  /* Total_Beneficiarios */
	  (COUNT(DISTINCT(t3."fld_benrut"))) AS Total_Beneficiarios, 
	  t1."bad_costot", 
	  t1."bad_cosfin", 
	  t1."bad_descto",
	  t1."bad_idn",
	  t1."bad_nombre",
	  t1."bad_costo",
	  t1.tipo_transac,
	  CASE
    	WHEN t1."bad_costo" = 0 
    	THEN t1."bad_cosfin"/t1."bad_costot" 
    	ELSE t1."bad_cosfin"/t1."bad_costo" 
	  END as Ventas_Totales
  FROM revision_bad t1
       LEFT JOIN BEN t3 ON (t1."fld_cotrut" = t3."fld_cotrut") AND (t1."fld_funfolio" = t3."fld_funfolio") 
       AND (t1."fld_funcorrel" = t3."fld_funcorrel")
  WHERE t1.lfc_periodo BETWEEN 202001 AND 202012
  GROUP BY 
  	   --(CALCULATED anno),
       t1.lfc_periodo,
       --(CALCULATED id_rut),
       t1."fld_funfolio",
       t1."fld_funcorrel",
       t1."bad_costot",
       t1."bad_cosfin",
       t1."bad_descto",
       t1."bad_idn",
       t1."bad_nombre",
       t1.tipo_transac,
       t1."bad_costot", 
	   t1."bad_cosfin", 
	   t1."bad_descto",
	   t1."bad_costo"
)


SELECT FROM est.P_DDV_EST.ca_badpenetracion


SELECT TOP 10 "fld_fechafun" ,* FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE "fld_funfolio" =5240398 AND "fld_funcorrel" = 8903

SELECT top 10 "bad_fecsus" ,* FROM AFI.P_RDV_DST_LND_SYB.BAD
WHERE "bad_cotrut" ='016204116-9' AND "bad_funfolio" = 5240398 AND "bad_funcorrel" = 8903



SELECT LFC_PERIODO , TIPO_TRANSAC , COUNT(DISTINCT("fld_cotrut")) FROM est.P_DDV_EST.ca_badpenetracion
WHERE "bad_idn" = 50
GROUP BY LFC_PERIODO ,TIPO_TRANSAC 
ORDER BY LFC_PERIODO 




CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_REVISION_BAD AS (
WITH BASE_BAD  AS (
SELECT
		T2."fld_cotrut",
		T1."bad_cotrut", 
		T2."fld_funfolio",
		T2."fld_funcorrel",
		T1."bad_funfolio",
		T1."bad_funcorrel",
		T2."fld_tpotransac",
		T2."fld_funnotificacod",
		T2."fld_funnotificacod"::int AS funcod,
		T2."fld_periodo",
		T1."bad_vigdes",
		T1."bad_vighas",
		t1."bad_producto",
		T3."bad_idn",
		T3."bad_nombre",
		T3."bad_costo",
		T1."bad_costot",
		T1."bad_cosfin",
		EST.P_DDV_EST.FECHA_PERIODO(T2."fld_fechafun") as lfc_periodo,
		T1."bad_descto"
FROM afi.P_RDV_DST_LND_SYB.BAD t1
     --INNER JOIN afi.P_RDV_DST_LND_SYB.LFC              t2 ON (t2."fld_funfolio" = t1."bad_funfolio" AND t2."fld_funcorrel" = t1."bad_funcorrel")
     INNER JOIN isa.P_RDV_DST_LND_SYB.CPW_BENADICIONAL  t3 ON (t1."bad_producto" = t3."bad_idn")
WHERE  
	--T2."fld_fechafun" BETWEEN '2017-01-01 00:00:00' AND '2020-12-31 00:00:00' /* AND    t2."fld_fechafun" = t1."bad_fecsus"*/)
,
revision_bad AS (
SELECT
		t1.lfc_periodo,
		t1."fld_cotrut",
		t1."fld_funfolio",
		t1."fld_funcorrel",
		t1."fld_tpotransac",
		t1."fld_funnotificacod",
		t1."bad_idn",
		t1."bad_nombre",
		t1."bad_costo",
		t1."bad_costot",
		t1."bad_cosfin",
		t1."bad_descto",
case when t1."fld_tpotransac" in('VN','CT') then 'VN' else 'MOD' end as tipo_transac
from BASE_BAD t1
where t1."bad_idn" IN (28,39,40,41,45,46,47,50,55,56) AND
      (t1."fld_funnotificacod" = 1 or (t1.funcod LIKE '%9%'))
)
--SELECT * FROM revision_bad
--WHERE "fld_cotrut"='016204116-9'
SELECT * FROM revision_bad)












WITH BASE_BAD  AS (
SELECT
		T2."fld_cotrut",
		T1."bad_cotrut", 
		T2."fld_funfolio",
		T2."fld_funcorrel",
		T1."bad_funfolio",
		T1."bad_funcorrel",
		T2."fld_tpotransac",
		T2."fld_funnotificacod",
		T2."fld_funnotificacod"::int AS funcod,
		T2."fld_periodo",
		T1."bad_vigdes",
		T1."bad_vighas",
		t1."bad_producto",
		T3."bad_idn",
		T3."bad_nombre",
		T3."bad_costo",
		T1."bad_costot",
		T1."bad_cosfin",
		EST.P_DDV_EST.FECHA_PERIODO(T2."fld_fechafun") as lfc_periodo,
		T1."bad_descto"
FROM afi.P_RDV_DST_LND_SYB.BAD t1
     INNER JOIN afi.P_RDV_DST_LND_SYB.LFC              t2 ON (t2."fld_funfolio" = t1."bad_funfolio" AND t2."fld_funcorrel" = t1."bad_funcorrel")
     INNER JOIN isa.P_RDV_DST_LND_SYB.CPW_BENADICIONAL  t3 ON (t1."bad_producto" = t3."bad_idn")
WHERE  T2."fld_fechafun" BETWEEN '2017-01-01 00:00:00' AND '2020-12-31 00:00:00' AND   t2."fld_fechafun" = t1."bad_fecsus")
SELECT top 10 * FROM BASE_BAD
WHERE "fld_cotrut" = '016204116-9'


SELECT TOP 10 * FROM afi.P_RDV_DST_LND_SYB.BAD
WHERE "bad_cotrut" ='015041701-5'

SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_cotrut" ='015708000-8'

