CREATE OR REPLACE TABLE est.P_DDV_EST.CA_bad_antiguos AS (
SELECT    
	  "fld_cotrut",
	  "fld_funfolio",
	  "fld_funcorrel", 
      /* bad_pharma */
        (case
	        when "fld_tipopharma" in (3, 4) then 'Pharma Plus'
	        when "fld_tipopharma" = 5 then 'Pharma Max'
	        else 'NO'
        end) AS bad_pharma, 
      /* bad_censantiav */
        (case
	        when "fld_tipseg" = 1 then 'Cesantia 6 viejo'
	        when "fld_tipseg" = 2 then 'Cesantia 9 viejo'
	        when "fld_tipseg" = 3 then 'Cesantia 12 viejo'
	        else 'NO'
        end) AS bad_censantiav, 
      /* bad_goldenmax */
        (case            
        when "fld_goldenmax" > 0.00 
        then 'golden_max' 
        else 'NO' 
        end) AS bad_goldenmax			
FROM AFI.P_RDV_DST_LND_SYB.CNT
	WHERE 	("fld_tipseg" IN (1,2,3) OR "fld_tipopharma" IN(3,4,5))
	AND	FLD_VIGREALDESDE <= 202012
    AND FLD_VIGREALHASTA >= 202012
)



SELECT top 10 * FROM est.P_DDV_EST.CA_bad_antiguos


CREATE OR REPLACE TABLE est.P_DDV_EST.ca_bad_nuevos AS (
SELECT    
	  t1."bad_cotrut", 
	  t1."bad_funfolio",
	  t1."bad_funcorrel", 
	  t1."bad_producto", 
	  t2."bad_nombre"
	  --YEAR(INTNX('MONTH',INPUT(PUT(&PeriodoN.*100 + 1, best8.), YYMMDD10.),+1))*100 + MONTH (INTNX('MONTH',INPUT(PUT(&PeriodoN.*100 + 1, best8.), YYMMDD10.),+1)) as periodo	
FROM AFI.P_RDV_DST_LND_SYB.BAD t1
	   LEFT JOIN ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL t2 ON (t1."bad_producto" = t2."bad_idn")
  		WHERE 202012 BETWEEN t1."bad_vigdes" AND t1."bad_vighas"
)



CREATE OR REPLACE TABLE est.P_DDV_EST.ca_cartera_bad AS (
WITH cartera_temp AS (
SELECT DISTINCT 
"fld_cotrut",
"fld_funfolio", 
"fld_funcorrel" 
FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE FLD_VIGREALDESDE <= 202012
    AND FLD_VIGREALHASTA >= 202012
)
SELECT
cn."fld_cotrut",
cn."fld_funfolio",
cn."fld_funcorrel",
CASE WHEN ba.BAD_PHARMA IS NULL THEN 'NO' ELSE ba.BAD_PHARMA END AS bad_pharma  ,
CASE WHEN ba.BAD_CENSANTIAV IS NULL THEN 'NO' ELSE ba.BAD_CENSANTIAV END AS bad_cesantiav ,
CASE WHEN ba.BAD_GOLDENMAX IS NULL THEN 'NO' ELSE ba.BAD_GOLDENMAX END AS bad_goldenmax ,
bn."bad_producto" ,
bn."bad_nombre" 
FROM cartera_temp cn
	LEFT JOIN est.P_DDV_EST.CA_bad_antiguos ba ON ( cn."fld_funfolio" = ba."fld_funfolio" AND cn."fld_funcorrel" = ba."fld_funcorrel")
    LEFT JOIN est.P_DDV_EST.ca_bad_nuevos bn ON (cn."fld_funfolio" = bn."bad_funfolio" AND cn."fld_funcorrel" = bn."bad_funcorrel")
)


SELECT COUNT(1)  FROM est.P_DDV_EST.ca_cartera_bad





	