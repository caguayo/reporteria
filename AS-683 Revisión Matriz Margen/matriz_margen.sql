
SELECT 
	* 
FROM "GTO"."P_DDV_GTO"."P_DDV_GTO_MATRIZ_MARGEN"

SELECT 
	*
FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO"
LIMIT 10



------------ codigo 
------ permanencia
WITH mm_permanencia AS (
SELECT DISTINCT
    rut_todo AS RUT_TITULAR,
    "CONSEXO" AS SEXO,
    EST.P_DDV_EST.FECHA_PERIODO(FECHA_INGRESO) AS PER_INGRESO,
    PERVIG,
    ANTIGUEDAD,
    (case 
            when t1.antiguedad between 0 and 12 then '00 a 12'
            when t1.antiguedad between 13 and 24 then '13 a 24'
            when t1.antiguedad between 25 and 36 then '25 a 36'
            when t1.antiguedad between 37 and 48 then '37 a 48'
            when t1.antiguedad between 49 and 60 then '49 a 60'
            when t1.antiguedad between 61 and 72 then '61 a 72'
            when t1.antiguedad between 73 and 84 then '73 a 84'
            when t1.antiguedad between 85 and 96 then '85 a 96'
            when t1.antiguedad between 97 and 108 then '97 a 108'
            when t1.antiguedad between 109 and 120 then '109 a 120'
            when t1.antiguedad between 121 and 240 then '121 a 240'
            when t1.antiguedad between 241 and 360 then '241 a 360'
         else '361 y m�s'
         end) AS RangoPermanencia  
FROM (    
     ----------- inicia calculo antiguedad ------------------
      SELECT 
          TODO.max_periodo AS PERVIG,
          TODO.rut_todo,
          A.FECHA_INGRESO,
          IFF(A.FECHA_INGRESO = A.FECHA_INGRESO_ISAPRE AND A.tipo_transaccion in ('VN','CT'), 'S','' ) AS ENCONTRADO,
          IFF(A.FECHA_INGRESO = A.FECHA_INGRESO_ISAPRE AND A.tipo_transaccion in ('VN','CT'), todo.min_periodo,null ) AS perdesde,
          IFF(ENCONTRADO= 'S', datediff(month, TO_DATE(perdesde || '01', 'yyyymmdd'), TO_DATE(PERVIG || '01', 'yyyymmdd'))+1,
          datediff(month, add_months(A.FECHA_INGRESO,2), TO_DATE(PERVIG || '01', 'yyyymmdd') )+1) AS ANTIGUEDAD,
          CON."CONSEXO"
      FROM(
            ------ INICIA TODO --------------    
            SELECT 
                max(periodo_vigencia) AS max_periodo,
                min(periodo_vigencia) AS min_periodo,
                rut_titular as rut_todo
            FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO"
            GROUP BY rut_titular 
            ------ TERMINA TODO --------------    
           ) TODO
      LEFT OUTER JOIN "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" A
            ON TODO.max_periodo = A.PERIODO_VIGENCIA AND TODO.rut_todo = A.RUT_TITULAR
      LEFT OUTER JOIN 
          (select "CONRUTBEN", MIN("CONSEXO") AS consexo from  "AFI"."P_DDV_AFI"."P_DDV_AFI_CON"
          GROUP BY  "CONRUTBEN") CON) t1
)
SELECT
	*
FROM mm_gasto
WHERE RUT_TITULAR = '015666377-8'
--GROUP BY RUT_TITULAR
ORDER BY rut_titular
LIMIT 100



-------- matriz margen gasto
----------------------------
create OR REPLACE temporary table "EST"."P_STG_EST"."P_STG_EST_TEMP_MARGEN_BASE_GASTOS" AS 
Select    s2.periodo::INT AS PERIODO, 
          s2.cod_categoria, 
          s2.cod_sucursal, 
          s2.RangoPermanencia, 
          s2.sexo, 
          ifnull((SUM(s2.MONTO)),0) AS MONTO, 
          s2.tipo_prestador    
from (
   ------- COMIENZA  CAEC  ----------------------
         SELECT 
                t1.periodo::INT AS PERIODO,
                t1.codcateg AS cod_categoria,
                t1.codsuc AS cod_sucursal,
                t2.RangoPermanencia,
                t2.sexo, 
                (SUM(t1.monto_caec)) AS MONTO,            
                (CASE             
                    WHEN t1.periodo > 0 then 'CAEC'            
                END) AS tipo_prestador
        FROM   "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS" t1
            LEFT JOIN "EST"."P_STG_EST"."P_STG_EST_TEMP_PERMANENCIA" t2 ON (t1.rutafi = t2.rut_titular)
        WHERE t1.periodo BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
          AND t1.monto_caec !=  0
        GROUP BY t1.periodo,
             t1.codcateg,
             t1.codsuc,
             t2.RangoPermanencia,
             t2.SEXO
  ------- TERMINA  CAEC  ----------------------    
    UNION ALL 
  -------------- INICIA SALUD -----------------
    SELECT 
        t1.periodo::INT AS PERIODO,
        t1.codcateg  AS cod_categoria,
        t1.codsuc  AS cod_sucursal,
        t2.RangoPermanencia,
        t2.sexo,  
        SUM(t1.mtobonificado) AS MONTO,
        (CASE 
                WHEN t1.tipoprestacion = 'A20' THEN 'HOSPITALARIO'
                WHEN CONTAINS(t1.tipoprestacion,'A') THEN 'AMBULATORIO'
                WHEN  CONTAINS(t1.tipoprestacion, 'GM') OR CONTAINS (t1.tipoprestacion,'GD') OR CONTAINS(t1.tipoprestacion,'GF') THEN 'GES'
                WHEN CONTAINS(t1.tipoprestacion,'GC') THEN 'CAEC'
                WHEN  CONTAINS ( t1.tipoprestacion,'H') OR CONTAINS(t1.tipoprestacion,'M') THEN 'HOSPITALARIO'
                ELSE 'HOSPITALARIO'
            END) AS tipo_prestador
        FROM "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS"  t1
        LEFT JOIN "EST"."P_STG_EST"."P_STG_EST_TEMP_PERMANENCIA" t2 
                ON (t1.rutafi = t2.rut_titular)  
        WHERE t1.periodo BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1.periodo,
                   t1.codcateg,
                   t1.codsuc,
                   t2.RangoPermanencia,
                   t2.sexo,
                    t1.tipoprestacion

    -------------- TERMINA SALUD -----------------
    UNION ALL  
  -------- INICIA LCC ------------------------
  SELECT 
    S1.PERIODOPAGO AS periodo,
    S1.CODCATEGORIA AS cod_categoria,
    S1.SUCURSALCONTRATO AS cod_sucursal,
    t2.RangoPermanencia, 
    t2.sexo,   
    SUM(s1.valorbonificado) AS MONTO,
    'LCC'  AS tipo_prestador
  
FROM "LCC"."P_DDV_LCC"."P_DDV_LCC_V_LIQ"  s1
LEFT JOIN "EST"."P_STG_EST"."P_STG_EST_TEMP_PERMANENCIA"  t2 
    ON ( s1.rut_titular = t2.rut_titular)    
WHERE S1.PERIODOPAGO BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT AND TIPOLICENCIA IN (1,7) and CODIGOESTADO in (3,4)
GROUP BY S1.PERIODOPAGO ,
S1.CODCATEGORIA,
S1.SUCURSALCONTRATO,
t2.RangoPermanencia,
t2.sexo,  
s1.sucursalpago   
  -------- TERMINA LCC ------------------------ 
    UNION ALL
  ------------ comienza IVA RECUPERADO ------------------
        SELECT 
            t1.periodo,
            t1.codcateg  AS cod_categoria,
            t1.codsuc  AS cod_sucursal,
            t2.RangoPermanencia,
            t2.sexo, 
            (SUM(t1.monto_credito_fiscal)*-1) AS MONTO,
            (case            
            when t1.periodo > 0 then 'IVA_RECUPERADO' end) AS tipo_prestador
        FROM "CON"."P_DDV_CON"."P_DDV_CON_IVA_RECUPERADO_BONOS" t1
            LEFT JOIN "EST"."P_STG_EST"."P_STG_EST_TEMP_PERMANENCIA"  t2 ON (t1.rut = t2.rut_titular)
        WHERE t1.periodo BETWEEN TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1.periodo,
               t1.codcateg,
               t1.codsuc,
               t2.RangoPermanencia,
               t2.sexo
  ------------ TERMINA IVA RECUPERADO ------------------
    UNION ALL
  ------------ comienza RECUPERACION GASTO------------------
         SELECT  t1."periodo_vig" AS periodo,
            t1."cod_categoria" AS cod_categoria,
            t1."cod_sucursal" AS cod_sucusral,
            t2.RangoPermanencia,
             t2.sexo, 
            ((SUM(t1."monto_recuperado"))*-1) AS MONTO,
            (case           
            when t1."periodo_vig" > 0 then 'RECUPERACION GASTO' end) AS tipo_prestador
        FROM "FIN"."P_DDV_FIN"."P_DDV_FIN_RECUPERACION_GASTO_NEW"  t1
            LEFT JOIN "EST"."P_STG_EST"."P_STG_EST_TEMP_PERMANENCIA" t2 ON (t1."rut_titular" = t2.rut_titular)
        WHERE t1."periodo_vig" BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1."periodo_vig",
               t1."cod_categoria",
               t1."cod_sucursal",
               t2.RangoPermanencia,
               t2.sexo
  ------------ TERMINA RECUPERACION GASTO------------------
    UNION ALL 
   ------------ comienza CONTRATO------------------
        SELECT t1.periodo_vigencia AS periodo,
            t1.cod_categoria,
            t1.cod_sucursal,
            t2.RangoPermanencia,
            t2.sexo, 
            (SUM(t1.gasto_pharma_pesos)) AS MONTO,
            (case            
            when t1.periodo_vigencia > 0 then 'PHARMA' end) AS tipo_prestador
        FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t1
            LEFT JOIN "EST"."P_STG_EST"."P_STG_EST_TEMP_PERMANENCIA"  t2 ON (t1.rut_titular = t2.rut_titular)
        WHERE t1.periodo_vigencia BETWEEN   TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT
                                                AND TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1.periodo_vigencia,
               t1.cod_categoria,
               t1.cod_sucursal,
               t2.RangoPermanencia,
               t2.SEXO
    )s2
group by 
               s2.periodo,
               s2.cod_categoria,
               s2.cod_sucursal,
               s2.RangoPermanencia,
               s2.sexo,
               s2.tipo_prestador
------------------------------------------



------- ingresos
-- ----- INICIA INGRESOS--------------
SELECT 
    t1.Periodo, 
    t1.cod_categoria, 
    t2.categoria_gls, 
    t2.tipo_plan AS serie, 
    t3.DETALLE_PRODUCTO AS preferente, 
    t3.TIPO_PLAN AS Tipo_Contrato, 
    t3.TIPO_PRODUCTO AS Tipo_Producto, 
    t1.ubicacion, 
    t1.RangoPermanencia, 
    t1.Sexo, 
    t1.Cotizantes, 
    t1.Cargas, 
    t1.Costo_final, 
    t3.LINEA_PLAN, 
    (SUM(t1.Total)) AS Total_ingreso, 
    (SUM(t1.INGRESO)) AS INGRESO, 
    (SUM(t1.IVA)) AS IVA
FROM 
(
          -- --------- INICIA SUM DE INGRESOS ------------
          SELECT 
          t1.Periodo,
          t1.cod_categoria,
          t1.ubicacion,
          case  when t1.RangoPermanencia = '' then '00 a 12' else t1.RangoPermanencia end
          AS RangoPermanencia,
          t1.SEXO, 
          SUM(t1.INGRESO)+SUM(t1.IVA)  AS Total,
          SUM(t1.Cotizantes) AS Cotizantes,
          SUM(t1.Cargas) AS Cargas,
          SUM(t1.INGRESO)+SUM(t1.IVA) AS Costo_final,
          SUM(t1.IVA) AS IVA,
          SUM(t1.INGRESO) AS INGRESO
          FROM
          (  
                -- ------ INICIA TRANSPOSICION DE INGRESOS--------------------
                SELECT PERIODO, COD_CATEGORIA, costo_final, UBICACION, RANGOPERMANENCIA, COTIZANTES, CARGAS, SEXO,
                ifnull("'INGRESO'",0) AS INGRESO,
                ifnull("'IVA'",0) AS IVA
                FROM "EST"."P_STG_EST"."P_STG_EST_TEMP_MARGEN_BASE_INGRESOS"
                pivot(sum(TOTAL) for TIPO_REGISTRO  in ('INGRESO', 'IVA')
                )  as p    
                -- ------ TERMINA TRANSPOSICION DE INGRESOS--------------------

          )  t1
          GROUP BY t1.Periodo,
          t1.cod_categoria,
          t1.ubicacion,
          t1.RangoPermanencia,
          t1.Sexo         
         -- --------- TERMINA SUM DE INGRESOS ------------
)   t1
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA" t2 ON (t1.cod_categoria = t2.cod_categoria)
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_PRM_AGRUPA_PLANES" t3 -- "IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES"  t3 
ON (t1.cod_categoria = t3."cod_categoria")
GROUP BY t1.Periodo,
t1.cod_categoria,
t2.categoria_gls,
t2.tipo_plan,
t3.DETALLE_PRODUCTO,
t3.TIPO_PLAN,
t3.TIPO_PRODUCTO,
t1.ubicacion,
t1.RangoPermanencia,
t1.Sexo,
t1.Cotizantes,
t1.Cargas,
t1.Costo_final,
t3.LINEA_PLAN ;

mm_permanenciamm_permanenciamm_permanenciamm_permanenciamm_permanenciamm_permanenciamm_permanenciamm_permanenciamm_permanenciamm_permanenciamm_permanenciamm_permanencia rut_titular = '012462377-4'

SET periodo_ini ='2000-01-01'

SET periodo_fin = getdate()::timestamp;

WITH nums(i) AS (
SELECT 
	0
	UNION ALL SELECT i + 1
FROM nums 
WHERE i <= datediff(MONTH, $periodo_ini, $periodo_fin)-1
)
,  periods AS 
(
 SELECT 
 	EST.P_DDV_EST.FECHA_PERIODO(dateadd(month, i::int, $periodo_ini)) AS periodo
 FROM nums
 )
 --SELECT min(periodo),max(periodo) FROM periods LIMIT 10 
, 
minima_vig_rank AS (
SELECT DISTINCT 
"fld_cotrut" , 
"fld_ingreso" ,
"fld_cntcateg",
MIN(FLD_VIGREALDESDE) AS minima_vigencia ,
row_number() over (partition by "fld_cotrut", "fld_ingreso" order by "fld_cotrut", minima_vigencia) AS n_veces 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
"fld_cotrut",
"fld_ingreso",
"fld_cntcateg"
ORDER BY "fld_cotrut" , minima_vigencia 
)
--SELECT * FROM minima_vig_rank
--WHERE "fld_cotrut" = '012073836-4'
, minima_vig AS (
SELECT * FROM minima_vig_rank
WHERE n_veces =1
)
--SELECT * FROM minima_vig 
--WHERE "fld_cotrut" = '012073836-4'
--LIMIT 100 
--WITH
, cartera_vigente AS ( 
SELECT DISTINCT 
"fld_cotrut" ,
FLD_VIGREALDESDE,
FLD_VIGREALHASTA,
"fld_ingreso",
"fld_cntcateg"
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE FLD_VIGREALDESDE <= EST.P_DDV_EST.FECHA_PERIODO($periodo_fin)  AND  FLD_VIGREALHASTA >= EST.P_DDV_EST.FECHA_PERIODO($periodo_ini)
)
, union_bases AS ( 
SELECT 
	a."fld_cotrut",
	c.periodo AS mes,
	b."fld_ingreso",
	b."fld_cntcateg",
	b.minima_vigencia ,
	last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig,
	a.FLD_VIGREALDESDE ,
	a.FLD_VIGREALHASTA 
FROM cartera_vigente  a
LEFT JOIN minima_vig b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
INNER JOIN periods  c ON (c.periodo BETWEEN a.FLD_VIGREALDESDE AND a.FLD_VIGREALHASTA)
)
SELECT 
	"fld_cotrut",
	"fld_ingreso",
	"fld_cntcateg",
	minima_vig ,
	dateadd(MONTH,11,minima_vig) doce_meses,
	EST.P_DDV_EST.PERIODO_FECHA(mes) AS mes,
	EST.P_DDV_EST.FECHA_PERIODO("fld_ingreso") AS PERIODO_INGRESO,
	EST.P_DDV_EST.FECHA_PERIODO(minima_vig) AS PERIODO_MIN_VIG,
	EST.P_DDV_EST.FECHA_PERIODO(doce_meses) AS PERIODO_DOCE_MESES,
	mes AS PERIODO_MES,
	DATEDIFF(month, minima_vig, EST.P_DDV_EST.PERIODO_FECHA(mes))+1 AS resta_meses
	--FLD_VIGREALDESDE ,
	--FLD_VIGREALHASTA 
FROM union_bases
WHERE  "fld_cotrut" = $rut_titular
ORDER BY "fld_cotrut" , mes
LIMIT 100


/* cantidad de ingresos que tiene una persona a la isapre
SELECT 
	"fld_cotrut",
	count(DISTINCT "fld_ingreso" ) AS cant
FROM union_bases
GROUP BY "fld_cotrut"
ORDER BY cant DESC 
LIMIT 1000
*/







------------------------------------------ ingresos
----------------------------------------------------
SET rut_titular = '012462377-4'

SET periodo_ini ='2000-01-01'

SET periodo_fin = getdate()::timestamp;

WITH nums(i) AS (
SELECT 
	0
	UNION ALL SELECT i + 1
FROM nums 
WHERE i <= datediff(MONTH, $periodo_ini, $periodo_fin)-1
)
,  periods AS 
(
 SELECT 
 	EST.P_DDV_EST.FECHA_PERIODO(dateadd(month, i::int, $periodo_ini)) AS periodo
 FROM nums
 )
 --SELECT min(periodo),max(periodo) FROM periods LIMIT 10 
, 
minima_vig_rank AS (
SELECT DISTINCT 
"fld_cotrut" , 
"fld_ingreso" ,
"fld_cntcateg",
MIN(FLD_VIGREALDESDE) AS minima_vigencia ,
row_number() over (partition by "fld_cotrut", "fld_ingreso" order by "fld_cotrut", minima_vigencia) AS n_veces 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
"fld_cotrut",
"fld_ingreso",
"fld_cntcateg"
ORDER BY "fld_cotrut" , minima_vigencia 
)
--SELECT * FROM minima_vig_rank
--WHERE "fld_cotrut" = '012073836-4'
, minima_vig AS (
SELECT * FROM minima_vig_rank
WHERE n_veces =1
)


---------------------------- 
-- permanencia por la cnt
WITH vigencia AS (
SELECT DISTINCT 
"fld_cotrut" , 
"fld_ingreso" ,
MIN(FLD_VIGREALDESDE) AS minima_vigencia 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
"fld_cotrut",
"fld_ingreso"
ORDER BY "fld_cotrut"  
), a_b AS ( 
SELECT DISTINCT 
a."fld_cotrut" , 
b."fld_ingreso",
last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig
FROM AFI.P_RDV_DST_LND_SYB.CNT a
LEFT JOIN vigencia b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
WHERE  201110 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA 
	  --FLD_VIGREALDESDE <= 202103 AND 
	  --FLD_VIGREALHASTA >= 201801
)
, a_b_c AS (
SELECT 
*,
'2022-11-30' AS periodo_resta,
DATEDIFF(month, minima_vig, '2022-11-30')+1 AS resta_meses
FROM a_b
)
SELECT 
	"fld_cotrut" ,
	"fld_ingreso" ,
	minima_vig ,
	dateadd(MONTH,11,minima_vig) doce_meses,
	to_date(periodo_resta) AS mes_resta,
	EST.P_DDV_EST.FECHA_PERIODO("fld_ingreso") AS PERIODO_INGRESO,
	EST.P_DDV_EST.FECHA_PERIODO(minima_vig) AS PERIODO_MIN_VIG,
	EST.P_DDV_EST.FECHA_PERIODO(doce_meses) AS PERIODO_DOCE_MESES,
	EST.P_DDV_EST.FECHA_PERIODO(periodo_resta) AS PERIODO_RESTA,
	resta_meses 
FROM a_b_c
WHERE "fld_cotrut" IN ('013757156-0')
LIMIT 10
      



SELECT * FROM "EST"."P_DDV_EST"."P_DDV_EST_MATRIZ_MARGEN"  LIMIT 10




------ union gasto matriz margen ----------------------------
WITH mm_permanencia AS (
SELECT DISTINCT
    rut_todo AS RUT_TITULAR,
    "CONSEXO" AS SEXO,
    EST.P_DDV_EST.FECHA_PERIODO(FECHA_INGRESO) AS PER_INGRESO,
    PERVIG,
    ANTIGUEDAD,
    (case 
            when t1.antiguedad between 0 and 12 then '00 a 12'
            when t1.antiguedad between 13 and 24 then '13 a 24'
            when t1.antiguedad between 25 and 36 then '25 a 36'
            when t1.antiguedad between 37 and 48 then '37 a 48'
            when t1.antiguedad between 49 and 60 then '49 a 60'
            when t1.antiguedad between 61 and 72 then '61 a 72'
            when t1.antiguedad between 73 and 84 then '73 a 84'
            when t1.antiguedad between 85 and 96 then '85 a 96'
            when t1.antiguedad between 97 and 108 then '97 a 108'
            when t1.antiguedad between 109 and 120 then '109 a 120'
            when t1.antiguedad between 121 and 240 then '121 a 240'
            when t1.antiguedad between 241 and 360 then '241 a 360'
         else '361 y m�s'
         end) AS RangoPermanencia  
FROM (    
     ----------- inicia calculo antiguedad ------------------
      SELECT 
          TODO.max_periodo AS PERVIG,
          TODO.rut_todo,
          A.FECHA_INGRESO,
          IFF(A.FECHA_INGRESO = A.FECHA_INGRESO_ISAPRE AND A.tipo_transaccion in ('VN','CT'), 'S','' ) AS ENCONTRADO,
          IFF(A.FECHA_INGRESO = A.FECHA_INGRESO_ISAPRE AND A.tipo_transaccion in ('VN','CT'), todo.min_periodo,null ) AS perdesde,
          IFF(ENCONTRADO= 'S', datediff(month, TO_DATE(perdesde || '01', 'yyyymmdd'), TO_DATE(PERVIG || '01', 'yyyymmdd'))+1,
          datediff(month, add_months(A.FECHA_INGRESO,2), TO_DATE(PERVIG || '01', 'yyyymmdd') )+1) AS ANTIGUEDAD,
          CON."CONSEXO"
      FROM(
            ------ INICIA TODO --------------    
            SELECT 
                max(periodo_vigencia) AS max_periodo,
                min(periodo_vigencia) AS min_periodo,
                rut_titular as rut_todo
            FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO"
            GROUP BY rut_titular 
            ------ TERMINA TODO --------------    
           ) TODO
      LEFT OUTER JOIN "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" A
            ON TODO.max_periodo = A.PERIODO_VIGENCIA AND TODO.rut_todo = A.RUT_TITULAR
      LEFT OUTER JOIN 
          (select "CONRUTBEN", MIN("CONSEXO") AS consexo from  "AFI"."P_DDV_AFI"."P_DDV_AFI_CON"
          GROUP BY  "CONRUTBEN") CON) t1
)
,  gasto AS 
(
Select    s2.periodo::INT AS PERIODO,
		  s2.rut_titular,
          s2.cod_categoria, 
          s2.cod_sucursal, 
          s2.RangoPermanencia, 
          s2.sexo, 
          ifnull((SUM(s2.MONTO)),0) AS MONTO, 
          s2.tipo_prestador    
from (
   ------- COMIENZA  CAEC  ----------------------
         SELECT 
                t1.periodo::INT AS PERIODO,
                t2.rut_titular,
                t1.codcateg AS cod_categoria,
                t1.codsuc AS cod_sucursal,
                t2.RangoPermanencia,
                t2.sexo, 
                (SUM(t1.monto_caec)) AS MONTO,            
                (CASE             
                    WHEN t1.periodo > 0 then 'CAEC'            
                END) AS tipo_prestador
        FROM   "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS" t1
            LEFT JOIN mm_permanencia t2 ON (t1.rutafi = t2.rut_titular)
        WHERE t1.periodo BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
          AND t1.monto_caec !=  0
        GROUP BY t1.periodo,
         		 t2.rut_titular,
	             t1.codcateg,
	             t1.codsuc,
	             t2.RangoPermanencia,
	             t2.SEXO
  ------- TERMINA  CAEC  ----------------------    
    UNION ALL 
  -------------- INICIA SALUD -----------------
    SELECT 
        t1.periodo::INT AS PERIODO,
        t2.rut_titular,
        t1.codcateg  AS cod_categoria,
        t1.codsuc  AS cod_sucursal,
        t2.RangoPermanencia,
        t2.sexo,  
        SUM(t1.mtobonificado) AS MONTO,
        (CASE 
                WHEN t1.tipoprestacion = 'A20' THEN 'HOSPITALARIO'
                WHEN CONTAINS(t1.tipoprestacion,'A') THEN 'AMBULATORIO'
                WHEN  CONTAINS(t1.tipoprestacion, 'GM') OR CONTAINS (t1.tipoprestacion,'GD') OR CONTAINS(t1.tipoprestacion,'GF') THEN 'GES'
                WHEN CONTAINS(t1.tipoprestacion,'GC') THEN 'CAEC'
                WHEN  CONTAINS ( t1.tipoprestacion,'H') OR CONTAINS(t1.tipoprestacion,'M') THEN 'HOSPITALARIO'
                ELSE 'HOSPITALARIO'
            END) AS tipo_prestador
        FROM "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS"  t1
        LEFT JOIN mm_permanencia t2 
                ON (t1.rutafi = t2.rut_titular)  
        WHERE t1.periodo BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1.periodo,
        		   t2.rut_titular,
                   t1.codcateg,
                   t1.codsuc,
                   t2.RangoPermanencia,
                   t2.sexo,
                    t1.tipoprestacion
    -------------- TERMINA SALUD -----------------
    UNION ALL  
  -------- INICIA LCC ------------------------
  SELECT 
    S1.PERIODOPAGO AS periodo,
    t2.rut_titular,
    S1.CODCATEGORIA AS cod_categoria,
    S1.SUCURSALCONTRATO AS cod_sucursal,
    t2.RangoPermanencia, 
    t2.sexo,   
    SUM(s1.valorbonificado) AS MONTO,
    'LCC'  AS tipo_prestador
FROM "LCC"."P_DDV_LCC"."P_DDV_LCC_V_LIQ"  s1
LEFT JOIN mm_permanencia  t2 
    ON ( s1.rut_titular = t2.rut_titular)    
WHERE S1.PERIODOPAGO BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                         and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT AND TIPOLICENCIA IN (1,7) and CODIGOESTADO in (3,4)
GROUP BY 	S1.PERIODOPAGO ,
			t2.rut_titular,
			S1.CODCATEGORIA,
			S1.SUCURSALCONTRATO,
			t2.RangoPermanencia,
			t2.sexo,  
			s1.sucursalpago  
  -------- TERMINA LCC ------------------------ 
    UNION ALL
  ------------ comienza IVA RECUPERADO ------------------
        SELECT 
            t1.periodo,
            t2.rut_titular,
            t1.codcateg  AS cod_categoria,
            t1.codsuc  AS cod_sucursal,
            t2.RangoPermanencia,
            t2.sexo, 
            (SUM(t1.monto_credito_fiscal)*-1) AS MONTO,
            (case            
            when t1.periodo > 0 then 'IVA_RECUPERADO' end) AS tipo_prestador
        FROM "CON"."P_DDV_CON"."P_DDV_CON_IVA_RECUPERADO_BONOS" t1
            LEFT JOIN mm_permanencia  t2 ON (t1.rut = t2.rut_titular)
        WHERE t1.periodo BETWEEN TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1.periodo,
        	    t2.rut_titular,
               	t1.codcateg,
               	t1.codsuc,
               	t2.RangoPermanencia,
               	t2.sexo
  ------------ TERMINA IVA RECUPERADO ------------------
    UNION ALL
  ------------ comienza RECUPERACION GASTO------------------  
         SELECT  t1."periodo_vig" AS periodo,
         		 t2.rut_titular,
	             t1."cod_categoria" AS cod_categoria,
	             t1."cod_sucursal" AS cod_sucusral,
	             t2.RangoPermanencia,
	             t2.sexo, 
	             ((SUM(t1."monto_recuperado"))*-1) AS MONTO,
	             (case           
	               when t1."periodo_vig" > 0 then 'RECUPERACION GASTO' end) 
	             AS tipo_prestador
        FROM "FIN"."P_DDV_FIN"."P_DDV_FIN_RECUPERACION_GASTO_NEW"  t1
            LEFT JOIN mm_permanencia t2 ON (t1."rut_titular" = t2.rut_titular)
        WHERE t1."periodo_vig" BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1."periodo_vig",
        		 t2.rut_titular,
               	 t1."cod_categoria",
               	 t1."cod_sucursal",
               	 t2.RangoPermanencia,
               	 t2.sexo  
  ------------ TERMINA RECUPERACION GASTO------------------
    UNION ALL 
   ------------ comienza CONTRATO------------------
        SELECT t1.periodo_vigencia AS periodo,
        	   t2.rut_titular,
               t1.cod_categoria,
               t1.cod_sucursal,
               t2.RangoPermanencia,
               t2.sexo, 
               (SUM(t1.gasto_pharma_pesos)) AS MONTO,
               (case            
                   when t1.periodo_vigencia > 0 then 'PHARMA' end) 
               AS tipo_prestador
        FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t1
            LEFT JOIN mm_permanencia  t2 ON (t1.rut_titular = t2.rut_titular)
        WHERE t1.periodo_vigencia BETWEEN   TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT
                                        AND TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1.periodo_vigencia,
        		 t2.rut_titular,
                 t1.cod_categoria,
                 t1.cod_sucursal,
                 t2.RangoPermanencia,
                 t2.SEXO
    ) s2
group by 
               s2.periodo,
               s2.rut_titular,
               s2.cod_categoria,
               s2.cod_sucursal,
               s2.RangoPermanencia,
               s2.sexo,
               s2.tipo_prestador
)
, ingresos AS ( 
SELECT 
    t1.Periodo, 
    t1.cod_categoria, 
    t2.categoria_gls, 
    t2.tipo_plan AS serie, 
    t3.DETALLE_PRODUCTO AS preferente, 
    t3.TIPO_PLAN AS Tipo_Contrato, 
    t3.TIPO_PRODUCTO AS Tipo_Producto, 
    t1.ubicacion, 
    t1.RangoPermanencia, 
    t1.Sexo, 
    t1.Cotizantes, 
    t1.Cargas, 
    t1.Costo_final, 
    t3.LINEA_PLAN, 
    (SUM(t1.Total)) AS Total_ingreso, 
    (SUM(t1.INGRESO)) AS INGRESO, 
    (SUM(t1.IVA)) AS IVA
FROM 
(
          -- --------- INICIA SUM DE INGRESOS ------------
          SELECT 
          t1.Periodo,
          t1.cod_categoria,
          t1.ubicacion,
          case  when t1.RangoPermanencia = '' then '00 a 12' else t1.RangoPermanencia end
          AS RangoPermanencia,
          t1.SEXO, 
          SUM(t1.INGRESO)+SUM(t1.IVA)  AS Total,
          SUM(t1.Cotizantes) AS Cotizantes,
          SUM(t1.Cargas) AS Cargas,
          SUM(t1.INGRESO)+SUM(t1.IVA) AS Costo_final,
          SUM(t1.IVA) AS IVA,
          SUM(t1.INGRESO) AS INGRESO
          FROM
          (  
                -- ------ INICIA TRANSPOSICION DE INGRESOS--------------------
                SELECT PERIODO, COD_CATEGORIA, costo_final, UBICACION, RANGOPERMANENCIA, COTIZANTES, CARGAS, SEXO,
                ifnull("'INGRESO'",0) AS INGRESO,
                ifnull("'IVA'",0) AS IVA
                FROM "EST"."P_STG_EST"."P_STG_EST_TEMP_MARGEN_BASE_INGRESOS"
                pivot(sum(TOTAL) for TIPO_REGISTRO  in ('INGRESO', 'IVA')
                )  as p    
                -- ------ TERMINA TRANSPOSICION DE INGRESOS--------------------

          )  t1
          GROUP BY t1.Periodo,
          t1.cod_categoria,
          t1.ubicacion,
          t1.RangoPermanencia,
          t1.Sexo         
         -- --------- TERMINA SUM DE INGRESOS ------------
)   t1
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA" t2 ON (t1.cod_categoria = t2.cod_categoria)
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_PRM_AGRUPA_PLANES" t3 -- "IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES"  t3 
ON (t1.cod_categoria = t3."cod_categoria")
GROUP BY 	t1.Periodo,
			t1.cod_categoria,
			t2.categoria_gls,
			t2.tipo_plan,
			t3.DETALLE_PRODUCTO,
			t3.TIPO_PLAN,
			t3.TIPO_PRODUCTO,
			t1.ubicacion,
			t1.RangoPermanencia,
			t1.Sexo,
			t1.Cotizantes,
			t1.Cargas,
			t1.Costo_final,
			t3.LINEA_PLAN 
)
SELECT 
	* 
	FROM gasto
	WHERE rut_titular = '013952270-2'
	ORDER BY periodo
	--LIMIT 10


;
               



