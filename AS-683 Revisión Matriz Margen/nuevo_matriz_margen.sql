SELECT * FROM isa.P_RDV_DST_LND_SYB.EMP  LIMIT 10

SELECT * FROM AFI.P_RDV_DST_LND_SYB.CNT  LIMIT 10


SELECT * FROM EST.P_DDV_EST.CA_PERMANENCIA_CARTERA 
WHERE "fld_cotrut" ='012073836-4'
--LIMIT 



SELECT 
	PERIODO_PROCESAMIENTO ,
	PERIODO ,
	sum(GASTO_HOSPITALARIO),
	sum(GASTO_AMBULATORIO),
	sum(GASTO_GES),
	sum(GASTO_CAEC),
	sum(GASTO_LCC),
	SUM(GASTO_TOTAL),
	sum(TOTAL_INGRESO)
FROM EST.P_DDV_EST.P_DDV_EST_MATRIZ_MARGEN
WHERE PERIODO_PROCESAMIENTO = 202204 AND PERIODO = 202204
GROUP BY PERIODO_PROCESAMIENTO , PERIODO
ORDER BY PERIODO DESC 
LIMIT 100


SELECT 
    sum(COTIZANTES) AS cant_titu,
	sum(total_ingreso),
	sum(costo_final),
	sum(gasto_total),
	sum(gasto_hospitalario),
	sum(gasto_ambulatorio),
	sum(gasto_caec),
	sum(gasto_ges),
	sum(gasto_pharma),
	sum(gasto_lcc),
	sum(RECUPERACION_GASTO),
	sum(IVA),
	sum(IVA_RECUPERADO)
FROM  EST.P_DDV_EST.P_DDV_EST_MATRIZ_MARGEN 
WHERE PERIODO_PROCESAMIENTO = 202204 AND PERIODO = 202204	
LIMIT 10 





----- matriz margon con datamart
SELECT 
	COUNT(DISTINCT rut_titular ) AS cant_titu,
	sum(total_ingreso),
	sum(costo_final),
	sum(gasto_total),
	sum(gasto_hospitalario),
	sum(gasto_ambulatorio),
	sum(gasto_caec),
	sum(gasto_ges),
	sum(gasto_pharma),
	sum(gasto_lcc),
	sum(IVA),
	sum(IVA_RECUPERADO)
FROM EST.P_DDV_EST.CA_MATRIZ_MARGEN_NUEVO  
WHERE periodo =202204 --AND rut_titular ='015308454-8'
--ORDER BY rut_titular, periodo
LIMIT 1000





SELECT 
	COUNT(DISTINCT rut_titular ) AS cant_titu,
	sum(total_ingreso),
	sum(costo_final),
	sum(gasto_total),
	sum(gasto_hospitalario),
	sum(gasto_ambulatorio),
	sum(gasto_caec),
	sum(gasto_ges),
	sum(gasto_pharma),
	sum(gasto_lcc)	
FROM EST.P_DDV_EST.CA_TEST_MATRIZ_MARGEN  
WHERE periodo =202204 --AND rut_titular ='015308454-8'
--ORDER BY rut_titular, periodo
LIMIT 1000




SELECT DISTINCT PERIODO FROM EST.P_DDV_EST.P_DDV_EST_MATRIZ_MARGEN 
WHERE PERIODO_PROCESAMIENTO = 202204
ORDER BY PERIODO desc
--LIMIT 10


SELECT * FROM EST.P_DDV_EST.CA_TEST_MATRIZ_MARGEN LIMIT 10



SELECT * FROM EST.P_DDV_EST.CA_TEST_MATRIZ_MARGEN_02 LIMIT 10



SELECT 
	* 
FROM EST.P_DDV_EST.CA_MATRIZ_MARGEN_NUEVO  
WHERE rut_titular ='012073836-4'
ORDER BY PERIODO 
LIMIT 10000



SELECT * FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE rut_titular ='015308454-8' AND PERIODO_VIGENCIA = 202011
LIMIT 10


SELECT * FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE RUT_TITULAR ='015308454-8' AND PERIODO = 202011
LIMIT 10


SELECT * FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS LIMIT 10	


SELECT * FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO LIMIT 10	



SET periodo_ini ='2015-01-01'


SET periodo_fin = getdate()::timestamp;



----- cambiando los gastos por datamart 
-----------------------------------------

CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_MATRIZ_MARGEN_NUEVO AS ( 
WITH nums(i) AS (
SELECT 
	0
	UNION ALL SELECT i + 1
FROM nums 
WHERE i <= datediff(MONTH, $periodo_ini, $periodo_fin)-1
)
,  periods AS 
(
 SELECT 
 	EST.P_DDV_EST.FECHA_PERIODO(dateadd(month, i::int, $periodo_ini)) AS periodo
 FROM nums
 )
 --SELECT min(periodo),max(periodo) FROM periods LIMIT 10 
, 
minima_vig_rank AS (
SELECT DISTINCT 
cnt."fld_cotrut" , 
cnt."fld_ingreso" ,
cnt."fld_cntcateg",
CASE 
	WHEN ben."fld_bensexo" = 1 THEN 'M'
	ELSE 'F'
END AS sexo,
MIN(FLD_VIGREALDESDE) AS minima_vigencia ,
row_number() over (partition by cnt."fld_cotrut", cnt."fld_ingreso" order by cnt."fld_cotrut", minima_vigencia) AS n_veces 
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio"=ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel" AND ben."fld_bencorrel"=0)
GROUP BY 
cnt."fld_cotrut",
cnt."fld_ingreso",
cnt."fld_cntcateg",
sexo
ORDER BY "fld_cotrut" , minima_vigencia 
)
--SELECT * FROM minima_vig_rank
--WHERE "fld_cotrut" = '012073836-4'
, 
minima_vig AS (
SELECT 
	* 
FROM minima_vig_rank
WHERE n_veces =1
)
--SELECT * FROM minima_vig 
--WHERE "fld_cotrut" = '012073836-4'
--LIMIT 100 
--WITH
, 
cartera_vigente AS ( 
SELECT DISTINCT 
	"fld_cotrut" ,
	FLD_VIGREALDESDE,
	FLD_VIGREALHASTA,
	"fld_ingreso",
	"fld_cntcateg"
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE FLD_VIGREALDESDE <= EST.P_DDV_EST.FECHA_PERIODO($periodo_fin)  AND  FLD_VIGREALHASTA >= EST.P_DDV_EST.FECHA_PERIODO($periodo_ini)
)
--SELECT DISTINCT FROM cartera_vigente LIMIT 10
, 
antiguedad AS ( 
SELECT 
	a."fld_cotrut",
	c.periodo AS mes,
	b."fld_ingreso",
	b."fld_cntcateg",
	b.minima_vigencia ,
	b.sexo,
	last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig,
	a.FLD_VIGREALDESDE ,
	a.FLD_VIGREALHASTA 
FROM cartera_vigente  a
LEFT JOIN minima_vig b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
INNER JOIN periods  c ON (c.periodo BETWEEN a.FLD_VIGREALDESDE AND a.FLD_VIGREALHASTA)
)
--SELECT * FROM permanencia WHERE "fld_cotrut"= '012073836-4'
, mm_antiguedad AS ( 
SELECT 
	"fld_cotrut",
	sexo,
	"fld_ingreso",
	--"fld_cntcateg",
	minima_vig ,
	--dateadd(MONTH,11,minima_vig) doce_meses,
	EST.P_DDV_EST.PERIODO_FECHA(mes) AS mes,
	--EST.P_DDV_EST.FECHA_PERIODO("fld_ingreso") AS PERIODO_INGRESO,
	EST.P_DDV_EST.FECHA_PERIODO(minima_vig) AS PERIODO_MIN_VIG,
	--EST.P_DDV_EST.FECHA_PERIODO(doce_meses) AS PERIODO_DOCE_MESES,
	mes AS PERIODO_MES,
	DATEDIFF(month, minima_vig, EST.P_DDV_EST.PERIODO_FECHA(mes))+1 AS antiguedad,
	(case 
            when antiguedad between 0 and 12 then '00 a 12'
            when antiguedad between 13 and 24 then '13 a 24'
            when antiguedad between 25 and 36 then '25 a 36'
            when antiguedad between 37 and 48 then '37 a 48'
            when antiguedad between 49 and 60 then '49 a 60'
            when antiguedad between 61 and 72 then '61 a 72'
            when antiguedad between 73 and 84 then '73 a 84'
            when antiguedad between 85 and 96 then '85 a 96'
            when antiguedad between 97 and 108 then '97 a 108'
            when antiguedad between 109 and 120 then '109 a 120'
            when antiguedad between 121 and 240 then '121 a 240'
            when antiguedad between 241 and 360 then '241 a 360'
         else '361 y m�s'   
     end) AS RangoAntiguedad 
	--FLD_VIGREALDESDE ,
	--FLD_VIGREALHASTA 
FROM antiguedad
--WHERE  "fld_cotrut" = '010108691-7'
ORDER BY "fld_cotrut" , mes
--LIMIT 50
)
--SELECT * FROM mm_permanencia WHERE "fld_cotrut"='012073836-4' 
,
mm_base_gasto AS ( 
Select    s2.periodo::INT AS PERIODO,
		  S2.rut_titular,
		  s2."fld_ingreso" AS fecha_ingreso,
		  s2.periodo_min_vig,
          s2.cod_categoria, 
          s2.cod_sucursal, 
          s2.antiguedad,
          s2.RangoAntiguedad , 
          s2.sexo, 
          s2.tipo_prestacion ,
          ifnull((sum(s2.cobrado)),0) AS cobrado,
          ifnull((SUM(s2.bonificado)),0) AS bonificado
from (
   ------- COMIENZA  CAEC  ----------------------
        SELECT 
			t1.PERIODO AS periodo,
			t2."fld_ingreso",
			t2.periodo_min_vig,
			t1.rut_titular,
			--t2."fld_cotrut",
			t1.COD_CATEGORIA,
			IFNULL(t1.SUCURSAL_EMISION,t1.SUCURSAL_RECEPCION) AS cod_sucursal,
			t2.antiguedad,
			t2.RangoAntiguedad,
			t2.Sexo,
			'CAEC' AS tipo_prestacion,
			sum(cobrado) AS cobrado,
			sum(monto_caec) AS bonificado
		FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO t1
		LEFT JOIN mm_antiguedad t2 ON (t1.RUT_TITULAR = t2."fld_cotrut" AND t1.PERIODO = t2.periodo_mes)
		WHERE PERIODO  BETWEEN est.P_DDV_EST.fecha_periodo($periodo_ini) AND est.P_DDV_EST.fecha_periodo($periodo_fin) --AND t1.RUT_TITULAR IS NOT NULL
         -- AND t1.monto_caec !=  0
        GROUP BY t1.periodo,
        		 T2."fld_ingreso",
        		 T2.periodo_min_vig,
        		 t1.rut_titular,
	             t1.cod_categoria,
	             cod_sucursal,
	             t2.antiguedad,
	             t2.RangoAntiguedad ,
	             t2.SEXO
	     ------- TERMINA  CAEC  ----------------------    
	    UNION ALL 
	  -------------- INICIA SALUD -----------------
		SELECT
			t1.PERIODO AS periodo,
			t2."fld_ingreso",
			t2.periodo_min_vig,
			t1.RUT_TITULAR,
			--t2."fld_cotrut",
			t1.COD_CATEGORIA,
			IFNULL(t1.SUCURSAL_EMISION,t1.SUCURSAL_RECEPCION) AS cod_sucursal,
			t2.antiguedad,
			t2.RangoAntiguedad,
			t2.Sexo,
			t1.origen AS tipo_prestacion,
			sum(COBRADO) AS cobrado,
			sum(BONIFICADO) AS bonificado
		FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO t1
		LEFT JOIN mm_antiguedad t2 ON (t1.RUT_TITULAR = t2."fld_cotrut" AND t1.PERIODO = t2.periodo_mes)
		WHERE PERIODO  BETWEEN est.P_DDV_EST.fecha_periodo($periodo_ini) AND est.P_DDV_EST.fecha_periodo($periodo_fin) --AND t1.RUT_TITULAR IS NOT NULL
					   AND origen in('HOSPITALARIO','AMBULATORIO','GES')
		GROUP BY 
			periodo,
			t1.RUT_TITULAR,
			t2."fld_ingreso",
			t2.periodo_min_vig,
			--t2."fld_cotrut",
			t1.cod_categoria,
			cod_sucursal,
			t2.antiguedad,
			t2.RangoAntiguedad ,
			t2.Sexo,
			tipo_prestacion   
	    -------------- TERMINA SALUD -----------------
	    UNION ALL  
		-------- INICIA LCC ------------------------
		SELECT
			t1.PERIODO AS periodo,
			t2."fld_ingreso",
			t2.periodo_min_vig,
			t1.RUT_TITULAR AS rut_titular,
			--t2."fld_cotrut",
			t1.COD_CATEGORIA,
			IFNULL(t1.SUCURSAL_EMISION,t1.SUCURSAL_RECEPCION) AS cod_sucursal,
			t2.antiguedad,
			t2.RangoAntiguedad,
			t2.Sexo,
			'LCC' AS tipo_prestacion,
			sum(COBRADO) AS cobrado,
			sum(BONIFICADO) AS bonificado
		FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO t1
		LEFT JOIN mm_antiguedad t2 ON (t1.RUT_TITULAR = t2."fld_cotrut" AND t1.PERIODO = t2.periodo_mes)
		WHERE PERIODO  BETWEEN est.P_DDV_EST.fecha_periodo($periodo_ini) AND est.P_DDV_EST.fecha_periodo($periodo_fin) --AND t1.RUT_TITULAR IS NOT NULL
					   AND origen = 'LICENCIA' AND tipo_licencia IN (1,7)
		GROUP BY 
			periodo,
			t2."fld_ingreso",
			t2.periodo_min_vig,
			t1.RUT_TITULAR ,
			--t2."fld_cotrut",
			t1.cod_categoria,
			cod_sucursal,
			t2.antiguedad,
			t2.RangoAntiguedad,
			Sexo ,
			tipo_prestacion	
		------- termina lcc ---------
    	UNION ALL
  		------- comienza IVA RECUPERADO ------------------
		SELECT 
            t1.periodo,
            t2."fld_ingreso",
            t2.periodo_min_vig,
            t1.RUT AS rut_titular,
            t1.codcateg  AS cod_categoria,
            t1.codsuc AS cod_sucursal,
            t2.antiguedad,
            t2.RangoAntiguedad,
            t2.sexo,
            (case            
            	when t1.periodo > 0 then 'IVA_RECUPERADO' 
             end)  AS tipo_prestacion,
             sum(0) AS cobrado,
            (SUM(t1.monto_credito_fiscal)*-1) AS bonificado
        FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS t1
            LEFT JOIN mm_antiguedad t2 ON (t1.RUT = t2."fld_cotrut" AND t1.periodo = t2.periodo_mes)
        WHERE t1.periodo BETWEEN est.P_DDV_EST.fecha_periodo($periodo_ini) AND est.P_DDV_EST.fecha_periodo($periodo_fin) 
        GROUP BY 
        	   t1.periodo,
        	   t2."fld_ingreso",
        	   t2.periodo_min_vig,
        	   rut_titular,
               t1.codcateg,
               t1.codsuc,
               t2.antiguedad,
               t2.RangoAntiguedad,
               t2.sexo,
               tipo_prestacion
  		------ TERMINA IVA RECUPERADO ------------------
        UNION ALL
  		------- comienza RECUPERACION GASTO------------------
        SELECT  
        	t1."periodo_vig" AS periodo,
        	t2."fld_ingreso",
        	t2.periodo_min_vig,
        	t1."rut_titular" AS rut_titular,
            t1."cod_categoria" AS cod_categoria,
            t1."cod_sucursal" AS cod_sucursal,
            t2.antiguedad,
            t2.RangoAntiguedad,
            t2.sexo, 
            (case           
            	when t1."periodo_vig" > 0 then 'RECUPERACION GASTO' 
             end) AS tipo_prestacion,
             sum(0) AS cobrado,
              ((SUM(t1."monto_recuperado"))*-1) AS bonificado
        FROM FIN.P_DDV_FIN.P_DDV_FIN_RECUPERACION_GASTO_NEW  t1
            LEFT JOIN mm_antiguedad t2 ON (t1."rut_titular" = t2."fld_cotrut" AND t1."periodo_vig" = t2.periodo_mes)
        WHERE t1."periodo_vig" BETWEEN  est.P_DDV_EST.fecha_periodo($periodo_ini) AND est.P_DDV_EST.fecha_periodo($periodo_fin) 
        GROUP BY 
        	   t1."periodo_vig",
        	   t2."fld_ingreso",
        	   t2.periodo_min_vig,
        	   rut_titular,
               t1."cod_categoria",
               t1."cod_sucursal",
               t2.antiguedad,
               t2.RangoAntiguedad,
               t2.sexo ,
               tipo_prestacion
  	  	--------- TERMINA RECUPERACION GASTO------------------
      	UNION ALL 
   	  	--------- comienza gasto pharma ------------------
      	SELECT 
        	t1.periodo_vigencia AS periodo,
        	t2."fld_ingreso",
        	t2.periodo_min_vig,
        	t1.rut_titular ,
            t1.cod_categoria,
            t1.cod_sucursal,
            t2.antiguedad,
            t2.RangoAntiguedad,
            t2.sexo, 
            (case            
            	when t1.periodo_vigencia > 0 then 'PHARMA' 
            end) AS tipo_prestacion,
            sum(0) AS cobrado,
            (SUM(t1.gasto_pharma_pesos)) AS bonificado
        FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO t1
            LEFT JOIN mm_antiguedad t2 ON (t1.rut_titular = t2."fld_cotrut" AND t1.periodo_vigencia = t2.periodo_mes)
        WHERE t1.periodo_vigencia BETWEEN  est.P_DDV_EST.fecha_periodo($periodo_ini) AND est.P_DDV_EST.fecha_periodo($periodo_fin) 
        GROUP BY 
        	   t1.periodo_vigencia,
        	   t2."fld_ingreso",
        	   t2.periodo_min_vig,
        	   t1.rut_titular,
               t1.cod_categoria,
               t1.cod_sucursal,
               t2.antiguedad,
               t2.RangoAntiguedad,
               t2.SEXO,
               tipo_prestacion     	
    ) s2
group by 
               s2.periodo,
               fecha_ingreso,
               s2.periodo_min_vig,
               s2.rut_titular,
               s2.cod_categoria,
               s2.cod_sucursal,
               s2.antiguedad,
               s2.RangoAntiguedad,
               s2.sexo,
               s2.tipo_prestacion
)
--SELECT * FROM mm_base_gasto WHERE rut_titular = '012073836-4' ORDER BY periodo ASC  --LIMIT 20
, 
mm_margen_gasto AS ( 
SELECT 
  t1.periodo AS periodo,
  t1.fecha_ingreso,
  t1.periodo_min_vig,
  t1.rut_titular,
  t1.cod_categoria, 
  t3.categoria_gls, 
  t1.antiguedad,
  (case
  	when t1.RangoAntiguedad = '' then '00 a 12' else t1.RangoAntiguedad end) AS RangoAntiguedad, 
  t1.ubicacion, 
  t1.Sexo, 
  t3.tipo_plan AS serie, 
  t2.DETALLE_PRODUCTO  AS preferente, 
  t2.TIPO_PLAN AS Tipo_Contrato, 
  t2.TIPO_PRODUCTO AS Tipo_Producto, 
  t2.LINEA_PLAN, 
  t1.Gasto,
  t1.cobrado,
  t1.HOSPITALARIO, 
  t1.AMBULATORIO, 
  t1.GES, 
  t1.CAEC, 
  t1.PHARMA, 
  t1.IVA_RECUPERADO, 
  t1.LCC, 
  t1.RECUPERACION_GASTO
FROM (
---------------  inicia GASTOS SUCURSAL-------------------
      SELECT 
        s1.periodo,
        s1.fecha_ingreso,
        s1.periodo_min_vig,
        s1.rut_titular,
        s1.cod_categoria,
        s1.antiguedad,
        s1.RangoAntiguedad,
        t2."UBICACION" as ubicacion,
        s1.sexo,
        s1.cobrado,
       (IFNULL(s1.PHARMA,0) + IFNULL(s1.GES ,0) + IFNULL(s1.IVA_RECUPERADO,0)
        + IFNULL(s1.AMBULATORIO,0) + IFNULL(s1.HOSPITALARIO,0) + IFNULL(s1.LCC,0)
        + IFNULL(s1.CAEC,0) + IFNULL(s1.RECUPERACION_GASTO,0)) AS Gasto,
        IFNULL(s1.HOSPITALARIO,0) AS HOSPITALARIO,
        IFNULL(s1.AMBULATORIO,0) AS AMBULATORIO,
        IFNULL(s1.GES ,0)  AS GES,
        IFNULL(s1.CAEC,0)  AS CAEC,
        IFNULL(s1.PHARMA,0)   AS PHARMA,
        IFNULL(s1.IVA_RECUPERADO,0)  AS IVA_RECUPERADO,
        IFNULL(s1.LCC ,0)  AS LCC,
        IFNULL(s1.RECUPERACION_GASTO,0) AS RECUPERACION_GASTO
      FROM
      ( 
      ----- INICIA TRANSPOSICION DE GASTOS-----------------------
          SELECT  PERIODO, 
          		  fecha_ingreso, 
          		  periodo_min_vig,
          		  RUT_TITULAR,
          		  COD_CATEGORIA, 
          		  COD_SUCURSAL, 
          		  antiguedad,
          		  RangoAntiguedad, 
          		  sexo,
          		  cobrado,
		          ifnull("'IVA_RECUPERADO'",0) AS IVA_RECUPERADO,
		          ifnull("'RECUPERACION GASTO'",0) AS RECUPERACION_GASTO,
		          ifnull("'PHARMA'",0) AS PHARMA,
		          ifnull("'GES'",0) AS GES,
		          ifnull("'AMBULATORIO'",0) AS AMBULATORIO,
		          ifnull("'CAEC'",0) AS CAEC,
		          ifnull("'LCC'",0) AS LCC,
		          ifnull("'HOSPITALARIO'",0) AS HOSPITALARIO
          FROM  mm_base_gasto
          pivot(sum(bonificado) for tipo_prestacion  in ('IVA_RECUPERADO', 'RECUPERACION GASTO', 'PHARMA', 'GES', 'AMBULATORIO', 'CAEC','LCC', 'HOSPITALARIO')) AS p
          --pivot(sum(cobrado) for   in ('IVA_RECUPERADO', 'RECUPERACION GASTO', 'PHARMA', 'GES', 'AMBULATORIO', 'CAEC','LCC', 'HOSPITALARIO')  as p2
          ----- TERMINA TRANSPOSICION DE GASTOS-----------------------
      )  s1
      LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_SUCURSAL2 t2  ON(s1.cod_sucursal = t2."COD_SUCURSAL")
----- termina GASTOS SUCURSAL 
)
t1
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_PRM_AGRUPA_PLANES t2     ON (t1.cod_categoria = t2."cod_categoria")
LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_CATEGORIA t3 		 ON (t1.cod_categoria = t3.cod_categoria)
)
--------- REVISION DE CASOS-----------------------
/*
SELECT 
	* 
FROM mm_margen_gasto
WHERE RUT_TITULAR ='012073836-4' --AND PERIODO = 202011  --
ORDER BY PERIODO 
LIMIT 100
*/
-------- FIN REVISION 
-- GASTOS  OK ---------------------------------------
,
---- COMIENZA INGRESOS
mm_base_ingreso AS ( 
   ----- INICIA INGRESOS ---------------------
SELECT 
	t1.Periodo::INT AS PERIODO,
	t1.fecha_ingreso,
	t1.periodo_min_vig,
	t1.rut_titular,
    t1.cod_categoria,
    ((SUM(t1.pagado_total_pesos))-(SUM(t1.monto_exceso))-(SUM(t1.monto_excedente))) AS Total,
    t2."UBICACION" as ubicacion,
    t1.antiguedad,
    t1.RangoAntiguedad,
    t1.Sexo, 
    (SUM(t1.Cotizantes)) AS Cotizantes,
    (SUM(t1.Cargas)) AS Cargas,
    IFNULL((SUM(t1.uf_costo_final)),0) AS costo_final,
    case when t1.Periodo >0 then 'INGRESO'
    end AS tipo_registro
FROM     (
          ----- INICIA CONTRATO -------------
         SELECT 
	            t1.periodo_vigencia AS Periodo,
	            t2."fld_ingreso" AS fecha_ingreso,
	            t2.periodo_min_vig,
	          	t1.rut_titular,
	            t1.cod_categoria,
	            t1.cod_sucursal,
	            ((SUM(t1.pagado_total_pesos))-(SUM(t1.monto_exceso))-(SUM(t1.monto_excedente))) AS Total,
	            (SUM(t1.pagado_total_pesos)) AS pagado_total_pesos,
	            (SUM(t1.monto_exceso))  AS monto_exceso,
	            (SUM(t1.monto_excedente)) AS monto_excedente,
	            (COUNT(DISTINCT(t1.rut_titular))) AS Cotizantes,
	            (SUM(t1.cnt_carfam))  AS Cargas,
	            t2.antiguedad,
	            t2.RangoAntiguedad,
	            t2.sexo, 
	            (SUM(t1.costo_final_uf))  AS uf_costo_final
         FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO t1
            LEFT OUTER JOIN mm_antiguedad  t2  ON (t1.rut_titular = t2."fld_cotrut" AND t1.periodo_vigencia = periodo_mes)
         WHERE  t1.periodo_vigencia BETWEEN  est.P_DDV_EST.fecha_periodo($periodo_ini) AND est.P_DDV_EST.fecha_periodo($periodo_fin) 
         GROUP BY   
         		t1.periodo_vigencia,
         		t2."fld_ingreso",
         		t2.periodo_min_vig,
        		t1.rut_titular,
                t1.cod_categoria,
                t1.cod_sucursal,
                t2.antiguedad,
                t2.RangoAntiguedad,
                t2.Sexo
            ) t1
	LEFT OUTER JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_SUCURSAL2 t2 ON (t1.cod_sucursal = t2."COD_SUCURSAL")
GROUP BY       
	   t1.Periodo,
	   fecha_ingreso,
	   periodo_min_vig,
       t1.cod_categoria,
       t1.rut_titular,
       t2."UBICACION",
       t1.antiguedad,
       t1.RangoAntiguedad,
       t1.sexo       
--------------------- termina ingresos----------------
  UNION ALL
------- INICIA IVA-------------
  SELECT 
  		  t1.Periodo AS Periodo,
  		  t1.fecha_ingreso,
  		  t1.periodo_min_vig,
  		  t1.rut_titular,
          t1.cod_categoria, 
          ((SUM(t1.Valor_Iva))*-1) AS Total, 
          t2."UBICACION" as ubicacion, 
          t1.antiguedad,
          t1.RangoAntiguedad, 
          t1.Sexo, 
           0  AS cotizantes,
           0  AS cargas,
           0  AS costo_final,
            case when t1.Periodo > 0 then 'IVA' end AS tipo_registro
      FROM
          (
     ------- INICIA LIBRO DE VENTAS -------------------
            SELECT 
	            	t1.perpag_esperado AS Periodo,
	            	t2."fld_ingreso" AS fecha_ingreso,
	            	t2.periodo_min_vig,
	            	t2."fld_cotrut" AS rut_titular,
	                t1.cod_categoria, 
	                t1.cod_sucursal, 
	                (SUM(t1.Valor_Iva))  AS Valor_Iva, 
	                t2.antiguedad,
	                t2.RangoAntiguedad,
	                t2.sexo
            FROM RCD.P_DDV_RCD.P_DDV_RCD_V_LIBRO_VENTAS t1
            	LEFT JOIN  mm_antiguedad t2 ON (t1.Rut_Cotizante = t2."fld_cotrut" AND t1.perpag_esperado = t2.periodo_mes )
            WHERE  t1.perpag_esperado BETWEEN  est.P_DDV_EST.fecha_periodo($periodo_ini) AND est.P_DDV_EST.fecha_periodo($periodo_fin) 
            AND t1.Codigo_Pago = 0
            GROUP BY 
            		 t1.perpag_esperado,
            		 t2."fld_ingreso",
            		 t2.periodo_min_vig,
            		 rut_titular,
		             t1.cod_categoria,
		             t1.cod_sucursal,
		             t2.antiguedad,
		             t2.RangoAntiguedad,
		             t2.sexo
            ------ TERMINA LIBRO DE VENTAS ----------------
            ) t1
           LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_SUCURSAL2 t2 
           ON (t1.cod_sucursal = t2."COD_SUCURSAL")
      GROUP BY 
      		t1.Periodo,
      		t1.fecha_ingreso,
      		t1.periodo_min_vig,
      		t1.rut_titular,
            t1.cod_categoria,
            t2."UBICACION",
            t1.antiguedad,
            t1.RangoAntiguedad,
            t1.Sexo
--------------------- termina ingresos----------------
)
--SELECT * FROM mm_base_ingreso LIMIT 10
, mm_margen_ingreso AS ( 
-- ----- INICIA INGRESOS--------------
SELECT 
    t1.Periodo,
    t1.fecha_ingreso,
    t1.periodo_min_vig,
    t1.rut_titular,
    t1.cod_categoria, 
    t2.categoria_gls, 
    t2.tipo_plan AS serie, 
    t3.DETALLE_PRODUCTO AS preferente, 
    t3.TIPO_PLAN AS Tipo_Contrato, 
    t3.TIPO_PRODUCTO AS Tipo_Producto, 
    t1.ubicacion,
    t1.antiguedad,
    t1.RangoAntiguedad, 
    t1.Sexo, 
    t1.Cotizantes, 
    t1.Cargas, 
    t1.Costo_final, 
    t3.LINEA_PLAN, 
    (SUM(t1.Total)) AS Total_ingreso, 
    (SUM(t1.INGRESO)) AS INGRESO, 
    (SUM(t1.IVA)) AS IVA
FROM 
(
          ----------- INICIA SUM DE INGRESOS ------------
          SELECT 
	           t1.Periodo,
	           t1.fecha_ingreso,
	           t1.periodo_min_vig,
	           t1.rut_titular,
	           t1.cod_categoria,
	           t1.ubicacion,
	           t1.antiguedad,
	           case  
	           		when t1.RangoAntiguedad = '' then '00 a 12' else t1.RangoAntiguedad 
	           end AS RangoAntiguedad,
	           t1.SEXO, 
	           SUM(t1.INGRESO)+SUM(t1.IVA)  AS Total,
	           SUM(t1.Cotizantes) AS Cotizantes,
	           SUM(t1.Cargas) AS Cargas,
	           SUM(t1.INGRESO)+SUM(t1.IVA) AS Costo_final,
	           SUM(t1.IVA) AS IVA,
	           SUM(t1.INGRESO) AS INGRESO
          FROM
          		(  
                -------- INICIA TRANSPOSICION DE INGRESOS--------------------
	                SELECT 
	                	PERIODO,
	                	fecha_ingreso,
	                	periodo_min_vig,
	                	RUT_TITULAR,
	                	COD_CATEGORIA, 
	                	costo_final, 
	                	UBICACION, 
	                	antiguedad,
	                	RangoAntiguedad, 
	                	COTIZANTES, 
	                	CARGAS, 
	                	SEXO,
	                	ifnull("'INGRESO'",0) AS INGRESO,
	                	ifnull("'IVA'",0) AS IVA
	                FROM mm_base_ingreso
	                pivot(sum(TOTAL) for TIPO_REGISTRO  in ('INGRESO', 'IVA')
	             )  as p    
                -------- TERMINA TRANSPOSICION DE INGRESOS--------------------
          		)  t1
          GROUP BY t1.Periodo,
          		   t1.fecha_ingreso,
          		   t1.periodo_min_vig,
          		   t1.rut_titular,
		           t1.cod_categoria,
		           t1.ubicacion,
		           t1.antiguedad,
		           t1.RangoAntiguedad,
		           t1.Sexo         
         ----------- TERMINA SUM DE INGRESOS ------------
)   t1
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA" t2 ON (t1.cod_categoria = t2.cod_categoria)
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_PRM_AGRUPA_PLANES" t3 ON (t1.cod_categoria = t3."cod_categoria") -- "IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES"  t3 
GROUP BY t1.Periodo,
		 t1.fecha_ingreso,
		 t1.periodo_min_vig,
		 t1.rut_titular,
		 t1.cod_categoria,
		 t2.categoria_gls,
		 t2.tipo_plan,
		 t3.DETALLE_PRODUCTO,
		 t3.TIPO_PLAN,
		 t3.TIPO_PRODUCTO,
		 t1.ubicacion,
		 t1.antiguedad,
		 t1.RangoAntiguedad,
		 t1.Sexo,
		 t1.Cotizantes,
		 t1.Cargas,
		 t1.Costo_final,
		 t3.LINEA_PLAN 
)
---- REVISION CASOS INGRESOS
/*
SELECT 
* 
FROM mm_margen_ingreso 
WHERE rut_titular ='012073836-4' --AND PERIODO =202011
LIMIT 10
*/
-----  FIN REVISION
------ INGRESOS OK
, fase_union AS
( 
SELECT    TO_VARCHAR(DATEADD(month,-1,CURRENT_DATE) ,'yyyymm')::int AS periodo_procesamiento,
		  t1.fecha_ingreso,
		  t1.periodo_min_vig,
          t1.Periodo::INT AS PERIODO,
          est.P_DDV_EST.periodo_fecha(t1.Periodo) AS periodo_fecha,
          t1.rut_titular,
          t1.cod_categoria, 
          t1.categoria_gls, 
          t1.Linea_Plan, 
          t1.serie, 
          t1.preferente AS PRODUCTO, 
          t1.Tipo_Contrato as TIPO_PLAN, 
          t1.Tipo_Producto, 
          t1.ubicacion,
          t1.antiguedad,
          case when t1.RangoAntiguedad is null then 'Sin informaci�n' else t1.RangoAntiguedad end  AS RangoAntiguedad,
          t1.Sexo,
          t1.Cotizantes, 
          t1.Cargas,
          t1.INGRESO as Total_Ingreso,       
          t1.Total_Ingreso AS INGRESO, 
          t1.IVA AS IVA, 
          t1.Costo_final, 
          t1.cobrado AS cobrado_total,
          t1.Gasto AS Gasto_total, 
          t1.HOSPITALARIO AS Gasto_HOSPITALARIO, 
          t1.AMBULATORIO AS Gasto_AMBULATORIO, 
          t1.GES AS Gasto_GES, 
          t1.CAEC AS Gasto_CAEC, 
          t1.PHARMA AS Gasto_PHARMA, 
          t1.IVA_RECUPERADO AS IVA_RECUPERADO, 
          t1.LCC AS Gasto_LCC, 
          t1.RECUPERACION_GASTO AS RECUPERACION_GASTO, 
          IFF(IFNULL((t1.Cotizantes + t1.Cargas),0) !=0,  t1.Costo_final/(t1.Cotizantes + t1.Cargas),0)  AS "Pactada/Beneficiario",          
          case 
        	when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <2 then 1
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND  (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=2 and (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <3  then 2
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND  (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=3 and (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <4  then 3
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=4 then 4
            else 5
          end AS ordenPactada
FROM (
        -- inicia INGRESOS VS GASTOS --------------
          SELECT DISTINCT 
                  IFNULL(t2.Periodo, t1.Periodo) AS Periodo,
                  IFNULL(t1.fecha_ingreso, t2.fecha_ingreso) AS fecha_ingreso,
                  IFNULL(t1.periodo_min_vig,t2.periodo_min_vig) AS periodo_min_vig,
                  IFNULL(t2.rut_titular, t1.rut_titular) AS rut_titular, 
                  IFNULL(t2.cod_categoria,t1.cod_categoria) AS cod_categoria, 
                  IFNULL(t2.categoria_gls,t1.categoria_gls) AS categoria_gls, 
                  IFNULL( t2.LINEA_PLAN,t1.LINEA_PLAN) AS Linea_Plan, 
                  IFNULL(t2.serie,t1.serie) AS serie, 
                  IFNULL(t2.preferente,t1.preferente) AS preferente, 
                  IFNULL(t2.Tipo_Contrato,t1.Tipo_Contrato) AS Tipo_Contrato, 
                  IFNULL(t2.Tipo_Producto,t1.Tipo_Producto) AS Tipo_Producto,           
                  IFNULL(t2.ubicacion,t1.ubicacion) AS ubicacion, 
                  IFNULL(t2.antiguedad , t1.antiguedad) AS antiguedad, 
                  IFNULL(t2.RangoAntiguedad, t1.RangoAntiguedad) AS RangoAntiguedad, 
                  IFNULL(t2.Sexo, t1.Sexo) AS Sexo, 
                  t2.Cotizantes, 
                  t2.Cargas, 
                  t2.Costo_final, 
                  t2.Total_Ingreso, 
                  t2.INGRESO, 
                  t2.IVA, 
                  SUM(t1.Gasto) AS Gasto, 
                  sum(t1.cobrado) AS cobrado,
                  SUM(t1.HOSPITALARIO) AS HOSPITALARIO, 
                  SUM(t1.AMBULATORIO) AS AMBULATORIO, 
                  SUM(t1.GES) AS GES, 
                  SUM(t1.CAEC) AS CAEC, 
                  SUM(t1.PHARMA) AS PHARMA, 
                  SUM(t1.IVA_RECUPERADO) AS IVA_RECUPERADO, 
                  SUM(t1.LCC) AS LCC, 
                  SUM(t1.RECUPERACION_GASTO) AS RECUPERACION_GASTO
            FROM mm_margen_gasto t1
            FULL JOIN mm_margen_ingreso t2  
                     ON (t1.cod_categoria 	= t2.cod_categoria) 
               		AND (t1.Periodo 		= t2.Periodo) 
               		AND (t1.rut_titular 	= t2.rut_titular)
                    AND (t1.categoria_gls 	= t2.categoria_gls) 
                    AND (t1.serie 			= t2.serie) 
                    AND (t1.ubicacion 		= t2.ubicacion) 
                    AND (t1.preferente 		= t2.preferente) 
                    AND (t1.Tipo_Producto 	= t2.Tipo_Producto) 
                    AND (t1.Tipo_Contrato 	= t2.Tipo_Contrato) 
                    AND (t1.antiguedad 		= t2.antiguedad)
                    AND (t1.RangoAntiguedad = t2.RangoAntiguedad)  
                    AND (t1.Sexo 			= t2.Sexo)
                    AND (t1.fecha_ingreso   = t2.fecha_ingreso)
                    AND (t1.periodo_min_vig = t2.periodo_min_vig)
              GROUP BY t2.Periodo, t1.Periodo,
                       t1.fecha_ingreso, t2.fecha_ingreso,
                       t1.periodo_min_vig, t2.periodo_min_vig,
                       t2.rut_titular, t1.rut_titular,
                       t2.cod_categoria, t1.cod_categoria,
                       t2.categoria_gls, t1.categoria_gls,
                       t2.LINEA_PLAN,  t1.LINEA_PLAN,
                       t2.serie,t1.serie,
                       t2.preferente, t1.preferente,
                       t2.Tipo_Contrato, t1.Tipo_Contrato,
                       t2.Tipo_Producto, t1.Tipo_Producto,
                       t2.ubicacion, t1.ubicacion,
                       t2.antiguedad, t1.antiguedad,
                       t2.RangoAntiguedad,t1.RangoAntiguedad,
                       t2.Sexo, t1.Sexo,
                       t2.Cotizantes,
                       t2.Cargas,
                       t2.Costo_final,
                       t2.Total_Ingreso,
                       t2.INGRESO,
                       t2.IVA
     -- TERMINA INGRESOS VS GASTOS --------------
  ) t1
)
SELECT 
	* 
FROM fase_union
--WHERE rut_titular= '012073836-4'
ORDER BY periodo, rut_titular
--LIMIT 10
)