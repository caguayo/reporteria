SELECT
	PERIODO_PROCESAMIENTO ,
	PERIODO ,
	SUM(GASTO_TOTAL),
	sum(TOTAL_INGRESO)
FROM EST.P_DDV_EST.ca_test_matriz_margen --- agrupados por ruts titulares
WHERE periodo_procesamiento = 202204
GROUP BY PERIODO_PROCESAMIENTO , PERIODO
ORDER BY periodo DESC 
LIMIT 12



SELECT
	PERIODO_PROCESAMIENTO ,
	PERIODO ,
	SUM(GASTO_TOTAL),
	sum(TOTAL_INGRESO)
FROM EST.P_DDV_EST.ca_test_matriz_margen_02  --- copiados como sin ruts (copia similar al matriz margen)
WHERE periodo_procesamiento = 202204
GROUP BY PERIODO_PROCESAMIENTO , PERIODO
ORDER BY periodo DESC 
LIMIT 12



SELECT
	*
FROM EST.P_DDV_EST.ca_test_matriz_margen
WHERE rut_titular = '000565921-3'
--GROUP BY PERIODO_PROCESAMIENTO , PERIODO
ORDER BY periodo DESC 
--LIMIT 10



SELECT 
	PERIODO_PROCESAMIENTO ,
	PERIODO ,
	SUM(GASTO_TOTAL),
	sum(TOTAL_INGRESO)
FROM EST.P_DDV_EST.P_DDV_EST_MATRIZ_MARGEN
WHERE PERIODO_PROCESAMIENTO = 202204
GROUP BY PERIODO_PROCESAMIENTO , PERIODO
ORDER BY PERIODO DESC 
LIMIT 100


SELECT * FROM EST.P_DDV_EST.CA_TEST_MATRIZ_MARGEN LIMIT 10



SELECT * FROM EST.P_DDV_EST.CA_TEST_MATRIZ_MARGEN_02 LIMIT 10



SELECT * FROM EST.P_DDV_EST.CA_TEST_MATRIZ_MARGEN LIMIT 10




CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TEST_MATRIZ_MARGEN_02 AS (  
WITH mm_permanencia AS (
SELECT DISTINCT 
    rut_todo AS RUT_TITULAR,
    "CONSEXO" AS SEXO,
    (case 
            when t1.antiguedad between 0 and 12 then '00 a 12'
            when t1.antiguedad between 13 and 24 then '13 a 24'
            when t1.antiguedad between 25 and 36 then '25 a 36'
            when t1.antiguedad between 37 and 48 then '37 a 48'
            when t1.antiguedad between 49 and 60 then '49 a 60'
            when t1.antiguedad between 61 and 72 then '61 a 72'
            when t1.antiguedad between 73 and 84 then '73 a 84'
            when t1.antiguedad between 85 and 96 then '85 a 96'
            when t1.antiguedad between 97 and 108 then '97 a 108'
            when t1.antiguedad between 109 and 120 then '109 a 120'
            when t1.antiguedad between 121 and 240 then '121 a 240'
            when t1.antiguedad between 241 and 360 then '241 a 360'
         else '361 y m�s'   
     end) AS RangoPermanencia         
FROM (    
     ----------- inicia calculo antiguedad ------------------
      SELECT 
          	TODO.max_periodo AS PERVIG, 
          	TODO.rut_todo, 
          	A.FECHA_INGRESO,  
          	IFF(A.FECHA_INGRESO = A.FECHA_INGRESO_ISAPRE AND A.tipo_transaccion in ('VN','CT'), 'S','' ) AS ENCONTRADO,
           	IFF(A.FECHA_INGRESO = A.FECHA_INGRESO_ISAPRE AND A.tipo_transaccion in ('VN','CT'), todo.min_periodo,null ) AS perdesde,
           	IFF(ENCONTRADO= 'S', datediff(month, TO_DATE(perdesde || '01', 'yyyymmdd'), TO_DATE(PERVIG || '01', 'yyyymmdd'))+1,
           	datediff(month, add_months(A.FECHA_INGRESO,2), TO_DATE(PERVIG || '01', 'yyyymmdd') )+1) AS ANTIGUEDAD,
           	CON."CONSEXO"
      FROM(
            ------ INICIA TODO --------------    
            SELECT 
                max(periodo_vigencia) AS max_periodo,
                min(periodo_vigencia) AS min_periodo,
                rut_titular as rut_todo
            FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO"
            GROUP BY rut_titular 
            ------ TERMINA TODO --------------    
           ) TODO
      LEFT OUTER JOIN "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" A ON TODO.max_periodo = A.PERIODO_VIGENCIA AND TODO.rut_todo = A.RUT_TITULAR
      LEFT OUTER JOIN (select 
      						"CONRUTBEN", 
      						MIN("CONSEXO") AS consexo 
      					from  "AFI"."P_DDV_AFI"."P_DDV_AFI_CON"
          				GROUP BY  "CONRUTBEN"
          			  ) CON  ON TODO.rut_todo = CON."CONRUTBEN"
      ----------- termina calculo antiguedad ------------------
  ) t1
)
--SELECT * FROM mm_permanencia LIMIT 10
,

mm_base_gasto AS ( 
Select    s2.periodo::INT AS PERIODO,
          s2.cod_categoria, 
          s2.cod_sucursal, 
          s2.RangoPermanencia, 
          s2.sexo, 
          ifnull((SUM(s2.MONTO)),0) AS MONTO, 
          s2.tipo_prestador    
from (
   ------- COMIENZA  CAEC  ----------------------
         SELECT 
                t1.periodo::INT AS PERIODO,
                t1.codcateg AS cod_categoria,
                t1.codsuc AS cod_sucursal,
                t2.RangoPermanencia,
                t2.sexo, 
                (SUM(t1.monto_caec)) AS MONTO,            
                (CASE             
                    WHEN t1.periodo > 0 then 'CAEC'            
                END) AS tipo_prestador
        FROM   "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS" t1
            LEFT JOIN mm_permanencia t2 ON (t1.rutafi = t2.rut_titular)
        WHERE t1.periodo BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                             and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
          AND t1.monto_caec !=  0
        GROUP BY t1.periodo,
	             t1.codcateg,
	             t1.codsuc,
	             t2.RangoPermanencia,
	             t2.SEXO
  ------- TERMINA  CAEC  ----------------------    
    UNION ALL 
  -------------- INICIA SALUD -----------------
    SELECT 
        t1.periodo::INT AS PERIODO,
        t1.codcateg  AS cod_categoria,
        t1.codsuc  AS cod_sucursal,
        t2.RangoPermanencia,
        t2.sexo,  
        SUM(t1.mtobonificado) AS MONTO,
        (CASE 
                WHEN t1.tipoprestacion = 'A20' THEN 'HOSPITALARIO'
                WHEN CONTAINS(t1.tipoprestacion,'A') THEN 'AMBULATORIO'
                WHEN  CONTAINS(t1.tipoprestacion, 'GM') OR CONTAINS (t1.tipoprestacion,'GD') OR CONTAINS(t1.tipoprestacion,'GF') THEN 'GES'
                WHEN CONTAINS(t1.tipoprestacion,'GC') THEN 'CAEC'
                WHEN  CONTAINS ( t1.tipoprestacion,'H') OR CONTAINS(t1.tipoprestacion,'M') THEN 'HOSPITALARIO'
                ELSE 'HOSPITALARIO'
            END) AS tipo_prestador
        FROM "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS"  t1
        LEFT JOIN mm_permanencia t2 
                ON (t1.rutafi = t2.rut_titular)  
        WHERE t1.periodo BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                             and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY   t1.periodo,
                   t1.codcateg,
                   t1.codsuc,
                   t2.RangoPermanencia,
                   t2.sexo,
                    t1.tipoprestacion
    -------------- TERMINA SALUD -----------------
    UNION ALL  
  -------- INICIA LCC ------------------------
  SELECT 
    S1.PERIODOPAGO AS periodo,
    S1.CODCATEGORIA AS cod_categoria,
    S1.SUCURSALCONTRATO AS cod_sucursal,
    t2.RangoPermanencia, 
    t2.sexo,   
    SUM(s1.valorbonificado) AS MONTO,
    'LCC'  AS tipo_prestador 
FROM "LCC"."P_DDV_LCC"."P_DDV_LCC_V_LIQ"  s1
LEFT JOIN mm_permanencia  t2  ON ( s1.rut_titular = t2.rut_titular)    
WHERE S1.PERIODOPAGO BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                         and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT AND TIPOLICENCIA IN (1,7) and CODIGOESTADO in (3,4)
GROUP BY 	S1.PERIODOPAGO,
			S1.CODCATEGORIA,
			S1.SUCURSALCONTRATO,
			t2.RangoPermanencia,
			t2.sexo,  
			s1.sucursalpago  
  -------- TERMINA LCC ------------------------ 
    UNION ALL
  ------------ comienza IVA RECUPERADO ------------------
        SELECT 
            t1.periodo,
            t1.codcateg  AS cod_categoria,
            t1.codsuc  AS cod_sucursal,
            t2.RangoPermanencia,
            t2.sexo, 
            (SUM(t1.monto_credito_fiscal)*-1) AS MONTO,
            (case            
            when t1.periodo > 0 then 'IVA_RECUPERADO' end) AS tipo_prestador
        FROM "CON"."P_DDV_CON"."P_DDV_CON_IVA_RECUPERADO_BONOS" t1
            LEFT JOIN mm_permanencia  t2 ON (t1.rut = t2.rut_titular)
        WHERE t1.periodo BETWEEN TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1.periodo,
                 t1.codcateg,
                 t1.codsuc,
                 t2.RangoPermanencia,
                 t2.sexo
  ------------ TERMINA IVA RECUPERADO ------------------
    UNION ALL
  ------------ comienza RECUPERACION GASTO------------------
         SELECT t1."periodo_vig" AS periodo,
	            t1."cod_categoria" AS cod_categoria,
	            t1."cod_sucursal" AS cod_sucusral,
	            t2.RangoPermanencia,
	             t2.sexo, 
	            ((SUM(t1."monto_recuperado"))*-1) AS MONTO,
	            (case           
	            when t1."periodo_vig" > 0 then 'RECUPERACION GASTO' end) AS tipo_prestador
	        FROM "FIN"."P_DDV_FIN"."P_DDV_FIN_RECUPERACION_GASTO_NEW"  t1
            LEFT JOIN mm_permanencia t2 ON (t1."rut_titular" = t2.rut_titular)
        WHERE t1."periodo_vig" BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                   and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY   t1."periodo_vig",
	               t1."cod_categoria",
	               t1."cod_sucursal",
	               t2.RangoPermanencia,
	               t2.sexo
  ------------ TERMINA RECUPERACION GASTO------------------
    UNION ALL 
   ------------ comienza CONTRATO------------------
        SELECT  t1.periodo_vigencia AS periodo,
	            t1.cod_categoria,
	            t1.cod_sucursal,
	            t2.RangoPermanencia,
	            t2.sexo, 
	            (SUM(t1.gasto_pharma_pesos)) AS MONTO,
	            (case            
	            when t1.periodo_vigencia > 0 then 'PHARMA' end) AS tipo_prestador
        FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t1
            LEFT JOIN mm_permanencia  t2 ON (t1.rut_titular = t2.rut_titular)
        WHERE t1.periodo_vigencia BETWEEN   TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT
                                        AND TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY   t1.periodo_vigencia,
	               t1.cod_categoria,
	               t1.cod_sucursal,
	               t2.RangoPermanencia,
	               t2.SEXO
    ) s2
group by 
               s2.periodo,
               s2.cod_categoria,
               s2.cod_sucursal,
               s2.RangoPermanencia,
               s2.sexo,
               s2.tipo_prestador
)
, 
mm_margen_gasto AS ( 
SELECT 
  t1.periodo AS periodo,
  t1.cod_categoria, 
  t3.categoria_gls, 
  (case
  when t1.RangoPermanencia = '' then '00 a 12' else t1.RangoPermanencia end) AS RangoPermanencia, 
  t1.ubicacion, 
  t1.Sexo, 
  t3.tipo_plan AS serie, 
  t2.DETALLE_PRODUCTO  AS preferente, 
  t2.TIPO_PLAN AS Tipo_Contrato, 
  t2.TIPO_PRODUCTO AS Tipo_Producto, 
  t2.LINEA_PLAN, 
  t1.Gasto, 
  t1.HOSPITALARIO, 
  t1.AMBULATORIO, 
  t1.GES, 
  t1.CAEC, 
  t1.PHARMA, 
  t1.IVA_RECUPERADO, 
  t1.LCC, 
  t1.RECUPERACION_GASTO
FROM (
---------------  inicia GASTOS SUCURSAL-------------------
      SELECT 
        s1.periodo,
        s1.cod_categoria,
        s1.RangoPermanencia,
        t2."UBICACION" as ubicacion,
        s1.sexo,
       (IFNULL(s1.PHARMA,0) + IFNULL(s1.GES ,0) + IFNULL(s1.IVA_RECUPERADO,0)
        + IFNULL(s1.AMBULATORIO,0) + IFNULL(s1.HOSPITALARIO,0) + IFNULL(s1.LCC,0)
        + IFNULL(s1.CAEC,0) + IFNULL(s1.RECUPERACION_GASTO,0)) AS Gasto,
        IFNULL(s1.HOSPITALARIO,0) AS HOSPITALARIO,
        IFNULL(s1.AMBULATORIO,0) AS AMBULATORIO,
        IFNULL(s1.GES ,0)  AS GES,
        IFNULL(s1.CAEC,0)  AS CAEC,
        IFNULL(s1.PHARMA,0)   AS PHARMA,
        IFNULL(s1.IVA_RECUPERADO,0)  AS IVA_RECUPERADO,
        IFNULL(s1.LCC ,0)  AS LCC,
        IFNULL(s1.RECUPERACION_GASTO,0) AS RECUPERACION_GASTO
      FROM
      ( 
      ----- INICIA TRANSPOSICION DE GASTOS-----------------------
          SELECT  PERIODO, 
          		  COD_CATEGORIA, 
          		  COD_SUCURSAL, 
          		  RANGOPERMANENCIA, 
          		  sexo,
		          ifnull("'IVA_RECUPERADO'",0) AS IVA_RECUPERADO,
		          ifnull("'RECUPERACION GASTO'",0) AS RECUPERACION_GASTO,
		          ifnull("'PHARMA'",0) AS PHARMA,
		          ifnull("'GES'",0) AS GES,
		          ifnull("'AMBULATORIO'",0) AS AMBULATORIO,
		          ifnull("'CAEC'",0) AS CAEC,
		          ifnull("'LCC'",0) AS LCC,
		          ifnull("'HOSPITALARIO'",0) AS HOSPITALARIO
          FROM  mm_base_gasto
          pivot(sum(monto) for TIPO_PRESTADOR  in ('IVA_RECUPERADO', 'RECUPERACION GASTO', 'PHARMA', 'GES', 'AMBULATORIO', 'CAEC','LCC', 'HOSPITALARIO')
          )  as p
          ----- TERMINA TRANSPOSICION DE GASTOS-----------------------
      )  s1
      LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_SUCURSAL2" t2  ON(s1.cod_sucursal = t2."COD_SUCURSAL")
----- termina GASTOS SUCURSAL 
)
t1
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_PRM_AGRUPA_PLANES" t2     ON (t1.cod_categoria = t2."cod_categoria")
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA" t3 		 ON (t1.cod_categoria = t3.cod_categoria)
)
, 
mm_base_ingreso AS ( 
   ----- INICIA INGRESOS ---------------------
SELECT 
		t1.Periodo::INT AS PERIODO,
        t1.cod_categoria,
        ((SUM(t1.pagado_total_pesos))-(SUM(t1.monto_exceso))-(SUM(t1.monto_excedente))) AS Total,
        t2."UBICACION" as ubicacion,
        t1.RangoPermanencia,
         t1.Sexo, 
        (SUM(t1.Cotizantes)) AS Cotizantes,
        (SUM(t1.Cargas)) AS Cargas,
       IFNULL((SUM(t1.uf_costo_final)),0) AS costo_final,
        case when t1.Periodo >0 then 'INGRESO'
        end AS tipo_registro
    FROM
        (
          ----- INICIA CONTRATO -------------
         SELECT 
            t1.periodo_vigencia AS Periodo,
            t1.cod_categoria,
            t1.cod_sucursal,
            ((SUM(t1.pagado_total_pesos))-(SUM(t1.monto_exceso))-(SUM(t1.monto_excedente))) AS Total,
            (SUM(t1.pagado_total_pesos)) AS pagado_total_pesos,
            (SUM(t1.monto_exceso))  AS monto_exceso,
            (SUM(t1.monto_excedente)) AS monto_excedente,
            (COUNT(DISTINCT(t1.rut_titular))) AS Cotizantes,
            (SUM(t1.cnt_carfam))  AS Cargas,
            t2.RangoPermanencia,
            t2.sexo, 
            (SUM(t1.costo_final_uf))  AS uf_costo_final
        FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t1
            LEFT OUTER JOIN mm_permanencia  t2
            ON (t1.rut_titular = t2.rut_titular)
        WHERE  t1.periodo_vigencia BETWEEN TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT
                                       AND TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY   t1.periodo_vigencia,
                   t1.cod_categoria,
                   t1.cod_sucursal,
                   t2.RangoPermanencia,
                   t2.Sexo
            )
    t1
        LEFT OUTER JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_SUCURSAL2" t2 
    ON(t1.cod_sucursal = t2."COD_SUCURSAL")
    GROUP BY       t1.Periodo,
                   t1.cod_categoria,
                   t2."UBICACION",
                   t1.RangoPermanencia,
                   t1.sexo          
UNION ALL
------- INICIA IVA-------------
  SELECT 
  		  t1.Periodo AS Periodo,
          t1.cod_categoria, 
          ((SUM(t1.Valor_Iva))*-1) AS Total, 
          t2."UBICACION" as ubicacion, 
          t1.RangoPermanencia, 
          t1.Sexo, 
           0  AS cotizantes,
           0  AS cargas,
           0  AS costo_final,
            case when t1.Periodo > 0 then 'IVA' end AS tipo_registro
      FROM
          (
     ------- INICIA LIBRO DE VENTAS -------------------
            SELECT 
            	t1.perpag_esperado AS Periodo,
                t1.cod_categoria, 
                t1.cod_sucursal, 
                (SUM(t1.Valor_Iva))  AS Valor_Iva, 
                t2.RangoPermanencia,
                t2.sexo
            FROM "RCD"."P_DDV_RCD"."P_DDV_RCD_V_LIBRO_VENTAS" t1
            LEFT JOIN  mm_permanencia t2 ON (t1.Rut_Cotizante = t2.rut_titular)
            WHERE  t1.perpag_esperado BETWEEN TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT
                                                AND TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
            AND t1.Codigo_Pago = 0
            GROUP BY t1.perpag_esperado,
		             t1.cod_categoria,
		             t1.cod_sucursal,
		             t2.RangoPermanencia,
		             t2.sexo
            ------ TERMINA LIBRO DE VENTAS ----------------
            ) t1
           LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_SUCURSAL2" t2 
           ON (t1.cod_sucursal = t2."COD_SUCURSAL")
      GROUP BY t1.Periodo,
               t1.cod_categoria,
               t2."UBICACION",
               t1.RangoPermanencia,
               t1.Sexo
--------------------- termina ingresos----------------
)
, mm_margen_ingreso AS ( 
-- ----- INICIA INGRESOS--------------
SELECT 
    t1.Periodo,
    t1.cod_categoria, 
    t2.categoria_gls, 
    t2.tipo_plan AS serie, 
    t3.DETALLE_PRODUCTO AS preferente, 
    t3.TIPO_PLAN AS Tipo_Contrato, 
    t3.TIPO_PRODUCTO AS Tipo_Producto, 
    t1.ubicacion, 
    t1.RangoPermanencia, 
    t1.Sexo, 
    t1.Cotizantes, 
    t1.Cargas, 
    t1.Costo_final, 
    t3.LINEA_PLAN, 
    (SUM(t1.Total)) AS Total_ingreso, 
    (SUM(t1.INGRESO)) AS INGRESO, 
    (SUM(t1.IVA)) AS IVA
FROM 
(
          ----------- INICIA SUM DE INGRESOS ------------
          SELECT 
	           t1.Periodo,
	           t1.cod_categoria,
	           t1.ubicacion,
	           case  
	           		when t1.RangoPermanencia = '' then '00 a 12' else t1.RangoPermanencia 
	           end AS RangoPermanencia,
	           t1.SEXO, 
	           SUM(t1.INGRESO)+SUM(t1.IVA)  AS Total,
	           SUM(t1.Cotizantes) AS Cotizantes,
	           SUM(t1.Cargas) AS Cargas,
	           SUM(t1.INGRESO)+SUM(t1.IVA) AS Costo_final,
	           SUM(t1.IVA) AS IVA,
	           SUM(t1.INGRESO) AS INGRESO
          FROM
          (  
                -------- INICIA TRANSPOSICION DE INGRESOS--------------------
                SELECT 
                	PERIODO,
                	COD_CATEGORIA, 
                	costo_final, 
                	UBICACION, 
                	RANGOPERMANENCIA, 
                	COTIZANTES, 
                	CARGAS, 
                	SEXO,
                	ifnull("'INGRESO'",0) AS INGRESO,
                	ifnull("'IVA'",0) AS IVA
                FROM mm_base_ingreso
                pivot(sum(TOTAL) for TIPO_REGISTRO  in ('INGRESO', 'IVA')
                )  as p    
                -------- TERMINA TRANSPOSICION DE INGRESOS--------------------
          )  t1
          GROUP BY t1.Periodo,
		           t1.cod_categoria,
		           t1.ubicacion,
		           t1.RangoPermanencia,
		           t1.Sexo         
         ----------- TERMINA SUM DE INGRESOS ------------
)   t1
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA" t2 ON (t1.cod_categoria = t2.cod_categoria)
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_PRM_AGRUPA_PLANES" t3 ON (t1.cod_categoria = t3."cod_categoria") -- "IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES"  t3 
GROUP BY t1.Periodo,
		 t1.cod_categoria,
		 t2.categoria_gls,
		 t2.tipo_plan,
		 t3.DETALLE_PRODUCTO,
		 t3.TIPO_PLAN,
		 t3.TIPO_PRODUCTO,
		 t1.ubicacion,
		 t1.RangoPermanencia,
		 t1.Sexo,
		 t1.Cotizantes,
		 t1.Cargas,
		 t1.Costo_final,
		 t3.LINEA_PLAN 
)
, fase_union AS
( 
SELECT    TO_VARCHAR(DATEADD(month,-1,CURRENT_DATE) ,'yyyymm')::int AS periodo_procesamiento,
          t1.Periodo:: INT AS PERIODO,
          t1.cod_categoria, 
          t1.categoria_gls, 
          t1.Linea_Plan, 
          t1.serie, 
          t1.preferente AS PRODUCTO, 
          t1.Tipo_Contrato as TIPO_PLAN, 
          t1.Tipo_Producto, 
          t1.ubicacion,
           case when t1.RangoPermanencia is null then 'Sin informaci�n' else t1.RangoPermanencia end  AS RangoPemanencia,
          t1.Sexo,
          t1.Cotizantes, 
          t1.Cargas,
          t1.INGRESO as Total_Ingreso,       
          t1.Total_Ingreso AS INGRESO, 
          t1.IVA AS IVA, 
          t1.Costo_final, 
          t1.Gasto AS Gasto_total, 
          t1.HOSPITALARIO AS Gasto_HOSPITALARIO, 
          t1.AMBULATORIO AS Gasto_AMBULATORIO, 
          t1.GES AS Gasto_GES, 
          t1.CAEC AS Gasto_CAEC, 
          t1.PHARMA AS Gasto_PHARMA, 
          t1.IVA_RECUPERADO AS IVA_RECUPERADO, 
          t1.LCC AS Gasto_LCC, 
          t1.RECUPERACION_GASTO AS RECUPERACION_GASTO, 
          IFF(IFNULL((t1.Cotizantes + t1.Cargas),0) !=0,  t1.Costo_final/(t1.Cotizantes + t1.Cargas),0)  AS "Pactada/Beneficiario",          
          case 
            when t1.RangoPermanencia is null then 14
            when t1.RangoPermanencia='00 a 12' then 1
            when t1.RangoPermanencia='13 a 24' then 2
            when t1.RangoPermanencia='25 a 36' then 3
            when t1.RangoPermanencia='37 a 48' then 4
            when t1.RangoPermanencia='49 a 60' then 5
            when t1.RangoPermanencia='61 a 72' then 6
            when t1.RangoPermanencia='73 a 84' then 7
            when t1.RangoPermanencia='85 a 96' then 8
            when t1.RangoPermanencia='97 a 108' then 9
            when t1.RangoPermanencia='109 a 120' then 10
            when t1.RangoPermanencia='121 a 240' then 11
            when t1.RangoPermanencia='241 a 360' then 12
            when t1.RangoPermanencia='361 y m�s' then 13
            when t1.RangoPermanencia='Sin informaci�n' then 14
            else 14
            end
            AS Permanencia,  
          case 
        	when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <2 then 1
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND  (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=2 and (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <3  then 2
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND  (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=3 and (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <4  then 3
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=4 then 4
            else 5
          end AS ordenPactada
FROM (
        -- inicia INGRESOS VS GASTOS --------------
          SELECT DISTINCT 
                  IFNULL(t2.Periodo, t1.Periodo) AS Periodo,
                  IFNULL(t2.cod_categoria,t1.cod_categoria) AS cod_categoria, 
                  IFNULL(t2.categoria_gls,t1.categoria_gls) AS categoria_gls, 
                  IFNULL( t2.LINEA_PLAN,t1.LINEA_PLAN) AS Linea_Plan, 
                  IFNULL(t2.serie,t1.serie) AS serie, 
                  IFNULL(t2.preferente,t1.preferente) AS preferente, 
                  IFNULL(t2.Tipo_Contrato,t1.Tipo_Contrato) AS Tipo_Contrato, 
                  IFNULL(t2.Tipo_Producto,t1.Tipo_Producto) AS Tipo_Producto,           
                  IFNULL(t2.ubicacion,t1.ubicacion) AS ubicacion, 
                  IFNULL(t2.RangoPermanencia, t1.RangoPermanencia) AS RangoPermanencia, 
                  IFNULL(t2.Sexo, t1.Sexo) AS Sexo, 
                  t2.Cotizantes, 
                  t2.Cargas, 
                  t2.Costo_final, 
                  t2.Total_Ingreso, 
                  t2.INGRESO, 
                  t2.IVA, 
                  SUM(t1.Gasto) AS Gasto, 
                  SUM(t1.HOSPITALARIO) AS HOSPITALARIO, 
                  SUM(t1.AMBULATORIO) AS AMBULATORIO, 
                  SUM(t1.GES) AS GES, 
                  SUM(t1.CAEC) AS CAEC, 
                  SUM(t1.PHARMA) AS PHARMA, 
                  SUM(t1.IVA_RECUPERADO) AS IVA_RECUPERADO, 
                  SUM(t1.LCC) AS LCC, 
                  SUM(t1.RECUPERACION_GASTO) AS RECUPERACION_GASTO
            FROM mm_margen_gasto t1
            FULL JOIN mm_margen_ingreso t2  
               ON (t1.cod_categoria = t2.cod_categoria) 
               		AND (t1.Periodo = t2.Periodo) 
                    AND (t1.categoria_gls = t2.categoria_gls) 
                    AND (t1.serie = t2.serie) 
                    AND (t1.ubicacion = t2.ubicacion) 
                    AND (t1.preferente = t2.preferente) 
                    AND (t1.Tipo_Producto = t2.Tipo_Producto) 
                    AND  (t1.Tipo_Contrato = t2.Tipo_Contrato) 
                    AND (t1.RangoPermanencia = t2.RangoPermanencia)  
                    AND (t1.Sexo = t2.Sexo)
              GROUP BY t2.Periodo,
                       t1.Periodo,
                       t2.cod_categoria, 
                       t1.cod_categoria,
                       t2.categoria_gls,
                       t1.categoria_gls,
                       t2.LINEA_PLAN, t1.LINEA_PLAN,
                       t2.serie,t1.serie,
                       t2.preferente, t1.preferente,
                       t2.Tipo_Contrato, t1.Tipo_Contrato,
                       t2.Tipo_Producto, t1.Tipo_Producto,
                       t2.ubicacion, t1.ubicacion,
                       t2.RangoPermanencia,t1.RangoPermanencia,
                       t2.Sexo, t1.Sexo,
                       t2.Cotizantes,
                       t2.Cargas,
                       t2.Costo_final,
                       t2.Total_Ingreso,
                       t2.INGRESO,
                       t2.IVA
     -- TERMINA INGRESOS VS GASTOS --------------
  ) t1
)
SELECT 
	* 
FROM fase_union
ORDER BY periodo
--LIMIT 100
)






CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TEST_MATRIZ_MARGEN AS (  
WITH mm_permanencia AS (
SELECT DISTINCT 
    rut_todo AS RUT_TITULAR,
    "CONSEXO" AS SEXO,
    (case 
            when t1.antiguedad between 0 and 12 then '00 a 12'
            when t1.antiguedad between 13 and 24 then '13 a 24'
            when t1.antiguedad between 25 and 36 then '25 a 36'
            when t1.antiguedad between 37 and 48 then '37 a 48'
            when t1.antiguedad between 49 and 60 then '49 a 60'
            when t1.antiguedad between 61 and 72 then '61 a 72'
            when t1.antiguedad between 73 and 84 then '73 a 84'
            when t1.antiguedad between 85 and 96 then '85 a 96'
            when t1.antiguedad between 97 and 108 then '97 a 108'
            when t1.antiguedad between 109 and 120 then '109 a 120'
            when t1.antiguedad between 121 and 240 then '121 a 240'
            when t1.antiguedad between 241 and 360 then '241 a 360'
         else '361 y m�s'   
     end) AS RangoPermanencia         
FROM (    
     ----------- inicia calculo antiguedad ------------------
      SELECT 
          	TODO.max_periodo AS PERVIG, 
          	TODO.rut_todo, 
          	A.FECHA_INGRESO,  
          	IFF(A.FECHA_INGRESO = A.FECHA_INGRESO_ISAPRE AND A.tipo_transaccion in ('VN','CT'), 'S','' ) AS ENCONTRADO,
           	IFF(A.FECHA_INGRESO = A.FECHA_INGRESO_ISAPRE AND A.tipo_transaccion in ('VN','CT'), todo.min_periodo,null ) AS perdesde,
           	IFF(ENCONTRADO= 'S', datediff(month, TO_DATE(perdesde || '01', 'yyyymmdd'), TO_DATE(PERVIG || '01', 'yyyymmdd'))+1,
           	datediff(month, add_months(A.FECHA_INGRESO,2), TO_DATE(PERVIG || '01', 'yyyymmdd') )+1) AS ANTIGUEDAD,
           	CON."CONSEXO"
      FROM(
            ------ INICIA TODO --------------    
            SELECT 
                max(periodo_vigencia) AS max_periodo,
                min(periodo_vigencia) AS min_periodo,
                rut_titular as rut_todo
            FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO"
            GROUP BY rut_titular 
            ------ TERMINA TODO --------------    
           ) TODO
      LEFT OUTER JOIN "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" A ON TODO.max_periodo = A.PERIODO_VIGENCIA AND TODO.rut_todo = A.RUT_TITULAR
      LEFT OUTER JOIN (select 
      						"CONRUTBEN", 
      						MIN("CONSEXO") AS consexo 
      					from  "AFI"."P_DDV_AFI"."P_DDV_AFI_CON"
          				GROUP BY  "CONRUTBEN"
          			  ) CON  ON TODO.rut_todo = CON."CONRUTBEN"
      ----------- termina calculo antiguedad ------------------
  ) t1
)
,
mm_base_gasto AS ( 
Select    s2.periodo::INT AS PERIODO,
		  S2.rut_titular,
          s2.cod_categoria, 
          s2.cod_sucursal, 
          s2.RangoPermanencia, 
          s2.sexo, 
          ifnull((SUM(s2.MONTO)),0) AS MONTO, 
          s2.tipo_prestador    
from (
   ------- COMIENZA  CAEC  ----------------------
         SELECT 
                t1.periodo::INT AS PERIODO,
                t2.rut_titular,
                t1.codcateg AS cod_categoria,
                t1.codsuc AS cod_sucursal,
                t2.RangoPermanencia,
                t2.sexo, 
                (SUM(t1.monto_caec)) AS MONTO,            
                (CASE             
                    WHEN t1.periodo > 0 then 'CAEC'            
                END) AS tipo_prestador
        FROM   "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS" t1
            LEFT JOIN mm_permanencia t2 ON (t1.rutafi = t2.rut_titular)
        WHERE t1.periodo BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                             and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
          AND t1.monto_caec !=  0
        GROUP BY t1.periodo,
        		 t2.rut_titular,
	             t1.codcateg,
	             t1.codsuc,
	             t2.RangoPermanencia,
	             t2.SEXO
  ------- TERMINA  CAEC  ----------------------    
    UNION ALL 
  -------------- INICIA SALUD -----------------
    SELECT 
        t1.periodo::INT AS PERIODO,
        t2.rut_titular,
        t1.codcateg  AS cod_categoria,
        t1.codsuc  AS cod_sucursal,
        t2.RangoPermanencia,
        t2.sexo,  
        SUM(t1.mtobonificado) AS MONTO,
        (CASE 
                WHEN t1.tipoprestacion = 'A20' THEN 'HOSPITALARIO'
                WHEN CONTAINS(t1.tipoprestacion,'A') THEN 'AMBULATORIO'
                WHEN  CONTAINS(t1.tipoprestacion, 'GM') OR CONTAINS (t1.tipoprestacion,'GD') OR CONTAINS(t1.tipoprestacion,'GF') THEN 'GES'
                WHEN CONTAINS(t1.tipoprestacion,'GC') THEN 'CAEC'
                WHEN  CONTAINS ( t1.tipoprestacion,'H') OR CONTAINS(t1.tipoprestacion,'M') THEN 'HOSPITALARIO'
                ELSE 'HOSPITALARIO'
            END) AS tipo_prestador
        FROM "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS"  t1
        LEFT JOIN mm_permanencia t2 
                ON (t1.rutafi = t2.rut_titular)  
        WHERE t1.periodo BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                             and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY   t1.periodo,
        		   t2.rut_titular,
                   t1.codcateg,
                   t1.codsuc,
                   t2.RangoPermanencia,
                   t2.sexo,
                    t1.tipoprestacion
    -------------- TERMINA SALUD -----------------
    UNION ALL  
  -------- INICIA LCC ------------------------
  SELECT 
    S1.PERIODOPAGO AS periodo,
    t2.rut_titular,
    S1.CODCATEGORIA AS cod_categoria,
    S1.SUCURSALCONTRATO AS cod_sucursal,
    t2.RangoPermanencia, 
    t2.sexo,   
    SUM(s1.valorbonificado) AS MONTO,
    'LCC'  AS tipo_prestador 
FROM "LCC"."P_DDV_LCC"."P_DDV_LCC_V_LIQ"  s1
LEFT JOIN mm_permanencia  t2  ON ( s1.rut_titular = t2.rut_titular)    
WHERE S1.PERIODOPAGO BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                         and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT AND TIPOLICENCIA IN (1,7) and CODIGOESTADO in (3,4)
GROUP BY 	S1.PERIODOPAGO,
			t2.rut_titular,
			S1.CODCATEGORIA,
			S1.SUCURSALCONTRATO,
			t2.RangoPermanencia,
			t2.sexo,  
			s1.sucursalpago  
  -------- TERMINA LCC ------------------------ 
    UNION ALL
  ------------ comienza IVA RECUPERADO ------------------
        SELECT 
            t1.periodo,
            t2.rut_titular,
            t1.codcateg  AS cod_categoria,
            t1.codsuc  AS cod_sucursal,
            t2.RangoPermanencia,
            t2.sexo, 
            (SUM(t1.monto_credito_fiscal)*-1) AS MONTO,
            (case            
            when t1.periodo > 0 then 'IVA_RECUPERADO' end) AS tipo_prestador
        FROM "CON"."P_DDV_CON"."P_DDV_CON_IVA_RECUPERADO_BONOS" t1
            LEFT JOIN mm_permanencia  t2 ON (t1.rut = t2.rut_titular)
        WHERE t1.periodo BETWEEN TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                    and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY t1.periodo,
        		 t2.rut_titular,
                 t1.codcateg,
                 t1.codsuc,
                 t2.RangoPermanencia,
                 t2.sexo
  ------------ TERMINA IVA RECUPERADO ------------------
    UNION ALL
  ------------ comienza RECUPERACION GASTO------------------
         SELECT t1."periodo_vig" AS periodo,
         		t2.rut_titular,
	            t1."cod_categoria" AS cod_categoria,
	            t1."cod_sucursal" AS cod_sucusral,
	            t2.RangoPermanencia,
	             t2.sexo, 
	            ((SUM(t1."monto_recuperado"))*-1) AS MONTO,
	            (case           
	            when t1."periodo_vig" > 0 then 'RECUPERACION GASTO' end) AS tipo_prestador
	        FROM "FIN"."P_DDV_FIN"."P_DDV_FIN_RECUPERACION_GASTO_NEW"  t1
            LEFT JOIN mm_permanencia t2 ON (t1."rut_titular" = t2.rut_titular)
        WHERE t1."periodo_vig" BETWEEN  TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT 
                                   and  TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY   t1."periodo_vig",
        		   t2.rut_titular,
	               t1."cod_categoria",
	               t1."cod_sucursal",
	               t2.RangoPermanencia,
	               t2.sexo
  ------------ TERMINA RECUPERACION GASTO------------------
    UNION ALL 
   ------------ comienza CONTRATO------------------
        SELECT  t1.periodo_vigencia AS periodo,
        		t2.rut_titular,
	            t1.cod_categoria,
	            t1.cod_sucursal,
	            t2.RangoPermanencia,
	            t2.sexo, 
	            (SUM(t1.gasto_pharma_pesos)) AS MONTO,
	            (case            
	            when t1.periodo_vigencia > 0 then 'PHARMA' end) AS tipo_prestador
        FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t1
            LEFT JOIN mm_permanencia  t2 ON (t1.rut_titular = t2.rut_titular)
        WHERE t1.periodo_vigencia BETWEEN   TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT
                                        AND TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY   t1.periodo_vigencia,
        		   t2.rut_titular,
	               t1.cod_categoria,
	               t1.cod_sucursal,
	               t2.RangoPermanencia,
	               t2.SEXO
    ) s2
group by 
               s2.periodo,
               s2.rut_titular,
               s2.cod_categoria,
               s2.cod_sucursal,
               s2.RangoPermanencia,
               s2.sexo,
               s2.tipo_prestador
)
, 
mm_margen_gasto AS ( 
SELECT 
  t1.periodo AS periodo,
  t1.rut_titular,
  t1.cod_categoria, 
  t3.categoria_gls, 
  (case
  when t1.RangoPermanencia = '' then '00 a 12' else t1.RangoPermanencia end) AS RangoPermanencia, 
  t1.ubicacion, 
  t1.Sexo, 
  t3.tipo_plan AS serie, 
  t2.DETALLE_PRODUCTO  AS preferente, 
  t2.TIPO_PLAN AS Tipo_Contrato, 
  t2.TIPO_PRODUCTO AS Tipo_Producto, 
  t2.LINEA_PLAN, 
  t1.Gasto, 
  t1.HOSPITALARIO, 
  t1.AMBULATORIO, 
  t1.GES, 
  t1.CAEC, 
  t1.PHARMA, 
  t1.IVA_RECUPERADO, 
  t1.LCC, 
  t1.RECUPERACION_GASTO
FROM (
---------------  inicia GASTOS SUCURSAL-------------------
      SELECT 
        s1.periodo,
        s1.rut_titular,
        s1.cod_categoria,
        s1.RangoPermanencia,
        t2."UBICACION" as ubicacion,
        s1.sexo,
       (IFNULL(s1.PHARMA,0) + IFNULL(s1.GES ,0) + IFNULL(s1.IVA_RECUPERADO,0)
        + IFNULL(s1.AMBULATORIO,0) + IFNULL(s1.HOSPITALARIO,0) + IFNULL(s1.LCC,0)
        + IFNULL(s1.CAEC,0) + IFNULL(s1.RECUPERACION_GASTO,0)) AS Gasto,
        IFNULL(s1.HOSPITALARIO,0) AS HOSPITALARIO,
        IFNULL(s1.AMBULATORIO,0) AS AMBULATORIO,
        IFNULL(s1.GES ,0)  AS GES,
        IFNULL(s1.CAEC,0)  AS CAEC,
        IFNULL(s1.PHARMA,0)   AS PHARMA,
        IFNULL(s1.IVA_RECUPERADO,0)  AS IVA_RECUPERADO,
        IFNULL(s1.LCC ,0)  AS LCC,
        IFNULL(s1.RECUPERACION_GASTO,0) AS RECUPERACION_GASTO
      FROM
      ( 
      ----- INICIA TRANSPOSICION DE GASTOS-----------------------
          SELECT  PERIODO, 
          		  RUT_TITULAR,
          		  COD_CATEGORIA, 
          		  COD_SUCURSAL, 
          		  RANGOPERMANENCIA, 
          		  sexo,
		          ifnull("'IVA_RECUPERADO'",0) AS IVA_RECUPERADO,
		          ifnull("'RECUPERACION GASTO'",0) AS RECUPERACION_GASTO,
		          ifnull("'PHARMA'",0) AS PHARMA,
		          ifnull("'GES'",0) AS GES,
		          ifnull("'AMBULATORIO'",0) AS AMBULATORIO,
		          ifnull("'CAEC'",0) AS CAEC,
		          ifnull("'LCC'",0) AS LCC,
		          ifnull("'HOSPITALARIO'",0) AS HOSPITALARIO
          FROM  mm_base_gasto
          pivot(sum(monto) for TIPO_PRESTADOR  in ('IVA_RECUPERADO', 'RECUPERACION GASTO', 'PHARMA', 'GES', 'AMBULATORIO', 'CAEC','LCC', 'HOSPITALARIO')
          )  as p
          ----- TERMINA TRANSPOSICION DE GASTOS-----------------------
      )  s1
      LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_SUCURSAL2" t2  ON(s1.cod_sucursal = t2."COD_SUCURSAL")
----- termina GASTOS SUCURSAL 
)
t1
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_PRM_AGRUPA_PLANES" t2     ON (t1.cod_categoria = t2."cod_categoria")
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA" t3 		 ON (t1.cod_categoria = t3.cod_categoria)
)
, 
mm_base_ingreso AS ( 
   ----- INICIA INGRESOS ---------------------
SELECT 
		t1.Periodo::INT AS PERIODO,
		t1.rut_titular,
        t1.cod_categoria,
        ((SUM(t1.pagado_total_pesos))-(SUM(t1.monto_exceso))-(SUM(t1.monto_excedente))) AS Total,
        t2."UBICACION" as ubicacion,
        t1.RangoPermanencia,
         t1.Sexo, 
        (SUM(t1.Cotizantes)) AS Cotizantes,
        (SUM(t1.Cargas)) AS Cargas,
       IFNULL((SUM(t1.uf_costo_final)),0) AS costo_final,
        case when t1.Periodo >0 then 'INGRESO'
        end AS tipo_registro
    FROM
        (
          ----- INICIA CONTRATO -------------
         SELECT 
            t1.periodo_vigencia AS Periodo,
          	t1.rut_titular,
            t1.cod_categoria,
            t1.cod_sucursal,
            ((SUM(t1.pagado_total_pesos))-(SUM(t1.monto_exceso))-(SUM(t1.monto_excedente))) AS Total,
            (SUM(t1.pagado_total_pesos)) AS pagado_total_pesos,
            (SUM(t1.monto_exceso))  AS monto_exceso,
            (SUM(t1.monto_excedente)) AS monto_excedente,
            (COUNT(DISTINCT(t1.rut_titular))) AS Cotizantes,
            (SUM(t1.cnt_carfam))  AS Cargas,
            t2.RangoPermanencia,
            t2.sexo, 
            (SUM(t1.costo_final_uf))  AS uf_costo_final
        FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t1
            LEFT OUTER JOIN mm_permanencia  t2
            ON (t1.rut_titular = t2.rut_titular)
        WHERE  t1.periodo_vigencia BETWEEN TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT
                                       AND TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
        GROUP BY   t1.periodo_vigencia,
        		   t1.rut_titular,
                   t1.cod_categoria,
                   t1.cod_sucursal,
                   t2.RangoPermanencia,
                   t2.Sexo
            )
    t1
        LEFT OUTER JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_SUCURSAL2" t2 
    ON(t1.cod_sucursal = t2."COD_SUCURSAL")
    GROUP BY       t1.Periodo,
                   t1.cod_categoria,
                   t1.rut_titular,
                   t2."UBICACION",
                   t1.RangoPermanencia,
                   t1.sexo          
UNION ALL
------- INICIA IVA-------------
  SELECT 
  		  t1.Periodo AS Periodo,
  		  t1.rut_titular,
          t1.cod_categoria, 
          ((SUM(t1.Valor_Iva))*-1) AS Total, 
          t2."UBICACION" as ubicacion, 
          t1.RangoPermanencia, 
          t1.Sexo, 
           0  AS cotizantes,
           0  AS cargas,
           0  AS costo_final,
            case when t1.Periodo > 0 then 'IVA' end AS tipo_registro
      FROM
          (
     ------- INICIA LIBRO DE VENTAS -------------------
            SELECT 
            	t1.perpag_esperado AS Periodo,
            	t2.rut_titular,
                t1.cod_categoria, 
                t1.cod_sucursal, 
                (SUM(t1.Valor_Iva))  AS Valor_Iva, 
                t2.RangoPermanencia,
                t2.sexo
            FROM "RCD"."P_DDV_RCD"."P_DDV_RCD_V_LIBRO_VENTAS" t1
            LEFT JOIN  mm_permanencia t2 ON (t1.Rut_Cotizante = t2.rut_titular)
            WHERE  t1.perpag_esperado BETWEEN TO_VARCHAR( dateadd(month,-12,CURRENT_DATE()),'yyyymm')::INT
                                                AND TO_VARCHAR( dateadd(month,-1,CURRENT_DATE()),'yyyymm')::INT
            AND t1.Codigo_Pago = 0
            GROUP BY t1.perpag_esperado,
            		 t2.rut_titular,
		             t1.cod_categoria,
		             t1.cod_sucursal,
		             t2.RangoPermanencia,
		             t2.sexo
            ------ TERMINA LIBRO DE VENTAS ----------------
            ) t1
           LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_SUCURSAL2" t2 
           ON (t1.cod_sucursal = t2."COD_SUCURSAL")
      GROUP BY t1.Periodo,
      		   t1.rut_titular,
               t1.cod_categoria,
               t2."UBICACION",
               t1.RangoPermanencia,
               t1.Sexo
--------------------- termina ingresos----------------
)
, mm_margen_ingreso AS ( 
-- ----- INICIA INGRESOS--------------
SELECT 
    t1.Periodo,
    t1.rut_titular,
    t1.cod_categoria, 
    t2.categoria_gls, 
    t2.tipo_plan AS serie, 
    t3.DETALLE_PRODUCTO AS preferente, 
    t3.TIPO_PLAN AS Tipo_Contrato, 
    t3.TIPO_PRODUCTO AS Tipo_Producto, 
    t1.ubicacion, 
    t1.RangoPermanencia, 
    t1.Sexo, 
    t1.Cotizantes, 
    t1.Cargas, 
    t1.Costo_final, 
    t3.LINEA_PLAN, 
    (SUM(t1.Total)) AS Total_ingreso, 
    (SUM(t1.INGRESO)) AS INGRESO, 
    (SUM(t1.IVA)) AS IVA
FROM 
(
          ----------- INICIA SUM DE INGRESOS ------------
          SELECT 
	           t1.Periodo,
	           t1.rut_titular,
	           t1.cod_categoria,
	           t1.ubicacion,
	           case  
	           		when t1.RangoPermanencia = '' then '00 a 12' else t1.RangoPermanencia 
	           end AS RangoPermanencia,
	           t1.SEXO, 
	           SUM(t1.INGRESO)+SUM(t1.IVA)  AS Total,
	           SUM(t1.Cotizantes) AS Cotizantes,
	           SUM(t1.Cargas) AS Cargas,
	           SUM(t1.INGRESO)+SUM(t1.IVA) AS Costo_final,
	           SUM(t1.IVA) AS IVA,
	           SUM(t1.INGRESO) AS INGRESO
          FROM
          (  
                -------- INICIA TRANSPOSICION DE INGRESOS--------------------
                SELECT 
                	PERIODO,
                	RUT_TITULAR,
                	COD_CATEGORIA, 
                	costo_final, 
                	UBICACION, 
                	RANGOPERMANENCIA, 
                	COTIZANTES, 
                	CARGAS, 
                	SEXO,
                	ifnull("'INGRESO'",0) AS INGRESO,
                	ifnull("'IVA'",0) AS IVA
                FROM mm_base_ingreso
                pivot(sum(TOTAL) for TIPO_REGISTRO  in ('INGRESO', 'IVA')
                )  as p    
                -------- TERMINA TRANSPOSICION DE INGRESOS--------------------
          )  t1
          GROUP BY t1.Periodo,
          		   t1.rut_titular,
		           t1.cod_categoria,
		           t1.ubicacion,
		           t1.RangoPermanencia,
		           t1.Sexo         
         ----------- TERMINA SUM DE INGRESOS ------------
)   t1
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA" t2 ON (t1.cod_categoria = t2.cod_categoria)
LEFT JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_PRM_AGRUPA_PLANES" t3 ON (t1.cod_categoria = t3."cod_categoria") -- "IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES"  t3 
GROUP BY t1.Periodo,
		 t1.rut_titular,
		 t1.cod_categoria,
		 t2.categoria_gls,
		 t2.tipo_plan,
		 t3.DETALLE_PRODUCTO,
		 t3.TIPO_PLAN,
		 t3.TIPO_PRODUCTO,
		 t1.ubicacion,
		 t1.RangoPermanencia,
		 t1.Sexo,
		 t1.Cotizantes,
		 t1.Cargas,
		 t1.Costo_final,
		 t3.LINEA_PLAN 
)
, fase_union AS
( 
SELECT    TO_VARCHAR(DATEADD(month,-1,CURRENT_DATE) ,'yyyymm')::int AS periodo_procesamiento,
          t1.Periodo:: INT AS PERIODO,
          t1.rut_titular,
          t1.cod_categoria, 
          t1.categoria_gls, 
          t1.Linea_Plan, 
          t1.serie, 
          t1.preferente AS PRODUCTO, 
          t1.Tipo_Contrato as TIPO_PLAN, 
          t1.Tipo_Producto, 
          t1.ubicacion,
           case when t1.RangoPermanencia is null then 'Sin informaci�n' else t1.RangoPermanencia end  AS RangoPemanencia,
          t1.Sexo,
          t1.Cotizantes, 
          t1.Cargas,
          t1.INGRESO as Total_Ingreso,       
          t1.Total_Ingreso AS INGRESO, 
          t1.IVA AS IVA, 
          t1.Costo_final, 
          t1.Gasto AS Gasto_total, 
          t1.HOSPITALARIO AS Gasto_HOSPITALARIO, 
          t1.AMBULATORIO AS Gasto_AMBULATORIO, 
          t1.GES AS Gasto_GES, 
          t1.CAEC AS Gasto_CAEC, 
          t1.PHARMA AS Gasto_PHARMA, 
          t1.IVA_RECUPERADO AS IVA_RECUPERADO, 
          t1.LCC AS Gasto_LCC, 
          t1.RECUPERACION_GASTO AS RECUPERACION_GASTO, 
          IFF(IFNULL((t1.Cotizantes + t1.Cargas),0) !=0,  t1.Costo_final/(t1.Cotizantes + t1.Cargas),0)  AS "Pactada/Beneficiario",          
          case 
            when t1.RangoPermanencia is null then 14
            when t1.RangoPermanencia='00 a 12' then 1
            when t1.RangoPermanencia='13 a 24' then 2
            when t1.RangoPermanencia='25 a 36' then 3
            when t1.RangoPermanencia='37 a 48' then 4
            when t1.RangoPermanencia='49 a 60' then 5
            when t1.RangoPermanencia='61 a 72' then 6
            when t1.RangoPermanencia='73 a 84' then 7
            when t1.RangoPermanencia='85 a 96' then 8
            when t1.RangoPermanencia='97 a 108' then 9
            when t1.RangoPermanencia='109 a 120' then 10
            when t1.RangoPermanencia='121 a 240' then 11
            when t1.RangoPermanencia='241 a 360' then 12
            when t1.RangoPermanencia='361 y m�s' then 13
            when t1.RangoPermanencia='Sin informaci�n' then 14
            else 14
            end
            AS Permanencia,  
          case 
        	when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <2 then 1
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND  (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=2 and (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <3  then 2
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND  (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=3 and (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) <4  then 3
            when IFNULL((t1.Cotizantes+t1.Cargas),0) !=0 AND (t1.Costo_final/IFNULL((t1.Cotizantes+t1.Cargas),0)) >=4 then 4
            else 5
          end AS ordenPactada
FROM (
        -- inicia INGRESOS VS GASTOS --------------
          SELECT DISTINCT 
                  IFNULL(t2.Periodo, t1.Periodo) AS Periodo,
                  IFNULL(t2.rut_titular, t2.rut_titular) AS rut_titular, 
                  IFNULL(t2.cod_categoria,t1.cod_categoria) AS cod_categoria, 
                  IFNULL(t2.categoria_gls,t1.categoria_gls) AS categoria_gls, 
                  IFNULL( t2.LINEA_PLAN,t1.LINEA_PLAN) AS Linea_Plan, 
                  IFNULL(t2.serie,t1.serie) AS serie, 
                  IFNULL(t2.preferente,t1.preferente) AS preferente, 
                  IFNULL(t2.Tipo_Contrato,t1.Tipo_Contrato) AS Tipo_Contrato, 
                  IFNULL(t2.Tipo_Producto,t1.Tipo_Producto) AS Tipo_Producto,           
                  IFNULL(t2.ubicacion,t1.ubicacion) AS ubicacion, 
                  IFNULL(t2.RangoPermanencia, t1.RangoPermanencia) AS RangoPermanencia, 
                  IFNULL(t2.Sexo, t1.Sexo) AS Sexo, 
                  t2.Cotizantes, 
                  t2.Cargas, 
                  t2.Costo_final, 
                  t2.Total_Ingreso, 
                  t2.INGRESO, 
                  t2.IVA, 
                  SUM(t1.Gasto) AS Gasto, 
                  SUM(t1.HOSPITALARIO) AS HOSPITALARIO, 
                  SUM(t1.AMBULATORIO) AS AMBULATORIO, 
                  SUM(t1.GES) AS GES, 
                  SUM(t1.CAEC) AS CAEC, 
                  SUM(t1.PHARMA) AS PHARMA, 
                  SUM(t1.IVA_RECUPERADO) AS IVA_RECUPERADO, 
                  SUM(t1.LCC) AS LCC, 
                  SUM(t1.RECUPERACION_GASTO) AS RECUPERACION_GASTO
            FROM mm_margen_gasto t1
            FULL JOIN mm_margen_ingreso t2  
               ON (t1.cod_categoria = t2.cod_categoria) 
               		AND (t1.Periodo = t2.Periodo) 
               		AND (t1.rut_titular =t2.rut_titular)
                    AND (t1.categoria_gls = t2.categoria_gls) 
                    AND (t1.serie = t2.serie) 
                    AND (t1.ubicacion = t2.ubicacion) 
                    AND (t1.preferente = t2.preferente) 
                    AND (t1.Tipo_Producto = t2.Tipo_Producto) 
                    AND  (t1.Tipo_Contrato = t2.Tipo_Contrato) 
                    AND (t1.RangoPermanencia = t2.RangoPermanencia)  
                    AND (t1.Sexo = t2.Sexo)
              GROUP BY t2.Periodo,
                       t1.Periodo,
                       t2.rut_titular,
                       t1.rut_titular,
                       t2.cod_categoria, 
                       t1.cod_categoria,
                       t2.categoria_gls,
                       t1.categoria_gls,
                       t2.LINEA_PLAN, t1.LINEA_PLAN,
                       t2.serie,t1.serie,
                       t2.preferente, t1.preferente,
                       t2.Tipo_Contrato, t1.Tipo_Contrato,
                       t2.Tipo_Producto, t1.Tipo_Producto,
                       t2.ubicacion, t1.ubicacion,
                       t2.RangoPermanencia,t1.RangoPermanencia,
                       t2.Sexo, t1.Sexo,
                       t2.Cotizantes,
                       t2.Cargas,
                       t2.Costo_final,
                       t2.Total_Ingreso,
                       t2.INGRESO,
                       t2.IVA
     -- TERMINA INGRESOS VS GASTOS --------------
  ) t1
)
SELECT 
	* 
FROM fase_union
ORDER BY periodo, rut_titular
--LIMIT 100
)


