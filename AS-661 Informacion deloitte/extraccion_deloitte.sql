            
------EXTRACCION INFORMACION DELOITTE 
----------EMPLEADORES UNICOS POR RUT
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.temp_emp AS ( 
SELECT DISTINCT 
--"fld_cotrut" , --as rut_cotizante
"fld_emprut", --AS rut_empleador	
"fld_funnotiffun", --AS  contrato,
"fld_funcorrel" --AS correlativo,	
FROM AFI.P_RDV_DST_LND_SYB.COB
order by "fld_emprut" 
);
----- replicando empleadores
CREATE OR REPLACE TEMPORARY TABLE est.P_DDV_EST.ca_emp AS (
WITH empleadores AS (
SELECT
--"fld_cotrut",
"fld_emprut",
"fld_funnotiffun",
"fld_funcorrel",
ROW_NUMBER () OVER (PARTITION BY "fld_funnotiffun","fld_funcorrel" ORDER BY "fld_funnotiffun","fld_funcorrel") AS n_veces
FROM EST.P_DDV_EST.temp_emp
)
SELECT fld_funnotiffun,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4
FROM empleadores 
pivot(max("fld_emprut") FOR n_veces IN (1,2,3,4))
AS P (fld_funnotiffun,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4));
-------------- GASTOS AMBULATORIS Y HOSPITALARIOS 
WITH resultado AS ( 
SELECT 
--PERIODO ,
gto.FUENTE_ORIGEN ,
GTO.ORIGEN,
gto.FOLIO AS idn,
gto.RUT_TITULAR AS rut_afiliado ,
gto.tipo_bono,
--gto.RUT_BENEFICIARIO ,
COALESCE (benv2."fld_benrut",ben."fld_benrut",gto.RUT_BENEFICIARIO) AS ruts_beneficiarios,
gto.NOMBRE_INSTITUCION AS nombre_prestador ,
gto.RUT_INSTITUCION AS rut_prestador,
emp.empleador_1 AS rut_empleador ,
sum(gto.COBRADO)::int AS cobrad,
sum(gto.BONIFICADO + gto.MONTO_CAEC)::int AS boni 
--sum(MONTO_CAEC)
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO gto
LEFT JOIN est.P_DDV_EST.ca_emp emp ON (SUBSTRING(gto.CONTRATO,0,9)::int = emp.fld_funnotiffun AND gto.CORRELATIVO = emp.fld_funcorrel)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN ben 	ON (gto.RUT_TITULAR = ben."fld_cotrut" AND gto.COD_BENEFICIARIO = ben."fld_bencorrel" AND gto.PERIODO BETWEEN ben.FLD_BENVIGREALDESDE AND ben.FLD_BENVIGREALHASTA)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN benv2 	ON (SUBSTRING(gto.CONTRATO,0,9)::int=benv2."fld_funfolio" AND gto.CORRELATIVO=benv2."fld_funcorrel" AND gto.COD_BENEFICIARIO = benv2."fld_bencorrel"  )
WHERE gto.FUENTE_ORIGEN NOT IN ('LIQ') AND --orige = 'HOSPITALARIO' and
gto.PERIODO BETWEEN 202101 AND  202106
GROUP BY idn, gto.FUENTE_ORIGEN , GTO.ORIGEN, TIPO_BONO,  rut_afiliado, RUTs_BENEFICIARIOs , nombre_prestador, rut_prestador, emp.empleador_1 
)
SELECT
IDN AS FOLIO,
RUT_AFILIADO,
RUTS_BENEFICIARIOS,
NOMBRE_PRESTADOR,
RUT_PRESTADOR,
RUT_EMPLEADOR,
COBRAD::INT AS COBRADO,
BONI::INT AS BONIFICADO,
COBRADO-BONIFICADO::INT AS COPAGO
FROM resultado
WHERE TIPO_BONO = 3 --ORIGEN ='GES' AND --rut_afiliado = '011952109-2'  --rut_afiliado = '0010769054-9'-- orige ='AMBULATORIO' 
 ---LIMIT 20
