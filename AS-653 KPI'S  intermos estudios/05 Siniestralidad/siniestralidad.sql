------ REVISANDO CODIGOS CARTERA_BEN Y CONTRATO
SELECT DISTINCT PERIODO 
FROM EST.P_STG_EST.CARTERA_BEN 


SELECT * 
FROM EST.P_STG_EST.CARTERA_BEN
WHERE RUT_TITULAR_CON IS NULL ---= '001773416-4'
LIMIT 10

SELECT PERIODO , SUM(PAGADO) FROM EST.P_STG_EST.CARTERA_BEN
WHERE REGISTRO_ID = 1
GROUP BY PERIODO 
ORDER BY PERIODO 
LIMIT 10


SELECT * FROM EST.P_STG_EST.CARTERA_BEN 
WHERE RUT_TITULAR IS NULL 
LIMIT 10



SELECT * FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
LIMIT 10

SELECT PERIODO_VIGENCIA , sum(PAGADO_TOTAL_PESOS), sum(MONTO_EXCESO), sum(MONTO_EXCEDENTE) FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE PERIODO_VIGENCIA BETWEEN 202104 AND 202109
GROUP BY PERIODO_VIGENCIA 
ORDER BY PERIODO_VIGENCIA 
LIMIT 10



---- chequeando
SELECT 
	ca.PERIODO ,
	ca.RUT_TITULAR ,
	ca.PAGADO ,
	co.RUT_TITULAR ,
	co.PAGADO_TOTAL_PESOS
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO co
LEFT JOIN EST.P_STG_EST.CARTERA_BEN ca  ON (co.RUT_TITULAR = ca.RUT_TITULAR AND co.PERIODO_VIGENCIA = ca.PERIODO  AND ca.REGISTRO_ID NOT IN (1))
WHERE  co.PERIODO_VIGENCIA BETWEEN 202107 AND 202108 AND ca.RUT_TITULAR IS NULL 
ORDER BY ca.RUT_TITULAR DESC  
LIMIT 100





----------tipo trabajador unicos
WITH empleador AS ( 
SELECT DISTINCT 
	--"fld_cotrut" , --as rut_cotizante
	"fld_emprut", --AS rut_empleador	
	"fld_funnotiffun" , --AS  contrato,
	"fld_funcorrel" ,--AS correlativo,
	"fld_cottipotrcod" 
FROM AFI.P_RDV_DST_LND_SYB.COB
order by "fld_funnotiffun", "fld_funcorrel", "fld_emprut"
)
, emptiptrab AS (
SELECT
	--"fld_cotrut",
	"fld_cottipotrcod",
	"fld_funnotiffun",
	"fld_funcorrel",
ROW_NUMBER () OVER (PARTITION BY "fld_funnotiffun","fld_funcorrel" ORDER BY "fld_funnotiffun","fld_funcorrel") AS n_veces
FROM empleador
)
, final_tiptrabajador AS ( 
SELECT 
	fld_funfolio,
	fld_funcorrel,
	trabajador_1,
	trabajador_2,
	trabajador_3,
	trabajador_4
FROM emptiptrab 
pivot(max("fld_cottipotrcod") FOR n_veces IN (1,2,3,4))
AS P (fld_funfolio,fld_funcorrel,trabajador_1,trabajador_2,trabajador_3,trabajador_4) 
)
----- fin tipo trabajador 
SELECT 
	fld_funfolio,
	fld_funcorrel,
	trabajador_1 AS tipo_trabajador
FROM final_tiptrabajador
LIMIT 50



SELECT CONTRATO ,CORRELATIVO , RUT_TITULAR , RUT_BENEFICIARIO , * FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE RUT_TITULAR IN ('009923893-3','011428557-9') AND PERIODO IN (202012 AND 202007)





SELECT * FROM AFI.P_RDV_DST_LND_SYB.COB LIMIT 10


SELECT * FROM isa.P_RDV_DST_LND_SYB.ISAPRETIPTRAB LIMIT 10


SELECT "fld_region" ,* FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE "fld_cotrut" ='010637643-3' --AND "fld_funcorrel" =
LIMIT 10


SELECT periodo_vigencia,count(DISTINCT(rut_titular)) AS cant  FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE periodo_vigencia BETWEEN 202001 AND 202109
GROUP BY periodo_vigencia
ORDER BY periodo_vigencia
--LIMIT 12

SELECT AVG(count(DISTINCT(rut_titular))) AS promedio  FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE periodo_vigencia BETWEEN 202010 AND 202109
--GROUP BY periodo_vigencia
--ORDER BY periodo_vigencia
LIMIT 12




SELECT * FROM gto.P_RDV_DST_LND_SYB.BON LIMIT 100


SELECT DISTINCT periodo, contrato, correlativo, rut_titular, RUT_BENEFICIARIO , DM_HOSPITALARIO ,DM_AMBULATORIO ,DM_GES ,DM_LICENCIA FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE TIPO_PLAN IS NULL OR CATEGORIA IS NULL 
ORDER BY PERIODO 
LIMIT 200


SELECT PERIODO , RUT_BENEFICIARIO , SUM(DM_HOSPITALARIO) FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO = 202107
	--AND  DM_HOSPITALARIO > 0 
	AND RUT_BENEFICIARIO ='001841035-4' 
GROUP BY PERIODO , RUT_BENEFICIARIO
ORDER BY  RUT_BENEFICIARIO , PERIODO DESC 
LIMIT 100



SELECT  
	 PERIODO ,
	 RUT_TITULAR ,
	 RUT_BENEFICIARIO ,
	 COD_BENEFICIARIO ,
	 NUM_CARGAS ,
	 REGISTRO_ID ,
	 VIGENTE ,
	 SUM(DM_HOSPITALARIO) --OVER  (PARTITION BY  RUT_TITULAR,RUT_BENEFICIARIO  ORDER BY PERIODO ASC   ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS GASTO_HOSPI_ACUMULADO
	 FROM est.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202005 AND 202108
	 AND RUT_TITULAR ='001841035-4' 
GROUP BY 
	 PERIODO ,
	 RUT_TITULAR ,
	 RUT_BENEFICIARIO ,
	 COD_BENEFICIARIO ,
	 NUM_CARGAS ,
	 REGISTRO_ID ,
	 VIGENTE 
	-- DM_HOSPITALARIO 
--ORDER BY PERIODO desc
LIMIT 200



WITH
pre AS (
    SELECT DISTINCT CA1.PERIODO,
                    CA1.RUT_TITULAR ,
                    CA1.RUT_BENEFICIARIO
        FROM EST.P_STG_EST.CA_SINIESTRALIDAD CA1       
)
SELECT  CA1.PERIODO,
        CA1.RUT_TITULAR ,
        CA1.RUT_BENEFICIARIO ,
        SUM(CA2.DM_HOSPITALARIO)    AS "CUMSUM_DM_HOS"
    FROM pre CA1
        LEFT JOIN EST.P_STG_EST.CA_SINIESTRALIDAD CA2 ON (  CA1.RUT_TITULAR = CA2.RUT_TITULAR AND
                                                            CA1.RUT_BENEFICIARIO  = CA2.RUT_BENEFICIARIO AND
                                                            CA2.PERIODO BETWEEN EST.P_DDV_EST.PERIODOADD(CA1.PERIODO, -11) AND CA1.PERIODO)
WHERE CA1.RUT_TITULAR = '001841035-4'
GROUP BY    CA1.PERIODO ,
            CA1.RUT_TITULAR ,
            CA1.RUT_BENEFICIARIO 
ORDER BY    CA1.PERIODO DESC ,
            CA1.RUT_BENEFICIARIO 
--LIMIT 100
;




SELECT 
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	SUM(DM_HOSPITALARIO)  
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE RUT_TITULAR = '001841035-4'
GROUP BY PERIODO ,
		 RUT_TITULAR ,
		 RUT_BENEFICIARIO 
ORDER BY PERIODO DESC 
LIMIT 20




SELECT 
	PERIODO , 
	SUM(DM_HOSPITALARIO)
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
GROUP BY PERIODO 
ORDER BY PERIODO DESC 
LIMIT 15




WITH gasto_acu AS ( 
SELECT  
	 --SUM(DM_HOSPITALARIO) OVER  (PARTITION BY   RUT_TITULAR , RUT_BENEFICIARIO ORDER BY PERIODO ASC   ROWS BETWEEN 12 PRECEDING AND CURRENT ROW) AS GASTO_HOSPI_ACUMULADO,{
	 SUM(DM_HOSPITALARIO) OVER  (PARTITION BY   RUT_TITULAR , RUT_BENEFICIARIO ORDER BY PERIODO,RUT_TITULAR , RUT_BENEFICIARIO ASC   ROWS BETWEEN 12 PRECEDING AND CURRENT ROW) AS GASTO_HOSPI_ACUMULADO,
	 SUM(DM_HOSPITALARIO) OVER  (PARTITION BY   RUT_TITULAR , RUT_BENEFICIARIO ORDER BY PERIODO , RUT_TITULAR , RUT_BENEFICIARIO ASC   ROWS BETWEEN 13 PRECEDING AND 1 PRECEDING ) AS GASTO_HOSPI_ACUMULADO_2,
	 *
	 FROM est.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 201810 AND 202109
	 AND RUT_TITULAR ='001841035-4' 
GROUP BY
	 PERIODO ,
	 ESTADO ,
	 PERIODO_FECHA ,
	 PERIODO_ANUAL ,
	 MES ,
	 ANIO,
	 CONTRATO ,
	 CORRELATIVO ,
	 ID_TITULAR ,
	 COD_BENEFICIARIO ,
	 NUM_CARGAS ,
	 TIPO_RUT ,
	 FECHA_INGRESO ,
	 ANTIGUEADAD ,
	 RANGO_ANTIGUEDAD ,
	 SEXO ,
	 EDAD ,
	 RANGO_EDAD ,
	 REGISTRO_ID ,
	 VIGENTE ,
	 COD_REGION ,
	 GLS_REGION ,
	 COD_COMUNA ,
	 GLS_COMUNA ,
	 COD_SUCURSAL ,
	 GLS_SUCUR ,
	 RUT_TITULAR ,
	 RUT_BENEFICIARIO ,
	 UBICACION ,
	 COD_CENTRO_COSTOS ,
	 CENTRO_COSTOS_GLS ,
	 NUM_EMPLEADORES ,
	 ACTIVIDAD ,
	 TIPO_TRABAJADOR ,
	 CATEGORIA ,
	 GLS_CATEGORIA ,
	 FECHA_NACIMIENTO ,
	 TIPO_PLAN ,
	 TIPO_PRODUCTO ,
	 SERIE ,
	 SEGMENTO_PLAN ,
	 SERIE_PLAN ,
	 SERIE_PLAN_2 ,
	 LINEA_PLAN ,
	 LINEA_PLAN_2 ,
	 VIG_INICIO_PLAN ,
	 VIG_FIN_PLAN ,
	 SEG_MUERTE ,
	 PRECIO_BASE ,
	 RANGO_PRECIO ,
	 FACTOR_RIESGO ,
	 COSTO_TOTAL ,
	 COSTO_BENEF_ADIC ,
	 COSTO_FINAL ,
	 RANGO_COTIZACION ,
	 VALOR_UF ,
	 RENTA_IMPONIBLE ,
	 RANGO_RENTA_IMPONIBLE ,
	 AGENCIA_VN ,
	 AGENTE_VN ,
	 CLASIF_RIESGO ,
	 IVA_RECUPERADO ,
	 RECUPERACION_GASTO ,
	 FLD_BENNOMBRE ,
	 FLD_BENAPEPA ,
	 FLD_BENAPEMA ,
	 FLD_FUNFOLIO ,
	 FLD_FUNCORREL ,
	 IVA ,
	 COD_NEGOCIO ,
	 TIPO_COTIZACION ,
	 TOTAL_PAGADO ,
	 EXCESOS ,
	 EXCEDENTES ,
	 GASTO_PHARMA ,
	 DM_AMBULATORIO ,
	 DM_GES ,
	 DM_LICENCIA ,
	 RECAUDADO ,
	 INGRESOS ,
	 DM_HOSPITALARIO 
ORDER BY PERIODO 
--LIMIT 200
)
SELECT
	PERIODO ,
	PERIODO_FECHA ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	SUM(DM_HOSPITALARIO), 
	SUM(GASTO_HOSPI_ACUMULADO),
	SUM(GASTO_HOSPI_ACUMULADO_2)
FROM gasto_acu
--WHERE PERIODO = 202108
GROUP BY 
	PERIODO,
	PERIODO_FECHA ,
	RUT_TITULAR , 
	RUT_BENEFICIARIO 
ORDER BY PERIODO DESC --,RUT_TITULAR ,RUT_BENEFICIARIO
LIMIT 100





SELECT * FROM EST.P_STG_EST.CA_SINIESTRALIDAD LIMIT 100


SELECT PERIODO ,sum(DM_HOSPITALARIO) FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO BETWEEN 202101 AND 202109
GROUP BY PERIODO 
ORDER BY PERIODO 
LIMIT 10


SELECT --PERIODO ,RUT_TITULAR ,RUT_BENEFICIARIO , DM_HOSPITALARIO  
*
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE RUT_TITULAR ='001841035-4'  AND PERIODO = 202102 
ORDER BY PERIODO 
LIMIT 50 


SELECT 
	COUNT(DISTINCT("fld_cotrut")),
	sum("fld_carganum")
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE FLD_VIGREALDESDE <= 202112 AND 
	  FLD_VIGREALHASTA >= 202112
LIMIT 2


SELECT DISTINCT PERIODO , TIPO_RUT, estado,  COUNT(DISTINCT (RUT_TITULAR)) FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO IN (202104)
GROUP BY PERIODO , estado, TIPO_RUT 


SELECT * FROM ISA.P_RDV_DST_LND_SYB.ISAPREREGION 
LIMIT 20

SELECT * FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
ORDER BY PERIODO , RUT_TITULAR , RUT_BENEFICIARIO 
LIMIT 1000


SELECT * FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
LIMIT 10



SELECT 
	PERIODO , 
	--RANGO_COTIZACION ,
	CASE 
		WHEN COSTO_FINAL >= 5.72 THEN '>=5.72 UF'
		ELSE 'Menor'
	END AS pactado,
	COUNT(DISTINCT RUT_TITULAR) AS cant 
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO = 202109 AND ESTADO ='vigente' 
GROUP BY PERIODO , pactado  
LIMIT 500



SELECT 
	PERIODO , 
	RUT_TITULAR ,
	COSTO_FINAL::float   ,
	RANGO_COTIZACION ,
	CASE 
		WHEN COSTO_FINAL >= 5.72 THEN '>=5.72 UF'
		ELSE 'Menor'
	END AS pactado,
	COUNT(DISTINCT RUT_TITULAR) AS cant 
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO = 202109 AND ESTADO ='vigente' AND pactado ='Menor' AND RANGO_COTIZACION ='>12.0 <= 14.0 UF'
GROUP BY PERIODO , RANGO_COTIZACION , pactado ,RUT_TITULAR ,COSTO_FINAL 
ORDER BY COSTO_FINAL desc, RANGO_COTIZACION 
LIMIT 500

SELECT * FROM DTM.P_STG_DTM_GTO.



SELECT 
	PERIODO_VIGENCIA ,
	count(DISTINCT ) 
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO_VIGENCIA = 202110
GROUP BY PERIODO_VIGENCIA 
LIMIT 10


SELECT * FROM EST.P_STG_EST.CA_SINIESTRALIDAD LIMIT 10

SELECT 
	PERIODO ,
	Count(DISTINCT RUT_TITULAR) AS cant_titular,
	sum(NUM_CARGAS) AS cant_carga
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO BETWEEN 202011 AND 202110 AND REGISTRO_ID =1--ESTADO ='vigente'
GROUP BY PERIODO 
--LIMIT 10



SELECT sum(DM_AMBULATORIO_UF+DM_HOSPITALARIO_UF+DM_GES_UF+GASTO_PHARMA_UF+DM_LICENCIA_UF) AS gasto_uf,
	   sum(IVA_RECUPERADO_uf + RECUPERACION_GASTO_uf) AS recupera_uf,
	   gasto_uf-recupera_uf AS total
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202011 AND 202110
--GROUP BY gasto_uf 


SELECT
	(datediff(MONTH, "fld_bennacfec" , getdate()) / 12)::INT AS Edad,
	* 
FROM AFI.P_RDV_DST_LND_SYB.BEN 
LIMIT 10


WITH media AS ( 
SELECT 
	PERIODO ,
	count(DISTINCT RUT_BENEFICIARIO)::int AS cant_benef,
	SUM(DM_HOSPITALARIO_UF)::int AS hospitalario_uf ,
	SUM(DM_AMBULATORIO_UF)::int AS ambulatorio_uf
	--avg()
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO BETWEEN 202011 AND 202110
GROUP BY PERIODO
)
SELECT 
	avg(cant_benef) AS benefi,
	sum(HOSPITALARIO_UF) AS hospita,
	sum(AMBULATORIO_UF) AS ambu,
	hospita/benefi AS hospitalario,
	ambu/benefi AS ambulatorio
FROM media
LIMIT 12


SELECT * FROM EST.P_STG_EST.CA_SINIESTRALIDAD
--WHERE RUT_TITULAR ='015152733-7'
ORDER BY PERIODO 
LIMIT 100


SELECT 
	PERIODO_VIGENCIA , 
	COUNT(DISTINCT RUT_TITULAR),
	sum(PAGADO_TOTAL_PESOS)
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE PERIODO_VIGENCIA >= 201901
GROUP BY PERIODO_VIGENCIA
ORDER BY PERIODO_VIGENCIA 
--LIMIT 10





SELECT 
	PERIODO ,
	ESTADO ,
	count(DISTINCT RUT_TITULAR) AS cant,
	sum(NUM_CARGAS) AS cargas
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO BETWEEN 202012 AND 202111 AND --RANGO_RENTA_IMPONIBLE IN ('sin informaci�n')
	  DETALLE_PRODUCTO IN ('PRF ADR') 
GROUP BY PERIODO ,ESTADO 
LIMIT 12 







SELECT 
	* 
FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CONTRATO ='011298744-4' AND CORRELATIVO  = 3 AND --RUT_TITULAR ='018392422-2' AND 
	  PERIODO IN (201812)
LIMIT 10

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_RESTRINGIDA _res


SELECT DISTINCT 
	DETALLE_PRODUCTO,
	SERIE 
FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES
WHERE DETALLE_PRODUCTO IN ('L.E.','LIBRE ELECCION')
LIMIT 100


SELECT DISTINCT
	PERIODO AS per ,
	TOTAL_PAGADO ,
	IVA ,
	*
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE --CONTRATO ='011298744-4' AND CORRELATIVO = 3 
      RUT_TITULAR ='017774991-5'
	  AND PERIODO >= 202012
	  --AND REGISTRO_ID = 1
--GROUP BY PERIODO , ESTADO 
ORDER BY PERIODO, RUT_TITULAR , RUT_BENEFICIARIO 
--LIMIT 12



SELECT 
	periodo,
	origen,
	sum(cobrado),
	sum(bonificado)
FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE  PERIODO  BETWEEN 202101 AND 202112
GROUP BY periodo, origen
LIMIT 100


SELECT
	* 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE origen = 'LICENCIA'
LIMIT 10


WITH dtm AS ( 
SELECT 
	PERIODO ,
	RUT_TITULAR,
	RUT_BENEFICIARIO,
	IFF(ORIGEN = 'HOSPITALARIO',sum(BONIFICADO + monto_caec),0 ) AS DM_HOSPITALARIO,
	IFF(ORIGEN = 'AMBULATORIO' ,sum(BONIFICADO + monto_caec),0 ) AS DM_AMBULATORIO,
	--IFF(ORIGEN = 'HOSPITALARIO',sum(BONIFICADO ),0 ) AS DM_HOSPITALARIO,
	--IFF(ORIGEN = 'AMBULATORIO' ,sum(BONIFICADO ),0 ) AS DM_AMBULATORIO,
	IFF(ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES
	--sum(MONTO_CAEC) AS DM_CAEC,
	---IFF(ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD)
FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO  BETWEEN 202101 AND 202112
GROUP BY ORIGEN, PERIODO, RUT_TITULAR , RUT_BENEFICIARIO 
) 
, agrupa_dtm AS ( 
SELECT 
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(dm_hospitalario) AS dm_hos,
	sum(dm_ambulatorio) AS dm_ambu,
	sum(dm_ges) AS dm_ges
FROM dtm 
GROUP BY PERIODO , RUT_TITULAR , RUT_BENEFICIARIO 
ORDER BY RUT_TITULAR, PERIODO   DESC
)
, siniestralidad AS ( 
SELECT
	periodo,
	RUT_TITULAR,
	RUT_BENEFICIARIO,
	sum(DM_HOSPITALARIO) AS S_HOSPI,
	sum(DM_AMBULATORIO) AS S_AMBU,
	sum(DM_GES) AS S_GES 
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO BETWEEN 202101 AND 202112
GROUP BY PERIODO , RUT_TITULAR , RUT_BENEFICIARIO 
---LIMIT 12
)
SELECT 
	s.*,
	dtm.*,
	CASE 
		WHEN (dm_hos+dm_ambu+dm_ges)=(S_HOSPI+S_AMBU+S_GES) THEN 'OK'
		ELSE 'NO'
	END AS chek_gastos
FROM agrupa_dtm dtm 
FULL OUTER JOIN siniestralidad s ON (dtm.periodo = s.periodo AND dtm.rut_titular = s.rut_titular AND dtm.rut_beneficiario = s.rut_beneficiario)
WHERE (S.PERIODO =202204 OR DTM.PERIODO =202106) AND  (dtm.RUT_TITULAR ='014055648-3' OR s.RUT_TITULAR ='014055648-3')
--WHERE (S.PERIODO =202109 OR DTM.PERIODO =202109) AND  (dtm.RUT_TITULAR ='005535627-0' OR s.RUT_TITULAR ='005535627-0')
--WHERE (S.PERIODO =202105 OR DTM.PERIODO =202105) AND  (dtm.RUT_TITULAR ='013357391-7' OR s.RUT_TITULAR ='013357391-7')
--WHERE (S.PERIODO =202103 OR DTM.PERIODO =202103) AND  (dtm.RUT_TITULAR ='016653202-7' OR s.RUT_TITULAR ='016653202-7')
--WHERE s.periodo IS NULL
LIMIT 1000




SELECT 
	PERIODO ,
	TIPO_TRABAJADOR ,
	count(DISTINCT RUT_TITULAR) AS cant_rut
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE ESTADO ='vigente' --AND  PERIODO = 202102 AND RUT_TITULAR ='023728342-2'
GROUP BY PERIODO ,TIPO_TRABAJADOR 
ORDER BY PERIODO 
--LIMIT 10




SELECT 
	* 
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
LIMIT 10

SELECT 
	IFF(ORIGEN = 'HOSPITALARIO',sum(BONIFICADO + monto_caec),0 ) AS DM_HOSPITALARIO 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE PERIODO = 202106 AND RUT_TITULAR ='023728342-2'
GROUP BY ORIGEN 
LIMIT 10


SELECT DISTINCT 
	sn.PERIODO ,
	sn.RUT_TITULAR ,
	sn.RUT_BENEFICIARIO,
	sn.DM_HOSPITALARIO ,
	sn.DM_AMBULATORIO ,
	sn.DM_GES ,
	sn.DM_LICENCIA ,
	dtm.PERIODO ,
	dtm.RUT_TITULAR /*,
	dtm.RUT_BENEFICIARIO
	sum(sn.DM_HOSPITALARIO),
	sum(sn.DM_AMBULATORIO),
	sum(sn.DM_GES),
	sum(sn.DM_LICENCIA)*/
FROM EST.P_STG_EST.CA_SINIESTRALIDAD sn
FULL JOIN  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO dtm ON (sn.RUT_TITULAR=dtm.RUT_TITULAR)
WHERE dtm.PERIODO BETWEEN  202101 AND 202112 AND sn.RUT_TITULAR IS NULL --AND sn.RUT_TITULAR ='016653202-7'
LIMIT 100




-------------- DTM VS MMARGEN VS SINIESTRALIDAD
-----------------------------------------------
------ DTM 
WITH dtm AS ( 
SELECT 
	PERIODO ,
	SUBSTRING(PERIODO,0,4) AS anio,  
	--RUT_TITULAR,
	--RUT_BENEFICIARIO,
	IFF(ORIGEN = 'HOSPITALARIO',sum(COBRADO),0 ) AS DM_HOSPITALARIO_cobrado,
	IFF(ORIGEN = 'AMBULATORIO' ,sum(COBRADO),0 ) AS DM_AMBULATORIO_cobrado,
	IFF(ORIGEN = 'GES',sum(COBRADO),0 ) AS DM_GES_cobrado,
	IFF(ORIGEN = 'HOSPITALARIO',sum(BONIFICADO + monto_caec),0 ) AS DM_HOSPITALARIO_boni,
	IFF(ORIGEN = 'AMBULATORIO' ,sum(BONIFICADO + monto_caec),0 ) AS DM_AMBULATORIO_boni,
	IFF(ORIGEN = 'LICENCIA' ,sum(BONIFICADO ),0 ) AS DM_LICENCIA_boni
	--sum(MONTO_CAEC) AS dm_caec
	--IFF(ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
	--SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
	--SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO BETWEEN 202201 AND 202206 --  BETWEEN 202404 AND 202112	
GROUP BY ORIGEN, PERIODO, TIPO_LICENCIA --, RUT_TITULAR , RUT_BENEFICIARIO 
)
SELECT 
	PERIODO ,
	--COUNT(DISTINCT RUT_TITULAR) AS CANT_TITU ,
	--COUNT(DISTINCT RUT_BENEFICIARIO) AS CANT_BENE ,
	anio,
	sum(dm_hospitalario_cobrado) AS dm_hos_cob,
	sum(dm_hospitalario_boni) AS dm_hos_bon,
	sum(dm_ambulatorio_cobrado) AS dm_ambu_cob,
	sum(dm_ambulatorio_boni) AS dm_ambu_bon,
	sum(dm_licencia_boni) AS dm_lic_bon,
	sum(dm_ges_cobrado) AS dm_ges
	--sum(dm_caec)
	--sum(dm_licencia)
FROM dtm 
GROUP BY anio ,PERIODO-- , RUT_TITULAR , RUT_BENEFICIARIO 
ORDER BY anio , PERIODO --RUT_TITULAR, PERIODO   desc
LIMIT 100



------- MATRIZ MARGEN 
SELECT 
	PERIODO_PROCESAMIENTO ,
	PERIODO ,
	sum(GASTO_HOSPITALARIO),
	sum(GASTO_AMBULATORIO),
	sum(GASTO_GES),
	sum(GASTO_CAEC),
	sum(GASTO_LCC),
	SUM(GASTO_TOTAL),
	sum(TOTAL_INGRESO)
FROM EST.P_DDV_EST.P_DDV_EST_MATRIZ_MARGEN
WHERE PERIODO_PROCESAMIENTO = 202204 AND PERIODO = 202204
GROUP BY PERIODO_PROCESAMIENTO , PERIODO
ORDER BY PERIODO DESC 
LIMIT 100



---------- SINIESTRALIDAD
SELECT
	--periodo,
	SUBSTRING(PERIODO,0,4) AS anio, 
	--count(DISTINCT RUT_TITULAR) AS cant_titulares,
	--count(DISTINCT RUT_BENEFICIARIO) AS cant_beneficiarios,
	--sum(INGRESOS) AS ingresos,
	sum(DM_HOSPITALARIO) AS hospi,
	sum(DM_AMBULATORIO) AS ambu,
	sum(DM_GES) AS ges
FROM EST.P_STG_EST.CA_SINIESTRALIDAD
--WHERE PERIODO BETWEEN 201901 AND 201912
GROUP BY SUBSTRING(PERIODO,0,4) --PERIODO
ORDER BY SUBSTRING(PERIODO,0,4) --PERIODO 
--LIMIT 12

SELECT * FROM EST.P_STG_EST.CA_SINIESTRALIDAD LIMIT 10

---------- MATRIZ MARGEN
SELECT 
	--PERIODO ,
	SUM(GASTO_HOSPITALARIO) 
FROM EST.P_DDV_EST.P_DDV_EST_MATRIZ_MARGEN
WHERE PERIODO BETWEEN 202101 AND 202112
--GROUP BY PERIODO 
LIMIT 10







SELECT 
	count("fld_cotrut") AS cantidad,
	"fld_cotrut" 
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
WHERE  lfc."fld_funnotificacod"  = 1 
	and lfc."fld_tpotransac" IN('VN') AND lfc."fld_periodoprod" between '2015-01-01 00:00:00' and '2021-12-31 23:59:59'
GROUP BY "fld_cotrut" 
ORDER BY cantidad desc
LIMIT 10



SELECT DISTINCT 
	ID_TITULAR ,
	RUT_TITULAR 
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE ID_TITULAR IN (327071,3103056,4078821,8673006) 
LIMIT 10




SELECT min(PERIODO),max(PERIODO) FROM EST.P_STG_EST.CA_SINIESTRALIDAD LIMIT 10 


-------- UNION CONTRATO Y DTM
-------- inicio y final del periodo
SET periodo_desde = 201908;
SET periodo_hasta = 202207;
CREATE OR REPLACE TABLE EST.P_STG_EST.CA_SINIESTRALIDAD AS ( 
--------- extraccion de la cartera
----------tipo trabajador unicos
WITH empleador AS ( 
SELECT DISTINCT 
	--"fld_cotrut" , --as rut_cotizante
	"fld_emprut", --AS rut_empleador,
	"fld_funnotiffun" , --AS  contrato,
	"fld_funcorrel" ,--AS correlativo,
	"fld_cottipotrcod" 
FROM AFI.P_RDV_DST_LND_SYB.COB
order by "fld_funnotiffun", 
		 "fld_funcorrel", 
		 "fld_emprut"
)
, emptiptrab AS (
SELECT
	--"fld_cotrut",
	"fld_cottipotrcod",
	"fld_funnotiffun",
	"fld_funcorrel",
ROW_NUMBER () OVER (PARTITION BY "fld_funnotiffun","fld_funcorrel" ORDER BY "fld_funnotiffun","fld_funcorrel") AS n_veces
FROM empleador
),
final_tiptrabajador AS ( 
SELECT 
	fld_funfolio,
	fld_funcorrel,
	trabajador_1,
	trabajador_2,
	trabajador_3,
	trabajador_4
FROM emptiptrab 
pivot(max("fld_cottipotrcod") FOR n_veces IN (1,2,3,4))
AS P (fld_funfolio,fld_funcorrel,trabajador_1,trabajador_2,trabajador_3,trabajador_4) 
)
,tiptrabunico AS (
SELECT 
	fld_funfolio,
	fld_funcorrel,
	trabajador_1,
	SUBSTRING(gls_tiptrab,0,1) AS gls_tipo_trabajado
FROM final_tiptrabajador t1
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPRETIPTRAB t2 ON (t1.trabajador_1 = t2.COD_TIPTRAB)
)
----- fin tipo trabajador
---- cartera vigente
,cartera AS 
(
SELECT 
	'vigente' AS ESTADO_CON, 
	PERIODO_VIGENCIA AS PERIODO_con,
	RUT_TITULAR AS rut_titular_con,
	NUM_CONTRATO AS contrato_con,
    NUM_CORRELATIVO AS correlativo_con,
	COD_ACTIVIDAD,
    TIPO_TRABAJADOR,
    TIPO_TRANSACCION,
    COD_CATEGORIA,
	PAGADO_PLAN_PESOS,
	pagado_total_pesos,
	PAGADO_PHARMA_PESOS,
    MONTO_EXCESO,
    MONTO_EXCEDENTE,
	COD_REGION,
    COD_COMUNA,
    COD_SUCURSAL,
    COD_AGENCIA AS AGENCIA_VN,
    COD_AGENTE AS AGENTE_VN,
    PERIODO_ANUALIDAD AS PERIODO_ANUAL,
    CLASIFICACION AS CLASIF_RIESGO,
    COD_MOROSIDAD AS CLASIF_MOROSIDAD,
    ANTIGUEDAD,
    CNT_CARFAM,
    COSTO_FINAL_UF,
    GASTO_PHARMA_PESOS,
   	PERIODO_ANUALIDAD,
   	COD_NEGOCIO
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO_VIGENCIA BETWEEN $periodo_desde AND $periodo_hasta
)
----- fin cartera vigente
----------- EXTRACCION DE GASTOS
, gastos AS 
( 
SELECT
	'no vigente' AS ESTADO_DM,
	PERIODO AS periodo_dm,
	RUT_TITULAR AS rut_titular_dm,
	CONTRATO AS contrato_dm,
	CORRELATIVO AS correlativo_dm,
	RUT_BENEFICIARIO ,
	COD_BENEFICIARIO ,
	HOLDING_PRESTADOR ,
	RUT_INSTITUCION ,
	NOMBRE_INSTITUCION ,
	COD_DIRECCION_INST ,
	COMUNA_INST ,
	GLS_COMUNA_INST ,
	REGION_INST ,
	GLS_REGION_INST ,
	IFF(ORIGEN = 'HOSPITALARIO',sum(COBRADO),0 ) AS COBRADO_HOSPITALARIO,
	IFF(ORIGEN = 'AMBULATORIO' ,sum(COBRADO),0 ) AS COBRADO_AMBULATORIO,
	IFF(ORIGEN = 'GES',sum(COBRADO),0 ) AS COBRADO_GES,
	IFF(ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),SUM(COBRADO),0) AS COBRADO_LICENCIA,
	IFF(ORIGEN = 'HOSPITALARIO',sum(BONIFICADO + monto_caec),0 ) AS DM_HOSPITALARIO,
	IFF(ORIGEN = 'AMBULATORIO' ,sum(BONIFICADO + monto_caec),0 ) AS DM_AMBULATORIO,
	IFF(ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES,
	--sum(MONTO_CAEC) AS DM_CAEC,
	IFF(ORIGEN = 'LICENCIA',sum(BONIFICADO),0 ) AS DM_LICENCIA
	--IFF(ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
	--SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
	--SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE PERIODO  BETWEEN $periodo_desde AND $periodo_hasta --AND t1.RUT_TITULAR IS NOT NULL
GROUP BY 
	periodo_dm,
	rut_titular_dm, 
	contrato_dm,
	correlativo_dm,
	RUT_BENEFICIARIO , 
	COD_BENEFICIARIO , 
	ORIGEN , 
	TIPO_LICENCIA,
	HOLDING_PRESTADOR ,
	RUT_INSTITUCION ,
	NOMBRE_INSTITUCION ,
	COD_DIRECCION_INST ,
	COMUNA_INST ,
	GLS_COMUNA_INST ,
	REGION_INST ,
	GLS_REGION_INST 
)
, cartera_gastos AS 
( 
SELECT	DISTINCT 
	COALESCE (t1.estado_con,t2.estado_dm ) AS estado,
	COALESCE (t1.periodo_con,t2.periodo_dm) AS periodo,
	COALESCE (t1.rut_titular_con, t2.rut_titular_dm) AS rut_titular,
	COALESCE (t1.contrato_con,t2.contrato_dm) AS contrato,
	COALESCE (t1.correlativo_con,t2.correlativo_dm) AS correlativo,
	--rut_titular_con,
	--periodo_dm,
	--rut_titular_dm,
	COALESCE (t2.rut_beneficiario,t1.rut_titular_con, t2.rut_titular_dm) AS rut_beneficiario,
	--t2.rut_beneficiario,
	t2.cod_beneficiario,
	t1.tipo_trabajador,
	t1.costo_final_uf,
	t1.periodo_anualidad,
	t1.cod_negocio,
	row_number() over (partition by t1.periodo_con, t1.rut_titular_con ORDER BY t2.rut_beneficiario asc) REGISTRO_ID,
	CASE 
		WHEN registro_id = 1 THEN 'Titular Vigente' 
		WHEN registro_id > 1 AND rut_titular_con IS NOT NULL THEN 'Beneficiario Vigente' 
		ELSE 'No Vigente'
	END AS vigente,
	--pagado_plan_pesos,
	t2.HOLDING_PRESTADOR ,
	t2.RUT_INSTITUCION ,
	t2.NOMBRE_INSTITUCION ,
	t2.COD_DIRECCION_INST ,
	t2.COMUNA_INST ,
	t2.GLS_COMUNA_INST ,
	t2.REGION_INST ,
	t2.GLS_REGION_INST,
	CASE 
		WHEN registro_id = 1 THEN t1.pagado_total_pesos 
		ELSE 0
	END AS total_pagado,
	IFF(REGISTRO_ID=1,t1.MONTO_EXCESO,0) AS EXCESOS,
    IFF(REGISTRO_ID=1,t1.MONTO_EXCEDENTE,0) AS EXCEDENTES,
    IFF(registro_id=1,t1.gasto_pharma_pesos,0) AS GASTO_PHARMA,
    t2.cobrado_hospitalario,
    t2.cobrado_ambulatorio,
    t2.cobrado_ges,
    t2.cobrado_licencia,
	t2.dm_hospitalario,
	t2.dm_ambulatorio,
	t2.dm_ges,
	t2.dm_licencia
FROM cartera t1
FULL OUTER JOIN gastos t2 ON (t1.rut_titular_con = t2.rut_titular_dm AND t1.periodo_con = t2.periodo_dm)
ORDER BY periodo, RUT_TITULAR 
)
, uni_final AS 
( 
SELECT 
	cg.estado,
	cg.periodo,
	est.P_DDV_EST.periodo_fecha(cg.periodo) AS periodo_fecha,
	to_number(SUBSTR(cg.periodo,5,2)) AS MES,
    SUBSTR(cg.periodo,1,4) AS ANIO,
	cg.contrato,
	cg.correlativo,
	to_number(SUBSTR(cg.rut_titular,1,9))*5 - 3265112 + 52658 AS ID_TITULAR,
	cg.rut_titular,
	cg.rut_beneficiario,
	cg.cod_beneficiario,
	IFF(REGISTRO_ID=1,COALESCE (cnt."fld_carganum" ,cntv2."fld_carganum"),0) as NUM_CARGAS,
	IFF(COALESCE (benv2."fld_bencorrel" ,ben."fld_bencorrel")=0 or COALESCE (benv2."fld_bencorrel" ,ben."fld_bencorrel") is null,'TITULAR','BENEFICIARIO') AS TIPO_RUT, 
	COALESCE (cnt."fld_ingreso" ,cntv2."fld_ingreso") AS fecha_ingreso,
	DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) AS antigueadad,
	CASE
          WHEN DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) <=12                     THEN '00 a 12 meses'
          WHEN DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) >12  AND DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) <=24 THEN '>12 a 24 meses'
          WHEN DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) >24  AND DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) <=36 THEN '>24 a 36 meses'
          WHEN DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) >36  AND DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) <=48 THEN '>36 a 48 meses'
          WHEN DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) >48  AND DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) <=60 THEN '>48 a 60 meses'
          WHEN DATEDIFF(month, fecha_ingreso, EST.P_DDV_EST.PERIODO_FECHA(cg.PERIODO)) >60                      THEN '>60' 
          ELSE 'sin informaci�n'
    END AS RANGO_ANTIGUEDAD,
	CASE 
		WHEN COALESCE (benv2."fld_bensexo",ben."fld_bensexo") = 1 THEN 'M'
        WHEN COALESCE (benv2."fld_bensexo",ben."fld_bensexo") = 2 THEN 'F'
        ELSE 'sin informacion'
    END AS SEXO,
    COALESCE (benv2."fld_benedad",ben."fld_benedad") AS EDAD_BENEFICIARIO,
        CASE
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 0  AND 19 THEN '< 20 A�os'
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 20 AND 24 THEN '20 a 24 A�os'
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 45 AND 49 THEN '45 a 49 A�os'          
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad") BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  COALESCE (benv2."fld_benedad",ben."fld_benedad")  > 60 THEN '> 60 y mas A�os'
            ELSE 'sin informaci�n'
    END AS RANGO_EDAD_BENEFICIARIO,
    COALESCE (benv4."fld_benedad",benv3."fld_benedad") AS EDAD_TITULAR,
    CASE
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 0  AND 19 THEN '< 20 A�os'
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 20 AND 24 THEN '20 a 24 A�os'
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 45 AND 49 THEN '45 a 49 A�os'          
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad") BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  COALESCE (benv4."fld_benedad",benv3."fld_benedad")  > 60 THEN '> 60 y mas A�os'
            ELSE 'sin informaci�n'
    END AS RANGO_EDAD_TITULAR,
	cg.registro_id,
	cg.vigente,
	COALESCE (cnt."fld_region" ,cntv2."fld_region") AS cod_region,
	COALESCE (reg.GLS_REGION ,regv2.GLS_REGION) AS GLS_REGION,
	COALESCE (cnt."fld_comunacod",cntv2."fld_comunacod") AS cod_comuna,
	comu.GLS_COMUNA ,
	COALESCE (cnt."fld_succod" ,cntv2."fld_succod") AS cod_sucursal,
	sucu.GLS_SUCUR ,
	ubi.UBICACION ,
	ubi.COD_CENTROCOSTOS AS COD_CENTRO_COSTOS,
    ubi.CENTROCOSTOS AS CENTRO_COSTOS_GLS,
    IFF(REGISTRO_ID=1,cob."fld_emprut",0) AS NUM_EMPLEADORES, 
    act.GLS_ACTIVI AS ACTIVIDAD,
    COALESCE (cg.tipo_trabajador, tra.gls_tipo_trabajado) AS tipo_trabajador,
	COALESCE (cntv2."fld_cntcateg",cnt."fld_cntcateg") AS categoria,
	COALESCE (cntv2."fld_glscateg",cnt."fld_glscateg") AS gls_categoria,
	--COALESCE (cnt."fld_planvig",cntv2."fld_planvig") AS tipo_plan	,
	COALESCE (benv2."fld_bennacfec",ben."fld_bennacfec") AS fecha_nacimiento,
	prm.TIPO_PLAN ,
	prm.TIPO_PRODUCTO ,
	prm.SERIE ,
	prm.SEGMENTO AS SEGMENTO_PLAN,
    prm.SERIE AS SERIE_PLAN,
    prm.SERIE_DOS AS SERIE_PLAN_2,
    prm.LINEA_PLAN ,
    prm.LINEA_PLAN_2 ,
    prm.VIG_INICIO AS VIG_INICIO_PLAN,
    prm.VIG_FIN  AS VIG_FIN_PLAN,
    prm.DETALLE_PRODUCTO ,
    COALESCE (cntv2."fld_segurohasta",cnt."fld_segurohasta") AS SEG_MUERTE,
    IFF(REGISTRO_ID=1,COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase"),0) AS PRECIO_BASE,
    CASE
          WHEN COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") =0      THEN 'igual a 0'
          WHEN COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") >0.01   AND COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") <=1.5 THEN '0.01 - 1.50'
          WHEN COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") >1.5001 AND COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") <=2   THEN '1.51 - 2.00'
          WHEN COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") >2.0001 AND COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") <=3   THEN '2.01 - 3.00'
          WHEN COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") >3.0001 AND COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") <=4   THEN '3.01 a 4.00'
          WHEN COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") >4.0001 AND COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") <=5   THEN '4.01 a 5.00'
          WHEN COALESCE (cntv2."fld_valorbase", cnt."fld_valorbase") >5.0001 THEN '5.01 y mas'
          ELSE 'sin informaci�n'
    END AS RANGO_PRECIO,
    IFF(REGISTRO_ID=1,COALESCE (ben."fld_factor", benv2."fld_factor"),0) AS FACTOR_RIESGO,
    IFF(REGISTRO_ID=1,COALESCE (cntv2."fld_costototal",cnt."fld_costototal"),0) AS COSTO_TOTAL,
    IFF(REGISTRO_ID=1,COALESCE (cntv2."fld_benefadic", cnt."fld_benefadic"),0) AS COSTO_BENEF_ADIC,
    IFF(REGISTRO_ID=1,COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal"),0) AS COSTO_FINAL,
    CASE	
          WHEN COSTO_FINAL >0  AND COSTO_FINAL <=2  THEN '>0 <= 2.0 UF'
          WHEN COSTO_FINAL >2  AND COSTO_FINAL <=4  THEN '>2.0 <= 4.0 UF'	
          WHEN COSTO_FINAL >4  AND COSTO_FINAL <=6  THEN '>4.0 <= 6.0 UF'	
          WHEN COSTO_FINAL >6  AND COSTO_FINAL <=8  THEN '>6.0 <= 8.0 UF'	
          WHEN COSTO_FINAL >8  AND COSTO_FINAL <=10 THEN '>8.0 <= 10.0 UF'
          WHEN COSTO_FINAL >10 AND COSTO_FINAL <=12 THEN '>10.0 <= 12.0 UF'
          WHEN COSTO_FINAL >12 AND COSTO_FINAL <=14 THEN '>12.0 <= 14.0 UF'
          WHEN COSTO_FINAL >14 AND COSTO_FINAL <=16 THEN '>14.0 <= 16.0 UF'
          WHEN COSTO_FINAL >16 AND COSTO_FINAL <=18 THEN '>16.0 <= 18.0 UF'
          WHEN COSTO_FINAL >18 AND COSTO_FINAL <=20 THEN '>18.0 <= 20.0 UF'
          WHEN COSTO_FINAL >20 THEN '>20.0 UF'
          ELSE 'sin informaci�n'
    END AS RANGO_COTIZACION_BENEFICIARIO,
    CASE	
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >0  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=2  THEN '>0 <= 2.0 UF'
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >2  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=3  THEN '>2.0 <= 3.0 UF'	
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >3  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=4  THEN '>3.0 <= 4.0 UF'	
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >4  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=5  THEN '>4.0 <= 5.0 UF'	
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >5  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=6  THEN '>5.0 <= 6.0 UF'
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >6  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=7  THEN '>6.0 <= 7.0 UF'
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >7  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=8  THEN '>7.0 <= 8.0 UF'
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >8  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=9  THEN '>8.0 <= 9.0 UF'
          WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >9  AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=10 THEN '>9.0 <= 10.0 UF'
		  WHEN  COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") >10 THEN '>10.0 UF'
          ELSE 'sin informaci�n'
    END AS RANGO_COTIZACION_TITULAR,
/*    CASE	
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 0 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=2 THEN '>0 <= 2.0 UF'
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 2 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=4 THEN '>2.0 <= 4.0 UF'	
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 4 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=6 THEN '>4.0 <= 6.0 UF'	
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 6 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=8 THEN '>6.0 <= 8.0 UF'	
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 8 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=10 THEN '>8.0 <= 10.0 UF'
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 10 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=12 THEN '>10.0 <= 12.0 UF'
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 12 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=14 THEN '>12.0 <= 14.0 UF'
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 14 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=16 THEN '>14.0 <= 16.0 UF'
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 16 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=18 THEN '>16.0 <= 18.0 UF'
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 18 AND COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") <=20 THEN '>18.0 <= 20.0 UF'
          WHEN COALESCE (cg.COSTO_FINAL_UF, cntv2."fld_concostototal",cnt."fld_concostototal") > 20 THEN '>20.0 UF'
          ELSE 'sin informaci�n'
    END AS RANGO_COTIZACION,*/
    uf.VAL_MON AS valor_uf,
    IFF(REGISTRO_ID=1,NVL(cob."fld_ultrenta",0),0) AS RENTA_IMPONIBLE,
    CASE	
        WHEN NVL(cob."fld_ultrenta",0) > 1       AND NVL(cob."fld_ultrenta",0) <=500000  	THEN '>1 <= 500'
        WHEN NVL(cob."fld_ultrenta",0) > 500001  AND NVL(cob."fld_ultrenta",0) <=1000000    THEN '>500 y <= 1000'	
        WHEN NVL(cob."fld_ultrenta",0) > 1000001 AND NVL(cob."fld_ultrenta",0) <=1500000    THEN '>1000 y <= 1500'	
        WHEN NVL(cob."fld_ultrenta",0) > 1500001 AND NVL(cob."fld_ultrenta",0) <=2300000    THEN '>1500 y <= 2300'	
        WHEN NVL(cob."fld_ultrenta",0) > 2300001									        THEN '> 2300'
        ELSE 'sin informaci�n'
    END AS  RANGO_RENTA_IMPONIBLE,
    COALESCE (cntv2."fld_agencianum", cnt."fld_agencianum") AS agencia_vn,
    COALESCE (cntv2."fld_agecod", cnt."fld_agecod") AS agente_vn,
    COALESCE (cntv2."fld_clasifica" ,cnt."fld_clasifica") AS clasif_riesgo,
    ----- agregar clasif_morosidad
    IFF(REGISTRO_ID=1,NVL(recu.IVA_RECUPERADO,0),0) AS IVA_RECUPERADO,
    IVA_RECUPERADO/valor_uf AS IVA_RECUPERADO_UF,
    IFF(REGISTRO_ID=1,NVL(rg.RECUPERACION_GASTO,0),0) AS RECUPERACION_GASTO ,
    recuperacion_gasto/valor_uf AS recuperacion_gasto_uf,
    cg.periodo_anualidad AS periodo_anual,
    COALESCE (ben."fld_bennombre",benv2."fld_bennombre") AS FLD_BENNOMBRE,
    COALESCE (ben."fld_benapepa",benv2."fld_benapepa") AS FLD_BENAPEPA, 
    COALESCE (ben."fld_benapema",benv2."fld_benapema") AS FLD_BENAPEMA,
    COALESCE (cntv2."fld_funfolio",cnt."fld_funfolio") AS FLD_FUNFOLIO,
    COALESCE (cntv2."fld_funcorrel",cnt."fld_funcorrel") AS FLD_FUNCORREL,
    IFF(REGISTRO_ID=1, iva.Valor_Iva,0) AS IVA,
    cg.COD_NEGOCIO,
	modc.GLS_MODCOTZ AS TIPO_COTIZACION,
	cg.HOLDING_PRESTADOR ,
	cg.RUT_INSTITUCION ,
	cg.NOMBRE_INSTITUCION ,
	cg.COD_DIRECCION_INST ,
	cg.COMUNA_INST ,
	cg.GLS_COMUNA_INST ,
	cg.REGION_INST ,
	cg.GLS_REGION_INST,
	cg.cobrado_hospitalario,
    cg.cobrado_ambulatorio,
    cg.cobrado_ges,
    cg.cobrado_licencia,
    cg.total_pagado,
    cg.total_pagado /valor_uf AS total_pagado_uf,
	cg.excesos,
	cg.excesos/valor_uf AS excesos_uf,
	cg.excedentes,
	cg.excedentes/valor_uf AS excedentes_uf,
	cg.gasto_pharma,
	cg.gasto_pharma/valor_uf AS gasto_pharma_uf,
	cg.dm_hospitalario,
	cg.dm_hospitalario/valor_uf AS dm_hospitalario_uf,
	cg.dm_ambulatorio,
	cg.dm_ambulatorio/valor_uf AS dm_ambulatorio_uf,
	cg.dm_ges,
	cg.dm_ges/valor_uf AS dm_ges_uf,
	cg.dm_licencia,
	cg.dm_licencia/valor_uf AS dm_licencia_uf,
    cg.total_pagado - cg.EXCESOS - cg.EXCEDENTES AS recaudado,
    recaudado/valor_uf AS recaudado_uf,
    cg.total_pagado - cg.EXCESOS - cg.EXCEDENTES - IVA aS ingresos,
    ingresos/valor_uf AS ingresos_uf
FROM cartera_gastos cg
		LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt 					ON (cg.RUT_TITULAR = cnt."fld_cotrut" AND cg.PERIODO BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA)
		LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cntv2 					ON (SUBSTRING(cg.CONTRATO,0,9)::int=cntv2."fld_funfolio" AND cg.CORRELATIVO=cntv2."fld_funcorrel")
		LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN ben 					ON (cg.rut_titular = ben."fld_benrut"  AND cg.PERIODO BETWEEN ben.FLD_BENVIGREALDESDE AND ben.FLD_BENVIGREALHASTA)
		LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN benv2 					ON (SUBSTRING(cg.CONTRATO,0,9)::int=benv2."fld_funfolio" AND cg.CORRELATIVO=benv2."fld_funcorrel" AND cg.rut_beneficiario = benv2."fld_benrut") ---AND cg.COD_BENEFICIARIO = benv2."fld_bencorrel"  )
		LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN benv3 					ON (cg.rut_titular = benv3."fld_cotrut" AND cg.PERIODO BETWEEN benv3.FLD_BENVIGREALDESDE AND benv3.FLD_BENVIGREALHASTA AND benv3."fld_bencorrel"=0)
		LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN benv4 					ON (SUBSTRING(cg.CONTRATO,0,9)::int=benv4."fld_funfolio" AND cg.CORRELATIVO=benv4."fld_funcorrel" AND cg.rut_titular = benv4."fld_cotrut"  AND benv4."fld_bencorrel"=0) ---AND cg.COD_BENEFICIARIO = benv2."fld_bencorrel"  )
		LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm 				ON (COALESCE (cnt."fld_cntcateg",cntv2."fld_cntcateg")= prm.COD_DESACA)	
		LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPRECOMUNAS comu  		ON (COALESCE (cnt."fld_comunacod",cntv2."fld_comunacod") = comu.COD_COMUNA)
		LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPRESUCURSAL sucu 		ON (COALESCE (cnt."fld_succod" ,cntv2."fld_succod") = sucu.COD_SUCUR)
		LEFT JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_SUCURSAL2 ubi  		ON (COALESCE (cnt."fld_succod" ,cntv2."fld_succod")  = ubi.cod_sucursal)
		LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREACTIVI act            ON (COALESCE (cnt."fld_conactivcod" ,cntv2."fld_conactivcod") = act.COD_ACTIVI)
		LEFT JOIN tiptrabunico tra									ON (COALESCE (cnt."fld_funfolio",cntv2."fld_funfolio")=tra.fld_funfolio AND COALESCE(cnt."fld_funcorrel",cntv2."fld_funcorrel")=tra.fld_funcorrel)
		LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA uf 			ON (cg.periodo = to_number(to_char(dateadd(month, 1, uf.fec_val),'YYYYMM')))
		LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ modc      	ON (COALESCE (cnt."fld_monedatipocod",cntv2."fld_monedatipocod")= modc.COD_MODCOTZ)
		LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION reg        	ON (COALESCE (cnt."fld_region" ,cntv2."fld_region")= reg.COD_REGION)		
	 	LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION regv2			ON (CASE 
																			WHEN len(COALESCE (cnt."fld_comunacod",cntv2."fld_comunacod")) = 5 THEN SUBSTRING(COALESCE(cnt."fld_comunacod",cntv2."fld_comunacod"),0,2)::int
																			ELSE SUBSTRING(COALESCE (cnt."fld_comunacod",cntv2."fld_comunacod"),0,1)::int 
																		END   = regv2.COD_REGION)		
		---------- valor iva
		LEFT JOIN 	(	
        				SELECT  
        						perpag_esperado, 
        						rut_cotizante, 
        						sum( Valor_Iva) as  Valor_Iva
                        FROM  RCD.P_DDV_RCD.P_DDV_RCD_V_LIBRO_VENTAS
                        WHERE  Codigo_Pago = 0
                        GROUP BY 
                        		perpag_esperado, 
                        		rut_cotizante
                   	) iva   ON  (cg.periodo = iva.perpag_esperado AND cg.rut_titular = iva.rut_cotizante)  
       ---------- renta imponible  
       LEFT JOIN   (	
        				SELECT 	
			        			"fld_funnotiffun",
			        			"fld_funcorrel" ,
			           			count("fld_emprut") as "fld_emprut", 
			           			sum("fld_ultrenta") as "fld_ultrenta" 
			           		 FROM AFI.P_RDV_DST_LND_SYB.COB
			           		 GROUP BY "fld_funnotiffun",
			        				  "fld_funcorrel" 
			           	  ) cob ON ( cob."fld_funnotiffun" = SUBSTRING(cg.CONTRATO,0,9)::int AND cob."fld_funcorrel" =cg.CORRELATIVO)
		------- liga iva recuperado
		LEFT JOIN (
                          SELECT 
                          		PERIODO,
                              	RUT AS RUT_TITULAR,
                              	SUM(monto_credito_fiscal) AS IVA_RECUPERADO
                          FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS                       
                          GROUP BY periodo, rut
                        )  recu   ON (cg.periodo= recu.periodo and  cg.rut_titular = recu.rut_titular)
        ------ Liga para agregar Recuperacion del Gasto
        LEFT JOIN (
				            SELECT 
				            	"periodo_vig" AS PERIODO,
				            	"rut_titular" AS RUT_TITULAR,
				            	SUM("monto_recuperado") RECUPERACION_GASTO   
				            FROM FIN.P_DDV_FIN.P_DDV_FIN_RECUPERACION_GASTO_NEW 
				            GROUP BY "periodo_vig", "rut_titular"
       					) rg  ON (cg.periodo= rg.periodo and cg.rut_titular = rg.rut_titular)
--WHERE fecha_nacimiento IS NULL 
--WHERE correlativo IS NULL 
ORDER BY  rut_titular
)
SELECT
	*
FROM uni_final
---LIMIT 50
)





SELECT 	
			"fld_funnotiffun",
"fld_funcorrel" ,
count("fld_emprut") as "fld_emprut", 
sum("fld_ultrenta") as "fld_ultrenta" 
 FROM AFI.P_RDV_DST_LND_SYB.COB
 WHERE "fld_funnotiffun" IN (10510964) AND "fld_funcorrel" IN (12)
 GROUP BY "fld_funnotiffun",
  "fld_funcorrel" 
 
SELECT 	
	*
 FROM AFI.P_RDV_DST_LND_SYB.COB
 WHERE "fld_funnotiffun" IN (10510964) AND "fld_funcorrel" IN (12)


SELECT
	REGISTRO_ID ,
	PERIODO ,
	CONTRATO ,
	CORRELATIVO ,
	RUT_TITULAR , 
	RANGO_RENTA_IMPONIBLE ,
	RENTA_IMPONIBLE ,
	SUBSTRING(CONTRATO,0,9)::int,
	*
FROM  EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO BETWEEN 202012 AND 202111 AND RANGO_RENTA_IMPONIBLE IN ('sin informaci�n') --AND RUT_TITULAR ='016111782-K'
--GROUP BY PERIODO ,RANGO_RENTA_IMPONIBLE
LIMIT 10





SELECT * FROM ISA.P_RDV_DST_LND_SYB.SISHOSPTIPPRE 


SELECT * FROM EST.P_DDV_EST.P_DDV_EST_MATRIZ_MARGEN LIMIT 10


SELECT * FROM CME.P_RDV_DST_LND_SYB.CPW_PRESPREFERENTE EFE

SELECT RECUPERACION_GASTO ,* FROM EST.P_STG_EST.CA_SINIESTRALIDAD
WHERE PERIODO = 202109 AND LINEA_PLAN ='ADT 120'
LIMIT 10

SELECT * FROM ISA.P_DDV_ISA.P_DDV_ISA_PRM_AGRUPA_PLANES 


SELECT * FROM  IQ.P_DDV_IQ.RECUPERACION_GASTO
WHERE 	"periodo_vig" = 202109 --AND "monto_recuperado" NOT IN (0)
		--AND "rut_titular" ='010331897-1'
LIMIT 10


SELECT DISTINCT  "periodo_vig" FROM  IQ.P_DDV_IQ.RECUPERACION_GASTO
ORDER BY "periodo_vig" DESC 



SELECT * FROM IQ.P_DDV_IQ.RECUPERACION_GASTO LIMIT 10



SELECT 
	"periodo_vig" ,
	"monto_recuperado" ,
	"rut_titular" 
FROM FIN.P_DDV_FIN.P_DDV_FIN_RECUPERACION_GASTO_NEW 
LIMIT 10


SELECT * FROM AFI.P_RDV_DST_LND_SYB.BEN LIMIT 10


SELECT 
	 periodo, 
	sum(total_pagado),
	sum(excesos),
	sum(excedentes),
	sum(recaudado),
	sum(ingresos),
	sum(dm_hospitalario),
	sum(dm_ambulatorio),
	sum(dm_ges),
	sum(dm_licencia),
	sum(gasto_pharma)
FROM uni_final
--WHERE registro_id = 1
GROUP BY periodo 
LIMIT 50











SELECT count(1) FROM uni_final
WHERE tipo_trabajador IS NULL 

















































SELECT periodo, count(1) FROM uni_final
--WHERE categoria IS NULL
WHERE fecha_nacimiento IS NULL 
GROUP BY periodo
LIMIT 300






SELECT
	*
FROM uni_final
LIMIT 300





















SELECT 
	 periodo, 
	sum(total_pagado),
	sum(dm_hospitalario),
	sum(dm_ambulatorio),
	sum(dm_ges),
	sum(dm_licencia)
FROM cartera_gastos
--WHERE registro_id = 1
GROUP BY periodo 
LIMIT 50
-----------------
---------------------------- FIN UNION CONTRATO Y DTM



SELECT PERIODO ,count(1) FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CONTRATO IS NULL AND PERIODO >= 202101
GROUP BY PERIODO 
ORDER BY PERIODO 


SELECT * FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE RUT_TITULAR ='007724115-9' AND PERIODO = 202106 LIMIT 10


SELECT PERIODO_VIGENCIA ,count(DISTINCT(RUT_TITULAR)), SUM(PAGADO_TOTAL_PESOS) FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE PERIODO_VIGENCIA >= 202101
GROUP BY PERIODO_VIGENCIA 
--WHERE rut_titular = '007724115-9' AND periodo_vigencia = 202106 LIMIT 10


SELECT "fld_morosidad" ,*  FROM afi.P_RDV_DST_LND_SYB.cnt LIMIT 10


SELECT DISTINCT PERIODO FROM EST.P_STG_EST.CARTERA_BEN LIMIT 10

SELECT * FROM EST.P_STG_EST.CARTERA_BEN LIMIT 10


--------------
---SINIESTRALIDAD POR CARTERA VIGENTE
   CREATE OR REPLACE TABLE EST.P_STG_EST.CARTERA_BEN AS 
    SELECT 
        t1.RUT_TITULAR,
        --t14.rut_titular AS titular_dm,
        to_number(SUBSTR(t1.RUT_TITULAR,1,9))*5 - 3265112 + 52658 AS ID_TITULAR, --- ok
        t4."fld_benrut" AS RUT_BENEFICIARIO, -----ok 
        t4."fld_bencorrel" AS COD_BENEFICIARIO, ----- ok
        IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,'TITULAR','BENEFICIARIO') AS TIPO_RUT, ---- ok
        t1.periodo_vigencia  AS PERIODO, -- ok
        est.P_DDV_EST.periodo_fecha(t1.periodo_vigencia) AS periodo_fecha, --ok
        SUBSTR(t1.periodo_vigencia,1,4) AS ANNO, --ok
        to_number(SUBSTR(t1.periodo_vigencia,5,2)) AS MES, ---ok
        (to_varchar(t1.FECHA_INGRESO,'YYYY-MM-DD')) AS FECHA_INGRESO, --- ok
        t1.ANTIGUEDAD, --ok
        	CASE T4."fld_bensexo" 
                WHEN 2 THEN 'F'
                ELSE 'M' 
            END AS SEXO, --ok
        row_number() over (partition by t1.periodo_vigencia, t1.rut_titular ORDER BY t4."fld_benrut" asc) REGISTRO_ID, ---ok
        IFF(REGISTRO_ID=1,t1.CNT_CARFAM,0) as NUM_CARGAS, --ok
        t1.NUM_CONTRATO AS CONTRATO, --ok
        t1.NUM_CORRELATIVO AS CORRELATIVO, --ok
        t1.COD_REGION, ---ok
        t8.GLS_REGION AS REGION_GLS, ----ok
        t1.COD_COMUNA,  ---ok
        t7.GLS_COMUNA AS COMUNA_GLS,  ---ok
        t11.UBICACION,
        t1.COD_SUCURSAL, ----ok
        t2.GLS_SUCUR AS SUCURSAL_GLS,  ---ok
        t11.COD_CENTROCOSTOS AS COD_CENTRO_COSTOS, --ok
        t11.CENTROCOSTOS AS CENTRO_COSTOS_GLS,  --ok
        TO_DATE(to_varchar(t4."fld_bennacfec",'YYYY-MM-DD')) AS FECHA_NACIMIENTO, --ok
        t4."fld_benedad" AS EDAD, --ok
        IFF(REGISTRO_ID=1,t10."fld_emprut",0) AS NUM_EMPLEADORES, --ok
        t1.COD_ACTIVIDAD	AS ACTIVIDAD, ---ok
        t1.TIPO_TRABAJADOR	AS TIPO_TRABAJADOR,  --ok
        t1.TIPO_TRANSACCION	AS TIPO_TRANSC, ----es necesario?
        t1.COD_CATEGORIA	AS CATEGORIA_COD, --ok
        t3.CATEGORIA_GLS	AS CATEGORIA_GLS, ---ok
        t5.SERIE, -- ok
        t5.TIPO_PLAN, --- ok
        t5.TIPO_PRODUCTO, ---ok,
        t5.DETALLE_PRODUCTO, -- ok
        --t5.LINEA_PLAN,
        t6."fld_segurohasta" AS SEG_MUERTE,-- ok
        prm.SEGMENTO AS SEGMENTO_PLAN, --ok
        prm.SERIE AS SERIE_PLAN, --ok
        prm.SERIE_DOS AS SERIE_PLAN_2, --ok
        prm.LINEA_PLAN , ---ok
        prm.LINEA_PLAN_2 , ---ok
        prm.VIG_INICIO AS VIG_INICIO_PLAN,  --ok
        prm.VIG_FIN  AS VIG_FIN_PLAN,  ---ok
        IFF(REGISTRO_ID=1,t1.COSTO_FINAL_UF,0) AS COSTO_FINAL, ---ok
        IFF(REGISTRO_ID=1,t6."fld_valorbase",0) AS PRECIO_BASE, --ok
        IFF(REGISTRO_ID=1,t4."fld_factor",0) AS FACTOR_RIESGO, --ok
        IFF(REGISTRO_ID=1,t6."fld_costototal",0) AS COSTO_TOTAL,  ---ok
        IFF(REGISTRO_ID=1,t6."fld_benefadic",0) AS COSTO_BENEF_ADIC, --ok
        IFF(REGISTRO_ID=1,t1.PAGADO_TOTAL_PESOS,0) AS PAGADO, ---ok
        IFF(REGISTRO_ID=1,t1.MONTO_EXCESO,0) AS EXCESOS, --ok
        IFF(REGISTRO_ID=1,t1.MONTO_EXCEDENTE,0) AS EXCEDENTES,  --ok
        NVL(t14.DM_AMBULATORIO,0) AS GASTO_AMBULATORIOS,  ---ok
        NVL(t14.DM_HOSPITALARIO,0) AS GASTO_HOSPITALARIOS,  ---ok
        --SUM(GASTO_HOSPITALARIOS) OVER (PARTITION BY RUT_BENEFICIARIO ORDER BY t1.periodo_vigencia desc ROWS BETWEEN 12 PRECEDING AND CURRENT ROW) AS GASTO_HOSPI_ACUMULADO,
        NVL(t14.DM_GES,0) AS GASTO_GES, ---ok
        NVL(t14.DM_CAEC_HOSPITALARIO,0) AS CAEC_HOSPITALARIO,  ----ok
        NVL(t14.DM_CAEC_AMBULATORIO,0) AS CAEC_AMBULATORIO,   ---ok
        NVL(t14.DM_CAEC,0) AS GASTO_CAEC,   ---ok
        IFF(REGISTRO_ID=1,t1.GASTO_PHARMA_PESOS,0) AS GASTO_PHARMA,  --- ok 
        IFF(REGISTRO_ID=1,NVL(t16.RECUPERACION_GASTO,0),0) AS RECUPERACION_GASTO, ---ok
        IFF(REGISTRO_ID=1,NVL(t15.IVA_RECUPERADO,0),0) AS IVA_RECUPERADO, ----ok
        NVL(t14.DM_LICENCIA,0) AS GASTO_LICENCIAS,  --ok
        IFF(REGISTRO_ID=1,NVL(t10."fld_ultrenta",0),0) AS RENTA_IMPONIBLE,  --ok
        CASE	
        WHEN RENTA_IMPONIBLE > 1 AND RENTA_IMPONIBLE <=500000	
            THEN '>1 <= 500'
        WHEN RENTA_IMPONIBLE > 500001 AND RENTA_IMPONIBLE <= 1000000
            THEN '>500 y <= 1000'	
        WHEN RENTA_IMPONIBLE > 1000001 AND RENTA_IMPONIBLE <=1500000
            THEN '>1000 y <= 1500'	
        WHEN RENTA_IMPONIBLE > 1500001 AND RENTA_IMPONIBLE <= 2300000
            THEN '>1500 y <= 2300'	
        WHEN RENTA_IMPONIBLE > 2300001
            THEN '> 2300'
        ELSE 'sin informaci�n'
    END AS  RANGO_RENTA_IMPONIBLE, ---ok
        t9.VAL_MON AS VALOR_UF, ---ok
        t1.COD_AGENCIA AS AGENCIA_VN,  ----ok
        t1.COD_AGENTE AS AGENTE_VN,   ----ok
        t1.PERIODO_ANUALIDAD AS PERIODO_ANUAL,  --ok
        t1.CLASIFICACION AS CLASIF_RIESGO,  ---ok
        t1.COD_MOROSIDAD AS CLASIF_MOROSIDAD, ------
        CASE
            WHEN  t4."fld_benedad" BETWEEN 0 AND 19 THEN '< 20 A�os'
            WHEN  t4."fld_benedad" BETWEEN 20 AND 24 THEN '20 a 24 A�os'
            WHEN  t4."fld_benedad" BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  t4."fld_benedad" BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  t4."fld_benedad" BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  t4."fld_benedad" BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  t4."fld_benedad" BETWEEN 45 AND 49 THEN '45 a 49 A�os'          
            WHEN  t4."fld_benedad" BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  t4."fld_benedad" BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  t4."fld_benedad" > 60 then '> 60 y mas A�os'
            ELSE 'sin informaci�n'
        END AS RANGO_EDAD,   ----- ok
         IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,FIRST_VALUE( t4."fld_benedad") over (partition by t1.periodo_vigencia, t1.RUT_TITULAR order by  t4."fld_bencorrel"),null) as EDAD_TITULAR,  
         CASE 
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 0 and 19 then '< 20 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 20 and 24 then '20 a 24 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 25 and 29 then '25 a 29 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 30 and 34 then '30 a 34 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 35 and 39 then '35 a 39 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 40 and 44 then '40 a 44 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 45 and 49 then '45 a 49 A�os'                  
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 50 and 54 then '50 a 54 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 55 and 60 then '55 a 60 A�os'        
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR > 60 then '> 60 y mas A�os'
            ELSE 'sin informaci�n'
        END AS RANGO_EDAD_TITULAR,   ----- ok
        CASE	
          WHEN t1.COSTO_FINAL_UF > 0 AND t1.COSTO_FINAL_UF <=2	
          THEN '>0 <= 2.0 UF'
          WHEN t1.COSTO_FINAL_UF > 2 AND t1.COSTO_FINAL_UF <=3	
          THEN '>2.0 <= 3.0 UF'	
          WHEN t1.COSTO_FINAL_UF > 3 AND t1.COSTO_FINAL_UF <=4	
          THEN '>3.0 <= 4.0 UF'	
          WHEN t1.COSTO_FINAL_UF > 4 AND t1.COSTO_FINAL_UF <=5	
          THEN '>4.0 <= 5.0 UF'	
          WHEN t1.COSTO_FINAL_UF > 5
          THEN '>5.0 U'
          ELSE 'sin informaci�n'
        END AS RANGO_COTIZACION,    ----ok
        CASE
          WHEN t1.antiguedad <=12
          THEN '00 a 12 meses'
          WHEN t1.antiguedad >12 AND t1.antiguedad <=24
          THEN '>12 a 24 meses'
          WHEN t1.antiguedad >24 AND t1.antiguedad <=36
          THEN '>24 a 36 meses'
          WHEN t1.antiguedad >36 AND t1.antiguedad <=48
          THEN '>36 a 48 meses'
          WHEN t1.antiguedad >48 AND t1.antiguedad <=60
          THEN '>48 a 60 meses'
          WHEN t1.antiguedad >60
          THEN '>60' 
          ELSE 'sin informaci�n'
        END AS RANGO_ANTIGUEDAD, --- ok
        CASE
          WHEN t6."fld_valorbase" =0
          THEN 'igual a 0'
          WHEN t6."fld_valorbase" >0.01 AND t6."fld_valorbase" <=1.5
          THEN '0.01 - 1.50'
          WHEN t6."fld_valorbase" >1.5001 AND t6."fld_valorbase" <=2
          THEN '1.51 - 2.00'
          WHEN t6."fld_valorbase" >2.0001 AND t6."fld_valorbase" <=3
          THEN '2.01 - 3.00'
          WHEN t6."fld_valorbase" >3.0001 AND t6."fld_valorbase" <=4
          THEN '3.01 a 4.00'
          WHEN t6."fld_valorbase" >4.0001 AND t6."fld_valorbase" <=5
          THEN '4.01 a 5.00'
          WHEN t6."fld_valorbase" >5.0001
          THEN '5.01 y mas'
          ELSE 'sin informaci�n'
        END AS RANGO_PRECIO,    ------ ok
        t4."fld_bennombre" AS FLD_BENNOMBRE,  ---ok
        t4."fld_benapepa" AS FLD_BENAPEPA, ----ok
        t4."fld_benapema" AS FLD_BENAPEMA,  ----ok
        t6."fld_funfolio" AS FLD_FUNFOLIO,   ---ok
        t6."fld_funcorrel" AS FLD_FUNCORREL,   ----ok
        IFF(REGISTRO_ID=1, t12.Valor_Iva,0) AS IVA,  -----ok
        t1.COD_NEGOCIO, ----ok
        t13.GLS_MODCOTZ AS TIPO_COTIZACION, -----ok
        PAGADO - EXCESOS - EXCEDENTES AS RECAUDADO,   ---ok
        PAGADO - EXCESOS - EXCEDENTES - IVA AS INGRESOS  -----ok
    FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO  t1
        -- Liga con Sucursal
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRESUCURSAL  t2 ON (t1.cod_sucursal = t2.COD_SUCUR)
        -- Liga con Categoria
        LEFT OUTER JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_CATEGORIA t3 ON (t1.cod_categoria = t3.cod_categoria)
        -- Liga para agregar Nombre y Genero
        -- Inicio BEN 
        LEFT OUTER JOIN AFI.P_RDV_DST_LND_SYB.BEN  t4 ON (t1.RUT_TITULAR = t4."fld_cotrut" AND t1.num_correlativo = t4."fld_funcorrel" AND t4.FLD_BENVIGREALDESDE <= t1.periodo_vigencia AND t4.FLD_BENVIGREALHASTA >= t1.periodo_vigencia)
        -- Fin BEN
        -- Liga para agregar tipo_plan, detalle_producto
        LEFT OUTER JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES t5 ON (t1.cod_categoria = t5.COD_DESACA) --"IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES" t5  
        -- Liga para agregar datos de CNT
        --LEFT OUTER JOIN AFI.P_RDV_DST_LND_SYB.CNT t6  ON (t1.RUT_TITULAR = t6."fld_cotrut" and t6.FLD_VIGREALDESDE <= t1.periodo_vigencia AND t6.FLD_VIGREALHASTA >= t1.periodo_vigencia)
        LEFT OUTER JOIN AFI.P_RDV_DST_LND_SYB.CNT t6  ON (SUBSTRING(t1.NUM_CONTRATO,0,9)::int = t6."fld_funfolio"  AND t1.NUM_CORRELATIVO = t6."fld_funcorrel")
        -- Liga para agregar COMUNA
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRECOMUNAS t7 ON (t1.COD_COMUNA = t7.COD_COMUNA )
        -- Liga para agregar Region
        --LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION t8 ON (t1.COD_REGION = t8.COD_REGION )
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION t8 ON (	CASE 
																		WHEN len(t1.COD_COMUNA) = 5 THEN SUBSTRING(t1.COD_COMUNA,0,2)::int
																		ELSE SUBSTRING(t1.COD_COMUNA,0,1)::int 
																	END   = t8.COD_REGION 
																 )	 	      
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA t9 ON (t1.periodo_vigencia = to_number(to_char(dateadd(month, 1, fec_val),'YYYYMM')))  
        -- Liga para obtener la Renta Imponible
        -- Inicio Renta Imponible
        LEFT OUTER JOIN   (	SELECT 	
			        			"fld_funnotiffun",
			        			"fld_funcorrel" ,
			           			count("fld_emprut") as "fld_emprut", 
			           			sum("fld_ultrenta") as "fld_ultrenta" 
			           		 FROM AFI.P_RDV_DST_LND_SYB.COB
			           		 GROUP BY "fld_funnotiffun",
			        				  "fld_funcorrel" 
			           	  ) t10 ON ( t10."fld_funnotiffun" = SUBSTRING(t1.NUM_CONTRATO,0,9)::int AND t10."fld_funcorrel" =t1.NUM_CORRELATIVO)
        -- Liga con Sucursal para obtener centro de costos y ubicacion
        LEFT OUTER JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_SUCURSAL2 t11  ON (t1.cod_sucursal = t11.cod_sucursal)
        -- Liga para obtener el iva
        LEFT OUTER JOIN (select perpag_esperado, rut_cotizante, SUM( Valor_Iva) as  Valor_Iva
                        from RCD.P_DDV_RCD.P_DDV_RCD_V_LIBRO_VENTAS
                        where  Codigo_Pago = 0
                        group by perpag_esperado, rut_cotizante) t12 ON (t1.periodo_vigencia = t12.perpag_esperado AND t1.rut_titular = rut_cotizante )
        -- Liga para agregar GLS_MODCOTZ Tipo Cotizacion
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ t13      ON (t6."fld_monedatipocod" = t13.COD_MODCOTZ)
        -- Liga para agregar Gasto directo del Datamart de Gastos - Hospitalario, Ambulatorio, Ges, CAEC y Licencias
        FULL OUTER JOIN (
             SELECT T1.PERIODO, 
                T1.RUT_TITULAR, 
                t1.cod_beneficiario, 	
                SUM(DM_HOSPITALARIO) AS DM_HOSPITALARIO, 
                SUM(DM_AMBULATORIO) AS DM_AMBULATORIO, 
                SUM(DM_GES) AS DM_GES, 
                SUM(DM_CAEC_HOSPITALARIO) AS DM_CAEC_HOSPITALARIO,
                SUM(DM_CAEC_AMBULATORIO) AS DM_CAEC_AMBULATORIO,
                SUM(DM_CAEC) AS DM_CAEC, 
                SUM(DM_LICENCIA) AS DM_LICENCIA 
            FROM 
                (		SELECT 
                        T1.PERIODO, 
                        T1.RUT_TITULAR, 
                        t1.cod_beneficiario, 
                        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO<>3,sum(BONIFICADO),0 ) AS DM_HOSPITALARIO,
                        IFF(T1.ORIGEN = 'AMBULATORIO'and T1.TIPO_BONO<>3 ,sum(BONIFICADO),0 ) AS DM_AMBULATORIO,
                        IFF(T1.ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES,
                        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO=3,sum(BONIFICADO),0 ) AS DM_CAEC_HOSPITALARIO,
                        IFF(T1.ORIGEN = 'AMBULATORIO' and T1.TIPO_BONO=3,sum(BONIFICADO),0 ) AS DM_CAEC_AMBULATORIO,
                        sum(MONTO_CAEC) AS DM_CAEC,
                        IFF(T1.ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
                                                       SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
                                                       SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
                        FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO T1
                        WHERE T1.PERIODO  BETWEEN 
                            -- Parametros para filtrar los gastos del Datamart
                            -- PERIODO INICIAL y PERIODO FINAL, se obtiene el maximo periodo de contrato y se restan 23 meses
                           201501 AND --EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-2))||' 23:59:59')) AND ------ asignar el periodo de inicio 
            			   EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59'))
                        GROUP BY T1.PERIODO,
                             T1.ORIGEN, t1.tipo_bono, T1.RUT_TITULAR, t1.cod_beneficiario, tipo_licencia
                ) t1
            group by  T1.PERIODO, T1.RUT_TITULAR, t1.cod_beneficiario
          ) t14
            ON (t1.periodo_vigencia = t14.periodo AND  t1.rut_titular = t14.rut_titular  and  NVL(t4."fld_bencorrel",0) = t14.cod_beneficiario)
        -- Liga para agregar Iva Recuperado
        LEFT OUTER JOIN (
                          SELECT t1.PERIODO,
                              t1.RUT AS RUT_TITULAR,
                              SUM(t1.monto_credito_fiscal) AS IVA_RECUPERADO
                          FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS t1                         
                          GROUP BY t1.periodo, t1.rut
                        )  t15   ON (t1.periodo_vigencia = t15.periodo and  t1.rut_titular = t15.rut_titular)
        -- Liga para agregar Recuperacion del Gasto
        LEFT OUTER JOIN (
            SELECT t1."periodo_vig" AS PERIODO,t1."rut_titular" AS RUT_TITULAR,SUM(t1."monto_recuperado") RECUPERACION_GASTO   
            FROM IQ.P_DDV_IQ.RECUPERACION_GASTO t1
            GROUP BY t1."periodo_vig", t1."rut_titular"
       ) t16
          ON (t1.periodo_vigencia = t16.periodo and t1.rut_titular = t16.rut_titular)
       LEFT OUTER JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm ON (t1.COD_CATEGORIA = prm.cod_desaca)
       WHERE  t1.periodo_vigencia 
            BETWEEN  
            201501 AND --EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-2))||' 23:59:59')) AND  ------ asignar el periodo de inicio 
            EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59'))    
------------------- fin cartera_ben
 
            

            
            
      
            
            
            
            
            
            
            
            

SELECT DISTINCT PERIODO, SUM(INGRESOS) FROM EST.P_STG_EST.CARTERA_BEN

GROUP BY PERIODO 
ORDER BY PERIODO 


            
         
CREATE OR REPLACE TABLE EST.P_DDV_EST.CARTERA_BEN_revision AS ( 
SELECT * FROM EST.P_STG_EST.CARTERA_BEN 
WHERE PERIODO IN (202106,202107))
            
SELECT PERIODO , SUM(GASTO_HOSPITALARIOS)::INT, SUM(GASTO_AMBULATORIOS)::INT FROM EST.P_STG_EST.GASTOS_BEN
WHERE PERIODO IN (202106,202107)
GROUP BY PERIODO 

SELECT PERIODO , ORIGEN ,sum(COBRADO)::int , sum(BONIFICADO)::int  FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE FUENTE_ORIGEN IN ('EAH','HMQ') AND PERIODO IN (202106, 202107) AND TIPO_BONO <> 3 AND ORIGEN IN ('HOSPITALARIO')
GROUP BY PERIODO, origen 
ORDER BY PERIODO  LIMIT 10


SELECT PERIODO, COUNT(1)  FROM EST.P_STG_EST.GASTOS_BEN
WHERE RUT_BENEFICIARIO  IS NULL 
GROUP BY PERIODO 


SELECT * FROM EST.P_STG_EST.GASTOS_BEN LIMIT 10

            
       
--------------- siniestralidad gastos_ben
CREATE OR REPLACE TABLE EST.P_STG_EST.GASTOS_BEN AS  
  SELECT
        t1.RUT_TITULAR,
        to_number(substr(t1.rut_titular,1,9))*5 - 3265112 + 52658 AS ID_TITULAR,
        NVL(t4."fld_benrut",t1.RUT_BENEFICIARIO) AS RUT_BENEFICIARIO,
        t1.COD_BENEFICIARIO AS COD_BENEFICIARIO,
        IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,'TITULAR','BENEFICIARIO') AS TIPO_RUT,       
        t1.periodo  AS PERIODO,
        substr(t1.periodo,1,4) AS ANNO,
        to_number(substr(t1.periodo,5,2)) AS MES,
        to_date(to_varchar(t17.FECHA_INGRESO,'YYYY-MM-DD')) AS FECHA_INGRESO,
        NVL(t1.ANTIGUEDAD,nvl(t17.ANTIGUEDAD,0)) AS GASTOS_ANTIGUEDAD,
        CASE T4."fld_bensexo" 
                WHEN 2 THEN 'F'
                ELSE 'M' 
              END AS SEXO,
        first_value(t1.ID) over (partition by t1.periodo, t1.RUT_TITULAR order by  t1.ID) as FIRST_ID,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID ,t17.CNT_CARFAM,0) as NUM_CARGAS,      
        NVL(t17.NUM_CONTRATO,t1.CONTRATO) AS CONTRATO,
        IFF(t17.RUT_TITULAR is null,'NO EXISTE','SI EXISTE') AS EXISTE_EN_CONTRATO,
      --  t17.NUM_CORRELATIVO AS CORRELATIVO,
        t1.CORRELATIVO AS CORRELATIVO, 
        t17.COD_REGION,
        t8.GLS_REGION AS REGION_GLS,       
        t1.COMUNATRABAJA AS COD_COMUNA,
        t7.GLS_COMUNA AS COMUNA_GLS,
        t11.UBICACION,       
        t1.SUCURSAL_CONTRATO AS COD_SUCURSAL,
        t2.GLS_SUCUR AS SUCURSAL_GLS,
        t11.COD_CENTROCOSTOS AS COD_CENTRO_COSTOS,
        t11.CENTROCOSTOS AS CENTRO_COSTOS_GLS,        
        to_date(to_varchar(t4."fld_bennacfec",'YYYY-MM-DD')) AS FECHA_NACIMIENTO,
        t4."fld_benedad" AS EDAD,        
        IFF(t1.ID = FIRST_ID,t10."fld_emprut",0) AS NUM_EMPLEADORES,        
        t17.COD_ACTIVIDAD	AS ACTIVIDAD,
        t17.TIPO_TRABAJADOR	AS TIPO_TRABAJADOR,
        t17.TIPO_TRANSACCION	AS TIPO_TRANSC,        
        t1.COD_CATEGORIA	AS CATEGORIA_COD,
        t3.CATEGORIA_GLS	AS CATEGORIA_GLS,       
        t5.SERIE,
        t5.TIPO_PLAN,
        t5.TIPO_PRODUCTO,
        t5.DETALLE_PRODUCTO,
        t5.LINEA_PLAN,        
        t6."fld_segurohasta" AS SEG_MUERTE,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.COSTO_FINAL_UF,0) AS COSTO_FINAL,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t6."fld_valorbase",0) AS PRECIO_BASE,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t4."fld_factor",0) AS FACTOR_RIESGO,       
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t6."fld_costototal",0) AS COSTO_TOTAL,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t6."fld_benefadic",0) AS COSTO_BENEF_ADIC,       
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.PAGADO_TOTAL_PESOS,0) AS PAGADO,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.MONTO_EXCESO,0) AS EXCESOS,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.MONTO_EXCEDENTE,0) AS EXCEDENTES,
        -- Obtener el numero de registro para seleccionar el primer registro y poder sumar
        row_number() over (partition by t1.periodo, t1.rut_titular, t1.ID order by t1.ID asc) REGISTRO_ID,
        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO<>3 and REGISTRO_ID=1,t1.BONIFICADO,0 ) AS GASTO_HOSPITALARIOS,
        IFF(T1.ORIGEN = 'AMBULATORIO' and T1.TIPO_BONO<>3 and REGISTRO_ID=1,BONIFICADO,0 ) AS GASTO_AMBULATORIOS,
        IFF(T1.ORIGEN = 'GES' and REGISTRO_ID=1,BONIFICADO,0 ) AS GASTO_GES,
        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO=3,BONIFICADO,0 ) AS CAEC_HOSPITALARIO,
        IFF(T1.ORIGEN = 'AMBULATORIO' and T1.TIPO_BONO=3,BONIFICADO,0 ) AS CAEC_AMBULATORIO,        
        IFF(REGISTRO_ID=1,NVL(MONTO_CAEC,0),0) AS GASTO_CAEC,        
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.GASTO_PHARMA_PESOS,0) AS GASTO_PHARMA,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,NVL(t16.RECUPERACION_GASTO,0),0) AS RECUPERACION_GASTO,       
        'pendiente' AS IVA_COTIZACIONES,
         IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,NVL(t15.IVA_RECUPERADO,0),0) AS IVA_RECUPERADO,     
         IFF(T1.ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(MONTO_COTIZACION > MONTO_SALUD,
                                           monto_subsidio + monto_caja + monto_invalidez_sobr + monto_afp  + monto_cotizacion,
                                           monto_subsidio + monto_caja + monto_invalidez_sobr + monto_afp  + monto_salud ),0 ) AS GASTO_LICENCIAS,
        IFF(t1.ID = FIRST_ID, t10."fld_ultrenta",0) AS RENTA, 
        CASE	
            WHEN RENTA > 1 and RENTA <=500000	
                THEN '>1 <= 500'
            WHEN RENTA > 500001 and RENTA <= 1000000	
                THEN '>500 y <= 1000'	
            WHEN RENTA > 1000001 and RENTA <=1500000
                THEN '>1000 y <= 1500'	
            WHEN RENTA > 1500001 and RENTA <= 2300000
                THEN '>1500 y <= 2300'	
            WHEN RENTA > 2300001
                THEN '> 2300'
            ELSE '' 
        END AS  RANGO_RENTA,
        t9.VAL_MON AS VALOR_UF,        
        t17.COD_AGENCIA AS AGENCIA_VN,
        t17.COD_AGENTE AS AGENTE_VN,
        t17.PERIODO_ANUALIDAD AS PERIODO_ANUAL,
        t17.CLASIFICACION AS CLASIF_RIESGO,
        t17.COD_MOROSIDAD AS CLASIF_MOROSIDAD,       
        case 
            when  t4."fld_benedad" between 0 and 19 then '< 20 a�os'
            when  t4."fld_benedad" between 20 and 24 then '20 a 24 a�os'
            when  t4."fld_benedad" between 25 and 29 then '25 a 29 a�os'
            when  t4."fld_benedad" between 30 and 34 then '30 a 34 a�os'
            when  t4."fld_benedad" between 35 and 39 then '35 a 39 a�os'
            when  t4."fld_benedad" between 40 and 44 then '40 a 44 a�os'
            when  t4."fld_benedad" between 45 and 49 then '45 a 49 a�os'                  
            when  t4."fld_benedad" between 50 and 54 then '50 a 54 a�os'
            when  t4."fld_benedad" between 55 and 60 then '55 a 60 a�os'        
            when  t4."fld_benedad" > 60 then '> 60 y mas a�os'
            else ''
        end AS RANGO_EDAD,        
         first_value( t4."fld_benedad") over (partition by t1.periodo, t1.RUT_TITULAR order by  t4."fld_bencorrel") as EDAD_TITULAR,
         case 
            when  EDAD_TITULAR between 0 and 19 then '< 20 a�os'
            when  EDAD_TITULAR between 20 and 24 then '20 a 24 a�os'
            when  EDAD_TITULAR between 25 and 29 then '25 a 29 a�os'
            when  EDAD_TITULAR between 30 and 34 then '30 a 34 a�os'
            when  EDAD_TITULAR between 35 and 39 then '35 a 39 a�os'
            when  EDAD_TITULAR between 40 and 44 then '40 a 44 a�os'
            when  EDAD_TITULAR between 45 and 49 then '45 a 49 a�os'                  
            when  EDAD_TITULAR between 50 and 54 then '50 a 54 a�os'
            when  EDAD_TITULAR between 55 and 60 then '55 a 60 a�os'        
            when  EDAD_TITULAR > 60 then '> 60 y mas a�os'
            else ''
        end AS RANGO_EDAD_TITULAR,   
        CASE	
          WHEN t17.COSTO_FINAL_UF > 0 and t17.COSTO_FINAL_UF <=2	
          THEN '>0 <= 2.0 UF'
          WHEN t17.COSTO_FINAL_UF > 2 and t17.COSTO_FINAL_UF <=3	
          THEN '>2.0 <= 3.0 UF'	
          WHEN t17.COSTO_FINAL_UF > 3 and t17.COSTO_FINAL_UF <=4	
          THEN '>3.0 <= 4.0 UF'	
          WHEN t17.COSTO_FINAL_UF > 4 and t17.COSTO_FINAL_UF <=5	
          THEN '>4.0 <= 5.0 UF'	
          WHEN t17.COSTO_FINAL_UF > 5	
          THEN '>5.0 U'
          ELSE ''
        END AS RANGO_COTIZACION,
        CASE
          WHEN GASTOS_ANTIGUEDAD <=12
          THEN '00 a 12 meses'
          WHEN GASTOS_ANTIGUEDAD >12 AND GASTOS_ANTIGUEDAD <=24
          THEN '>12 a 24 meses'
          WHEN GASTOS_ANTIGUEDAD >24 AND GASTOS_ANTIGUEDAD <=36
          THEN '>24 a 36 meses'
          WHEN GASTOS_ANTIGUEDAD >36 AND GASTOS_ANTIGUEDAD <=48
          THEN '>36 a 48 meses'
          WHEN GASTOS_ANTIGUEDAD >48 AND GASTOS_ANTIGUEDAD <=60
          THEN '>48 a 60 meses'
          WHEN GASTOS_ANTIGUEDAD >60
          THEN '>60' 
          ELSE ''        
        END AS RANGO_ANTIGUEDAD,       
        CASE
          WHEN t6."fld_valorbase" =0
          THEN 'igual a 0'
          WHEN t6."fld_valorbase" > 0.01 AND t6."fld_valorbase" <= 1.50
          THEN '0.01 - 1.50'
          WHEN t6."fld_valorbase" >1.51 AND t6."fld_valorbase" <= 2
          THEN '1.51 - 2.00'
          WHEN t6."fld_valorbase" >2.01 AND t6."fld_valorbase" <=3
          THEN '2.01 - 3.00'
          WHEN t6."fld_valorbase" >3.01 AND t6."fld_valorbase" <=4
          THEN '3.01 a 4.00'
          WHEN t6."fld_valorbase" >4.01 AND t6."fld_valorbase" <=5
          THEN '4.01 a 5.00'
          WHEN t6."fld_valorbase" >5.01 
          THEN '5.01 y mas'
          ELSE ''
        END AS RANGO_PRECIO,
        t4."fld_bennombre" AS FLD_BENNOMBRE,
        t4."fld_benapepa" AS FLD_BENAPEPA, 
        t4."fld_benapema" AS FLD_BENAPEMA,       
        t6."fld_funfolio" AS FLD_FUNFOLIO,
        t6."fld_funcorrel" AS FLD_FUNCORREL,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t12.Valor_Iva,0) AS IVA,
        t17.COD_NEGOCIO,        
        t13.GLS_MODCOTZ AS TIPO_COTIZACION,
        IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,PAGADO - EXCESOS - EXCEDENTES,0) AS RECAUDADO,
        IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,PAGADO - EXCESOS - EXCEDENTES - IVA,0) AS INGRESOS,
        t1.FECHA_EMISION_PAGO,
        t1.ID,
        t1.FUENTE_ORIGEN,
        t1.ORIGEN,
        t1.TIPO_BONO,
        t1.GLS_TIPO_BONO,
        t1.COD_GRUPO,
        t1.GRUPO,
        t1.COD_SUBGRUPO,
        t1.SUBGRUPO,
        t1.COD_CIE,
        t1.GLOSA_CIE,
        t1.GRUPO_CIE,  
        t1.HOLDING_PRESTADOR,
        t1.RUT_INSTITUCION,
        t1.NOMBRE_INSTITUCION,
         IFF(t1.rut_institucion=t18."rut",t18."cod_prestad",t1.NOMBRE_INSTITUCION) AS PRESTADOR,
        IFF(REGISTRO_ID=1,nvl(t1.COBRADO,0),0) COBRADO,
        IFF(REGISTRO_ID=1,nvl(t1.BONIFICADO,0),0) BONIFICADO,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_SUBSIDIO,0),0) MONTO_SUBSIDIO,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_CAJA,0),0) MONTO_CAJA,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_INVALIDEZ_SOBR,0),0) MONTO_INVALIDEZ_SOBR,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_AFP,0),0) MONTO_AFP,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_COTIZACION,0),0) MONTO_COTIZACION,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_SALUD,0),0) MONTO_SALUD,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_SEGURO,0),0) MONTO_SEGURO,       
        t19.PERIODO_VIGENCIA AS Z_PERIODO, 
        t19.NUM_CONTRATO  AS Z_NUM_CONTRATO, 
        t19.NUM_CORRELATIVO AS Z_NUM_CORRELATIVO, 
        t19.rut_titular AS Z_RUT_TITULAR,
        t19."fld_cotrut" AS Z_FLD_COTRUT, 
        t19."fld_bencorrel" AS Z_FLD_BENCORREL,
        CASE t19."fld_bensexo" 
          WHEN 2 THEN 'F'
          ELSE 'M' 
        END AS Z_SEXO,  
        to_date(to_varchar(t19."fld_bennacfec",'YYYY-MM-DD')) AS Z_FECHA_NACIMIENTO,
        t19."fld_benedad" AS Z_EDAD,
        t19.COSTO_FINAL_UF AS Z_COSTO_FINAL_UF,
        t19."fld_bennombre" AS Z_FLD_BENNOMBRE,
        t19."fld_benapepa" AS Z_FLD_BENAPEPA, 
        t19."fld_benapema" AS Z_FLD_BENAPEMA    
     FROM "DTM"."P_STG_DTM_GTO"."P_STG_DTM_GTO_HECHOS_GTO" t1
        LEFT OUTER JOIN "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" 			t17 ON (t1.periodo = t17.periodo_vigencia and t1.rut_titular = t17.rut_titular)
        -- Liga con Sucursal
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPRESUCURSAL" 		t2  ON (t17.COD_SUCURSAL = t2.COD_SUCUR)
        -- Liga con Categoria
        LEFT OUTER JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA"   t3  ON (t1.cod_categoria = t3.cod_categoria)
        -- INICIO BEN Liga para agregar Nombre y Genero
        LEFT OUTER JOIN  "AFI"."P_RDV_DST_LND_SYB"."BEN" |				t4  ON (t1.RUT_TITULAR = t4."fld_cotrut" and t1.cod_beneficiario = t4."fld_bencorrel" and t4.FLD_BENVIGREALDESDE <= t1.periodo AND t4.FLD_BENVIGREALHASTA >= t1.periodo )
        -- FIN BEN        
        -- Liga para agregar tipo_plan, detalle_producto
        LEFT OUTER JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES 				t5  ON (t1.cod_categoria = t5.COD_DESACA)        
        -- Liga para agregar datos de CNT
        LEFT OUTER JOIN "AFI"."P_RDV_DST_LND_SYB"."CNT" 				t6  ON (t1.RUT_TITULAR = t6."fld_cotrut" and t6.FLD_VIGREALDESDE <= t1.periodo AND t6.FLD_VIGREALHASTA >= t1.periodo )
        -- Liga para agregar COMUNA       
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPRECOMUNAS"       t7  ON (t1.COMUNATRABAJA = t7.COD_COMUNA )
        -- Liga para agregar Region
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPREREGION"        t8  ON (t17.COD_REGION = t8.COD_REGION )      
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPREMONEDA" 		t9  ON (t1.periodo = to_number(to_char(dateadd(month, 1, fec_val),'YYYYMM')))  
        -- Liga para obtener la renta_imponible
        -- Inicio Renta Imponible
        LEFT OUTER JOIN   (		
        						SELECT 	
				        			"fld_funnotiffun",
				        			"fld_funcorrel" ,
				           			count("fld_emprut") as "fld_emprut", 
				           			sum("fld_ultrenta") as "fld_ultrenta" 
			           		 	FROM AFI.P_RDV_DST_LND_SYB.COB
			           		 	GROUP BY 
			           		 		"fld_funnotiffun",
			        				"fld_funcorrel" 
			           	   ) t10 ON ( t10."fld_funnotiffun" = SUBSTRING(T1.CONTRATO,0,9)::int AND t10."fld_funcorrel" =t1.CORRELATIVO)
        -- Fin Renta Imponible
        -- Liga con Sucursal para obtener centro de costos y ubicacion
        LEFT OUTER JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_SUCURSAL2"  	t11 ON (t1.SUCURSAL_CONTRATO = t11.cod_sucursal)
        -- Liga para obtener el iva   -- se agrega minuto tiempo corrida, al agregar la columna
        LEFT OUTER JOIN (	
        					select 
        						perpag_esperado, 
        						rut_cotizante, 
        						sum( Valor_Iva) as  Valor_Iva
                        	from "RCD"."P_DDV_RCD"."P_DDV_RCD_V_LIBRO_VENTAS"
                        	where  Codigo_Pago = 0
                        	group by 
                        		perpag_esperado, 
                        		rut_cotizante
                        ) t12   ON  (t1.periodo = t12.perpag_esperado and t1.rut_titular = rut_cotizante )                
        -- Liga para agregar GLS_MODCOTZ - Tipo de Cotizacion
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPREMODCOTZ" 		t13  ON (t6."fld_monedatipocod" = t13.COD_MODCOTZ)            
        -- Liga para agregar Iva Recuperado
        LEFT OUTER JOIN (    SELECT t1.PERIODO,
				                  t1.RUT AS RUT_TITULAR,
				                  SUM(t1.monto_credito_fiscal) AS IVA_RECUPERADO
				              FROM "CON"."P_DDV_CON"."P_DDV_CON_IVA_RECUPERADO_BONOS" t1                         
				              GROUP BY 
				              	t1.periodo, 
				              	t1.rut          
        				) t15   ON (t1.periodo = t15.periodo and t1.rut_titular = t15.rut_titular)  
        -- Liga para agregar recuperacion del gasto
        LEFT OUTER JOIN (
				            SELECT 
				            	t1."periodo_vig" AS PERIODO,
				            	t1."rut_titular" AS RUT_TITULAR,
				            	SUM(t1."monto_recuperado") RECUPERACION_GASTO   
				            FROM "IQ"."P_DDV_IQ"."RECUPERACION_GASTO" t1
				            GROUP BY 
				            	t1."periodo_vig", 
				            	t1."rut_titular"
       					) t16    ON (t1.periodo = t16.periodo and t1.rut_titular = t16.rut_titular)  
        --Liga para agregar el Prestador
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."PRM_AGRUPA_PRESTADORES" t18 ON (t1.rut_institucion = t18."rut")       
        -- Ligar contrato para traer valores vacios de otros periodos
        -- Inicio para Contratos           
        LEFT OUTER JOIN (
				          SELECT s1.PERIODO_VIGENCIA, 
				              	s1.NUM_CONTRATO, 
				                s1.NUM_CORRELATIVO, 
				                s1.rut_titular,
				                s4."fld_cotrut", 
				                s4."fld_bencorrel",
				                s4."fld_bensexo",  
				                s4."fld_bennacfec",
				                s4."fld_benedad",
				                s1.COSTO_FINAL_UF,
				                s4."fld_bennombre",
				                s4."fld_benapepa", 
				                s4."fld_benapema"    
				        from (( SELECT 
				                 	r1.PERIODO_VIGENCIA, 
				                 	r1.NUM_CONTRATO, 
				                 	r1.NUM_CORRELATIVO, 
				                 	r1.RUT_TITULAR,
				                    r2.FECHA_INGRESO, 
				                    r2.COD_REGION, 
				                    r2.COD_ACTIVIDAD, 
				                    r2.TIPO_TRABAJADOR, 
				                    r2.TIPO_TRANSACCION, 
				                    r2.COSTO_FINAL_UF, 
				                    r2.PAGADO_TOTAL_PESOS, 
				                    r2.MONTO_EXCESO,
				                    r2.MONTO_EXCEDENTE, 
				                    r2.GASTO_PHARMA_PESOS, 
				                    r2.COD_AGENCIA, 
				                    r2.COD_AGENTE, 
				                    r2.PERIODO_ANUALIDAD, 
				                    r2.CLASIFICACION, 
				                    r2.COD_MOROSIDAD, 
				                    r2.COD_NEGOCIO
				                FROM
				                    ( SELECT  
				                    		MAX(s1.PERIODO_VIGENCIA) AS PERIODO_VIGENCIA, 
				                    		s1.NUM_CONTRATO, 
				                    		s1.NUM_CORRELATIVO, 
				                    		s1.RUT_TITULAR
				                        from "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" s1  
				                        GROUP BY 
				                        	s1.NUM_CONTRATO, 
				                        	s1.NUM_CORRELATIVO, 
				                        	s1.RUT_TITULAR 
				                    ) r1
				                LEFT OUTER JOIN "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" r2  ON (r1.PERIODO_VIGENCIA = r2.PERIODO_VIGENCIA  AND r1.NUM_CONTRATO  = r2.NUM_CONTRATO AND r1.NUM_CORRELATIVO = r2.NUM_CORRELATIVO AND r1.RUT_TITULAR = r2.RUT_TITULAR)
				               ) s1
				                LEFT OUTER JOIN  "AFI"."P_RDV_DST_LND_SYB"."BEN"       s4  ON (s1.RUT_TITULAR = s4."fld_cotrut" and s1.num_correlativo = s4."fld_funcorrel" AND  s4.FLD_BENVIGREALDESDE <= s1.periodo_vigencia AND s4.FLD_BENVIGREALHASTA >= s1.periodo_vigencia)
				                ) 
				        ) t19 ON (t1.rut_titular = t19.rut_titular  and t1.correlativo = t19.NUM_CORRELATIVO  and t1.cod_beneficiario = t19."fld_bencorrel" and t1.CONTRATO = t19.NUM_CONTRATO)            
        -- Fin para Contratos        
    WHERE    t1.periodo BETWEEN
    		EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-48))||' 23:59:59')) AND 
            EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59'))  
----------------------------------------
            
            
            
            
            
SELECT DISTINCT periodo, COUNT(1) FROM est.P_STG_EST.GASTOS_BEN 
WHERE FECHA_INGRESO IS NULL
GROUP BY PERIODO 
LIMIT 10
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            



--------- cartera mensual por vigenca desde hasta.
SET meses = 7;
SET periodo_ini = dateadd('month',-$meses, time_slice(current_date(), 1, 'month', 'start')) ; --'2020-01-01 00:00:00.000'::timestamp;
SET periodo_fin = TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),0))||' 23:59:59')::timestamp;
WITH
  nums(i) AS (
        SELECT 0
        		UNION ALL SELECT i + 1
        FROM nums
        WHERE i <=$meses
	         )
             ,
  periods AS (
		SELECT dateadd(month, i::int, $periodo_ini) AS period
		FROM nums
		     ),
-------- cartera por el periodo designado 
 _sample AS (
    SELECT DISTINCT 
    	   cnt."fld_funfolio" as contrato,
           cnt."fld_funcorrel" as correlativo,
           cnt."fld_cotrut" AS rut_afiliado,
           ben."fld_bennacfec",
		   ben."fld_bensexo",
		   cnt."fld_carganum",
		   cnt."fld_region" ,
		   cnt."fld_valorbase",
           cnt."fld_inivigfec" as inivig, 
           cnt."fld_finvigfec" as finvig,
       	   cnt."fld_planvig" AS tipo_plan,
       	   cnt."fld_cntcateg" AS categoria,
       	   cnt."fld_concostototal" AS costo_total,
       	   cnt."fld_agecod" AS codigo_agente,
       	   cnt."fld_agencianum" AS codigo_agencia   	          		
    FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
   	LEFT JOIN  
		( SELECT DISTINCT 
				ben."fld_funfolio" , 
				ben."fld_funcorrel" , 
				ben."fld_bennacfec" ,
				ben."fld_bensexo" 
		  FROM AFI.P_RDV_DST_LND_SYB.BEN 
		  WHERE ben."fld_bencorrel" IN (0)
		 ) ben ON  cnt."fld_funfolio" = ben."fld_funfolio" AND  cnt."fld_funcorrel" = ben."fld_funcorrel" 
    WHERE cnt."fld_inivigfec" <= $periodo_fin AND     --'2019-12-31 23:59:59.000'::timestamp
       	  cnt."fld_finvigfec" >= $periodo_ini         --'2019-01-01 00:00:00.000'::timestamp
)
,  union_ AS ( 
-------- union periodos y cartera
	SELECT DISTINCT 
		   EST.P_DDV_EST.FECHA_PERIODO(m.period) AS periodo_ciclo,
		   m.period::date AS periodo_fecha,
		   cnt.*
		   --contrato,
		   --correlativo,
		   --rut_afiliado
	FROM _sample  cnt
	FULL JOIN  periods  m ON m.period BETWEEN cnt.inivig AND cnt.finvig
	--group by m.period
	ORDER BY periodo_ciclo
)
SELECT 
	uni.periodo_ciclo,
	uni.periodo_fecha,
	sum(con.PAGADO_TOTAL_PESOS),
	sum(t14.dm_hospitalario)
	FROM UNION_ uni
	LEFT JOIN afi.P_DDV_AFI.P_DDV_AFI_CONTRATO con ON (uni.rut_afiliado=con.RUT_TITULAR  AND uni.periodo_ciclo=con.PERIODO_VIGENCIA )
	LEFT JOIN (
             SELECT 
             	t1.PERIODO, 
                t1.RUT_TITULAR, 
                cod_beneficiario, 	
                SUM(DM_HOSPITALARIO) AS DM_HOSPITALARIO, 
                SUM(DM_AMBULATORIO) AS DM_AMBULATORIO, 
                SUM(DM_GES) AS DM_GES, 
                SUM(DM_CAEC_HOSPITALARIO) AS DM_CAEC_HOSPITALARIO,
                SUM(DM_CAEC_AMBULATORIO) AS DM_CAEC_AMBULATORIO,
                SUM(DM_CAEC) AS DM_CAEC, 
                SUM(DM_LICENCIA) AS DM_LICENCIA 
            FROM 
                (		SELECT 
                        T1.PERIODO, 
                        T1.RUT_TITULAR, 
                        t1.cod_beneficiario, 
                        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO<>3,sum(BONIFICADO),0 ) AS DM_HOSPITALARIO,
                        IFF(T1.ORIGEN = 'AMBULATORIO'and T1.TIPO_BONO<>3 ,sum(BONIFICADO),0 ) AS DM_AMBULATORIO,
                        IFF(T1.ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES,
                        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO=3,sum(BONIFICADO),0 ) AS DM_CAEC_HOSPITALARIO,
                        IFF(T1.ORIGEN = 'AMBULATORIO' and T1.TIPO_BONO=3,sum(BONIFICADO),0 ) AS DM_CAEC_AMBULATORIO,
                        sum(MONTO_CAEC) AS DM_CAEC,
                        IFF(T1.ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
                                                       SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
                                                       SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
                        FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO T1
                        WHERE T1.PERIODO  BETWEEN 
                            -- Parametros para filtrar los gastos del Datamart
                            -- PERIODO INICIAL y PERIODO FINAL, se obtiene el maximo periodo de contrato y se restan 23 meses
                            --to_number(to_char(dateadd(month,-35,EST.P_STG_EST.CONTRATO_MAX_PERIDO()),'YYYYMM'))  AND 
                            --to_number(to_char(EST.P_STG_EST.CONTRATO_MAX_PERIDO(),'YYYYMM'))
                            EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-7))||' 23:59:59')) AND 
            				EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59'))
                        GROUP BY T1.PERIODO,
                             T1.ORIGEN, t1.tipo_bono, T1.RUT_TITULAR, t1.cod_beneficiario, tipo_licencia
                ) t1
            group by  T1.PERIODO, T1.RUT_TITULAR, t1.cod_beneficiario
          ) t14 ON (uni.periodo_ciclo = t14.periodo and  uni.rut_afiliado = t14.rut_titular)  --and  NVL(t4."fld_bencorrel",0) = t14.cod_beneficiario)
WHERE periodo_ciclo = 202105
GROUP BY periodo_ciclo,periodo_fecha
ORDER BY periodo_ciclo
LIMIT 10
--------------------------------------------- fin cartera por mes vigencia desde hasta 





------------ pagos por la tabla pag 
WITH pagos AS ( 
SELECT 
pag."pag_percot",
cnt."fld_funfolio" ,
cnt."fld_funcorrel" ,
cnt."fld_cotrut" ,
pag."pag_tipdoc" ,
pag."pag_pagval" 
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
INNER JOIN RCD.P_DDV_RCD.P_DDV_RCD_V_PAG pag 
	ON (cnt."fld_funfolio" = pag."pag_funfol" AND cnt."fld_funcorrel" = pag."pag_funcor" AND pag."pag_percot"='2021/04')
WHERE 202104 BETWEEN FLD_VIGREALDESDE  AND FLD_VIGREALHASTA 
), ordenar_pagos AS ( 
SELECT 
	"pag_percot" ,
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_cotrut" ,
	"pag_tipdoc",
	CASE 
		WHEN "pag_tipdoc" NOT IN (8) THEN "pag_pagval"
		ELSE 0
	END AS pagos,
	CASE 
		WHEN "pag_tipdoc" IN (8) THEN "pag_pagval"
		ELSE 0
	END AS devoluciones,
	pagos-devoluciones AS pago_final
FROM pagos
ORDER BY "fld_cotrut" 
/*GROUP BY 
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_cotrut" ,
	"pag_tipdoc"*/
--WHERE "pag_pagval" >= 999999 ---"fld_cotrut" = '002305373-K' AND   -- AND "pag_tipdoc" NOT IN (8) --"pag_tipdoc"  IS NULL 
)
SELECT "pag_percot", count(DISTINCT("fld_cotrut")) AS cant_afiliados,  sum(pago_final)::int AS monto FROM ordenar_pagos
--WHERE "pag_tipdoc" NOT in (8)
GROUP BY "pag_percot"
--ORDER BY "pag_tipdoc"
---------------- fin pagos por la tabla pag
















SELECT * FROM uni_con_dtm
ORDER BY rut_titular_con
LIMIT 50


SELECT 
	count(1),
	count(DISTINCT(rut_titular_con)),
	sum(PAGADO_TOTAL_PESOS)
FROM uni_con_dtm
WHERE rev_titular ='titular'























