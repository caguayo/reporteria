SELECT  * FROM ISA.P_RDV_DST_LND_SYB.EMP_COB
WHERE "fld_cotrut" = '000035865-7'


SELECT top 200 periodo ,RUT_AFILIADO , contrato, correlativo,"fld_cottipotrcod" , tipo_trabajador  FROM est.P_STG_EST.ca_cartera
ORDER BY RUT_AFILIADO , periodo


SELECT top 10 * FROM est.P_STG_EST.ca_cartera


SELECT * FROM est.P_STG_EST.ca_cartera
WHERE identificar_vigencia='mes vigente'



SELECT top 10 * FROM est.P_STG_EST.ca_cartera
WHERE rango_edad = '' AND IDENTIFICAR_VIGENCIA ='mes vigente'




SELECT DISTINCT "fld_funnotiffun", "fld_funcorrel" , "fld_cottipotrcod" FROM afi.P_RDV_DST_LND_SYB.COB
WHERE "fld_funnotiffun" = 10673014 and "fld_funcorrel" = 10

SELECT periodo, tipo_trabajador, count(1), count(DISTINCT RUT_AFILIADO) AS cant_afi FROM est.P_STG_EST.ca_cartera
GROUP BY periodo , tipo_trabajador
ORDER BY periodo


SELECT "fld_cotrut" , "fld_funfolio" , "fld_funcorrel" , "fld_concostototal" FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE  "fld_funfolio" IN (4244344)



WHERE 202106 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA 
ORDER BY "fld_concostototal"  DESC 


SELECT  * FROM ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA


SELECT  * FROM afi.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_cotrut" = '000035865-7'
ORDER BY "fld_funfolio" , "fld_funcorrel" 






---- calculando permanencia 
WITH vigencia AS (
	SELECT DISTINCT 
			"fld_cotrut" , 
			"fld_ingreso" ,
			MIN(FLD_VIGREALDESDE) AS minima_vigencia 
	FROM AFI.P_RDV_DST_LND_SYB.CNT 
	GROUP BY 
			"fld_cotrut",
			"fld_ingreso"
	ORDER BY "fld_cotrut"
), a_b AS ( 
	SELECT DISTINCT 
		a."fld_funfolio",
		a."fld_funcorrel",
		a."fld_cotrut" , 
		b."fld_ingreso",
		last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig
	FROM AFI.P_RDV_DST_LND_SYB.CNT a
		LEFT JOIN vigencia b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
	    WHERE a."fld_inivigfec" <= $periodo_fin AND     --'2019-12-31 23:59:59.000'::timestamp
       	      a."fld_finvigfec" >= $periodo_ini         --'2019-01-01 00:00:00.000'::timestamp
), a_b_c AS (
	SELECT 
	*,
	'2021-03-31' AS periodo_resta,
	DATEDIFF(month, minima_vig, '2021-03-31')+1 AS resta_meses
	FROM a_b)
SELECT
*
FROM a_b_c
WHERE "fld_cotrut" ='010628175-0'	
/*
SELECT 
count(DISTINCT "fld_cotrut") AS cant_titulares,
sum(resta_meses) AS total_meses,
TOTAL_MESES/cant_titulares AS div_meses_titu,
div_meses_titu/12 AS div_final
FROM a_b_c
*/
------------------ fin calculo permanencia






------ loop de extraccion de la cartera por periodo mensual
-----------------------------------------------------------

SET periodo_ini = dateadd('month',-12, time_slice(current_date(), 1, 'month', 'start'));   --'2020-01-01 00:00:00.000'::timestamp;

SET periodo_fin = TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),0))||' 23:59:59')::timestamp;

CREATE OR REPLACE TABLE est.P_STG_EST.ca_cartera as (
-------- tabla con los periodos del ciclo
WITH
  nums(i) AS (
        SELECT 0
        		UNION ALL SELECT i + 1
        FROM nums
        WHERE i <=12
	         )
             ,
  periods AS (
		SELECT dateadd(month, i::int, $periodo_ini) AS period
		FROM nums
		     ),
-------- cartera por el periodo designado 
 _sample AS (
    SELECT DISTINCT 
    	   cnt."fld_funfolio" as contrato,
           cnt."fld_funcorrel" as correlativo,
           cnt."fld_cotrut" AS rut_afiliado,
           ben."fld_bennacfec",
		   ben."fld_bensexo",
		   cnt."fld_carganum",
		   cnt."fld_region" ,
		   cnt."fld_valorbase",
           cnt."fld_inivigfec" as inivig, 
           cnt."fld_finvigfec" as finvig,
       	   cnt."fld_planvig" AS tipo_plan,
       	   cnt."fld_cntcateg" AS categoria,
       	   cnt."fld_concostototal" AS costo_total,
       	   cnt."fld_agecod" AS codigo_agente,
       	   cnt."fld_agencianum" AS codigo_agencia   	          		
    FROM "AFI"."P_RDV_DST_LND_SYB"."CNT" cnt
   	LEFT JOIN  
		( SELECT DISTINCT 
				ben."fld_funfolio" , 
				ben."fld_funcorrel" , 
				ben."fld_bennacfec" ,
				ben."fld_bensexo" 
		  FROM AFI.P_RDV_DST_LND_SYB.BEN 
		  WHERE ben."fld_bencorrel" IN (0)
		 ) ben ON  cnt."fld_funfolio" = ben."fld_funfolio" AND  cnt."fld_funcorrel" = ben."fld_funcorrel" 
    WHERE cnt."fld_inivigfec" <= $periodo_fin AND     --'2019-12-31 23:59:59.000'::timestamp
       	  cnt."fld_finvigfec" >= $periodo_ini         --'2019-01-01 00:00:00.000'::timestamp
),  union_ AS ( 
-------- union periodos y cartera
	SELECT DISTINCT 
		   m.period,
		   cnt.*
		   --contrato,
		   --correlativo,
		   --rut_afiliado
	FROM _sample  cnt
	FULL JOIN  periods  m ON m.period BETWEEN cnt.inivig AND cnt.finvig
	--group by m.period
	ORDER BY m.period
)
SELECT  
		CASE 
			WHEN uni."fld_region"  IN (13) THEN 'Metropolitana'
			ELSE 'Regiones'
		END AS region,
		CASE 
			WHEN uni.tipo_plan IN ('E6    ') THEN 'Colectivo'
			ELSE 'Individual'
		END AS tipo_plan_gls,
		CASE 
			WHEN uni."fld_bensexo" = 1 THEN 'Hombre'
			ELSE 'Mujer'
		END AS sexo,
		CASE 
			WHEN dateadd('month',-1, time_slice(current_date(), 1, 'month', 'start')) = uni.period
			THEN 'mes vigente'
			ELSE 'otro mes'
		END AS identificar_vigencia,      
        CASE
          WHEN uni."fld_valorbase" =0
          THEN 'igual a 0'
          WHEN uni."fld_valorbase" >0.01 AND uni."fld_valorbase" <= 1.50
          THEN '0.01 - 1.50'
          WHEN uni."fld_valorbase" >1.51 AND uni."fld_valorbase" <= 2
          THEN '1.51 - 2.00'
          WHEN uni."fld_valorbase" >2.01 AND uni."fld_valorbase" <= 3
          THEN '2.01 - 3.00'
          WHEN uni."fld_valorbase" >3.01 AND uni."fld_valorbase" <= 4
          THEN '3.01 a 4.00'
          WHEN uni."fld_valorbase" >4.01 AND uni."fld_valorbase" <= 5
          THEN '4.01 a 5.00'
          WHEN uni."fld_valorbase" >5.01 
          THEN '5.01 y mas'
          ELSE ''
        END AS RANGO_PRECIO,    
        est.P_DDV_EST.fecha_periodo(uni.period) AS periodo,
        (datediff(month,uni."fld_bennacfec", uni.period) / 12)::int AS Edad,
        CASE 
            WHEN  Edad BETWEEN 0 AND 19 THEN '< 20 A�os'
            WHEN  Edad BETWEEN 20 AND 24 THEN '20 a 24 A�os'
            WHEN  Edad BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  Edad BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  Edad BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  Edad BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  Edad BETWEEN 45 AND 49 THEN '45 a 49 A�os'                  
            WHEN  Edad BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  Edad BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  Edad > 60 then '> 60 y mas A�os'
            ELSE ''
        END AS RANGO_EDAD,
        cob."fld_cottipotrcod",
        CASE 
        	WHEN cob."fld_cottipotrcod" = 1 THEN 'Dependiente'
        	WHEN cob."fld_cottipotrcod"  = 2 THEN 'Independiente'
        	WHEN cob."fld_cottipotrcod"  = 3 THEN 'Pensionado'
        	WHEN cob."fld_cottipotrcod"  = 4 THEN 'Voluntario'
        ELSE 'Otros'
        END AS tipo_trabajador,
        money.VAL_MON::int  AS mes_uf,
        (CASE 
        	WHEN costo_total >= 100 THEN costo_total/mes_uf 
        	ELSE costo_total
        END)::double  AS costo_total_final,
        CASE	
          WHEN costo_total_final > 0 AND costo_total_final <= 2	
          THEN '>0 <= 2.0 UF'
          WHEN costo_total_final > 2 AND costo_total_final <= 3	
          THEN '>2.0 <= 3.0 UF'	
          WHEN costo_total_final > 3 AND costo_total_final <= 4	
          THEN '>3.0 <= 4.0 UF'	
          WHEN costo_total_final > 4 AND costo_total_final <= 5	
          THEN '>4.0 <= 5.0 UF'	
          WHEN costo_total_final > 5	
          THEN '>5.0 U'
          ELSE ''
        END AS RANGO_COTIZACION,  
		uni.*
FROM union_ uni
LEFT JOIN  	( 
			 SELECT DISTINCT 
			 		"fld_funnotiffun", 
			 		"fld_funcorrel" , 
			 		"fld_cottipotrcod" 
			 FROM afi.P_RDV_DST_LND_SYB.COB
			) cob ON ( uni.contrato = cob."fld_funnotiffun" and uni.correlativo = cob."fld_funcorrel" )
LEFT JOIN  ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA money ON (est.P_DDV_EST.fecha_periodo(uni.period) = est.P_DDV_EST.fecha_periodo(money.FEC_VAL))
)


SELECT * FROM EST.P_STG_EST.CA_CARTERA LIMIT 10



--------------- ejecucion por transacciones 

SET periodo_ini =dateadd('month',-12, time_slice(current_date(), 1, 'month', 'start'));--'2020-01-01 00:00:00.000'::timestamp;

SET periodo_fin = TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59')::timestamp;
---------- ejecucion tabla transacciones
CREATE OR replace TABLE est.P_STG_EST.ca_transacciones AS ( 
WITH transacciones_ AS (
	select 
		est.P_DDV_EST.fecha_periodo(lfc."fld_periodoprod") AS periodo,
		lfc."fld_periodoprod" as fecha_produccion,
	    lfc."fld_cotrut" as rut_afiliado,
	    --SUBSTRING("fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	    ben."fld_bennacfec",
	    ben."fld_bensexo" ,
	    lfc."fld_fechafun" as fecha_fun,
	    lfc."fld_funfolio" as contrato,
	    lfc."fld_funcorrel" as correlativo,
	    lfc."fld_folio" as folio,
		lfc."fld_emprut" as rut_empleador,
	    lfc."fld_fectraspaso" as fecha_traspaso,
	    lfc."fld_fechareal" as fecha_real,
		lfc."tipo_plan" AS tipo_plan,
		lfc."fld_costofinalcto" AS costo_final,
		lfc."fld_cntcateg",
		lfc."fld_agente" ,
		lfc."fld_agencia" ,
		lfc."fld_succod" ,
		CASE 
			WHEN lfc."fld_funnotificacod"  = 1  AND lfc."fld_tpotransac" IN('VN','CT') THEN 'Afiliacion'
			WHEN (lfc."fld_funnotificacod"=2 or lfc."fld_funnotificacod"=333) THEN 'Desafiliacion'
		END AS tipo_transaccion
	FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
		LEFT JOIN  
			( SELECT DISTINCT 
					ben."fld_funfolio" , 
					ben."fld_funcorrel" , 
					ben."fld_bennacfec" ,
					ben."fld_bensexo" 
			  FROM AFI.P_RDV_DST_LND_SYB.BEN 
			  WHERE ben."fld_bencorrel" IN (0)
			 ) ben ON  lfc."fld_funfolio" = ben."fld_funfolio" AND  lfc."fld_funcorrel" = ben."fld_funcorrel"
	WHERE   "fld_periodoprod" between $periodo_ini and $periodo_fin AND 
	    	 ("fld_funnotificacod"  = 1  AND  "fld_tpotransac" IN('VN','CT') OR  
	    	 ("fld_funnotificacod"=2 or "fld_funnotificacod"=333))
			)
SELECT 
	*,
	CASE 
		WHEN "fld_bensexo" = 1 THEN 'Hombre'
		ELSE 'Mujer'
	END AS sexo,
	CASE 
		WHEN dateadd('month',-1, time_slice(current_date(), 1, 'month', 'start')) = fecha_produccion
		THEN 'mes vigente'
		ELSE 'otro mes'
	END AS identificar_vigencia,
	CASE 
		WHEN tipo_plan IN ('E6') THEN 'Colectivo'
		ELSE 'Individual'
	END AS tip_plan
FROM transacciones_
)
	


SELECT * FROM EST.P_STG_EST.CA_TRANSACCIONES LIMIT 10



		
SELECT top 23 fecha_produccion, dateadd('month',-1, time_slice(current_date(), 1, 'month', 'start'))  AS test, * FROM est.P_STG_EST.ca_transacciones 
WHERE identificar_vigencia = 'mes vigente'





SELECT top 10 * FROM est.P_STG_EST.ca_cartera
 


SELECT DISTINCT tipo_plan, tipo_plan_gls, sexo, identificar_vigencia FROM est.P_STG_EST.ca_cartera

SELECT periodo, tipo_transaccion ,count(1) FROM est.P_STG_EST.ca_transacciones 
GROUP BY periodo, tipo_transaccion
ORDER BY periodo





SELECT top 10 * FROM est.P_STG_EST.ca_transacciones


SELECT DISTINCT sexo, tipo_plan_glosa, identificar_vigencia, "fld_planvig" FROM est.P_STG_EST.ca_transacciones


SELECT periodo, count(1) FROM est.P_STG_EST.ca_transacciones
GROUP BY periodo
ORDER BY PERIODO 






-------- basuras de codigos
--------------------------

------------------------------
---------AFILIACIONES
WITH transacciones_ AS (
	select 
		lfc."fld_periodoprod" as fecha_produccion,
	    lfc."fld_cotrut" as rut_afiliado,
	    --SUBSTRING("fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	    ben."fld_bennacfec",
	    ben."fld_bensexo" ,
	    lfc."fld_fechafun" as fecha_fun,
	    lfc."fld_funfolio" as contrato,
	    lfc."fld_funcorrel" as correlativo,
	    lfc."fld_folio" as folio,
		lfc."fld_emprut" as rut_empleador,
	    lfc."fld_fectraspaso" as fecha_traspaso,
	    lfc."fld_fechareal" as fecha_real,
		lfc."tipo_plan" AS tipo_plan,
		lfc."fld_costofinalcto" AS costo_final,
		lfc."fld_cntcateg",
		lfc."fld_agente" ,
		lfc."fld_agencia" ,
		lfc."fld_succod" ,
		CASE 
			WHEN lfc."fld_funnotificacod"  = 1  AND lfc."fld_tpotransac" IN('VN','CT') THEN 'Afiliacion'
			WHEN (lfc."fld_funnotificacod"=2 or lfc."fld_funnotificacod"=333) THEN 'Desafiliacion'
		END AS tipo_transaccion
	FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
		LEFT JOIN  
			( SELECT DISTINCT 
					ben."fld_funfolio" , 
					ben."fld_funcorrel" , 
					ben."fld_bennacfec" ,
					ben."fld_bensexo" 
			  FROM AFI.P_RDV_DST_LND_SYB.BEN 
			  WHERE ben."fld_bencorrel" IN (0)
			 ) ben ON  lfc."fld_funfolio" = ben."fld_funfolio" AND  lfc."fld_funcorrel" = ben."fld_funcorrel"
	WHERE   "fld_periodoprod" between '2015-04-01 00:00:00' and '2021-12-31 23:59:59' AND 
	    	 ("fld_funnotificacod"  = 1  AND  "fld_tpotransac" IN('VN','CT') OR  
	    	 ("fld_funnotificacod"=2 or "fld_funnotificacod"=333))
			)
SELECT top 200
	*,
	CASE 
		WHEN "fld_bensexo" = 1 THEN 'Hombre'
		ELSE 'Mujer'
	END AS sexo,
	CASE 
		WHEN dateadd('month',-1, time_slice(current_date(), 1, 'month', 'start')) = fecha_produccion
		THEN 'mes vigente'
		ELSE 'otro mes'
	END AS identificar_vigencia,
	CASE 
		WHEN tipo_plan IN ('E6') THEN 'Colectivo'
		ELSE 'Individual'
	END AS tip_plan
FROM transacciones_
--ORDER BY periodo DESC 
			
			
			
			
			
			
SELECT 
	est.P_DDV_EST.fecha_periodo(fecha_produccion) AS periodo,
	tipo_transaccion,
	count(DISTINCT rut_afiliado) AS cantidad ,
	sum(costo_final)::double AS costo_final_cto
FROM vn
GROUP BY periodo, tipo_transaccion 
ORDER BY periodo  
			
			
			
			
			
SELECT top 200
	*,
	CASE 
		WHEN "fld_bensexo" = 1 THEN 'Hombre'
		ELSE 'Mujer'
	END AS sexo,
	CASE 
		WHEN dateadd('month',-1, time_slice(current_date(), 1, 'month', 'start')) = periodo
		THEN 'mes vigente'
		ELSE 'otro mes'
	END AS identificar_vigencia,
	CASE 
		WHEN tipo_plan IN ('E6') THEN 'Colectivo'
		ELSE 'Individual'
	END AS tip_plan
FROM vn
--ORDER BY periodo DESC 


SELECT 
	est.P_DDV_EST.fecha_periodo(fecha_produccion) AS periodo,
	tipo_transaccion,
	count(DISTINCT id_rut_afiliado) AS cantidad ,
	sum(costo_final)::double AS costo_final_cto
FROM vn
GROUP BY periodo, tipo_transaccion 
ORDER BY periodo  







SELECT DISTINCT "tipo_plan" , count(1)FROM afi.P_RDV_DST_LND_SYB.LFC 
GROUP BY "tipo_plan"


SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.LFC 





-------------FIN AFILIACIONES





--------- desafilicaciones
WITH desafi AS (
select 
    lfc."fld_cotrut" as rut_afiliado,
    SUBSTRING(lfc."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
    lfc."fld_funfolio" as contrato,
    lfc."fld_funcorrel" as correlativo,
    lfc."fld_periodoprod"	as fecha,
 case (lfc."fld_funnotificacod")
	    when 2 then 'Desafiliacion'
		when 333 then 'renuncia voluntaria'
    end as motivo, 
    cnt."fld_ingreso" as fecha_ingreso
from AFI.P_RDV_DST_LND_SYB.LFC lfc
	inner join AFI.P_RDV_DST_LND_SYB.CNT cnt
	on lfc."fld_funfolio" = cnt."fld_funfolio"
	and lfc."fld_funcorrel" = cnt."fld_funcorrel"
	and "fld_periodoprod" between '2018-01-01 00:00:00' and '2018-12-31 23:59:59'
	and ("fld_funnotificacod"=2 or "fld_funnotificacod"=333)
)
SELECT 
	motivo, 
	COUNT(DISTINCT rut_afiliado) FROM desafi
GROUP BY motivo












SELECT * FROM EST.P_DDV_EST.CA_AFILIACIONES
WHERE rut_afiliado = '017768608-5'
	
SELECT est.P_DDV_EST.fecha_periodo(fecha_fun) AS fecha, count(1)  FROM EST.P_DDV_EST.CA_AFILIACIONES
GROUP BY fecha
ORDER BY fecha




SELECT 
	period,
	COUNT(1)
FROM union_
GROUP BY period
ORDER BY period




SELECT top 100 *
	--period,
	--COUNT(1)
FROM union_
WHERE rut_afiliado ='025573326-5'
--GROUP BY period
ORDER BY period




 
 
SELECT top 3 * FROM AFI.P_RDV_DST_LND_SYB.BEN 
  
SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.LFC 

SELECT top 3 * FROM afi.P_RDV_DST_LND_SYB.CNT 

 
 SELECT   * FROM "EST"."P_STG_EST".periods
 ORDER BY period ASC 
 		
 
SELECT count(1) FROM transacciones_
 
 
 
 select "fld_funfolio" as contrato,
       "fld_funcorrel" as correlativo,
       "fld_cotrut" AS rut_afiliado,
       "fld_inivigfec" as inivig, 
       "fld_finvigfec" as finvig
from "AFI"."P_RDV_DST_LND_SYB"."CNT"
LEFT JOIN 
where "fld_inivigfec" <= '2019-12-31 23:59:59.000'::timestamp
  and "fld_finvigfec" >= '2019-01-01 00:00:00.000'::timestamp
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 ---------- revision codigo javascript
  
  
  
-------------------
SET periodo_ini ='2019-01-01 00:00:00.000'::timestamp;

SET periodo_fin = TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),12))||' 23:59:59')::timestamp;


--create or replace view "EST"."P_STG_EST".periods as (
-------- tabla con los periodos del ciclo
with 
  nums(i) as (
        select 0
        	union all select i + 1 
        from nums 
        where i <=12
	         ),
  periods AS (
		select dateadd(month, i, '2019-01-01 00:00:00.000'::timestamp) as period 
		from nums
		     )
SELECT period FROM periods




create or replace view "EST"."P_STG_EST".periods as (
  with 
    nums(i) as (
        select 0
        	union all select i + 1 
        from nums 
        where i <=12
    )
  select dateadd(month, i, '2019-01-01 00:00:00.000'::timestamp) as period from nums
)



with
  nums(i) as (
        select 0
        	union all select i + 1
        from nums
        where i <=12
	         )
             ,
  periods AS (
		select dateadd(month, i::int, '2019-01-01 00:00:00.000'::timestamp) as period
		from nums
		     )
SELECT period FROM periods



SELECT * FROM "EST"."P_STG_EST".periods




  
  
 ------------------------------------
 CREATE OR REPLACE PROCEDURE "EST"."P_DDV_EST".JC_base_antiguedad()
  RETURNS FLOAT
  LANGUAGE JAVASCRIPT
  AS $$
// Inicializo algunas variables.
    var periodo_min = 0;
    var periodo_base = 0; 
    
// ============ Busco el �ltimo periodo que se encuentra cargado en la tabla.
    var q1 = 'SELECT MIN("periodo"), MAX("periodo") FROM "EST"."P_DDV_EST"."JC_antiguedad" ;' ;
    var statement1 = snowflake.createStatement({sqlText: q1}) ;
    var resultado = statement1.execute();
   
    if(resultado.next()){
        periodo_min = resultado.getColumnValue(1);
        periodo_base = resultado.getColumnValue(2);
    } 
    
//    Chequeo si regreso null, debiese ocurrir solamente cuando la tabla esta vac�a
    if(periodo_base !== null) {
        // NADA continue;
        }
    else{
        periodo_min = 198904;           //inicio de vigencia del primer contrato en CNT
        periodo_base = periodo_min;
        }
    
//  Arbitrariamente se decide borrar los �ltimos 3 meses y volver a calcularlo por si hubo alguna modificaci�n en los contratos.
//  Le resto 3 meses al periodo base y reviso que no sea menor al periodo m�nimo de la tabla ( importante para la primera carga solamente)
   
    var q2 = `SELECT "EST"."P_DDV_EST".PERIODOADD(:1, -3),
                     "EST"."P_DDV_EST".PERIODOADD(:1, -4),
                     "EST"."P_DDV_EST".PERIODOADD(:2, -1) ;` ;
    
    var stmt_q2 = snowflake.createStatement( {sqlText: q2, binds: [periodo_base, periodo_min]});
    var res2 = stmt_q2.execute();
    
    if(res2.next()){
        var temp = res2.getColumnValue(1);
        if( temp <= periodo_min){
            periodo_base = periodo_min;
            periodo_anterior = res2.getColumnValue(3);
        }
        else {
            periodo_base = temp;
            periodo_anterior = res2.getColumnValue(2);
        }
    }
    
// ============ borro los periodos
    var del = 'DELETE FROM "EST"."P_DDV_EST"."JC_antiguedad" WHERE "periodo" >= :1 ;' ;
    snowflake.execute({sqlText: del, binds: [periodo_base]}); 
    
// ============ calculo el n�mero de vueltas para el loop
    var q3 = 'SELECT DATEDIFF(month, "EST"."P_DDV_EST".PERIODO_FECHA(:1), CURRENT_DATE()) ;' ;
    var res3 = snowflake.createStatement({sqlText: q3, binds: [periodo_base]}).execute();
    if(res3.next()) { meses = res3.getColumnValue(1); }
    var periodo_cal = periodo_base;
    
    var i;
    for (i = 1; i<=meses+1; i++){
        var tabla_aux = `CREATE OR REPLACE TEMPORARY TABLE "EST"."P_DDV_EST"."AUX_ANT" AS 
                         SELECT A.* FROM "EST"."P_DDV_EST"."JC_antiguedad" A
                         WHERE A."periodo" = :1 ;` ;
                         
        var stmt_aux = snowflake.createStatement({sqlText: tabla_aux, binds: [periodo_anterior]});
        
        var res_aux   = stmt_aux.execute();
        
// ============ C�lculo la antig�edad y la inserto a la tabla
        var tabla_anti = `INSERT INTO "EST"."P_DDV_EST"."JC_antiguedad"
                          SELECT    :1                          AS "periodo",
                                    A."fld_cotrut"              AS "fld_cotrut",
                                    IFNULL(B."antiguedad"+1, 1) AS "antiguedad",
                                    IFNULL(B."ini_vig", :1)     AS "ini_vig"
                                    
                                FROM "AFI"."P_RDV_DST_LND_SYB"."CNT" A
                                    LEFT JOIN "EST"."P_DDV_EST"."AUX_ANT" B ON (A."fld_cotrut" = B."fld_cotrut")
                                    
                           WHERE    A."FLD_VIGREALDESDE" <= :1
                                AND A."FLD_VIGREALHASTA" >= :1; ` ;
                                
         var stmt_anti = snowflake.createStatement({sqlText: tabla_anti, binds: [periodo_cal]}) ;
         stmt_anti.execute();
         
         periodo_anterior = periodo_cal;
         
         var cambio_per = snowflake.execute({sqlText: 'SELECT "EST"."P_DDV_EST".PERIODOADD(:1, 1) ;', binds: [periodo_cal]});
         cambio_per.next();
         periodo_cal = cambio_per.getColumnValue(1) ;
         
        
    }
    return meses + 0;
    
  $$
  ;
  
 

 