----extraccion de deuda un a�o movil
--CREATE OR REPLACE TABLE est.P_DDV_EST.deuda_vigente_movil as(
SELECT DISTINCT 
DTI_RUT_COT , 
DTI_FUNFOLIO ,
DTI_FUNCORREL, 
sum(DTI_MTO_DIF_CTO) AS total_deuda 
FROM ISA.P_RDV_DST_LND_SYB.DTI_NEW
WHERE DTI_ESTADO IN (0,2) AND 
DTI_PER_COT BETWEEN 201909 AND 202008
GROUP BY 
DTI_RUT_COT , 
DTI_FUNFOLIO ,
DTI_FUNCORREL)

SELECT COUNT(1) FROM est.P_DDV_EST.ca_bad_vig



---- pivot bad por periodo de vigencia
CREATE OR REPLACE TABLE est.P_DDV_EST.ca_bad_vig AS ( 
WITH bad_temporal AS ( 
SELECT 
"bad_cotrut" ,
"bad_funfolio",
"bad_funcorrel",
"bad_producto" ,
row_number() over (partition by "bad_cotrut" order by "bad_cotrut") AS n_veces ---contar 
FROM afi.P_RDV_DST_LND_SYB.BAD 
WHERE "bad_vigdes" <= 202012 AND "bad_vighas" >= 202009 )
SELECT bad_cotrut,bad_funfolio,bad_funcorrel,bad_1,bad_2,bad_3,bad_4,bad_5,bad_6 
FROM bad_temporal 
pivot(max("bad_producto") FOR n_veces IN (1,2,3,4,5,6))
AS P (bad_cotrut,bad_funfolio,bad_funcorrel,bad_1,bad_2,bad_3,bad_4,bad_5,bad_6 ))

SELECT top 10 * FROM est.P_DDV_EST.ca_bad_vig

CREATE OR REPLACE TABLE est.P_DDV_EST.ca_moneda AS (
SELECT DISTINCT COD_DESACA ,"fld_monedatipo" FROM AFI.P_RDV_DST_LND_SYB.PLA_MPL
ORDER BY COD_DESACA )


CREATE OR REPLACE TABLE EST.P_DDV_EST.ca_resto AS (
SELECT DISTINCT 
a."fld_cotrut" ,
a."fld_tipotrab" 
FROM AFI.P_RDV_DST_LND_SYB.LFC a
INNER JOIN (SELECT * FROM est.P_DDV_EST.CA_base_JMV  
WHERE TIPO_TRABAJADOR IS null
) b ON a."fld_cotrut"= b."fld_cotrut"
)

SELECT "fld_cotrut" FROM est.P_DDV_EST.CA_base_JMV  
WHERE TIPO_TRABAJADOR IS null


SELECT COUNT(1) FROM est.P_DDV_EST.CA_base_JMV  

CREATE OR REPLACE TABLE est.P_DDV_EST.CA_base_JMV AS(
SELECT DISTINCT 
a."fld_funfolio",
a."fld_funcorrel",
a."fld_cotrut" ,
c."fld_benedad" ,
(CASE 
	WHEN a."fld_mail" =' ' THEN 'NO'
	ELSE 'SI'
END) AS correo,
(CASE 
	WHEN "fld_foncelu" =0 THEN 'NO'
	ELSE 'SI'
END) AS fono_celular,
CASE 
	WHEN "fld_monedatipocod" IN (1,6) THEN 'SI'
	ELSE 'NO'
END AS plan_uf,
--tipo plan  UF
a."fld_concostototal"::float AS monto_plan,
---exce
CASE
	WHEN e."cta_saldodisp" >=1500 THEN 'SI'
	ELSE 'NO'
END AS excedentes,
---deuda
(CASE
	WHEN g."TOTAL_DEUDA" >=2020 THEN 'SI' --- definido por el 0,07 valor uf
	ELSE 'NO'
END) AS deuda,
---tipo trabajador
h.TIPO_TRABAJADOR,
--(case
--  when h."fld_tipotrab"=1 then 'DEPENDIENTE'
--	when h."fld_tipotrab"=2 then 'INDEPENDIENTE'
--	when h."fld_tipotrab"=3 then 'PENSIONADO'
--	when h."fld_tipotrab"=4 then 'VOLUNTARIO'
--END) as tipo_trabajador,
----tipo plan
(CASE 
	WHEN "fld_planvig" IN ('E1') THEN 'COLECTIVO'
	ELSE 'INDIVIDUAL'
END) AS TIPO_PLAN,
(CASE 
	WHEN a."fld_carganum" =0 THEN 'NO'
	ELSE 'SI'
END) AS con_beneficiarios,
a."fld_carganum"  AS n_beneficiarios,
(CASE 
	WHEN b.BAD_1 IN (50,36,37,38,42,43,44,48,49,45,46,47,39,40,10,55,56,51,52,53,54,22,23,27) THEN 'SI'
	WHEN b.BAD_2 IN (50,36,37,38,42,43,44,48,49,45,46,47,39,40,10,55,56,51,52,53,54,22,23,27) THEN 'SI'
	WHEN b.BAD_3 IN (50,36,37,38,42,43,44,48,49,45,46,47,39,40,10,55,56,51,52,53,54,22,23,27) THEN 'SI'
	WHEN b.BAD_4 IN (50,36,37,38,42,43,44,48,49,45,46,47,39,40,10,55,56,51,52,53,54,22,23,27) THEN 'SI'
	WHEN b.BAD_5 IN (50,36,37,38,42,43,44,48,49,45,46,47,39,40,10,55,56,51,52,53,54,22,23,27) THEN 'SI'
	WHEN b.BAD_6 IN (50,36,37,38,42,43,44,48,49,45,46,47,39,40,10,55,56,51,52,53,54,22,23,27) THEN 'SI' 
	WHEN a."fld_tipseg" in (1,2,3) THEN 'SI'
	WHEN a."fld_tipopharma" = 5 THEN 'SI'
	ELSE 'NO'
END) AS tiene_beneficios,
--bad_pharma
(CASE 
	WHEN b.BAD_1 =50 THEN 'SI'
	WHEN b.BAD_2 =50 THEN 'SI'
	WHEN b.BAD_3 =50 THEN 'SI'
	WHEN b.BAD_4 =50 THEN 'SI'
	WHEN b.BAD_5 =50 THEN 'SI'
	WHEN b.BAD_6 =50 THEN 'SI'
	ELSE 'NO'
END) AS Beneficio_Catastrofico,
(CASE
	WHEN a."fld_tipseg" = 1 then 'SI'---'Cesantia 6 viejo'
	WHEN a."fld_tipseg" = 2 then 'SI'---'Cesantia 9 viejo'
	WHEN a."fld_tipseg" = 3 then 'SI'---'Cesantia 12 viejo'
	else 'NO'
END) AS bad_censantiaViejo,
(CASE 
	WHEN b.BAD_1 IN (36,37,38,42,43,44,48,49) THEN 'SI'
	WHEN b.BAD_2 IN (36,37,38,42,43,44,48,49) THEN 'SI'
	WHEN b.BAD_3 IN (36,37,38,42,43,44,48,49) THEN 'SI'
	WHEN b.BAD_4 IN (36,37,38,42,43,44,48,49) THEN 'SI'
	WHEN b.BAD_5 IN (36,37,38,42,43,44,48,49) THEN 'SI'
	WHEN b.BAD_6 IN (36,37,38,42,43,44,48,49) THEN 'SI'
	ELSE 'NO'
END) AS bad_cesantia,
(CASE 
	WHEN b.BAD_1 IN (45,46,47,39,40,10) THEN 'SI'
	WHEN b.BAD_2 IN (45,46,47,39,40,10) THEN 'SI'
	WHEN b.BAD_3 IN (45,46,47,39,40,10) THEN 'SI'
	WHEN b.BAD_4 IN (45,46,47,39,40,10) THEN 'SI'
	WHEN b.BAD_5 IN (45,46,47,39,40,10) THEN 'SI'
	WHEN b.BAD_6 IN (45,46,47,39,40,10) THEN 'SI'
	ELSE 'NO'
END) AS bad_Pago_Cotizacion,
(CASE 
	WHEN b.BAD_1 IN (55,56) THEN 'SI'
	WHEN b.BAD_2 IN (55,56) THEN 'SI'
	WHEN b.BAD_3 IN (55,56) THEN 'SI'
	WHEN b.BAD_4 IN (55,56) THEN 'SI'
	WHEN b.BAD_5 IN (55,56) THEN 'SI'
	WHEN b.BAD_6 IN (55,56) THEN 'SI'
	ELSE 'NO'
END) AS bad_Urgencia,
(CASE 
	WHEN b.BAD_1 IN (51,52,53,54) THEN 'SI'
	WHEN b.BAD_2 IN (51,52,53,54) THEN 'SI'
	WHEN b.BAD_3 IN (51,52,53,54) THEN 'SI'
	WHEN b.BAD_4 IN (51,52,53,54) THEN 'SI'
	WHEN b.BAD_5 IN (51,52,53,54) THEN 'SI'
	WHEN b.BAD_6 IN (51,52,53,54) THEN 'SI'
    ELSE 'NO'
END) AS bad_dental,
(CASE
	WHEN a."fld_tipopharma" = 5 then 'SI'
	ELSE 'NO'
END) AS bad_Pharmamax, 
(CASE
	WHEN b.BAD_1 IN (22,23,27) then 'SI'
	WHEN b.BAD_2 IN (22,23,27) then 'SI'
	WHEN b.BAD_3 IN (22,23,27) then 'SI'
	WHEN b.BAD_4 IN (22,23,27) then 'SI'
	WHEN b.BAD_5 IN (22,23,27) then 'SI'
	WHEN b.BAD_6 IN (22,23,27) then 'SI'
	ELSE 'NO'
END) AS bad_Dr_online, 
(CASE
	WHEN "fld_region" = 13 THEN 'SANTIAGO'
	ELSE 'REGIONES'
END) AS santiago_regiones 
FROM AFI.P_RDV_DST_LND_SYB.CNT a
LEFT JOIN est.P_DDV_EST.ca_bad_vig b ON (a."fld_funfolio"= b.BAD_FUNFOLIO AND a."fld_funcorrel"= b.BAD_FUNCORREL)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN c ON (a."fld_funfolio"= c."fld_funfolio" AND a."fld_funcorrel"= c."fld_funcorrel" AND c."fld_bencorrel"=0)
LEFT JOIN RCD.P_RDV_DST_LND_SYB.EXC_CTA e ON (a."fld_cotrut"=e."cta_rut" AND e."cta_estado"=0)
LEFT JOIN EST.P_DDV_EST.deuda_vigente_movil g ON(a."fld_cotrut"=g."DTI_RUT_COT")
LEFT JOIN EST.P_DDV_EST.CA_CONTRATO_TT2 h ON(a."fld_cotrut"=H.RUT_TITULAR)--ya nose repiten los tipos de trabajador
WHERE FLD_VIGREALDESDE <= 202011 AND FLD_VIGREALHASTA >= 202011
ORDER BY "fld_cotrut" 
)


CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_CONTRATO_TT2 AS(
SELECT 
A.RUT_TITULAR , 
A.TIPO_TRABAJADOR, 
b.PERIODO
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO a
INNER JOIN (SELECT RUT_TITULAR , MAX(PERIODO_VIGENCIA) AS PERIODO
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
GROUP BY RUT_TITULAR ) b ON a.RUT_TITULAR =b.rut_titular AND a.PERIODO_VIGENCIA = b.PERIODO
)




SELECT * FROM EST.P_DDV_EST.CA_CONTRATO_TT2


SELECT * 
FROM EST.P_DDV_EST.CA_CONTRATO_TT
WHERE rut_titular ='001532128-8'


---CAST(substring(H.NUM_CONTRATO,0,9) AS int
--SELECT top 100 CAST(substring(NUM_CONTRATO,0,9) AS int) AS funfolio, * FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
--WHERE RUT_TITULAR ='001535849-1' AND NUM_CORRELATIVO =7 --- sacar por el maximo vigencia



SELECT top 10 * from est.P_DDV_EST.CA_base_JMV 

SELECT COUNT(1) from est.P_DDV_EST.CA_base_JMV 

SELECT DISTINCT COD_MONEDA FROM AFI.P_RDV_DST_LND_SYB.ISAPREDESACA


SELECT top 10 * FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE FLD_VIGREALDESDE <= 202011 AND FLD_VIGREALHASTA >= 202011



SELECT top 10 * FROM isa.P_RDV_DST_LND_SYB.ISAPREMODCOTZ

SELECT  top 50 * FROM AFI.P_RDV_DST_LND_SYB.PLA_MPL
ORDER BY COD_DESACA 




SELECT top 10 * FROM RCD.P_RDV_DST_LND_SYB.EXC_CTA 
WHERE "cta_rut" ='013272402-4'



SELECT top 10 * FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE "fld_cotrut" ='001535849-1'


CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_TPO_TRAB_UNIC as(
SELECT DISTINCT 
"fld_cotrut" ,
"fld_funfolio" ,
"fld_funcorrel" ,
"tipo_plan" ,
"fld_tipotrab" , 
MAX("fld_periodoprod") AS produccion
FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_cotrut" ='001708530-1'
GROUP BY 
"fld_cotrut" ,
"fld_funcorrel", 
"fld_funfolio",
"tipo_plan" ,
"fld_tipotrab" 
)

SELECT * FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_cotrut" ='001708530-1' AND "fld_funcorrel" =26



SELECT * FROM EST.P_DDV_EST.CA_TPO_TRAB_UNIC


SELECT COUNT( DISTINCT ("fld_cotrut")) FROM EST.P_DDV_EST.CA_TPO_TRAB_UNIC


SELECT DISTINCT 
"fld_cotrut", 
"tipo_plan",
"fld_tipotrab", 
"fld_periodoprod"
FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE "fld_periodoprod" = MAX("fld_periodoprod")
ORDER BY 
"fld_cotrut" 


SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.BAD 






SELECT top 10 * FROM AFI.P_RDV_DST_LND_SYB.BEN 
WHERE "fld_cotrut" ='019267522-7' 



SELECT DISTINCT "bad_idn", "bad_nombre" 
FROM ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL 
WHERE CONTAINS ("bad_nombre",'Dr')
ORDER BY "bad_idn" 



SELECT top 3 * FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO

SELECT TOP 3 * FROM GTO.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM

SELECT DISTINCT origen FROM GTO.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM
           
           