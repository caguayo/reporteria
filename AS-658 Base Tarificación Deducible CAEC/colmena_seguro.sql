SELECT top 10 * FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO


SELECT DISTINCT ID_TITULAR ,RUT_TITULAR  FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE ID_TITULAR  IN (327071, 3103056, 4078821, 8673006) LIMIT 10


SELECT DISTINCT 
	NUM_CONTRATO,
	NUM_CORRELATIVO ,
	MAX(PERIODO_VIGENCIA) AS PERIODO
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE rut_titular = '006' AND PERIODO_VIGENCIA > 201901
GROUP BY 
	NUM_CONTRATO,
	NUM_CORRELATIVO 



WITH MAXIMO_CONTRATO AS (
SELECT DISTINCT 
	NUM_CONTRATO,
	NUM_CORRELATIVO ,
	MAX(PERIODO_VIGENCIA) AS PERIODO
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
GROUP BY 
	NUM_CONTRATO,
	NUM_CORRELATIVO 
), PRECIO_UNICO_CONTRATO AS (
SELECT DISTINCT 
			A.num_contrato, 
			A.num_correlativo , 
			A.costo_final_uf
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO A
INNER JOIN MAXIMO_CONTRATO B ON (A.NUM_CONTRATO = B.NUM_CONTRATO AND A.NUM_CORRELATIVO = B.NUM_CORRELATIVO AND A.PERIODO_VIGENCIA = B.PERIODO )
)
SELECT * FROM PRECIO_UNICO_CONTRATO
WHERE NUM_CONTRATO = '004252503-0' AND NUM_CORRELATIVO = 1449




WITH folio_ree AS ( 
--------- union 
---eah
SELECT DISTINCT top 50
	"eah_idn" AS folio , 
	"eah_conefe" AS periodo_ree 
FROM HOS.P_RDV_DST_LND_SYB.SH_EAH
WHERE "eah_tipemi" IN ('R') --"eah_idn"= 103832955;
union
---eam 
SELECT DISTINCT top 50
	"eam_idn" AS folio ,
	"eam_conefe" AS periodo_ree  
FROM hos.P_RDV_DST_LND_SYB.SH_EAM 
WHERE "eam_tipemi" IN ('R') --"eam_idn" = 62046750 LIMIT 10  ;
union
---ree
SELECT DISTINCT top 50
	"fld_reefolio" AS folio, 
	"fld_reeemifec" AS periodo_ree 
	FROM gto.P_RDV_DST_LND_SYB.SRE 
---fld_reefolio , fld_reememifec
)
SELECT * FROM folio_ree


WITH vigencia AS (
SELECT DISTINCT 
	"fld_cotrut" , 
	"fld_ingreso" ,
	"fld_cntcateg",
	MIN(FLD_VIGREALDESDE) AS minima_vigencia 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
	"fld_cotrut",
	"fld_ingreso",
	"fld_cntcateg" 
	ORDER BY "fld_cotrut"  
)
SELECT * FROM vigencia
WHERE "fld_cotrut" = '025719488-4'


DROP TABLE est.P_DDV_EST.muestra_cs



SELECT count(1) FROM est.P_DDV_EST.ca_cia_seguro_2016 LIMIT 10

---cnt vigencias
---eah
---eam
---sre
---contrato maximo vigencia
---PRM_AGRUPA_PLANES 


SELECT * FROM est.P_DDV_EST.ca_cia_seguro_2018
WHERE ID_RUT_TITULAR IN (3103056) AND PERIODO_RED = 201803 
LIMIT 100



CREATE OR REPLACE TABLE est.P_DDV_EST.ca_cia_seguro_2020 AS ( 
------------- extraccion maxima fecha vigencia
WITH vigencia AS (
SELECT DISTINCT 
	"fld_cotrut" , 
	"fld_ingreso" ,
	"fld_cntcateg",
	MIN(FLD_VIGREALDESDE) AS minima_vigencia 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
	"fld_cotrut",
	"fld_ingreso",
	"fld_cntcateg" 
	ORDER BY "fld_cotrut"  
), folio_ree AS 
( 
--------- union 
---eah
SELECT DISTINCT 
	"eah_idn" AS folio , 
	"eah_conefe" AS periodo_ree 
FROM HOS.P_RDV_DST_LND_SYB.SH_EAH
WHERE "eah_tipemi" IN ('R') --"eah_idn"= 103832955;
union
---eam 
SELECT DISTINCT 
	"eam_idn" AS folio ,
	"eam_conefe" AS periodo_ree  
FROM hos.P_RDV_DST_LND_SYB.SH_EAM 
WHERE "eam_tipemi" IN ('R') --"eam_idn" = 62046750 LIMIT 10  ;
union
---ree
SELECT DISTINCT 
	"fld_reefolio" AS folio, 
	"fld_reeemifec" AS periodo_ree 
	FROM gto.P_RDV_DST_LND_SYB.SRE 
---fld_reefolio , fld_reememifec
), 
MAXIMO_CONTRATO AS (
SELECT DISTINCT 
	NUM_CONTRATO,
	NUM_CORRELATIVO ,
	MAX(PERIODO_VIGENCIA) AS PERIODO
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE costo_final_uf IS NOT null
GROUP BY 
	NUM_CONTRATO,
	NUM_CORRELATIVO 
), 
PRECIO_UNICO_CONTRATO AS (
SELECT DISTINCT 
			A.num_contrato, 
			A.num_correlativo , 
			A.costo_final_uf
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO A
INNER JOIN MAXIMO_CONTRATO B ON (A.NUM_CONTRATO = B.NUM_CONTRATO AND A.NUM_CORRELATIVO = B.NUM_CORRELATIVO AND A.PERIODO_VIGENCIA = B.PERIODO )
), match_datos AS (
SELECT
	--SUBSTRING(dm.CONTRATO,0,9)::int AS funfolio,
	--dm.CONTRATO,
	--dm.CORRELATIVO ,
	--con.NUM_CONTRATO ,
	--con.NUM_CORRELATIVO ,
	--dm.FOLIO ,
	--dm.PERIODO , 
	--dm.RUT_TITULAR , 
	--COALESCE (benv2."fld_benrut",ben."fld_benrut",dm.RUT_BENEFICIARIO) AS rut_beneficiario_ben,
	--dm.RUT_BENEFICIARIO AS rut_beneficiario_dm , 
	--dm.COD_BENEFICIARIO ,
	SUBSTRING(dm.RUT_TITULAR,1,9)::int*5-3265112+52658  AS id_rut_titular,
	COALESCE (SUBSTRING(benv2."fld_benrut" ,1,9)::int*5-3265112+52658,SUBSTRING(ben."fld_benrut" ,1,9)::int*5-3265112+52658) AS id_rut_beneficiario,
	dm.COD_CIE AS codigo_cie , 
	dm.GLOSA_CIE AS glosa_cie , 
	caec."codclasif"::int AS cod_descripcion_caec,
	caec."glsclasif" AS gls_descripcion_caec,
	CASE 
		WHEN COALESCE(benv2."fld_bensexo" ,ben."fld_bensexo") = 1 THEN 'M'
		WHEN COALESCE(benv2."fld_bensexo" ,ben."fld_bensexo") = 2 THEN 'F'
		ELSE 'sin registro'
	END AS sexo_beneficiario,
	COALESCE(benv2."fld_bennacfec",ben."fld_bennacfec") AS fecha_nacimiento,
	COALESCE(cntv2."fld_carganum",cnt."fld_carganum") AS cantidades_carga,
	dm.HOLDING_PRESTADOR ,
	---- CLASIFICACION GASTOS
	dm.ORIGEN AS tipo_cobertura ,
	COALESCE(eah."eah_urgencia", eam."eam_urgencia") AS derivado_urgencia,
	dm.COD_CATEGORIA::int AS codigo_plan_isapre , 
	prm.GLS_DESACA AS glosa_plan_isapre,
	COALESCE (con.COSTO_FINAL_UF,conv2.COSTO_FINAL_UF)::float AS  pactado_final,
	--conv2.COSTO_FINAL_UF AS pactado_final,
	dm.REGION_INST::int AS cod_region ,
	dm.GLS_REGION_INST AS glosa_region ,
	COALESCE (cntv2."fld_ingreso",cnt."fld_ingreso") AS fecha_ingreso_isapre,
	---fecha ingreso al plan�
	--dateadd('month',-2,est.P_DDV_EST.periodo_fecha(vig.minima_vigencia))::datetime AS periodo_ingreso_plan,
	EST.P_DDV_EST.PERIODOADD(vig.minima_vigencia::int, -2) AS periodo_ingreso_plan,
	vig.minima_vigencia::int AS fecha_vigencia_plan,
	dm.FECHA_INTERVENCION AS fecha_ocurrencia_gasto,
	fr.periodo_ree AS fecha_reemb_isapre,
	dm.COBRADO::int AS cobrado ,
	dm.BONIFICADO::int AS bonificado ,
	caec."aporte_al_deducible" AS deducible_plan_salud,
	dm.monto_caec::int AS monto_caec,
	(dm.COBRADO - dm.BONIFICADO)::int  AS copago,
	dm.RUT_INSTITUCION AS rut_prestador,
	dm.NOMBRE_INSTITUCION AS nombre_prestador,
	dm.GRUPO ,
	dm.SUBGRUPO,
	dm.TIPO_BONO::int AS tipo_bono ,
	dm.GLS_TIPO_BONO ,
	dm.SOLICITUD_DE_TRATAMIENTO::int AS solicitud_tratamiento ,
	dm.RUBRO, 
	dm.NOMBRE_RUBRO ,
	dm.ACTOR  ,
	dm.NOMBRE_ACTOR 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO dm
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt 	ON (dm.RUT_TITULAR = cnt."fld_cotrut" AND dm.PERIODO BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA)
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cntv2 	ON (SUBSTRING(dm.CONTRATO,0,9)::int=cntv2."fld_funfolio" AND dm.CORRELATIVO=cntv2."fld_funcorrel")
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN ben 	ON (dm.RUT_TITULAR = ben."fld_cotrut" AND dm.COD_BENEFICIARIO = ben."fld_bencorrel" AND dm.PERIODO BETWEEN ben.FLD_BENVIGREALDESDE AND ben.FLD_BENVIGREALHASTA)
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN benv2 	ON (SUBSTRING(dm.CONTRATO,0,9)::int=benv2."fld_funfolio" AND dm.CORRELATIVO=benv2."fld_funcorrel" AND dm.COD_BENEFICIARIO = benv2."fld_bencorrel"  )
	LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm ON (dm.COD_CATEGORIA = prm.COD_DESACA)
	LEFT JOIN vigencia vig 						ON (vig."fld_cotrut"=dm.RUT_TITULAR AND vig."fld_ingreso"=COALESCE (cntv2."fld_ingreso",cnt."fld_ingreso") AND vig."fld_cntcateg"= dm.COD_CATEGORIA)
	LEFT JOIN GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO caec ON (dm.FOLIO=caec."folio_doc")
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EAH eah 	ON (dm.folio = eah."eah_idn")
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EAM eam 	ON (dm.folio = eam."eam_idn")
	LEFT JOIN folio_ree fr 						ON (dm.folio = fr.folio)
	LEFT JOIN PRECIO_UNICO_CONTRATO con 		ON (SUBSTRING(dm.CONTRATO,0,9)::int=SUBSTRING(con.NUM_CONTRATO,0,9)::int AND dm.correlativo=con.NUM_CORRELATIVO)
	LEFT JOIN AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO conv2 		ON (dm.PERIODO=conv2.PERIODO_VIGENCIA AND DM.RUT_TITULAR = CONV2.RUT_TITULAR)
WHERE dm.periodo BETWEEN 202001 AND 202012  AND dm.ORIGEN NOT IN ('LICENCIA') --AND id_rut_afiliado=67383476 AND dm.FOLIO =95211108 AND DM.COBRADO = 2180.0000--AND dm.ORIGEN IN('HOSPITALARIO') --AND --dm.RUT_TITULAR ='009251705-5'
--ges."codclasif" IS NOT NULL --ORIGEN IN('HOSPITALARIO') AND dm.RUT_TITULAR ='009251705-5' AND dm.CORRELATIVO =402
ORDER BY DM.PERIODO, DM.RUT_TITULAR, COPAGO
)
SELECT *
FROM MATCH_DATOS
)


WITH asd AS ( 
SELECT 	DISTINCT 
		SUBSTRING(IFNULL(RUT_TITULAR,'0000000000'),1,9)::int*5-3265112+52658  AS  id_rut_titular,
		RUT_TITULAR ,
	    SUBSTRING(ifnull(RUT_BENEFICIARIO,'00000000') ,1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	    RUT_BENEFICIARIO 
FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO >= 201501
)
SELECT 
	*
FROM asd
WHERE id_rut_beneficiario =66123141
ORDER BY id_rut_beneficiario
LIMIT 10




SELECT DISTINCT 
PERIODO ,
--ORIGEN ,
sum(COBRADO)
FROM  DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO BETWEEN 201501 AND 201512 AND ORIGEN NOT IN ('LICENCIA')-- AND RUT_BENEFICIARIO ='013867119-4'
GROUP BY 
	PERIODO 
	--, ORIGEN 
ORDER BY PERIODO


SELECT 
	PERIODO_PRESTACION ,
	--TIPO_COBERTURA ,
	sum(COBRADO)
FROM EST.P_DDV_EST.CA_CIA_SEGURO_2015_REV 
GROUP BY 
	PERIODO_PRESTACION 
	--,TIPO_COBERTURA 
ORDER BY PERIODO_PRESTACION 
LIMIT 12



SELECT 
	*
FROM EST.P_DDV_EST.CA_CIA_SEGURO_2018
WHERE ID_RUT_BENEFICIARIO =3103056 AND 
--LIMIT 10


SELECT * FROM EST.P_DDV_EST.CA_CIA_SEGURO_2020 LIMIT 1000


SELECT "fld_compensacion" ,* FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE "fld_compensacion" > 0
LIMIT 1000









-----------------REVISION DE CASOS DE CIA SEGUROS
-------------------------------------------------
-------- inicio y final del periodo
SET periodo_desde = 201501;
SET periodo_hasta = 201512;
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_CIA_SEGURO_20152 AS ( 
WITH vigencia AS (
SELECT DISTINCT 
	"fld_cotrut" , 
	"fld_ingreso" ,
	"fld_cntcateg",
	MIN(FLD_VIGREALDESDE) AS minima_vigencia 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
	"fld_cotrut",
	"fld_ingreso",
	"fld_cntcateg" 
	ORDER BY "fld_cotrut"  
), folio_ree AS 
( 
--------- union 
---eah
SELECT DISTINCT 
	"eah_idn" AS folio , 
	"eah_conefe" AS periodo_ree 
FROM HOS.P_RDV_DST_LND_SYB.SH_EAH
WHERE "eah_tipemi" IN ('R') --"eah_idn"= 103832955;
union
---eam 
SELECT DISTINCT 
	"eam_idn" AS folio ,
	"eam_conefe" AS periodo_ree  
FROM hos.P_RDV_DST_LND_SYB.SH_EAM 
WHERE "eam_tipemi" IN ('R') --"eam_idn" = 62046750 LIMIT 10  ;
union
---ree
SELECT DISTINCT 
	"fld_reefolio" AS folio, 
	"fld_reeemifec" AS periodo_ree 
	FROM gto.P_RDV_DST_LND_SYB.SRE 
---fld_reefolio , fld_reememifec
), 
MAXIMO_CONTRATO AS (
SELECT DISTINCT 
	NUM_CONTRATO,
	NUM_CORRELATIVO ,
	MAX(PERIODO_VIGENCIA) AS PERIODO
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE costo_final_uf IS NOT null
GROUP BY 
	NUM_CONTRATO,
	NUM_CORRELATIVO 
), 
PRECIO_UNICO_CONTRATO AS (
SELECT DISTINCT 
			A.num_contrato, 
			A.num_correlativo , 
			A.costo_final_uf
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO A
INNER JOIN MAXIMO_CONTRATO B ON (A.NUM_CONTRATO = B.NUM_CONTRATO AND A.NUM_CORRELATIVO = B.NUM_CORRELATIVO AND A.PERIODO_VIGENCIA = B.PERIODO )
), match_datos AS (
SELECT
	SUBSTRING(dm.CONTRATO,0,9)::int AS funfolio,
	dm.FECHA_DIGITACION ,
	dm.CONTRATO,
	dm.CORRELATIVO ,
	con.NUM_CONTRATO ,
	con.NUM_CORRELATIVO ,
    dm.FOLIO ,
	ROW_NUMBER () OVER (PARTITION BY dm.folio ORDER BY dm.folio ) AS n_veces_dm,
	--caec.n_veces AS n_veces_caec,
	dm.folio AS folio_n,
	dm.PERIODO , 
	dm.RUT_TITULAR , 
	COALESCE (benv2."fld_benrut",ben."fld_benrut",dm.RUT_BENEFICIARIO) AS rut_beneficiario_ben,
	dm.RUT_BENEFICIARIO AS rut_beneficiario_dm , 
	dm.COD_BENEFICIARIO ,
	SUBSTRING(dm.RUT_TITULAR,1,9)::int*5-3265112+52658  AS id_rut_titular,
	COALESCE (SUBSTRING(benv2."fld_benrut" ,1,9)::int*5-3265112+52658,SUBSTRING(ben."fld_benrut" ,1,9)::int*5-3265112+52658) AS id_rut_beneficiario,
	dm.COD_CIE AS codigo_cie , 
	dm.GLOSA_CIE AS glosa_cie , 
	--caec."codclasif"::int AS cod_descripcion_caec,
	--caec."glsclasif" AS gls_descripcion_caec,
	CASE 
		WHEN COALESCE(benv2."fld_bensexo" ,ben."fld_bensexo") = 1 THEN 'M'
		WHEN COALESCE(benv2."fld_bensexo" ,ben."fld_bensexo") = 2 THEN 'F'
		ELSE 'sin registro'
	END AS sexo_beneficiario,
	COALESCE(benv2."fld_bennacfec",ben."fld_bennacfec") AS fecha_nacimiento,
	COALESCE(cntv2."fld_carganum",cnt."fld_carganum") AS cantidades_carga,
	dm.HOLDING_PRESTADOR ,
	---- CLASIFICACION GASTOS
	dm.ORIGEN AS tipo_cobertura ,
	dm.PRESTACION ,
	dm.SUB_TIPO_PRESTACION ,
	COALESCE(eah."eah_urgencia", eam."eam_urgencia") AS derivado_urgencia,
	dm.COD_CATEGORIA::int AS codigo_plan_isapre , 
	prm.GLS_DESACA AS glosa_plan_isapre,
	COALESCE (con.COSTO_FINAL_UF,conv2.COSTO_FINAL_UF)::float AS  pactado_final,
	COALESCE (cnt."fld_compensacion",cntv2."fld_compensacion") AS compensacion,
	--conv2.COSTO_FINAL_UF AS pactado_final,
	dm.REGION_INST::int AS cod_region ,
	dm.GLS_REGION_INST AS glosa_region ,
	COALESCE (cntv2."fld_ingreso",cnt."fld_ingreso") AS fecha_ingreso_isapre,
	---fecha ingreso al plan�
	--dateadd('month',-2,est.P_DDV_EST.periodo_fecha(vig.minima_vigencia))::datetime AS periodo_ingreso_plan,
	EST.P_DDV_EST.PERIODOADD(vig.minima_vigencia::int, -2) AS periodo_ingreso_plan,
	vig.minima_vigencia::int AS fecha_vigencia_plan,
	dm.FECHA_INTERVENCION AS fecha_ocurrencia_gasto,
	fr.periodo_ree AS fecha_reemb_isapre,
	dm.COBRADO::int AS cobrado ,
	dm.BONIFICADO::int AS bonificado ,
	--caec."aporte_al_deducible" AS deducible_plan_salud,
	dm.monto_caec::int AS monto_caec,
	(dm.COBRADO - dm.BONIFICADO)::int  AS copago,
	dm.RUT_INSTITUCION AS rut_prestador,
	dm.NOMBRE_INSTITUCION AS nombre_prestador,
	dm.GRUPO ,
	dm.SUBGRUPO,
	dm.TIPO_BONO::int AS tipo_bono ,
	dm.GLS_TIPO_BONO ,
	dm.SOLICITUD_DE_TRATAMIENTO::int AS solicitud_tratamiento ,
	dm.RUBRO, 
	dm.NOMBRE_RUBRO ,
	dm.ACTOR  ,
	dm.NOMBRE_ACTOR ,
	dm.CODATE_DETALLE ,
	gls_1."fld_prestades" AS gls_codate_detalle,
	dm.CODATE_TRAZADORA ,
	gls_2."fld_prestades" AS gls_codate_trazadora,
	dm.CODATE_TRAZADORA_2 ,
	gls_3."fld_prestades" AS gls_codate_trazadora_2,
	dm.CODATE_TRAZADORA_3 ,
	gls_4."fld_prestades" AS gls_codate_trazadora_3
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO dm
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt 	ON (dm.RUT_TITULAR = cnt."fld_cotrut" AND dm.PERIODO BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA)
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cntv2 	ON (SUBSTRING(dm.CONTRATO,0,9)::int=cntv2."fld_funfolio" AND dm.CORRELATIVO=cntv2."fld_funcorrel")
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN ben 	ON (dm.RUT_TITULAR = ben."fld_cotrut" AND dm.COD_BENEFICIARIO = ben."fld_bencorrel" AND dm.PERIODO BETWEEN ben.FLD_BENVIGREALDESDE AND ben.FLD_BENVIGREALHASTA)
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN benv2 	ON (SUBSTRING(dm.CONTRATO,0,9)::int=benv2."fld_funfolio" AND dm.CORRELATIVO=benv2."fld_funcorrel" AND dm.COD_BENEFICIARIO = benv2."fld_bencorrel"  )
	LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm ON (dm.COD_CATEGORIA = prm.COD_DESACA)
	LEFT JOIN vigencia vig 						ON (vig."fld_cotrut"=dm.RUT_TITULAR AND vig."fld_ingreso"=COALESCE (cntv2."fld_ingreso",cnt."fld_ingreso") AND vig."fld_cntcateg"= dm.COD_CATEGORIA)	
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EAH eah 	ON (dm.folio = eah."eah_idn")
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EAM eam 	ON (dm.folio = eam."eam_idn")
	LEFT JOIN folio_ree fr 						ON (dm.folio = fr.folio)
	LEFT JOIN PRECIO_UNICO_CONTRATO con 		ON (SUBSTRING(dm.CONTRATO,0,9)::int=SUBSTRING(con.NUM_CONTRATO,0,9)::int AND dm.correlativo=con.NUM_CORRELATIVO)
	LEFT JOIN AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO conv2 		ON (dm.PERIODO=conv2.PERIODO_VIGENCIA AND DM.RUT_TITULAR = CONV2.RUT_TITULAR)
	LEFT JOIN (SELECT DISTINCT 
					"fld_prestacod", 
					"fld_prestades" 
			 FROM isa.P_RDV_DST_LND_SYB.CDTCOMP) gls_1 ON (dm.CODATE_DETALLE = gls_1."fld_prestacod")
	LEFT JOIN (SELECT DISTINCT 
					"fld_prestacod", 
					"fld_prestades" 
			 FROM isa.P_RDV_DST_LND_SYB.CDTCOMP) gls_2 ON (dm.codate_trazadora = gls_2."fld_prestacod")
	LEFT JOIN (SELECT DISTINCT 
					"fld_prestacod", 
					"fld_prestades" 
			 FROM isa.P_RDV_DST_LND_SYB.CDTCOMP) gls_3 ON (dm.codate_trazadora_2 = gls_3."fld_prestacod")
	LEFT JOIN (SELECT DISTINCT 
					"fld_prestacod", 
					"fld_prestades" 
			 FROM isa.P_RDV_DST_LND_SYB.CDTCOMP) gls_4 ON (dm.codate_trazadora_3 = gls_4."fld_prestacod")
WHERE  dm.ORIGEN NOT IN ('LICENCIA') AND dm.periodo BETWEEN $periodo_desde  AND $periodo_hasta  --, 202001, 202002, 202003) --AND id_rut_afiliado=67383476 AND dm.FOLIO =95211108 AND DM.COBRADO = 2180.0000--AND dm.ORIGEN IN('HOSPITALARIO') --AND --dm.RUT_TITULAR ='009251705-5'
--ges."codclasif" IS NOT NULL --ORIGEN IN('HOSPITALARIO') AND dm.RUT_TITULAR ='009251705-5' AND dm.CORRELATIVO =402
ORDER BY DM.PERIODO, folio_n
)
SELECT
	a.periodo AS periodo_prestacion,
	a.fecha_digitacion,
	a.folio_n AS folio_bono,
	--b."aporte_al_deducible",
	--a.rut_titular,
	a.id_rut_titular,
	a.id_rut_beneficiario,
	--a.rut_beneficiario_dm,
	a.codigo_cie , 
	a.glosa_cie ,
	b."periodo_red" AS PERIODO_RED,
	b."folio_caec" AS FOLIO_CAEC,
	b."codclasif"::int AS cod_descripcion_caec,
	b."glsclasif" AS gls_descripcion_caec,
	a.sexo_beneficiario,
	a.fecha_nacimiento,
	a.cantidades_carga,
	a.HOLDING_PRESTADOR ,
	a.tipo_cobertura ,
	a.derivado_urgencia,
	a.codigo_plan_isapre , 
	a.glosa_plan_isapre,
	a.pactado_final,
	--a.compensacion,
	a.pactado_final + a.compensacion AS pactado_plan,
	a.cod_region ,
	a.glosa_region ,
	a.fecha_ingreso_isapre,
	a.periodo_ingreso_plan,
	a.fecha_vigencia_plan,
	a.fecha_ocurrencia_gasto,
	a.fecha_reemb_isapre,
	a.cobrado ,
	a.bonificado ,
	b."aporte_al_deducible" AS deducible_caec,
	c.deducible_ges,
	a.monto_caec,
	a.copago,
	a.rut_prestador,
	a.nombre_prestador,
	a.prestacion,
	a.sub_tipo_prestacion,
	a.GRUPO ,
	a.SUBGRUPO,
	a.tipo_bono ,
	a.GLS_TIPO_BONO ,
	a.solicitud_tratamiento ,
	a.RUBRO, 
	a.NOMBRE_RUBRO ,
	a.ACTOR  ,
	a.NOMBRE_ACTOR,
	CODATE_DETALLE,
	gls_codate_detalle,
	CODATE_TRAZADORA ,
	gls_codate_trazadora,
	CODATE_TRAZADORA_2 ,
	gls_codate_trazadora_2,
	CODATE_TRAZADORA_3 ,
	gls_codate_trazadora_3
FROM MATCH_DATOS a
	LEFT JOIN (SELECT DISTINCT 
						ROW_NUMBER () OVER (PARTITION BY "folio_doc" ORDER BY "folio_doc" ) AS n_veces,
						"folio_doc" ,
						"folio_caec",
						"periodo_red",
						"codclasif",
						"glsclasif",
						"aporte_al_deducible" 
				   FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO 
				   ORDER BY "folio_doc"
			  ) b ON (a.FOLIO=b."folio_doc" AND n_veces_dm = b.n_veces)
	LEFT JOIN (SELECT DISTINCT 
					--"folio_caso", 
					1 AS n_veces,
					try_to_number("folio_doc") AS folio_ges, 
					sum("monto_copago_ges")::int AS deducible_ges
				FROM ges.P_RDV_DST_LND_SYB.GES_ESTUDIO 
				GROUP BY 
					--"folio_caso",
					folio_ges
				) c ON (a.folio = c.folio_ges AND n_veces_dm = c.n_veces)
--WHERE deducible_caec IS NOT NULL AND c.deducible_ges IS NOT null --folio_n = 96825759--id_rut_titular = 9763036
ORDER BY n_veces_dm , folio 
LIMIT 5000
)





SELECT PERIODO ,TIPO_BONO, PRESTACION ,SUB_TIPO_PRESTACION ,GLS_TIPO_BONO ,GRUPO , COD_GRUPO , SUBGRUPO , COD_SUBGRUPO FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO LIMIT 100


WITH asd AS ( 
SELECT DISTINCT 
	ROW_NUMBER () OVER (PARTITION BY "folio_doc" ORDER BY "folio_doc" ) AS n_veces,
	"periodo_red" ,
	"folio_doc" ,
	"codclasif",
	"glsclasif",
	"aporte_al_deducible" 
FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO 
--WHERE "folio_doc" >0 
 --HAVING n_veces >0
ORDER BY "folio_doc" desc
LIMIT 1000
)
SELECT
	"periodo_red",
	"folio_doc",
	count("folio_doc") AS cantidad
FROM  asd
--WHERE n_veces >1
GROUP BY "periodo_red","folio_doc" 


 SELECT "periodo_red", "folio_doc", "folio_caec" , * FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO  LIMIT 10


WITH pagos AS ( 
SELECT 
cnt."fld_funfolio" ,
cnt."fld_funcorrel" ,
cnt."fld_cotrut" ,
pag."pag_tipdoc" ,
pag."pag_pagval" 
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
INNER JOIN RCD.P_DDV_RCD.P_DDV_RCD_V_PAG pag 
	ON (cnt."fld_funfolio" = pag."pag_funfol" AND cnt."fld_funcorrel" = pag."pag_funcor" AND pag."pag_percot"='2021/04')
WHERE 202105 BETWEEN FLD_VIGREALDESDE  AND FLD_VIGREALHASTA 
), ordenar_pagos AS ( 
SELECT 
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_cotrut" ,
	"pag_tipdoc",
	CASE 
		WHEN "pag_tipdoc" NOT IN (8) THEN "pag_pagval"
		ELSE 0
	END AS pagos,
	CASE 
		WHEN "pag_tipdoc" IN (8) THEN "pag_pagval"
		ELSE 0
	END AS devoluciones,
	pagos-devoluciones AS pago_final
FROM pagos
ORDER BY "fld_cotrut" 
/*GROUP BY 
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_cotrut" ,
	"pag_tipdoc"*/
--WHERE "pag_pagval" >= 999999 ---"fld_cotrut" = '002305373-K' AND   -- AND "pag_tipdoc" NOT IN (8) --"pag_tipdoc"  IS NULL 
)
SELECT count(DISTINCT("fld_cotrut")) AS cant_afiliados,  sum(pago_final)::int AS monto FROM ordenar_pagos
--WHERE "pag_tipdoc" NOT in (8)
ORDER BY "pag_tipdoc"



SELECT	
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_cotrut" ,
	sum(pago_final) AS pagados
	FROM ordenar_pagos
GROUP BY 
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_cotrut" 
ORDER BY pagados asc



SELECT count(DISTINCT("fld_cotrut")) AS cant_afiliados,  sum("pag_pagval") AS monto FROM pagos
WHERE "pag_tipdoc" NOT in (8)
ORDER BY "pag_tipdoc"







SELECT count(1) AS registros, sum(PAGADO) FROM gto.P_DDV_GTO.P_DDV_GTO_LTV
WHERE PERIODO = 202105 AND VIGENTE ='V'LIMIT 10 


SELECT count(DISTINCT("fld_cotrut")) AS cant_afiliados, "pag_tipdoc" , sum("pag_pagval") AS monto FROM pagos
GROUP BY "pag_tipdoc"
ORDER BY "pag_tipdoc"

SELECT count(DISTINCT "fld_cotrut") FROM pagos











SELECT count(DISTINCT("fld_cotrut")) AS cant_afiliados, "pag_tipdoc" , sum("pag_pagval") AS monto FROM pagos
GROUP BY "pag_tipdoc"
ORDER BY "pag_tipdoc"



SELECT * FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG
WHERE "pag_funfol" =5241807 AND "pag_funcor" = 5622 AND "pag_percot" = '2021/04'



SELECT * FROM EST.P_DDV_EST.CA_TEST_CS 
ORDER BY folio_bono
LIMIT 100




SELECT tipo_cobertura, count(1) , sum(cobrado), sum(bonificado),sum(monto_caec), sum(DEDUCIBLE_GES)::int AS deducib_ges ,sum(DEDUCIBLE_CAEC)
FROM est.P_DDV_EST.ca_test_cs 
GROUP BY tipo_cobertura


SELECT 


SELECT b."fld_prestades" , a.* FROM est.P_DDV_EST.CA_TEST_CS  a
INNER JOIN (SELECT DISTINCT 
					"fld_prestacod", 
					"fld_prestades" 
			 FROM isa.P_RDV_DST_LND_SYB.CDTCOMP) b ON (a.codate_trazadora = b."fld_prestacod")
WHERE folio_bono = 95207367



SELECT FOLIO_BONO , count(FOLIO_BONO) AS cant_folios , sum(DEDUCIBLE_CAEC) AS monto_dedu
FROM EST.P_DDV_EST.CA_TEST_CS
--WHERE folio_bono = 94879432--(DEDUCIBLE_CAEC IS NOT NULL AND DEDUCIBLE_GES IS NOT NULL)
GROUP BY folio_bono
ORDER BY cant_folios desc


SELECT * FROM isa.P_RDV_DST_LND_SYB.CDTCOMP
WHERE "fld_prestacod" = 1103066
LIMIT 10


SELECT DISTINCT "fld_prestacod" , "fld_prestades" ,count("fld_prestacod") AS cantidad FROM isa.P_RDV_DST_LND_SYB.CDTCOMP
--WHERE "fld_prestacod" =1802060
GROUP BY "fld_prestacod" , "fld_prestades" 
ORDER BY cantidad desc







SELECT * FROM isa.P_RDV_DST_LND_SYB.CDTCOMP_HISTORICA
WHERE "fld_prestacod" =1802060

SELECT DISTINCT "fld_prestacod" , "fld_prestades" ,count("fld_prestacod") AS cantidad FROM isa.P_RDV_DST_LND_SYB.CDTCOMP_HISTORICA
--WHERE "fld_prestacod" =1802060
GROUP BY "fld_prestacod" , "fld_prestades" 
ORDER BY "fld_prestacod" 





SELECT * FROM codat



SELECT DISTINCT 
periodo, FOLIO_N , rut_titular, TIPO_BONO, GLS_TIPO_BONO, DEDUCIBLE_CAEC , deducible_ges::int AS deducibl_ges  
--TIPO_BONO , GLS_TIPO_BONO 
FROM est.P_DDV_EST.ca_test_cs
WHERE (DEDUCIBLE_CAEC IS NOT NULL AND DEDUCIBLE_GES IS NOT NULL)  AND rut_titular = '013494874-4'


SELECT DISTINCT TIPO_BONO , GLS_TIPO_BONO FROM est.P_DDV_EST.ca_test_cs 
WHERE (DEDUCIBLE_CAEC IS NOT NULL AND DEDUCIBLE_GES IS NOT NULL) 


SELECT * FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO
WHERE "folio_doc" = '96232204'
---LIMIT 10


SELECT "codclasif" , "glsclasif" ,* FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
WHERE "folio_doc"  = 96232204



SELECT tipo_cobertura, count(1) , sum(cobrado), sum(bonificado),sum(monto_caec), sum(DEDUCIBLE_GES)::int AS deducib_ges ,sum(DEDUCIBLE_CAEC)
FROM est.P_DDV_EST.ca_test_cs 
GROUP BY tipo_cobertura






SELECT * FROM GES.P_RDV_DST_LND_SYB.ges_dedindiv 
WHERE "din_idn" =453063  ---2810
LIMIT 10



SELECT * FROM GES.P_RDV_DST_LND_SYB.ges_deducible
WHERE "ded_idn" = 453063--"ded_rutcot" = '018371655-7' --- 4678






SELECT DISTINCT 
		--1 AS n_veces,
		--"folio_doc"::int AS folio,
		try_to_number("folio_doc") AS folio, 
		sum("monto_copago_ges") AS deducible_ges
	FROM ges.P_RDV_DST_LND_SYB.GES_ESTUDIO 
	GROUP BY 
		folio
		LIMIT 10




SELECT "folio_caso" , "folio_doc" ,"rutcot" , "rutben" , COUNT("folio_doc") AS cant_folios , sum("monto_copago_ges") AS total_copagos_ges 
FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO
WHERE "periodo_solges" = 201912--  "folio_caso"  = 376660 -- AND 
GROUP BY "folio_doc", "folio_caso" ,"rutcot", "rutben" 
ORDER BY cant_folios DESC 
LIMIT 10


SELECT * FROM gtn.P_RDV_DST_LND_SYB.SAS_INGRESOS LIMIT 10

SELECT * FROM rcd.P_DDV_RCD.P_DDV_RCD_INGRESOS LIMIT 10

SELECT * FROM isa.P_RDV_DST_LND_SYB.EMP_COB LIMIT 10

SELECT * FROM afi.P_RDV_DST_LND_SYB.COB LIMIT 10









SELECT * FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE FOLIO = 104508330
LIMIT 10
--WHERE folio = 351026


SELECT * FROM gto.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS 
WHERE FOLIO = 351026
LIMIT 10



SELECT 
	ROW_NUMBER () OVER (PARTITION BY "folio_doc" ORDER BY "folio_doc" ) AS n_veces,
	"folio_doc" ,
	"codclasif" ,
	"glsclasif" ,
	"aporte_al_deducible" 
FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO 
WHERE "folio_doc" =95161708
ORDER BY "folio_doc"
LIMIT 100



SELECT "codclasif" , "glsclasif" ,* FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
WHERE "folio_doc"  = 95161708


SELECT 
folio ,
folio AS folio_n,
ROW_NUMBER () OVER (PARTITION BY folio ORDER BY folio ) AS n_veces,
* 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE rut_titular = '004556206-9' AND PERIODO IN (201912,202001)
ORDER BY PERIODO , folio_n , n_veces


-----------FIN DE REVISION
--------------------------





SELECT * FROM 	GS_DEDINDIV
				GS_DEDUCIBLE






SELECT origen, count(1), sum(cobrado), sum(bonificado),SUM(monto_caec) FROM match_datos
--WHERE RUT_BENEFICIARIO IS NULL
GROUP BY origen
ORDER BY origen
















--WHERE pactado_final IS NULL --AND rut_titular ='003568157-4'











SELECT origen, count(1), sum(cobrado), sum(bonificado), sum(MONTO_CAEC)
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE periodo = 201912 AND ORIGEN NOT IN ('LICENCIA')
GROUP BY origen
ORDER BY origen




SELECT 
*
FROM MATCH_DATOS





SELECT origen, count(1), sum(cobrado), sum(bonificado),SUM(monto_caec) FROM match_datos
GROUP BY origen
ORDER BY origen



SELECT 
*
FROM MATCH_DATOS LIMIT 100



SELECT COSTO_FINAL_UF ,* FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE rut_titular ='003568157-4'-- num_contrato = '005241769-4' AND NUM_CORRELATIVO = 20
ORDER BY PERIODO_VIGENCIA 

--RUT_TITULAR = '025719488-4'





LIMIT 100






SELECT * FROM contrato


SELECT top 10 * FROM 



SELECT * FROM gto.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS 
WHERE rutafi ='013233525-7'


SELECT * FROM AFI.P_RDV_DST_LND_SYB.BEN
WHERE "fld_cotrut"='013233525-7'-- AND 200411 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA

SELECT DISTINCT 
"fld_cotrut" ,"fld_funfolio" , "fld_funcorrel" , "fld_benrut" , "fld_bencorrel" , FLD_BENVIGREALDESDE , FLD_BENVIGREALHASTA 
FROM AFI.P_RDV_DST_LND_SYB.BEN
WHERE "fld_cotrut"='013233525-7'
ORDER BY FLD_BENVIGREALDESDE 


SELECT DISTINCT PERIODO , RUT_TITULAR , rut_beneficiario, COD_BENEFICIARIO ,CONTRATO , correlativo, ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE rut_titular = '004556206-9' 


SELECT folio , * 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE rut_titular = '004556206-9' AND PERIODO = 201912


SELECT * FROM hos.P_RDV_DST_LND_SYB.SH_EAM
WHERE "eam_idn" = 62046750



SELECT "codclasif" , "glsclasif" ,* FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
WHERE "folio_doc"  = 78742406




SELECT SUBSTRING(CONTRATO,0,9)::int AS contrato_v2 , CORRELATIVO ,*FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO LIMIT 10


SELECT DISTINCT TIPO_EMISION ,tipo, gls_tipo FROM gto.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS 
ORDER BY tipo, tipo_emision

SELECT top 10 * FROM gto.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS

SELECT DISTINCT ORIGEN FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO

SELECT RUBRO,NOMBRE_RUBRO ,NOMBRE_ACTOR FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE folio = 103832955




SELECT top 10 * FROM gto.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM 


SELECT DISTINCT origen FROM gto.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM



SELECT DISTINCT TIPO_BONO, GLS_TIPO_BONO, PRESTACION FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
--WHERE TIPO_BONO GLS_TIPO_BONO 
ORDER BY TIPO_BONO 




SELECT * FROM gto.P_RDV_DST_LND_SYB.SRE LIMIT 100
---fld_reefolio , fld_reememifec



SELECT top 10 * FROM hos.P_RDV_DST_LND_SYB.SH_EAM
WHERE "eam_idn" = 62046750




SELECT top 10 * FROM HOS.P_RDV_DST_LND_SYB.SH_EAH 
WHERE "eah_idn"= 103832955




SELECT * FROM gto.P_RDV_DST_LND_SYB.BON
WHERE "fld_bonfolio" =43918674 





SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_EAH LIMIT 100



SELECT top 10
	sol."sol_rutafi",
	ep."epr_aturgen", 
	*
FROM HOS.P_RDV_DST_LND_SYB.SH_EPREFA ep
INNER JOIN hos.P_RDV_DST_LND_SYB.SH_SOLTRA sol ON (ep."epr_nrostr"=sol."sol_nrosol")
WHERE ep."epr_aturgen" ='S'











---------------------------------
---revision de codigo 
WITH cronicos AS ( 
SELECT DISTINCT 
	   --est.P_DDV_EST.periodo_fecha(A.PERIODO) AS pererido,
	   SUBSTRING(a.contrato,0,9)::int AS funfolio,
	   a.num_corr,
	   (CASE
	   		WHEN (A.GLS_TIPO IN ('GES','GES-CAEC','CAEC') AND NOT A.PROBLEMA_SALUD IN ('19','20','23','26','29','30','35','36','46','47','54','66')) THEN 1 -- SI CRONICO
	   		ELSE 0 -- NO CRONICO
	   	END)AS CRONICO_GRAVE
FROM GTO.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS A
--LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN BN ON (BN."fld_benrut"= A.RUTAFI AND A.PERIODO BETWEEN BN.FLD_BENVIGREALDESDE AND BN.FLD_BENVIGREALHASTA)
WHERE cronico_grave = 1 and  a.PERIODO = 201912 --LIKE '2019%' -- AND a.rutafi='006627768-2' 
), UNION_ AS (
SELECT DISTINCT 
	 "fld_funfolio",
	 "fld_funcorrel",
	 "fld_cotrut",
	 CASE 
	 	WHEN cr.cronico_grave IS NULL THEN 'no cronico'
	 	ELSE 'cronico'
	 END AS estado_cronico
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
LEFT JOIN  cronicos cr ON (cnt."fld_funfolio"=cr.funfolio AND  cnt."fld_funcorrel"=cr.num_corr)
WHERE 201912 BETWEEN fld_vigrealdesde AND FLD_VIGREALHASTA 
)
SELECT 
	estado_cronico,
	count(1)
FROM union_
GROUP BY estado_cronico







SELECT  * FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE 201912 BETWEEN fld_vigrealdesde AND FLD_VIGREALHASTA 
LIMIT 10



SELECT count(1) FROM cronicos
GROUP BY CRONICO_GRAVE




SELECT CRONICO_GRAVE, count(1) FROM cronicos
GROUP BY CRONICO_GRAVE


SELECT * FROM cronicos 
WHERE periodo LIKE '2019%'
ORDER BY rutafi, periodo 




SELECT CRONICO_GRAVE, count(1) FROM cronicos
GROUP BY CRONICO_GRAVE



SELECT * FROM GTO.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS
WHERE folio = 94414046


SELECT * FROM AFI.P_RDV_DST_LND_SYB.BEN LIMIT 10


---------- respaldo query
SELECT DISTINCT 
	   --est.P_DDV_EST.periodo_fecha(PERIODO) AS periodo,
	   periodo,
	   SUBSTRING(contrato,0,9)::int AS contrato,
	   num_corr,
	   (CASE
	   		WHEN (GLS_TIPO IN ('GES','GES-CAEC','CAEC') AND NOT PROBLEMA_SALUD IN ('19','20','23','26','29','30',
																					'35','36','46','47','54','66')) THEN 1 -- SI CRONICO
	   		ELSE 0 -- NO CRONICO
	   	END)AS CRONICO_GRAVE
FROM GTO.P_DDV_GTO.P_DDV_GTO_VGASTOEMISIONBONOS
--WHERE cronico_grave = 1
WHERE rutafi='001757033-1' AND cronico_grave = 1 ---AND periodo BETWEEN 201910 AND 2020
ORDER BY periodo, contrato, NUM_CORR 


SELECT * FROM afi.P_RDV_DST_LND_SYB.CNT LIMIT 10

---  poder amarrar con la cnt funfolio - correlativo - periodo - rutcoti


SELECT DISTINCT RUT_TITULAR , bad."bad_fecsus"::date FROM EST.P_DDV_EST.CA_BAC_CSV csv
	INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD bad ON (CSV.rut_titular = bad."bad_cotrut" AND bad."bad_producto"=50)

SELECT * FROM afi.P_RDV_DST_LND_SYB.BAD







SELECT "fld_cotrut" , count("fld_cotrut") AS cantidad FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_tpotransac" = 'VN' AND "fld_funnotificacod" = 1
GROUP BY "fld_cotrut"
ORDER BY cantidad DESC  LIMIT 10













