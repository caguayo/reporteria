import os

dirfile = "/mnt/c/Users/NB-COLMENA/OneDrive/Documents/maestros/Archivos"
m3 = 0
m65 = 0


def maestr3(in_file):
    out_file = in_file + "_out"
    with open(in_file, encoding="ISO-8859-1") as f:
        with open(out_file, "w") as w:
            for line in f:
                line_array = line.split('|')
                line_array[4] = str(5 * int(line_array[4]) - 3265112 + 52658)

                new_line = "|".join(line_array)
                w.write(new_line)


def maestr65(in_file):
    out_file = in_file + "_out"
    with open(in_file, encoding="ISO-8859-1") as f:
        with open(out_file, "w") as w:
            for line in f:
                line_array = line.split('|')
                line_array[2] = str(5 * int(line_array[2]) - 3265112 + 52658)
                line_array[5] = str(5 * int(line_array[5]) - 3265112 + 52658)
                line_array[8] = "               "
                line_array[9] = "               "
                line_array[10] = "                   "

                new_line = "|".join(line_array)
                w.write(new_line)


listdir = os.listdir(dirfile)

for d in listdir:
    listfile = os.listdir(dirfile + "/" + d)
    # print(listfile)
    for f in listfile:
        if f.endswith(".065"):  # si f temina en 065
            # print(dirfile + "/" + d + "/" + f)
            maestr65(dirfile + "/" + d + "/" + f)
            m65 += 1
            print("m65: " + str(m65))
        elif f.endswith(".003"):  # si f temina en 065
            # print(dirfile + "/" + d + "/" + f)
            maestr3(dirfile + "/" + d + "/" + f)
            m3 += 1
            print("m3: " + str(m3))
