SELECT DISTINCT 
	"my_fecha_informacion" 
FROM ISA.P_RDV_DST_LND_SYB.SISP_PRESTBONIFICADAS  
WHERE CONTAINS ("my_fecha_informacion",'2022')
-- LIMIT 10

              	

------ Prestaciones Bonificadas 031
------ AMBULATIRIO-------------------------------------
-------------------------------------------------------
SELECT 
	SUBSTRING("my_fecha_informacion",3,4) AS anio,
	--SUBSTRING("my_fecha_informacion",0,2) AS mes,
	CONCAT(SUBSTRING("my_fecha_informacion",3,4),SUBSTRING("my_fecha_informacion",0,2)) AS periodo,
	"my_cod_isapre" ,
	"my_fecha_informacion" ,
	"my_Tipo_Registro" ,
	"my_numbeneficiario" ,
	SUBSTRING("my_numbeneficiario",1,9)::int*5-3265112+52658 AS "my_numbeneficiario", 
	"my_dvbeneficiario" ,
	"my_ben_sexo" ,
	"my_ben_edad" ,
	"my_Tipo_Benef" ,
	"my_numrut_prestador" ,
	"my_dvrut_prestador" ,
	"my_region" ,
	"my_comuna" ,
	"my_ptd_pref" ,
	"Pam_principal" ,
	"Pam_complementario" ,
	"my_folio_documento" ,  ---- codigo bono
	"my_folio_reem" ,
	"my_cod_prestacion" ,
	"my_cod_pertenecia" ,
	"my_cobertura_financ" , ---- 1:GES ; 2:CAEC ; 3:GES-CAEC ; 4:PLAN COMPLEMENTARIO ; 5:NO BONIFICADO ; 6:OTROS
	CASE
		WHEN "my_cobertura_financ" = 1 THEN 'GES'
		WHEN "my_cobertura_financ" = 2 THEN 'CAEC'
		WHEN "my_cobertura_financ" = 3 THEN 'GES-CAEC'
		WHEN "my_cobertura_financ" = 4 THEN 'PLAN COMPLEMENTARIO'
		WHEN "my_cobertura_financ" = 5 THEN 'NO BONIFICADA'
		WHEN "my_cobertura_financ" = 6 THEN 'OTROS'
	END AS gls_cobertura,
	"my_fecha_bonif" ,
	"my_Identificacion_plan" ,
	"my_prestador_tipo" ,
	"my_tipo_atencion" , --- 1:AMBULATORIO ; 2:HOSPITALARIO
	CASE 
		WHEN "my_tipo_atencion" = 1 THEN 'AMBULATORIO'
		ELSE 'HOSPITALARIO'
	END AS tipo_prestacion,
	"my_cantidad_prest" ,
	"my_val_atencion" ,
	"my_val_bonif" ,
	"my_val_copago" ,
	"my_bonif_restringida" ,
	"my_tipo_plan" ,
	"my_horario" ,
	"my_modalidad" ,
	"my_ley_urgencia" ,
	"my_cod_gpp" ,
	"my_ges" 
FROM ISA.P_RDV_DST_LND_SYB.SISP_PRESTBONIFICADAS 
WHERE 
		CONCAT(SUBSTRING("my_fecha_informacion",3,4),SUBSTRING("my_fecha_informacion",0,2)) IN (202203) AND 
		"my_tipo_atencion" = 1 AND 
		"my_folio_reem" IN (0) AND 
		"my_folio_documento" =108673130
ORDER BY "my_folio_documento" 
LIMIT 1000
------------------------------ FIN MAESTROS----------------------------------





-------------------
------------- BASES RAICES 
/*AMBULATORIO*/

------------BONOS
WITH revi_ambulatorio AS (
select 
	bon."fld_bonfolio" as idn, /*folio del bono*/
	bon."fld_cotrut"   as rut_afiliado, /*rut del titular*/ 
	SUBSTRING(bon."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	bon."fld_bencorrel" as bencorrel, /*c�digo beneficiario*/
	ben."fld_benrut" as rut_beneficiario,
	SUBSTRING(ben."fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	--bon."fld_funfolio" as contrato, /*contrato*/	
	--bon."fld_funcorrel" as correlativo, /*correlativo del contrato*/    	
	emp.empleador_1,
	emp.empleador_2,	
	bon."fld_medrut" as rut_prestador, /*rut del prestador*/
	prest."gls_prestador" AS nombre_prestador,
	bon."fld_bonemifec"::date as fecha, /*fecha prestaci�n*/
	EST.P_DDV_EST.FECHA_PERIODO(bon."fld_bonemifec") AS periodo,
	--year(datepart(bon."fld_bonemifec"))*100 + month(datepart(bon."fld_bonemifec")) as periodo, /*periodo*/ 
	case
    	when bon."cso_idn" > 0 and bon."fld_folioderivages" = 0 then 'CAEC'
		when bon."cso_idn" = 0 and bon."fld_folioderivages" > 0 then 'GES'
		when bon."fld_folioderivages" > 0 and bon."cso_idn" > 0 then 'GES-CAEC'
		else 'OTRO' 
	end  as activacion, /*GES, CAEC o plan complementario*/
	bon."fld_totprestaval"::int as cobrado, /*Valor cobrado por el prestador*/
	bon."fld_totbonific"::int as bonificado, /*Valor bonificado por la isapre*/
	cobrado-bonificado AS copago,
	'bon' as origen
from GTO.P_RDV_DST_LND_SYB.BON bon
LEFT JOIN ISA.P_RDV_DST_LND_SYB.PRM_AGRUPA_PRESTADORES prest ON (bon."fld_medrut"=prest."rut")
inner join AFI.P_RDV_DST_LND_SYB.BEN ben
	on bon."fld_funfolio" = ben."fld_funfolio"
	and bon."fld_funcorrel" = ben."fld_funcorrel"
	and bon."fld_bencorrel" = ben."fld_bencorrel"
	and bon."fld_bonestado" not in (3,4) 
	and bon."fld_bonemifec" between '2021-07-01 00:00:00' and '2021-12-31 23:59:59'  ----cambiar el periodo por le solicitado
left join est.P_DDV_EST.ca_emp emp
	on bon."fld_funfolio" = emp.fld_funfolio
	and bon."fld_funcorrel" = emp.fld_funcorrel	
----------------------- FIN BONOS
union 
----------------/**REEMBOLSOS**/
select 
	ree."fld_reefolio" as idn,
	ree."fld_cotrut" as rut_afiliado,
	SUBSTRING(ree."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	cdt."fld_bencorrel" as bencorrel, /*aca esta en la tabla de detalle*/
	cdt."fld_benrut" as rut_beneficiario,
	SUBSTRING(cdt."fld_benrut",1,9)::int*5-3265112+52658  AS ID_RUT_BENEFICIARIO,
	--ree."fld_funfolio" as contrato, /*contrato*/
	--ree."fld_funcorrel" as correlativo, /*correlativo del contrato*/  
	emp.empleador_1,
	emp.empleador_2, 
	cdt."fld_medrut" as rut_prestador,
	prest."gls_prestador" AS nombre_prestador,
	ree."fld_reepagfec"::date as fecha, /*fecha prestaci�n*/
	EST.P_DDV_EST.FECHA_PERIODO(ree."fld_reepagfec") AS periodo,
	--year(datepart(ree.fld_reepagfec))*100 + month(datepart(ree.fld_reepagfec)) as periodo, /*periodo*/
	case
	   when ree."cso_idn" > 0 and ree."fld_folioderivages" = 0 then 'CAEC'
	   when ree."cso_idn" = 0 and ree."fld_folioderivages" > 0 then 'GES'
	   when ree."fld_folioderivages" > 0 and ree."cso_idn" > 0 then 'GES-CAEC'
	   else 'OTRO' 
	end as activacion, /*GES, CAEC o plan complementario*/
	ree."fld_totprestaval"::int as cobrado,
	(ree."fld_totprestaval" - ree."fld_totbonific")::int as bonificado,
	cobrado-bonificado  AS copago,
	'ree' as origen
from GTO.P_RDV_DST_LND_SYB.SRE  ree
inner join GTO.P_RDV_DST_LND_SYB.SRE_CDT cdt
	on ree."fld_reefolio" = cdt."fld_reefolio"
	and cdt."fld_srecorrel" = 1
	and ree."fld_reeestado" not in (3,4) 
	and ree."fld_reepagfec" between '2021-07-01 00:00:00' and '2021-12-30 23:59:59'
LEFT JOIN ISA.P_RDV_DST_LND_SYB.PRM_AGRUPA_PRESTADORES prest ON (cdt."fld_medrut"=prest."rut")
left join est.P_DDV_EST.ca_emp emp
	on ree."fld_funfolio" = emp.fld_funfolio
	and ree."fld_funcorrel" = emp.fld_funcorrel
------------- FIN REEM
)
SELECT
	*
FROM revi_ambulatorio
LIMIT 10
-----------FIN GASTOS AMBULATORIOS




------- HOSPITALARIO
------- PRESTACIONES HOSPITALARIO       
WITH hospitalario AS ( 
----- eah 
select distinct    
	eah."eah_idn" as idn_bono,/*idn prestacion*/
	eah."sol_nrosol" AS st,
	eah."eah_rutcot" as rut_afiliado,/*rut afiliado*/
	eah."eah_codben" as bencorrel,/*codigo beneficiario*/
	eah."eah_nrocon" as contrato, /*contrato*/
	eah."eah_nrocor" as correlativo, /*correlativo*/
	eah."eah_conefe"::date as fecha, /*fecha contable*/
	est.P_DDV_EST.FECHA_PERIODO(eah."eah_conefe") AS periodo,/*periodo_contable*/
	--year(datepart(eah.eah_conefe))*100 + month(datepart(eah.eah_conefe)) as periodo,/*periodo contable*/
	sol."sol_insrut" as rut_prestador, /*rut prestador*/
	case
	    when eah."der_idn" > 0 and eah."cso_idn" = 0 then 'GES'
		when eah."cso_idn" > 0 and eah."der_idn" = 0 then 'CAEC'
		when eah."der_idn" > 0 and eah."cso_idn" > 0 then 'GES-CAEC'
		else 'OTRO' 
	end as activacion, /*prestacion ges-caec, ges o caec*/
	eah."eah_catego" AS categoria ,
	0 AS codate,
	dah."dah_codrub" AS rubro,
	eah."eah_urgencia" as urgencia, /*es urgencia*/	
	eah."eah_cobtot"::int as cobrado, /*cobrado*/
	eah."eah_valbon"::int + eah."fld_gescaec"::int as bonificado /*bonificado*/
from HOS.P_RDV_DST_LND_SYB.SH_EAH  eah 
	LEFT  JOIN  HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol on (eah."sol_nrosol" = sol."sol_nrosol" and eah."eah_rutcot" = sol."sol_rutafi" and eah."eah_codben" = sol."sol_codben")
	INNER JOIN HOS.P_RDV_DST_LND_SYB.SH_DAH dah ON (eah."eah_idn" = dah."eah_idn")
	WHERE  eah."eah_conefe" BETWEEN '2015-01-01 00:00:00' and '2021-01-30 23:59:59' AND eah."eah_estado" not in (3,4)
---fin eah
UNION
----eam 
select  
	eam."eam_idn"  as idn_bono,
	eam."sol_nrosol" AS st,
	eam."eam_rutcot"  as rut_afiliado,
	eam."eam_codben"  as bencorrel,
	eam."eam_nrocon" as contrato,
	eam."eam_nrocor"  as correlativo,
	eam."eam_conefe"::date as fecha,
	est.P_DDV_EST.fecha_periodo(eam."eam_conefe") AS periodo,
	sol."sol_insrut"  as rut_prestador,
	 /*prestacion ges-caec, ges o caec*/
	case
	    when eam."der_idn">0 and eam."cso_idn" = 0 then 'GES'
		when eam."cso_idn">0 and eam."der_idn" = 0 then 'CAEC'
		when eam."der_idn">0 and eam."cso_idn">0   then 'GES-CAEC'
		else 'OTRO' 
	end  as activacion,
	eam."eam_catego" AS categoria,
	dam."dam_codate" AS codate,
	0 AS rubro,
	/*es urgencia*/	eam."eam_urgencia" as urgencia,
	/*cobrado*/eam."eam_valcob"::int  as cobrado,
 	/*bonificado*/ eam."eam_valbon"::int + eam."fld_gescaec"::int as bonificado
from HOS.P_RDV_DST_LND_SYB.SH_EAM eam 
    	    LEFT  JOIN HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol on (eam."sol_nrosol" = sol."sol_nrosol" and eam."eam_rutcot" = sol."sol_rutafi" and eam."eam_codben" = sol."sol_codben")
    	    INNER JOIN HOS.P_RDV_DST_LND_SYB.SH_DAM dam ON (eam."eam_idn" = dam."dam_idn") ---no aplica el rubro desde la eam
WHERE eam."eam_conefe" between '2015-01-01 00:00:00' and '2021-01-30 23:59:59' and eam."eam_estado" not in (3,4)
---fin eam
), paquetes AS ( 
--SELECT * FROM hospitalario 
--WHERE st = 3604444
SELECT   
	--epr."pqt_idn" AS pqt_epr,
	--pro."pqt_idn" AS pqt_proced ,
	--vis."pqt_idn" AS pqt_vis,
	--hon."pqt_idn" AS pqt_hon,
	--hos.idn,
	--hos.st,
	--COALESCE (epr."pqt_idn", pro."pqt_idn", vis."pqt_idn" , hon."pqt_idn") AS pqt_idn,
	--pqt."pqt_tecnica" ,
	--pqt."pqt_codate",
    --epr."epr_nrostr" ,
	hos.*,
	hges."hos_valcob" AS cobrado_hos, 
	hges."hos_valbonsis" AS bonificado_hos,
	hges."hos_codate"  ,
	hges."epr_codidn" 
	--cat.TIPO_PRODUCTO 
FROM hospitalario hos
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EPREFA  		epr ON  (hos.st = epr."epr_nrostr" AND hos.cobrado = epr."epr_monexe") ----
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_PROCED  		pro ON  (hos.st = pro."proc_nrostr" AND hos.cobrado = pro."proc_valtot") ----
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_VISITA  		vis ON  (hos.st = vis."vis_nrostr" AND hos.cobrado = vis."vis_valtot" ) ----
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_HONMED  		hon ON  (hos.st = hon."hon_nrostr" AND  hos.cobrado = hon."pqt_monto" ) -----
	LEFT JOIN CME.P_RDV_DST_LND_SYB.CM_PQTN         pqt ON  (COALESCE (epr."pqt_idn", pro."pqt_idn", vis."pqt_idn" , hon."pqt_idn") = pqt."pqt_idn") -----
	--LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES       cat ON  (hos.categoria = cat.COD_DESACA) -----
	FULL JOIN HOS.P_RDV_DST_LND_SYB.SH_HOSGES       hges ON (epr."epr_codidn" = hges."epr_codidn" AND rubro = hges."hos_codrub") ----- 
--WHERE st = 3233574
--ORDER BY st 
--LIMIT 5000
)
, presta_hos AS (
SELECT
	idn_bono,
	st,
	rut_afiliado ,
	bencorrel ,
	contrato ,
	correlativo ,
	fecha ,
	periodo ,
	rut_prestador ,
	activacion ,
	categoria ,
	"epr_codidn" ,
	"hos_codate" ,
	rubro ,
	urgencia ,
	cobrado_hos::INT AS cobrado ,
	bonificado_hos::INT AS bonificado
FROM paquetes 
WHERE periodo =201806 --AND idn_bono =83697594 --AND --rubro =7--PQT_IDN != 0 --AND codate != 0  --codate = 2004003--
ORDER BY periodo , idn_bono , rubro 
--LIMIT 1000
)
SELECT 
	rut_afiliado ,
	idn_bono ,
	periodo ,
	rubro ,
	--count(DISTINCT idn_bono) AS cant_bonos,
	sum(cobrado) AS cob,
	sum(bonificado) AS bon
FROM presta_hos
GROUP BY rut_afiliado ,periodo ,rubro , idn_bono 
ORDER BY rubro 
LIMIT 10
---------------- HOSPITALARIO







------ EXTRACCIONES DE MAESTROS------------------------


------ Prestaciones Bonificadas 031--------------------
------ AMBULATORIO-------------------------------------
SELECT 
	--SUBSTRING("my_fecha_informacion",3,4) AS anio,
	--SUBSTRING("my_fecha_informacion",0,2) AS mes,
	--CONCAT(SUBSTRING("my_fecha_informacion",3,4),SUBSTRING("my_fecha_informacion",0,2)) AS periodo,
	"my_cod_isapre" ,
	"my_fecha_informacion" ,
	"my_Tipo_Registro" ,
	"my_numbeneficiario" ,
	SUBSTRING("my_numbeneficiario",1,9)::int*5-3265112+52658 AS "my_numbeneficiario", 
	"my_dvbeneficiario" ,
	"my_ben_sexo" ,
	"my_ben_edad" ,
	"my_Tipo_Benef" ,
	"my_numrut_prestador" ,
	"my_dvrut_prestador" ,
	"my_region" ,
	"my_comuna" ,
	"my_ptd_pref" ,
	"Pam_principal" ,
	"Pam_complementario" ,
	"my_folio_documento" ,  ---- codigo bono
	"my_folio_reem" ,
	"my_cod_prestacion" ,
	"my_cod_pertenecia" ,
	"my_cobertura_financ" , ---- 1:GES ; 2:CAEC ; 3:GES-CAEC ; 4:PLAN COMPLEMENTARIO ; 5:NO BONIFICADO ; 6:OTROS
	"my_fecha_bonif" ,
	"my_Identificacion_plan" ,
	"my_prestador_tipo" ,
	"my_tipo_atencion" , --- 1:AMBULATORIO ; 2:HOSPITALARIO
	"my_cantidad_prest" ,
	"my_val_atencion" ,
	"my_val_bonif" ,
	"my_val_copago" ,
	"my_bonif_restringida" ,
	"my_tipo_plan" ,
	"my_horario" ,
	"my_modalidad" ,
	"my_ley_urgencia" ,
	"my_cod_gpp" ,
	"my_ges" 
FROM ISA.P_RDV_DST_LND_SYB.SISP_PRESTBONIFICADAS 
WHERE 
		CONCAT(SUBSTRING("my_fecha_informacion",3,4),SUBSTRING("my_fecha_informacion",0,2)) IN (202203)
ORDER BY "my_folio_documento" 
LIMIT 10
------------------------------ FIN MAESTROS----------------------------------





-------- 066
--------------------------------------
SELECT
	"fld_perpag" ,
	----"fld_cotrut" ,
	SUBSTRING("fld_cotrut" ,1,9)::int*5-3265112+52658 AS "id_rut" ,
	"fld_tipodocu_circ" ,
	"fld_origen" ,
	"fld_tip_trab_circ" ,
	"fld_emprut" ,
	"fld_nom_emp" ,
	"fld_com_emp" ,
	"fld_ciu_emp" ,
	"fld_reg_emp" ,
	"fld_tip_trab" ,
	"fld_cot_legal" ,
	"fld_cot_adic_vol_circ" ,
	"fld_cot_pactada" ,
	"fld_pagado" ,
	"oport_pag_circ" ,
	"fld_percot" ,
	"fld_fecha_pago" ,
	"fld_grat_desde" ,
	"fld_grat_hasta" ,
	"fld_rut_sbs" ,
	"fld_manual_circ"
FROM LCC.P_RDV_DST_LND_SYB.LINEAS_ESTUDIO
WHERE "fld_perpag" = 202203
LIMIT 10






SELECT DISTINCT
	"fld_perpag"
FROM  LCC.P_RDV_DST_LND_SYB.LINEAS_ESTUDIO
ORDER BY "fld_perpag" ASC
--LIMIT 10







-------  003 
SELECT 
	* 
FROM LCC.P_RDV_DST_LND_SYB.LCC_SISPLIC 
LIMIT 10







------  004 cuarto campo 1-redictament por compin  2-redictamen por la isapre 3-reliquidacion
SELECT DISTINCT 
	"si_tiprec" 
FROM LCC.P_RDV_DST_LND_SYB.LCC_SISPREC 
LIMIT 10








---- EXTRACCION MAESTROS POR MES ---

--(1) Archivo Maestro de Licencias M�dicas (03)																	
SELECT 
	"si_color",
	"si_numero",
	"si_isacod",
	"si_perinf",
	--"si_rutnum", --- encriptar rut
	SUBSTRING("si_rutnum" ,1,9)::int*5-3265112+52658 AS "id_rut" ,
	"si_rutdig",
	"si_fecemi",
	"si_soldia",
	"si_solini",
	"si_edad",
	"si_sexo",
	"si_actlab",
	"si_ocupacion",
	"si_soltip",
	"si_solrep",
	"si_medrutnum",
	"si_medrutdig",
	"si_medtip",
	"si_auttip",
	"si_autdia",
	"si_ciepri",
	"si_autres",
	"si_autcon",
	"si_autrep",
	"si_autder",
	"si_fecrec",
	"si_autfec",
	"si_empfec",
	"si_tracom",
	"si_trareg",
	"si_caltra",
	"si_pagent",
	"si_pagdia",
	"si_mtosub",
	"si_mtosal",
	"si_afpcod",
	"si_pagini",
	"si_recupe",
	"si_mescon",
	"si_mtopre",
	"si_ciesec",
	"si_hijrutnum",
	"si_hijrutdig",
	"si_replug",
	"si_invalidez",
	"si_caurec",
	"si_diapre",
	"si_fecpriafi",
	"si_feccon",
	"si_bascal",
	"si_emprutnum",
	"si_emprutdig",
	"si_oricol",
	"si_orinum",
	"si_fecnac"
FROM LCC.P_RDV_DST_LND_SYB.LCC_SISPLIC
WHERE  CONCAT(SUBSTRING("si_perinf",3,4),SUBSTRING("si_perinf",0,2)) IN (202203)
--LIMIT 10





--(4) Archivo Maestro de Redict�menes de la COMPIN, Reconsideraciones de la Isapre y Reliquidaciones (04)			
SELECT 
	"si_color",
	"si_numero",
	"si_isacod",
	"si_perinf",
	"si_tiprec",
	"si_recres",
	"si_recfec",
	"si_reccom",
	"si_recnum",
	"si_recdia",
	"si_mtosub",
	"si_mtosal",
	"si_plafec",
	"si_reccum",
	"si_mtopre"
FROM LCC.P_RDV_DST_LND_SYB.LCC_SISPREC
WHERE CONCAT(SUBSTRING("si_perinf",3,4),SUBSTRING("si_perinf",0,2)) IN (202203)
--LIMIT 10






--(2) Archivo Maestro de Prestaciones Bonificadas (031)															
SELECT 
	"my_cod_isapre",
	"my_fecha_informacion",
	"my_Tipo_Registro",
	--"my_numbeneficiario",   --- encriptar rut
	SUBSTRING("my_numbeneficiario" ,1,9)::int*5-3265112+52658 AS "id_rut" ,
	"my_dvbeneficiario",
	"my_ben_sexo",
	"my_ben_edad",
	"my_Tipo_Benef",
	"my_numrut_prestador",
	"my_dvrut_prestador",
	"my_region",
	"my_comuna",
	"my_ptd_pref",
	"Pam_principal",
	"Pam_complementario",
	"my_folio_documento",
	"my_folio_reem",
	"my_cod_prestacion",
	"my_cod_pertenecia",
	"my_cobertura_financ",
	"my_fecha_bonif",
	"my_Identificacion_plan",
	"my_prestador_tipo",
	"my_tipo_atencion",
	"my_cantidad_prest",
	"my_val_atencion",
	"my_val_bonif",
	"my_val_copago",
	"my_bonif_restringida",
	"my_tipo_plan",
	"my_horario",
	"my_modalidad",
	"my_ley_urgencia",
	"my_cod_gpp",
	"my_ges"
FROM ISA.P_RDV_DST_LND_SYB.SISP_PRESTBONIFICADAS 
WHERE  CONCAT(SUBSTRING("my_fecha_informacion" ,3,4),SUBSTRING("my_fecha_informacion" ,0,2)) IN (202203)
--LIMIT 10




--(5) Archivo Maestro de Contratos (064)
SELECT
	"mcto_perinf",
	"mcto_asegcod",
	--"mcto_cotrun",
	SUBSTRING("mcto_cotrun" ,1,9)::int*5-3265112+52658 AS "id_cotrut" ,
	"mcto_cotdv",
	"mcto_runcotalt",
	"mcto_fecsus",
	"mcto_tipsus",
	"mcto_inivigfec",
	"mcto_tiptrab",
	"mcto_mesanual",
	"mcto_glscateg",
	"mcto_tipplan",
	"mcto_mtoges",
	"mcto_mtocaec",
	"mcto_mtoadic",
	"mcto_mtocotizpact",
	"mcto_mtocotiztot",
	"mcto_ultmovfec",
	"mcto_movtip",
	"mcto_renunexced",
	"mcto_existecaec",
	"mcto_terminoctofec",
	"mcto_terminobenfec",
	"mcto_causatermino",
	"mcto_rutagte",
	"mcto_dvagte",
	"mcto_numemp",
	"mcto_modcotiz",
	"mcto_tiprelemp"
FROM ISA.P_RDV_DST_LND_SYB.MAESTROCONTRATOS_032022
--LIMIT 10





--(3) Archivo Maestro de Cotizantes y cargas Isapres (065)														
SELECT 
	"mcar_perinf",
	"mcar_asegcod",
	--"mcar_cotrun",
	SUBSTRING("mcar_cotrun" ,1,9)::int*5-3265112+52658 AS "id_cotrut" ,
	"mcar_cotdv",
	"mcar_cotrunalt",
	---"mcar_benrun",
	SUBSTRING("mcar_benrun",1,9)::int*5-3265112+52658 AS "id_benrut" ,
	"mcar_bendv",
	"mcar_benrunalt",
	"mcar_benappat",
	"mcar_benapmat",
	"mcar_bennombre",
	"mcar_bennacfec",
	"mcar_cotfecfallec",
	"mcar_bensexo",
	"mcar_cotestcivil",
	"mcar_cotnacionalidad",
	"mcar_cotcomunacod",
	"mcar_cotregioncod",
	"mcar_cargarelcod",
	"mcar_bentipcod",
	"mcar_estvigencia"
FROM ISA.P_RDV_DST_LND_SYB.MAESTROCOTCARGAS_032022
--WHERE "mcar_perinf" =202203 --CONCAT(SUBSTRING("mcar_perinf" ,3,4),SUBSTRING("mcar_perinf" ,0,2)) IN (202203)
--LIMIT 10





--(6) Archivo Maestro de Cotizaciones de Salud (066)															
SELECT 
	"fld_perpag",
	--"fld_cotrut",
	SUBSTRING("fld_cotrut" ,1,9)::int*5-3265112+52658 AS "id_cotrut" ,
	"fld_tipodocu_circ",
	"fld_origen",
	"fld_tip_trab_circ",
	"fld_emprut",
	"fld_nom_emp",
	"fld_com_emp",
	"fld_ciu_emp",
	"fld_reg_emp",
	"fld_tip_trab",
	"fld_cot_legal",
	"fld_cot_adic_vol_circ",
	"fld_cot_pactada",
	"fld_pagado",
	"oport_pag_circ",
	"fld_percot",
	"fld_fecha_pago",
	"fld_grat_desde",
	"fld_grat_hasta",
	"fld_rut_sbs",
	"fld_manual_circ"
FROM LCC.P_RDV_DST_LND_SYB.LINEAS_ESTUDIO
WHERE "fld_perpag" BETWEEN 202001 AND 202012







--(7) Archivo Maestro GES 																						
SELECT 
	"sges_isacod",
	"sges_fecinf",
	"sges_folio",
	"sges_fecsol",
	"sges_tipsol",
	--"sges_rutsol",
	SUBSTRING("sges_rutsol" ,1,9)::int*5-3265112+52658 AS "id_rutsol" ,
	"sges_dvsol",
	"sges_altsol",
	"sges_tipcot",
	--"sges_rutcot",
	SUBSTRING("sges_rutcot" ,1,9)::int*5-3265112+52658 AS "id_rutcot" ,
	"sges_dvcot",
	"sges_altcot",
	"sges_tipben",
	--"sges_rutben",
	SUBSTRING("sges_rutben" ,1,9)::int*5-3265112+52658 AS "id_rutben" ,
	"sges_dvben",
	"sges_altben",
	"sges_sexo",
	"sges_fecnac",
	"sges_ps",
	"sges_evento",
	"sges_feceve",
	"sges_resol",
	"sges_fecres",
	"sges_resolglo",
	"sges_notif",
	"sges_fecnotif",
	"sges_rutpredes",
	"sges_dvpredes",
	"sges_nompredes",
	"sges_resben",
	"sges_rechben",
	"sges_idn",
	"sges_reenvio",
	"sges_fereen",
	"sges_cie",
	"sges_rama",
	"sges_estado",
	"sges_causacier"
FROM ISA.P_RDV_DST_LND_SYB.GES_HISANEXO1_2010
WHERE CONCAT(SUBSTRING("sges_fecinf" ,3,4),SUBSTRING("sges_fecinf" ,0,2)) = 202202--BETWEEN 202101 AND 202112 
--LIMIT 10






