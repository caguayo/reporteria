
SELECT * FROM EST.P_DDV_EST.CA_BASE_CIA_SEG

SET periodo_ini ='2015-01-01'

SET periodo_fin = getdate()::timestamp;

--TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),0))||' 23:59:59')::timestamp;
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_BASE_CIA_SEG AS ( 
WITH nums(i) AS (
SELECT 
	0
	UNION ALL SELECT i + 1
FROM nums 
WHERE i <= datediff(MONTH, $periodo_ini, $periodo_fin)-1
)
,  periods AS 
(
 SELECT 
 	EST.P_DDV_EST.FECHA_PERIODO(dateadd(month, i::int, $periodo_ini)) AS periodo,
 	dateadd(month, i::int, $periodo_ini) AS periodo_fecha
 FROM nums
 )
 --SELECT
 --	*
 --min(periodo),max(periodo) 
 --FROM periods LIMIT 10 
  , 
minima_vig_rank AS (
SELECT DISTINCT 
	--"fld_funfolio",
	--"fld_funcorrel",
	"fld_cotrut" ,
	"fld_ingreso" ,
	"fld_cntcateg",
	MIN(FLD_VIGREALDESDE) AS minima_vigencia ,
row_number() over (partition by "fld_cotrut", "fld_ingreso" order by "fld_cotrut", minima_vigencia) AS n_veces 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
	--"fld_funfolio",
	--"fld_funcorrel",
	"fld_cotrut",
	"fld_ingreso",
	"fld_cntcateg"
ORDER BY "fld_cotrut" , minima_vigencia 
)
--SELECT * FROM minima_vig_rank
--WHERE "fld_cotrut" = '012073836-4'
, minima_vig AS (
SELECT * FROM minima_vig_rank
WHERE n_veces =1
)
--SELECT * FROM minima_vig 
--WHERE "fld_cotrut" = '012073836-4'
--LIMIT 100 
--WITH 
, cartera_vigente AS ( 
SELECT DISTINCT 
	per.periodo,
	--per.periodo_fecha,
	--EST.P_DDV_EST.PERIODO_FECHA(per.periodo::FLOAT) AS periodo_fecha,
	cnt."fld_funfolio"  						AS contrato,
	cnt."fld_funcorrel"  						AS correlativo,
	cnt."fld_cotrut" 							AS rut_cot,
	ben."fld_benrut" 							AS rut_beneficiario,
	SUBSTRING(cnt."fld_cotrut" ,1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	SUBSTRING(ben."fld_benrut" ,1,9)::int*5-3265112+52658  AS id_rut_beneficirio,
	CASE 
		WHEN ben."fld_bencorrel" = 0 THEN 'cotizante'
		ELSE 'carga'
	END AS parentesco,							--parentesco (tipo de beneficiario) 
	ben."fld_benedad" 							AS edad,
	ben."fld_bennacfec" 						AS fec_nacimiento,
	CASE 
		WHEN ben."fld_bensexo" = 1 THEN 'F'
		ELSE 'M'
	END AS sexo,								--sexo
	cnt."fld_carganum" 							AS cant_cargas, 
	prm.VENTAPLAN_EN 							AS venta_en,
	prm.DETALLE_PRODUCTO    					AS detalle_producto,
	mv.minima_vigencia	 						AS primera_vigencia,
	--cnt.FLD_VIGREALDESDE,
	--cnt.FLD_VIGREALHASTA,		
	cnt."fld_ingreso"							AS fec_ingreso_isapre,
	DATEDIFF(month, cnt."fld_ingreso" , per.periodo_fecha)+1 AS antiguedad,
	cnt."fld_cntcateg"							AS codigo_plan,
	cnt."fld_glscateg" 							AS glosa_plan,
	lfc.minimo_ingreso 							AS primer_ingreso_plan,	--fecha ingreso plan
	cnt."fld_comunacod" 						AS comuna_reside,
	cnt."fld_region" 							AS region_reside,
	cnt."fld_valorbase" 	                    AS precio_base,
	cnt."fld_costototal" 						AS pactado_total,
	cnt."fld_concostototal" 					AS pactado_final,
	prm."CLUSTER" 								AS cluster_,
	cnt."fld_clasifica" 						AS clasificacion_riesgo
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio" = ben."fld_funfolio" AND cnt."fld_funcorrel" = ben."fld_funcorrel")
LEFT JOIN periods per ON (periodo BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA)
LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm ON (cnt."fld_cntcateg" = prm.COD_DESACA)
LEFT JOIN minima_vig mv ON (cnt."fld_cotrut"=mv."fld_cotrut" AND cnt."fld_ingreso"=mv."fld_ingreso")
LEFT JOIN (
			SELECT  
				"fld_cotrut" ,
				"fld_cntcateg",
				MIN("fld_periodoprod") AS minimo_ingreso ,
				row_number() over (partition by "fld_cotrut","fld_cntcateg" order by "fld_cotrut" , minimo_ingreso) AS n_veces 
			FROM AFI.P_RDV_DST_LND_SYB.LFC  
			--WHERE "fld_tpotransac" IN ('VN','CA') AND "fld_cotrut" = '015721645-7' ---'010980173-9'  ---'015603914-4'  -- lfc."fld_funfolio" =11420848 AND lfc."fld_funcorrel" =1 AND lfc."fld_cntcateg" =4216 AND lfc."fld_tpotransac" IN ('VN','CA')  ---000000747-1
			GROUP BY 	
				"fld_cotrut" ,
				"fld_cntcateg"
		  ) lfc ON (cnt."fld_cotrut" = lfc."fld_cotrut" AND cnt."fld_cntcateg"=lfc."fld_cntcateg")
WHERE cnt.FLD_VIGREALDESDE <= EST.P_DDV_EST.FECHA_PERIODO($periodo_fin)  AND  cnt.FLD_VIGREALHASTA >= EST.P_DDV_EST.FECHA_PERIODO($periodo_ini)
ORDER BY per.periodo asc, cnt."fld_funfolio" , cnt."fld_funcorrel" ASC , parentesco desc
)
SELECT 
	* 
FROM cartera_vigente 
WHERE periodo =202012-- "fld_cotrut" ='005924110-9'
ORDER BY periodo ASC, contrato,correlativo ASC
--LIMIT 1000
)







SELECT 
	"fld_valorbase" ,*
FROM AFI.P_RDV_DST_LND_SYB.CNT LIMIT 10


SELECT 
	periodo,
	parentesco,
	--COUNT(DISTINCT "fld_cotrut") AS cto_afi,
	COUNT(DISTINCT "fld_benrut") AS cto_bene
FROM EST.P_DDV_EST.CA_BASE_CIA_SEG
GROUP BY periodo,parentesco
ORDER BY periodo
--LIMIT 10



SELECT 
	* 
FROM  EST.P_DDV_EST.CA_BASE_CIA_SEG
WHERE "fld_cotrut" ='001953541-K'
ORDER BY periodo


WITH rev AS (
SELECT DISTINCT 
	"fld_cotrut" AS rut,
	count("fld_cotrut") cto_rut,
	count(DISTINCT "fld_cntcateg") AS cto_categ
FROM EST.P_DDV_EST.CA_BASE_CIA_SEG
--WHERE count(DISTINCT "fld_cntcateg") > 1
GROUP BY "fld_cotrut" 
ORDER BY "fld_cotrut" --ASC--, --periodo ASC
)
SELECT 
	*
FROM rev
WHERE cto_categ >2
LIMIT 10



SELECT  
	--"fld_funfolio",
	--"fld_funcorrel",
	"fld_cotrut" ,
	"fld_cntcateg",
	--"fld_tpotransac",
	--cnt."fld_ingreso" AS ingres,
	--cnt.FLD_VIGREALDESDE AS prime_vig,
	MIN("fld_periodoprod") AS minimo_ingreso ,
	row_number() over (partition by "fld_cotrut","fld_cntcateg" order by "fld_cotrut" , minimo_ingreso) AS n_veces 
FROM AFI.P_RDV_DST_LND_SYB.LFC  
--INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" AND lfc."fld_cntcateg"=cnt."fld_cntcateg")
WHERE "fld_tpotransac" IN ('VN','CA') AND "fld_cotrut" = '015721645-7' ---'010980173-9'  ---'015603914-4'  -- lfc."fld_funfolio" =11420848 AND lfc."fld_funcorrel" =1 AND lfc."fld_cntcateg" =4216 AND lfc."fld_tpotransac" IN ('VN','CA')  ---000000747-1
--ORDER BY lfc."fld_cotrut" , lfc."fld_funfolio" , lfc."fld_funcorrel"
GROUP BY 	
	"fld_cotrut" ,
	"fld_cntcateg"
LIMIT 1000



SELECT 
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_cotrut" ,
	min("fld_periodoprod") AS min_perprod 
FROM AFI.P_RDV_DST_LND_SYB.LFC
GROUP BY "fld_funfolio" , "fld_funcorrel" , "fld_cotrut" 
LIMIT 10


SELECT * FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES 
WHERE COD_DESACA = 4481
LIMIT 10

SELECT * FROM AFI.P_RDV_DST_LND_SYB.BEN  LIMIT 100

SELECT * FROM AFI.P_RDV_DST_LND_SYB.CNT LIMIT 100

SELECT * FROM bon LIMIT 10


WITH ESPECIE AS(
  SELECT 
  		ID_MUESTR,
  		TIPO_USO
  FROM SAYCO_PARCELA
  where ID_MUESTR in (4200008929,14290)
  GROUP BY 
  		ID_MUESTR, 
  		TIPO_USO
)
  SELECT 
  	STRING_AGG(TIPO_USO , ', ') Especie
  FROM ESPECIE
  group by id_muestr

SELECT  FROM 
  
SELECT DISTINCT 
	"fld_cotrut" ,
	count(DISTINCT "fld_funcorrel"),
	CASE 
		WHEN count(DISTINCT "fld_funcorrel") = 1 THEN 999 ELSE "fld_funcorrel" END AS wea
FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_cotrut" ='000000747-1'
GROUP BY "fld_cotrut", "fld_funcorrel" 







WITH identifica AS ( 
SELECT DISTINCT 
		muestra,
		count(DISTINCT tipo_uso) AS cto
FROM tu.tabla
)
, asignar AS ( 
SELECT DISTINCT 
		muestra,
		tipo_uso
FROM tu.tabla
)
, uni_bases AS ( 
SELECT DISTINCT 
		t1.muestra,
		CASE 
			WHEN t1.cto = 1 THEN t2.tipo_uso 
		ELSE 999 END AS wea 
FROM identifica t1
INNER JOIN asignar t2


, union_bases AS ( 
SELECT 
	a."fld_cotrut",
	c.periodo AS mes,
	b."fld_ingreso",
	b."fld_cntcateg",
	b.minima_vigencia ,
	last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig,
	a.FLD_VIGREALDESDE ,
	a.FLD_VIGREALHASTA 
FROM cartera_vigente  a
LEFT JOIN minima_vig b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
INNER JOIN periods  c ON (c.periodo BETWEEN a.FLD_VIGREALDESDE AND a.FLD_VIGREALHASTA)
)
SELECT 
	"fld_cotrut",
	"fld_ingreso",
	"fld_cntcateg",
	minima_vig ,
	dateadd(MONTH,11,minima_vig) doce_meses,
	EST.P_DDV_EST.PERIODO_FECHA(mes) AS mes,
	EST.P_DDV_EST.FECHA_PERIODO("fld_ingreso") AS PERIODO_INGRESO,
	EST.P_DDV_EST.FECHA_PERIODO(minima_vig) AS PERIODO_MIN_VIG,
	EST.P_DDV_EST.FECHA_PERIODO(doce_meses) AS PERIODO_DOCE_MESES,
	mes AS PERIODO_MES,
	DATEDIFF(month, minima_vig, EST.P_DDV_EST.PERIODO_FECHA(mes))+1 AS resta_meses
	--FLD_VIGREALDESDE ,
	--FLD_VIGREALHASTA 
FROM union_bases
--WHERE  "fld_cotrut" = '012073836-4'
ORDER BY "fld_cotrut" , mes
--LIMIT 100
)
------------ TERMINO CODIGO 


SELECT * FROM EST.P_DDV_EST.CA_BASE_CIA_SEG LIMIT 10


