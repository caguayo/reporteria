WITH contrato_aporte AS (
SELECT 	DISTINCT
	ap."fld_funfolio" ,
	ap."fld_funcorrel" ,
	cnt."fld_carganum"
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN_CONTRATO ap
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (ap."fld_funfolio" = cnt."fld_funfolio" AND ap."fld_funcorrel"=cnt."fld_funcorrel")
)
SELECT
	count( DISTINCT "fld_funfolio") AS cantidad_contratos,
	sum("fld_carganum") AS cantidad_cargas
FROM contrato_aporte





SELECT  
	ap."fld_funfolio" ,
	ap."fld_funcorrel" ,
	ap.EDAD_BENEFICIARIO ,
	ap.EXCESOS ,
	ap.EXCEDENTES ,
	ap.IVA_RECUPERADO ,
	ap.
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN_CONTRATO ap 
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (ap."fld_funfolio" = cnt."fld_funfolio" AND ap."fld_funcorrel"=cnt."fld_funcorrel")





------------- POR CONTRATO
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_BENEFI_APORTE_BEN_CONTRATO AS ( 


WITH bene_aporte AS ( 
SELECT DISTINCT 
   ben."fld_funfolio"
   ,ben."fld_funcorrel" 
   --,(datediff(month, ben."fld_bennacfec" , a."pla_fecha_pago") / 12)::int  as edad_beneficiario
   --,ben."fld_bencorrel" AS bencorrel
   ,dateadd('month',0, time_slice(a."pla_fecha_pago", 1, 'month', 'start')) AS pla_fecha_pago
   ,EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago") AS PERIODO_PAGO
   ,uf.VAL_MON AS valor_uf
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_TIPO_EMPLEADOR e ON (a."epa_idn"=e."epa_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_OFICINA_RECAUDADORA f ON (c."ofr_idn"=f."ofr_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_CUENTA g ON (b."cta_autorizacion_idn"=g."cta_idn")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN  ben ON (ben."fld_benrut" = b."lin_pagado_rut" and EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago") between ben.FLD_BENVIGREALDESDE and ben.FLD_BENVIGREALHASTA)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA uf 			ON (last_day(a."pla_fecha_pago")= uf.fec_val)
																WHERE 	   a."pla_fecha_pago" BETWEEN '2021-11-01 00:00:00' AND '2021-11-30 23:59:59'
	 															AND  b."npa_idn" = 10 --- marca de los beneficiarios que aportan --BENEF. CON APORTE
--AND  b."lin_pagado_rut" in ( 	'018188986-1')
--GROUP BY b."lin_pagado_rut",b."lin_acreditado_rut",a."pla_fecha_pago"
ORDER BY ben."fld_funfolio" ,ben."fld_funcorrel"
)
SELECT 
	COUNT(DISTINCT CONCAT("fld_funfolio","fld_funcorrel")) AS cant
	--*
FROM bene_aporte LIMIT 10



, vigente AS ( 
SELECT DISTINCT 
	--"fld_benrut"
	"fld_funfolio",
	"fld_funcorrel"
FROM AFI.P_RDV_DST_LND_SYB.BEN 
WHERE FLD_BENVIGREALDESDE <= 202112 AND FLD_BENVIGREALHASTA >= 202112
)
, uni AS ( 
SELECT DISTINCT 
	  b.*
FROM vigente a 
INNER  JOIN bene_aporte b ON (a."fld_funfolio"=b."fld_funfolio" AND a."fld_funcorrel" = b."fld_funcorrel")
--WHERE b."lin_pagado_rut" IS NOT NULL
)
SELECT
	*
	--COUNT(DISTINCT CONCAT("fld_funfolio","fld_funcorrel")) AS cant
FROM uni LIMIT 10



, siniestralidad AS ( 
SELECT DISTINCT 
	PERIODO ,
	PERIODO_FECHA,
	--RUT_TITULAR ,
	--RUT_BENEFICIARIO ,
	CONTRATO,
	CORRELATIVO,
	sum(DM_HOSPITALARIO) AS hospitalario ,
	sum(DM_AMBULATORIO) AS ambulatorio,
	sum(DM_GES) AS ges ,
	sum(DM_LICENCIA) AS licencia ,
	sum(GASTO_PHARMA) AS pharma ,
	sum(EXCEDENTES) AS excesos,
	sum(EXCEDENTES) AS excedentes,
	sum(RECAUDADO) AS recaudado,
	sum(INGRESOS) AS ingresos ,
	sum(iva_recuperado) AS iva_recuperado,
	sum(recuperacion_gasto) AS recuperacion_gasto
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202101 AND 202112
GROUP BY 
	PERIODO,
	PERIODO_FECHA,
	--RUT_TITULAR,
	--RUT_BENEFICIARIO,
	CONTRATO,
	CORRELATIVO
)
SELECT 
		row_number() over (partition by pla_fecha_pago, CONCAT(a."fld_funfolio",a."fld_funcorrel") ORDER BY CONCAT(a."fld_funfolio",a."fld_funcorrel") ,periodo  asc) REGISTRO_ID
		,a.*
		--b.periodo,
		,CASE 
			WHEN b.periodo IS NULL THEN a.periodo_pago
			ELSE b.periodo
		END AS periodo
		--b.periodo_fecha,
		,CASE
            WHEN  edad_beneficiario BETWEEN 0  AND 17 THEN '< 18 A�os'
            WHEN  edad_beneficiario BETWEEN 18 AND 24 THEN '18 a 24 A�os'
            WHEN  edad_beneficiario BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  edad_beneficiario BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  edad_beneficiario BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  edad_beneficiario BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  edad_beneficiario BETWEEN 45 AND 49 THEN '45 a 49 A�os'          
            WHEN  edad_beneficiario BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  edad_beneficiario BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  edad_beneficiario  > 60 THEN '> 60 y mas A�os'
            ELSE 'sin informaci�n'
       END AS RANGO_EDAD_BENEFICIARIO
	   ,prm.TIPO_PLAN 
	   ,prm.TIPO_PRODUCTO 
	   ,prm.SERIE 
	   ,prm.SEGMENTO AS SEGMENTO_PLAN
	   ,prm.SERIE AS SERIE_PLAN
	   ,prm.SERIE_DOS AS SERIE_PLAN_2
	   ,prm.LINEA_PLAN 
	   ,prm.LINEA_PLAN_2 
	   ,prm.VIG_INICIO AS VIG_INICIO_PLAN
	   ,prm.VIG_FIN  AS VIG_FIN_PLAN
	   ,prm.DETALLE_PRODUCTO
	   ,CASE 
	   		WHEN cnt."fld_region" IN (13) THEN 'METROPOLITANA'
	   		ELSE 'REGIONES'
	   	END AS ubicacion
	   ,IFNULL(excesos,0) AS excesos
       ,IFNULL(excedentes,0) AS excedentes
	   ,IFNULL(iva_recuperado,0) AS iva_recuperado
	   ,IFNULL(recuperacion_gasto,0) AS recuperacion_gasto
	   ,IFNULL(recaudado,0) AS recaudado
	   ,IFNULL(ingresos,0) AS ingresos
	   ,IFNULL(hospitalario,0) AS hospitalario
	   ,IFNULL(ambulatorio,0) AS ambulatorio
	   ,IFNULL(ges,0) AS ges
	   ,IFNULL(licencia,0) AS licencia
	   ,IFNULL(pharma,0) AS pharma
FROM uni a 
LEFT JOIN siniestralidad b ON (a."fld_funfolio"= SUBSTRING(b.CONTRATO,0,9)::int AND a."fld_funcorrel"=b.correlativo)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (a."fld_funfolio"= cnt."fld_funfolio" AND a."fld_funcorrel"= cnt."fld_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION reg        	ON (cnt."fld_region"= reg.COD_REGION)		
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION regv2			ON (CASE 
																	WHEN len(cnt."fld_comunacod") = 5 THEN SUBSTRING(cnt."fld_comunacod",0,2)::int
																	ELSE SUBSTRING(cnt."fld_comunacod",0,1)::int 
																END   = regv2.COD_REGION)
LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm 				ON (cnt."fld_cntcateg"= prm.COD_DESACA)
)
;
CREATE OR REPLACE TABLE  EST.P_DDV_EST.CA_BENEFI_APORTE_BEN_CARTERA AS ( 
SELECT 
	PERIODO,
	COUNT(DISTINCT "lin_pagado_rut") AS cant_beneficiarios,
	count(DISTINCT "lin_acreditado_rut") AS cant_cotizantes
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN
WHERE PERIODO IS NOT NULL 
GROUP BY periodo
)



SELECT 
	*
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN
ORDER BY "lin_pagado_rut"
LIMIT 10

SELECT
	"lin_pagado_rut",
	count("lin_pagado_rut") AS cantidad
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN
GROUP BY "lin_pagado_rut"
ORDER BY cantidad desc
LIMIT 10


SELECT 
	count(DISTINCT "lin_pagado_rut"),
	sum(INGRESOS),
	SUM(HOSPITALARIO),
	sum(AMBULATORIO),
	sum(GES),
	sum(LICENCIA),
	sum(PHARMA),
	sum(HOSPITALARIO+AMBULATORIO+GES+LICENCIA+PHARMA)
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN 
LIMIT 10




SELECT 
	count(DISTINCT  b."lin_pagado_rut") AS cant
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
WHERE a."pla_fecha_pago" BETWEEN '2021-11-01 00:00:00' AND '2021-11-30 23:59:59' AND  b."npa_idn" = 10


SELECT * FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN  LIMIT 10

------ POR BENEFICIARIO
------------------------------------------------
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_BENEFI_APORTE_BEN AS ( 
WITH bene_aporte_orden_2 AS ( 
SELECT DISTINCT
	   --EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago") AS PERIODO ,
	   b."lin_pagado_rut" ,
	   ben."fld_funfolio",
	   ben."fld_funcorrel",
	   (datediff(month, ben."fld_bennacfec", getdate()) / 12)::int  as edad,
	   row_number() over (partition by b."lin_pagado_rut" order BY ben."fld_funfolio", ben."fld_funcorrel" desc ) AS n_veces ---contar
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN  ben ON (ben."fld_benrut" = b."lin_pagado_rut" and  ben.FLD_BENVIGREALDESDE  <= 202111 and ben.FLD_BENVIGREALHASTA >= 202111)
WHERE a."pla_fecha_pago" BETWEEN '2021-11-01 00:00:00' AND '2021-11-30 23:59:59' AND  b."npa_idn" = 10
ORDER BY b."lin_pagado_rut", n_veces
--HAVING n_veces =1
--LIMIT 100
)
, bene_aporte_orden AS ( 
SELECT 
	--count(DISTINCT concat("fld_funfolio","fld_funcorrel")) AS cant
	*
FROM bene_aporte_orden_2
WHERE n_veces = 1
)
--SELECT 
--*
--"fld_funfolio",
--"fld_funcorrel",
--"lin_pagado_rut" ,
--concat("fld_funfolio","fld_funcorrel") AS contratos
--count(DISTINCT "lin_pagado_rut" ) AS cant
--concat("fld_funfolio","fld_funcorrel") AS contratos,
--count(DISTINCT concat("fld_funfolio","fld_funcorrel")) AS cant_2
--FROM bene_aporte_orden
--GROUP BY contratos
--ORDER BY "fld_funfolio" , "fld_funcorrel"
--LIMIT 10
,
--WITH 
bene_ingreso AS ( 
SELECT DISTINCT 
   b."lin_pagado_rut" ,
   count(DISTINCT EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago")) AS cant_periodos,
   SUM(b."lin_cotizacion_pagada") AS COTIZACION_PAGADA
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_TIPO_EMPLEADOR e ON (a."epa_idn"=e."epa_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_OFICINA_RECAUDADORA f ON (c."ofr_idn"=f."ofr_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_CUENTA g ON (b."cta_autorizacion_idn"=g."cta_idn")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN  ben ON (ben."fld_benrut" = b."lin_pagado_rut" and EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago") between ben.FLD_BENVIGREALDESDE and ben.FLD_BENVIGREALHASTA)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA uf 			ON (last_day(a."pla_fecha_pago")= uf.fec_val)
																WHERE 	   a."pla_fecha_pago" BETWEEN '2021-01-01 00:00:00' AND '2021-12-31 23:59:59'
	 															AND  b."npa_idn" = 10 --- marca de los beneficiarios que aportan --BENEF. CON APORTE
--AND  b."lin_pagado_rut" in ( 	'018188986-1')
--GROUP BY b."lin_pagado_rut",b."lin_acreditado_rut",a."pla_fecha_pago"
GROUP BY  b."lin_pagado_rut"
ORDER BY b."lin_pagado_rut" 
)
,
--WITH 
siniestralidad_benef AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--PERIODO_FECHA,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(DM_HOSPITALARIO) AS hospitalario ,
	sum(DM_AMBULATORIO) AS ambulatorio,
	sum(DM_GES) AS ges ,
	sum(DM_LICENCIA) AS licencia ,
	sum(GASTO_PHARMA) AS pharma ,
	sum(EXCEDENTES) AS excesos,
	sum(EXCEDENTES) AS excedentes,
	sum(iva) AS iva,
	--sum(RECAUDADO) AS recaudado,
	--sum(INGRESOS) AS ingresos ,
	sum(iva_recuperado) AS iva_recuperado,
	sum(recuperacion_gasto) AS recuperacion_gasto
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202101 AND 202112
GROUP BY 
	--PERIODO,
	--PERIODO_FECHA,
	--RUT_TITULAR,
	RUT_BENEFICIARIO
) 
--SELECT * FROM siniestralidad_benef LIMIT 10
SELECT DISTINCT 
		--row_number() over (partition by pla_fecha_pago, "lin_pagado_rut" ORDER BY "lin_pagado_rut" asc) REGISTRO_ID
	    a.*
		--b.periodo,
		--,CASE 
			--WHEN b.periodo IS NULL THEN a.periodo_pago
			--ELSE b.periodo
		--END AS periodo
		--b.periodo_fecha,
	    ,b.cant_periodos
		,CASE
            WHEN  edad BETWEEN 0  AND 17 THEN '< 18 A�os'
            WHEN  edad BETWEEN 18 AND 24 THEN '18 a 24 A�os'
            WHEN  edad BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  edad BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  edad BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  edad BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  edad BETWEEN 45 AND 49 THEN '45 a 49 A�os'          
            WHEN  edad BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  edad BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  edad > 60 THEN '> 60 y mas A�os'
            ELSE 'sin informaci�n'
       END AS RANGO_EDAD_BENEFICIARIO
	   ,prm.TIPO_PLAN 
	   ,prm.TIPO_PRODUCTO 
	   ,prm.SERIE 
	   ,prm.SEGMENTO AS SEGMENTO_PLAN
	   ,prm.SERIE AS SERIE_PLAN
	   ,prm.SERIE_DOS AS SERIE_PLAN_2
	   ,prm.LINEA_PLAN 
	   ,prm.LINEA_PLAN_2 
	   ,prm.VIG_INICIO AS VIG_INICIO_PLAN
	   ,prm.VIG_FIN  AS VIG_FIN_PLAN
	   ,prm.DETALLE_PRODUCTO
	   ,CASE 
	   		WHEN cnt."fld_region" IN (13) THEN 'METROPOLITANA'
	   		ELSE 'REGIONES'
	   	END AS ubicacion
	   ,IFNULL(c.excesos,0) AS excesos
       ,IFNULL(c.excedentes,0) AS excedentes
	   ,IFNULL(c.iva_recuperado,0) AS iva_recuperado
	   ,IFNULL(c.recuperacion_gasto,0) AS recuperacion_gasto
	   --,IFNULL(b.recaudado,0) AS recaudado
	   ,IFNULL(b.COTIZACION_PAGADA,0) AS ingresos
	   ,IFNULL(c.hospitalario,0) AS hospitalario
	   ,IFNULL(c.ambulatorio,0) AS ambulatorio
	   ,IFNULL(c.ges,0) AS ges
	   ,IFNULL(c.licencia,0) AS licencia
	   ,IFNULL(c.pharma,0) AS pharma
FROM bene_aporte_orden a 
LEFT JOIN bene_ingreso b ON (a."lin_pagado_rut" = b."lin_pagado_rut" AND a.n_veces=1) 
LEFT JOIN siniestralidad_benef c ON (a."lin_pagado_rut"= c.RUT_BENEFICIARIO)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (a."fld_funfolio"= cnt."fld_funfolio" AND a."fld_funcorrel"= cnt."fld_funcorrel" AND a.n_veces=1)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION reg        	ON (cnt."fld_region"= reg.COD_REGION)		
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION regv2			ON (CASE 
																	WHEN len(cnt."fld_comunacod") = 5 THEN SUBSTRING(cnt."fld_comunacod",0,2)::int
																	ELSE SUBSTRING(cnt."fld_comunacod",0,1)::int 
																END   = regv2.COD_REGION)
LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm 				ON (cnt."fld_cntcateg"= prm.COD_DESACA)
ORDER BY "fld_funfolio"
--LIMIT 100
)





/*
SELECT 
	count(DISTINCT "lin_pagado_rut" ) AS cant,
	count(DISTINCT concat("fld_funfolio","fld_funcorrel")) AS cant_2,
	sum(excesos),
	sum(excedentes),
	sum(ingresos),
	sum(hospitalario),
	sum(ambulatorio),
	sum(ges),
	sum(licencia),
	sum(pharma)
FROM revision LIMIT 10
*/




------ POR CONTRATO
------------------------------------------------
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_BENEFI_APORTE_BEN_CONTRATO AS ( 

WITH bene_aporte_orden AS ( 
SELECT DISTINCT
	   --EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago") AS PERIODO ,
	   b."lin_pagado_rut" ,
	   ben."fld_funfolio",
	   ben."fld_funcorrel",
	   (datediff(month, ben."fld_bennacfec", getdate()) / 12)::int  as edad,
	   row_number() over (partition by b."lin_pagado_rut" order BY ben."fld_funfolio", ben."fld_funcorrel" desc ) AS n_veces ---contar
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN  ben ON (ben."fld_benrut" = b."lin_pagado_rut" and  ben.FLD_BENVIGREALDESDE  <= 202112 and ben.FLD_BENVIGREALHASTA >= 202101)
WHERE a."pla_fecha_pago" BETWEEN '2021-01-01 00:00:00' AND '2021-12-31 23:59:59' AND  b."npa_idn" = 10
ORDER BY b."lin_pagado_rut", n_veces
--HAVING n_veces =1
--LIMIT 100
)
--, bene_aporte_orden_filtro AS ( 
--SELECT 
--	*
--FROM bene_aporte_orden
--WHERE n_veces = 1
--)
, siniestralidad_benef AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--PERIODO_FECHA,
	--RUT_TITULAR ,
	CONTRATO,
	CORRELATIVO ,
	--RUT_BENEFICIARIO ,
	sum(DM_HOSPITALARIO) AS hospitalario ,
	sum(DM_AMBULATORIO) AS ambulatorio,
	sum(DM_GES) AS ges ,
	sum(DM_LICENCIA) AS licencia ,
	sum(GASTO_PHARMA) AS pharma ,
	sum(EXCEDENTES) AS excesos,
	sum(EXCEDENTES) AS excedentes,
	sum(iva) AS iva,
	sum(RECAUDADO) AS recaudado,
	sum(INGRESOS) AS ingresos ,
	sum(iva_recuperado) AS iva_recuperado,
	sum(recuperacion_gasto) AS recuperacion_gasto
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202101 AND 202112
GROUP BY 
	--PERIODO,
	--PERIODO_FECHA,
	--RUT_TITULAR,
	--RUT_BENEFICIARIO,
	CONTRATO,
	CORRELATIVO
)
, union_final AS ( 
SELECT DISTINCT 
		--row_number() over (partition by pla_fecha_pago, "lin_pagado_rut" ORDER BY "lin_pagado_rut" asc) REGISTRO_ID
	    a.*
		--b.periodo,
		--,CASE 
			--WHEN b.periodo IS NULL THEN a.periodo_pago
			--ELSE b.periodo
		--END AS periodo
		--b.periodo_fecha,
		,CASE
            WHEN  edad BETWEEN 0  AND 17 THEN '< 18 A�os'
            WHEN  edad BETWEEN 18 AND 24 THEN '18 a 24 A�os'
            WHEN  edad BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  edad BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  edad BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  edad BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  edad BETWEEN 45 AND 49 THEN '45 a 49 A�os'          
            WHEN  edad BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  edad BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  edad > 60 THEN '> 60 y mas A�os'
            ELSE 'sin informaci�n'
       END AS RANGO_EDAD_BENEFICIARIO
	   ,prm.TIPO_PLAN 
	   ,prm.TIPO_PRODUCTO 
	   ,prm.SERIE 
	   ,prm.SEGMENTO AS SEGMENTO_PLAN
	   ,prm.SERIE AS SERIE_PLAN
	   ,prm.SERIE_DOS AS SERIE_PLAN_2
	   ,prm.LINEA_PLAN 
	   ,prm.LINEA_PLAN_2 
	   ,prm.VIG_INICIO AS VIG_INICIO_PLAN
	   ,prm.VIG_FIN  AS VIG_FIN_PLAN
	   ,prm.DETALLE_PRODUCTO
	   ,CASE 
	   		WHEN cnt."fld_region" IN (13) THEN 'METROPOLITANA'
	   		ELSE 'REGIONES'
	   	END AS ubicacion
	   ,IFNULL(c.excesos,0) AS excesos
       ,IFNULL(c.excedentes,0) AS excedentes
	   ,IFNULL(c.iva_recuperado,0) AS iva_recuperado
	   ,IFNULL(c.recuperacion_gasto,0) AS recuperacion_gasto
	   ,IFNULL(c.recaudado,0) AS recaudado
	   ,IFNULL(c.ingresos,0) AS ingresos
	   ,IFNULL(c.hospitalario,0) AS hospitalario
	   ,IFNULL(c.ambulatorio,0) AS ambulatorio
	   ,IFNULL(c.ges,0) AS ges
	   ,IFNULL(c.licencia,0) AS licencia
	   ,IFNULL(c.pharma,0) AS pharma
FROM bene_aporte_orden a 
LEFT JOIN siniestralidad_benef c ON (a."fld_funfolio"= SUBSTRING(c.CONTRATO,0,9)::int AND a."fld_funcorrel"=c.CORRELATIVO AND a.n_veces = 1)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (a."fld_funfolio"= cnt."fld_funfolio" AND a."fld_funcorrel"= cnt."fld_funcorrel" AND a.n_veces=1)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION reg        	ON (cnt."fld_region"= reg.COD_REGION)		
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION regv2			ON (CASE 
																	WHEN len(cnt."fld_comunacod") = 5 THEN SUBSTRING(cnt."fld_comunacod",0,2)::int
																	ELSE SUBSTRING(cnt."fld_comunacod",0,1)::int 
																END   = regv2.COD_REGION)
LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm 				ON (cnt."fld_cntcateg"= prm.COD_DESACA)
WHERE a.n_veces =1
ORDER BY "fld_funfolio"
)
SELECT 
	count(DISTINCT concat("fld_funfolio","fld_funcorrel")) AS cant_contratos,
	COUNT(DISTINCT "lin_pagado_rut"),
	sum(ingresos),
	sum(HOSPITALARIO),
	SUM(AMBULATORIO),
	SUM(GES),
	SUM(LICENCIA),
	SUM(PHARMA)
FROM union_final 
--LIMIT 100
LIMIT 100

)





--SELECT
--    *
	--count(DISTINCT a."lin_pagado_rut")
--FROM bene_aporte_orden a
--INNER JOIN bene_ingreso b ON (a."lin_pagado_rut" = b."lin_pagado_rut" AND a.n_veces=1) 
--LIMIT 10
, vigente AS ( 
SELECT DISTINCT 
	ben."fld_benrut",
	ben."fld_cotrut",
	ben."fld_benedad",
	ben."fld_bencorrel",
	ben."fld_cargarelcod" ,
	tc.GLS_CODRELAC ,
	ben."fld_cargatipcod" ,
	tc2.GLS_TIPOCA 
	--"fld_funfolio",
	--"fld_funcorrel"
FROM AFI.P_RDV_DST_LND_SYB.BEN ben
INNER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRECODRELAC tc ON (ben."fld_cargarelcod"  = tc.COD_CODRELAC)
INNER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRETIPOCA  tc2 ON(	ben."fld_cargatipcod"  = tc2.COD_TIPOCA) 
WHERE FLD_BENVIGREALDESDE <= 202202 AND FLD_BENVIGREALHASTA >= 202202 AND "fld_bencorrel" NOT IN (0)
)
, uni AS ( 
SELECT DISTINCT 
	  *
FROM bene_aporte a 
LEFT JOIN vigente  b ON (a."lin_pagado_rut"=b."fld_benrut")
--WHERE b."lin_pagado_rut" IS NOT NULL
--LIMIT 10
)
, siniestralidad_benef AS ( 
SELECT DISTINCT 
	--PERIODO ,
	--PERIODO_FECHA,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(DM_HOSPITALARIO) AS hospitalario ,
	sum(DM_AMBULATORIO) AS ambulatorio,
	sum(DM_GES) AS ges ,
	sum(DM_LICENCIA) AS licencia ,
	sum(GASTO_PHARMA) AS pharma ,
	sum(EXCEDENTES) AS excesos,
	sum(EXCEDENTES) AS excedentes,
	sum(iva) AS iva,
	--sum(RECAUDADO) AS recaudado,
	--sum(INGRESOS) AS ingresos ,
	sum(iva_recuperado) AS iva_recuperado,
	sum(recuperacion_gasto) AS recuperacion_gasto
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202101 AND 202112
GROUP BY 
	--PERIODO,
	--PERIODO_FECHA,
	--RUT_TITULAR,
	RUT_BENEFICIARIO
)
SELECT DISTINCT 
		row_number() over (partition by pla_fecha_pago, "lin_pagado_rut" ORDER BY "lin_pagado_rut" asc) REGISTRO_ID
		,a.*
		--b.periodo,
		--,CASE 
			--WHEN b.periodo IS NULL THEN a.periodo_pago
			--ELSE b.periodo
		--END AS periodo
		--b.periodo_fecha,
		,CASE
            WHEN  edad_beneficiario BETWEEN 0  AND 17 THEN '< 18 A�os'
            WHEN  edad_beneficiario BETWEEN 18 AND 24 THEN '18 a 24 A�os'
            WHEN  edad_beneficiario BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  edad_beneficiario BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  edad_beneficiario BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  edad_beneficiario BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  edad_beneficiario BETWEEN 45 AND 49 THEN '45 a 49 A�os'          
            WHEN  edad_beneficiario BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  edad_beneficiario BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  edad_beneficiario  > 60 THEN '> 60 y mas A�os'
            ELSE 'sin informaci�n'
       END AS RANGO_EDAD_BENEFICIARIO
	   ,prm.TIPO_PLAN 
	   ,prm.TIPO_PRODUCTO 
	   ,prm.SERIE 
	   ,prm.SEGMENTO AS SEGMENTO_PLAN
	   ,prm.SERIE AS SERIE_PLAN
	   ,prm.SERIE_DOS AS SERIE_PLAN_2
	   ,prm.LINEA_PLAN 
	   ,prm.LINEA_PLAN_2 
	   ,prm.VIG_INICIO AS VIG_INICIO_PLAN
	   ,prm.VIG_FIN  AS VIG_FIN_PLAN
	   ,prm.DETALLE_PRODUCTO
	   ,CASE 
	   		WHEN cnt."fld_region" IN (13) THEN 'METROPOLITANA'
	   		ELSE 'REGIONES'
	   	END AS ubicacion
	   ,IFNULL(b.excesos,0) AS excesos
       ,IFNULL(b.excedentes,0) AS excedentes
	   ,IFNULL(b.iva_recuperado,0) AS iva_recuperado
	   ,IFNULL(b.recuperacion_gasto,0) AS recuperacion_gasto
	   --,IFNULL(b.recaudado,0) AS recaudado
	   ,IFNULL(a.COTIZACION_PAGADA,0) AS ingresos
	   ,IFNULL(b.hospitalario,0) AS hospitalario
	   ,IFNULL(b.ambulatorio,0) AS ambulatorio
	   ,IFNULL(b.ges,0) AS ges
	   ,IFNULL(b.licencia,0) AS licencia
	   ,IFNULL(b.pharma,0) AS pharma
FROM uni a 
LEFT JOIN siniestralidad_benef b ON (a."lin_pagado_rut"=b.rut_beneficiario)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (a."fld_funfolio"= cnt."fld_funfolio" AND a."fld_funcorrel"= cnt."fld_funcorrel")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION reg        	ON (cnt."fld_region"= reg.COD_REGION)		
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION regv2			ON (CASE 
																	WHEN len(cnt."fld_comunacod") = 5 THEN SUBSTRING(cnt."fld_comunacod",0,2)::int
																	ELSE SUBSTRING(cnt."fld_comunacod",0,1)::int 
																END   = regv2.COD_REGION)
LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm 				ON (cnt."fld_cntcateg"= prm.COD_DESACA)
)



;
CREATE OR REPLACE TABLE  EST.P_DDV_EST.CA_BENEFI_APORTE_BEN_CARTERA AS ( 
SELECT 
	--PERIODO,
	COUNT(DISTINCT "lin_pagado_rut") AS cant_beneficiarios,
	count(DISTINCT "fld_cotrut" ) AS cant_cotizantes
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN
--WHERE PERIODO IS NOT NULL 
--GROUP BY periodo
)
------- FIN PAGOS POR BENEFICIARIO







SELECT 
	COUNT(1),
	COUNT(DISTINCT "fld_benrut") 
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN  
--WHERE "fld_benrut" ='005085652-6'
LIMIT 100


SELECT DISTINCT 
	RUT_BENEFICIARIO ,
	IFF(ORIGEN = 'HOSPITALARIO',sum(BONIFICADO + monto_caec),0 ) AS DM_HOSPITALARIO,
	IFF(ORIGEN = 'AMBULATORIO' ,sum(BONIFICADO + monto_caec),0 ) AS DM_AMBULATORIO,
	IFF(ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES,
	IFF(ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
	SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
	SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE PERIODO BETWEEN 202101 AND 202112 AND RUT_BENEFICIARIO ='005085652-6'
GROUP BY RUT_BENEFICIARIO , ORIGEN  ,TIPO_LICENCIA 



WITH dtm AS (
SELECT DISTINCT 
	RUT_BENEFICIARIO ,
	IFF(ORIGEN = 'HOSPITALARIO',sum(BONIFICADO + monto_caec),0 ) AS DM_HOSPITALARIO,
	IFF(ORIGEN = 'AMBULATORIO' ,sum(BONIFICADO + monto_caec),0 ) AS DM_AMBULATORIO,
	IFF(ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES,
	IFF(ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
	SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
	SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE PERIODO BETWEEN 202101 AND 202112
GROUP BY RUT_BENEFICIARIO , ORIGEN  ,TIPO_LICENCIA 
),
agrupa_dtm AS (
SELECT 
	RUT_BENEFICIARIO ,
	sum(DM_HOSPITALARIO) AS DM_HOSP,
	sum(DM_AMBULATORIO) AS DM_AMBU,
	sum(DM_GES) AS DM_GES,
	SUM(DM_LICENCIA) AS DM_LIC
FROM dtm
GROUP BY RUT_BENEFICIARIO 
)
SELECT 
	ap."fld_benrut" ,
	ap.INGRESOS ,
	ap.LICENCIA AS LIC_AP , 
	IFNULL(dt.DM_LIC,0) AS DM_LIC,
	ap.HOSPITALARIO AS HOSPI_AP,
	IFNULL(dt.DM_HOSP,0) AS DM_HOSP,
	ap.AMBULATORIO AS AMBU_AP,
	IFNULL(dt.DM_AMBU,0) AS DM_AMBU,
	ap.GES AS GES_AP,
	IFNULL(dt.DM_GES,0) AS DM_GES,
	CASE 
		WHEN (LIC_AP+HOSPI_AP+AMBU_AP+GES_AP) = (DM_LIC+DM_HOSP+DM_AMBU+DM_GES) THEN 'OK'
		ELSE 'NO'
	END CHECK_GASTO
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN ap
LEFT JOIN agrupa_dtm dt ON (ap."fld_benrut"=dt.rut_beneficiario)
WHERE "fld_benrut" IS NOT NULL --EDAD_BENEFICIARIO >24
--GROUP BY ap."fld_benrut" 
ORDER BY DM_HOSP DESC 
LIMIT 1000






SELECT 
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	IFF(ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),SUM(COBRADO),0) AS COBRADO_LICENCIA,
	IFF(ORIGEN = 'HOSPITALARIO',sum(BONIFICADO + monto_caec),0 ) AS DM_HOSPITALARIO,
	IFF(ORIGEN = 'AMBULATORIO' ,sum(BONIFICADO + monto_caec),0 ) AS DM_AMBULATORIO,
	IFF(ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE PERIODO BETWEEN 202012 AND 202111 AND RUT_BENEFICIARIO ='020340598-7'
GROUP BY RUT_TITULAR ,RUT_BENEFICIARIO , ORIGEN , TIPO_LICENCIA 
LIMIT 10


SELECT 
	--PERIODO ,
	--PERIODO_FECHA,
	--RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(DM_HOSPITALARIO) AS hospitalario ,
	sum(DM_AMBULATORIO) AS ambulatorio,
	sum(DM_GES) AS ges ,
	sum(DM_LICENCIA) AS licencia ,
	sum(GASTO_PHARMA) AS pharma ,
	sum(EXCEDENTES) AS excesos,
	sum(EXCEDENTES) AS excedentes,
	sum(iva) AS iva,
	--sum(RECAUDADO) AS recaudado,
	--sum(INGRESOS) AS ingresos ,
	sum(iva_recuperado) AS iva_recuperado,
	sum(recuperacion_gasto) AS recuperacion_gasto
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202012 AND 202111 AND RUT_BENEFICIARIO ='020340598-7'
GROUP BY 
	--PERIODO,
	--PERIODO_FECHA,
	--RUT_TITULAR,
	RUT_BENEFICIARIO



SELECT 
	"fld_cotrut" ,
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_benrut" ,
	"fld_bencorrel" ,
	"fld_benedad" ,
	"fld_bennombre" ,
	"fld_benapepa" ,
	"fld_benapema" ,
	"fld_cargarelcod" ,
	tc.GLS_CODRELAC ,
	"fld_cargatipcod",
	tc2.GLS_TIPOCA 
FROM  AFI.P_RDV_DST_LND_SYB.BEN ben
INNER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRECODRELAC tc ON (ben."fld_cargarelcod"  = tc.COD_CODRELAC)
INNER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRETIPOCA  tc2 ON(	ben."fld_cargatipcod"  = tc2.COD_TIPOCA) 
WHERE "fld_cotrut" ='005070564-1' AND FLD_BENVIGREALDESDE <= 202112 AND FLD_BENVIGREALHASTA >= 202112
LIMIT 10



SELECT 
	  "fld_benrut",
	  "fld_benedad",
	  "fld_cargarelcod",
	  GLS_CODRELAC,
	  "fld_cargatipcod",
	  GLS_TIPOCA
FROM uni 
WHERE "lin_pagado_rut" IS NULL AND "fld_benedad" >= 24 
--LIMIT 1000





------ POR BENEFICIARIO
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_BENEFI_APORTE_BEN AS (
WITH bene_aporte AS ( 
SELECT DISTINCT 
	b."lin_pagado_rut" 
   --,ben."fld_bennombre" AS lin_nombres
   --,ben."fld_benapepa" AS lin_apellido_paterno
   --,ben."fld_benapema" AS lin_apellido_materno
   --,'lin_nombres' --= isnull((select Max(BEN.fld_bennombre) from isapre..BEN BEN where BEN.fld_benrut = RCD_LINEA.lin_pagado_rut and RCD_PLANILLA.pla_periodo_remuneracion between BEN.FLD_BENVIGREALDESDE and BEN.FLD_BENVIGREALHASTA),null)
   --,'lin_apellido_paterno' --= isnull((select Max(BEN.fld_benapepa) from isapre..BEN BEN where BEN.fld_benrut = RCD_LINEA.lin_pagado_rut and RCD_PLANILLA.pla_periodo_remuneracion between BEN.FLD_BENVIGREALDESDE and BEN.FLD_BENVIGREALHASTA),null)
   --,'lin_apellido_materno' --= isnull((select Max(BEN.fld_benapema) from isapre..BEN BEN where BEN.fld_benrut = RCD_LINEA.lin_pagado_rut and RCD_PLANILLA.pla_periodo_remuneracion between BEN.FLD_BENVIGREALDESDE and BEN.FLD_BENVIGREALHASTA),null)
   ,b."lin_acreditado_rut"
   --,cnt."fld_funfolio" 
   --,cnt."fld_funcorrel" 
   --,cnt."fld_cotnombre" AS fld_cotnombre --= Isnull((Select max(fld_cotnombre) From isapre..CNT   Where fld_cotrut = RCD_LINEA.lin_acreditado_rut  and RCD_PLANILLA.pla_periodo_remuneracion  between FLD_VIGREALDESDE and FLD_VIGREALHASTA),"") 
   --,cnt."fld_cotapepa" AS fld_cotapepa --= Isnull((Select max(fld_cotapepa) From isapre..CNT     Where fld_cotrut = RCD_LINEA.lin_acreditado_rut  and RCD_PLANILLA.pla_periodo_remuneracion  between FLD_VIGREALDESDE and FLD_VIGREALHASTA),"") 
   --,cnt."fld_cotapema" AS fld_cotapema --= Isnull((Select max(fld_cotapema) From isapre..CNT     Where fld_cotrut = RCD_LINEA.lin_acreditado_rut  and RCD_PLANILLA.pla_periodo_remuneracion  between FLD_VIGREALDESDE and FLD_VIGREALHASTA),"") 
   ,a."pla_pagador_rut"
   ,a."pla_pagador_razon_social"
   --,c."rem_idn"
   --,a."pla_idn"
   --,b."lin_idn"
   --,b."lin_cotizacion_periodo"
   --,a."pla_fecha_pago" --'pla_fecha_pago' --= convert (varchar(10),RCD_PLANILLA.pla_fecha_pago,103)
   ,'GLS_SUCUR'   --= Isnull((Select max(GLS_SUCUR)   From isapre..CNT,isapre..ISAPRESUCURSAL (index IDXISAPRESUCURSAL)  Where fld_cotrut = RCD_LINEA.lin_acreditado_rut  and RCD_PLANILLA.pla_periodo_remuneracion between FLD_VIGREALDESDE and FLD_VIGREALHASTA and fld_succod = COD_SUCUR),'Inexistente' )
   ,e."epa_glosa"
   --,b."lin_renta_imponible"
   --,b."lin_cotizacion_legal"
   --,b."lin_cotizacion_pagada"
   --,g."cta_nombre" --'cuenta_destino' --= isnull(RCD_CUENTA.cta_nombre, "")
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_TIPO_EMPLEADOR e ON (a."epa_idn"=e."epa_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_OFICINA_RECAUDADORA f ON (c."ofr_idn"=f."ofr_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_CUENTA g ON (b."cta_autorizacion_idn"=g."cta_idn")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN  ben ON (ben."fld_benrut" = b."lin_pagado_rut" and a."pla_periodo_remuneracion" between ben.FLD_BENVIGREALDESDE and ben.FLD_BENVIGREALHASTA)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (cnt."fld_cotrut" = b."lin_acreditado_rut"  and a."pla_periodo_remuneracion"  between cnt.FLD_VIGREALDESDE and cnt.FLD_VIGREALHASTA)
WHERE 	   a."pla_fecha_pago" BETWEEN '2021-01-01 00:00:00' AND '2021-12-31 00:00:00' 
	  AND  b."npa_idn" = 10 --- marca de los beneficiarios que aportan
	  --AND  b."lin_pagado_rut" in ( 	'018188986-1')
ORDER BY b."lin_pagado_rut" 
)
, vigentes AS ( 
SELECT 
	"fld_cotrut" ,
	"fld_benrut" ,
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_benedad" ,
	"fld_bensexo" 
FROM AFI.P_RDV_DST_LND_SYB.BEN 
WHERE FLD_BENVIGREALDESDE <= 202101 AND FLD_BENVIGREALHASTA >= 202101 
)
, siniestralidad AS ( 
SELECT
	PERIODO ,
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(DM_HOSPITALARIO) AS hospitalario ,
	sum(DM_AMBULATORIO) AS ambulatorio,
	sum(DM_GES) AS ges ,
	sum(DM_LICENCIA) AS licencia ,
	sum(GASTO_PHARMA) AS pharma ,
	sum(EXCEDENTES) AS excesos,
	sum(EXCEDENTES) AS excedentes,
	sum(RECAUDADO) AS recaudado,
	sum(INGRESOS) AS ingresos 
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202001 AND 202112
GROUP BY 
	PERIODO,
	RUT_TITULAR,
	RUT_BENEFICIARIO
)
SELECT 
	*
FROM bene_aporte a 
LEFT JOIN vigentes b ON (a."lin_pagado_rut"=b."fld_benrut") 
LEFT JOIN siniestralidad c ON (a."lin_pagado_rut"=c.rut_beneficiario)
WHERE b."fld_cotrut" IS NOT NULL
--LIMIT 10
)







SELECT * FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO 
WHERE "periodo_red" 
LIMIT 10




----- REVISION DE UN RUT  006953376-0
WITH siniestralidad_agrupada AS (
SELECT
	PERIODO ,
	PERIODO_FECHA 
	RUT_TITULAR ,
	RUT_BENEFICIARIO ,
	sum(DM_HOSPITALARIO) AS hospitalario ,
	sum(DM_AMBULATORIO) AS ambulatorio,
	sum(DM_GES) AS ges ,
	sum(DM_LICENCIA) AS licencia ,
	sum(GASTO_PHARMA) AS pharma ,
	sum(RECAUDADO) AS recaudado,
	sum(INGRESOS) AS ingresos 
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
WHERE PERIODO BETWEEN 202001 AND 202111 AND RUT_BENEFICIARIO ='006953376-0'
GROUP BY 
	PERIODO,
	RUT_TITULAR,
	RUT_BENEFICIARIO 
)




------------- REVISION CANTIDAD DE REGISTROS
WITH bene_aportes AS ( 
SELECT DISTINCT 
	b."lin_pagado_rut" 
   --,ben."fld_bennombre" AS lin_nombres
   --,ben."fld_benapepa" AS lin_apellido_paterno
   --,ben."fld_benapema" AS lin_apellido_materno
   --,'lin_nombres' --= isnull((select Max(BEN.fld_bennombre) from isapre..BEN BEN where BEN.fld_benrut = RCD_LINEA.lin_pagado_rut and RCD_PLANILLA.pla_periodo_remuneracion between BEN.FLD_BENVIGREALDESDE and BEN.FLD_BENVIGREALHASTA),null)
   --,'lin_apellido_paterno' --= isnull((select Max(BEN.fld_benapepa) from isapre..BEN BEN where BEN.fld_benrut = RCD_LINEA.lin_pagado_rut and RCD_PLANILLA.pla_periodo_remuneracion between BEN.FLD_BENVIGREALDESDE and BEN.FLD_BENVIGREALHASTA),null)
   --,'lin_apellido_materno' --= isnull((select Max(BEN.fld_benapema) from isapre..BEN BEN where BEN.fld_benrut = RCD_LINEA.lin_pagado_rut and RCD_PLANILLA.pla_periodo_remuneracion between BEN.FLD_BENVIGREALDESDE and BEN.FLD_BENVIGREALHASTA),null)
   ,b."lin_acreditado_rut"
   --,cnt."fld_funfolio" 
   --,cnt."fld_funcorrel" 
   --,cnt."fld_cotnombre" AS fld_cotnombre --= Isnull((Select max(fld_cotnombre) From isapre..CNT   Where fld_cotrut = RCD_LINEA.lin_acreditado_rut  and RCD_PLANILLA.pla_periodo_remuneracion  between FLD_VIGREALDESDE and FLD_VIGREALHASTA),"") 
   --,cnt."fld_cotapepa" AS fld_cotapepa --= Isnull((Select max(fld_cotapepa) From isapre..CNT     Where fld_cotrut = RCD_LINEA.lin_acreditado_rut  and RCD_PLANILLA.pla_periodo_remuneracion  between FLD_VIGREALDESDE and FLD_VIGREALHASTA),"") 
   --,cnt."fld_cotapema" AS fld_cotapema --= Isnull((Select max(fld_cotapema) From isapre..CNT     Where fld_cotrut = RCD_LINEA.lin_acreditado_rut  and RCD_PLANILLA.pla_periodo_remuneracion  between FLD_VIGREALDESDE and FLD_VIGREALHASTA),"") 
   --,a."pla_pagador_rut"
   --,a."pla_pagador_razon_social"
   --,c."rem_idn"
   --,a."pla_idn"
   --,b."lin_idn"
   --,b."lin_cotizacion_periodo"
   ,EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago") AS PERIODO_PAGO --'pla_fecha_pago' --= convert (varchar(10),RCD_PLANILLA.pla_fecha_pago,103)
   --,'GLS_SUCUR'   --= Isnull((Select max(GLS_SUCUR)   From isapre..CNT,isapre..ISAPRESUCURSAL (index IDXISAPRESUCURSAL)  Where fld_cotrut = RCD_LINEA.lin_acreditado_rut  and RCD_PLANILLA.pla_periodo_remuneracion between FLD_VIGREALDESDE and FLD_VIGREALHASTA and fld_succod = COD_SUCUR),'Inexistente' )
   --,e."epa_glosa"
   --,b."lin_renta_imponible"
   --,b."lin_cotizacion_legal"
   --,b."lin_cotizacion_pagada"
   --,g."cta_nombre" --'cuenta_destino' --= isnull(RCD_CUENTA.cta_nombre, "")
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_TIPO_EMPLEADOR e ON (a."epa_idn"=e."epa_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_OFICINA_RECAUDADORA f ON (c."ofr_idn"=f."ofr_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_CUENTA g ON (b."cta_autorizacion_idn"=g."cta_idn")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN  ben ON (ben."fld_benrut" = b."lin_pagado_rut" and a."pla_periodo_remuneracion" between ben.FLD_BENVIGREALDESDE and ben.FLD_BENVIGREALHASTA)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (cnt."fld_cotrut" = b."lin_acreditado_rut"  and a."pla_periodo_remuneracion"  between cnt.FLD_VIGREALDESDE and cnt.FLD_VIGREALHASTA)
WHERE 	   a."pla_fecha_pago" BETWEEN '2021-01-01 00:00:00' AND '2021-12-31 00:00:00' 
	  AND  b."npa_idn" = 10 --- marca de los beneficiarios que aportan
	  --AND  b."lin_pagado_rut" in ( 	'018188986-1')
ORDER BY b."lin_pagado_rut" 
)
, vigente AS ( 
SELECT DISTINCT 
	"fld_cotrut" ,
	"fld_benrut" 
	--"fld_funfolio" ,
	--"fld_funcorrel" ,
	--"fld_benedad" ,
	--"fld_bensexo" 
FROM AFI.P_RDV_DST_LND_SYB.BEN 
WHERE FLD_BENVIGREALDESDE <= 202112 AND FLD_BENVIGREALHASTA >= 202101 
)
, uni AS ( 
SELECT DISTINCT 
	  b.*
	 ,a.*
	--count(DISTINCT "lin_pagado_rut")
FROM vigente a 
LEFT JOIN bene_aportes b ON (a."fld_benrut"=b."lin_pagado_rut")
WHERE b."lin_pagado_rut" IS NOT NULL
)
, agrupar AS ( 
SELECT
	"lin_pagado_rut",
	count(periodo_pago) AS cant_meses
FROM uni
GROUP BY "lin_pagado_rut"
ORDER BY cant_meses DESC 
--LIMIT 100
)
SELECT 
	cant_meses,
	count(DISTINCT "lin_pagado_rut") AS cant_ben
FROM agrupar 
GROUP BY cant_meses
ORDER BY cant_meses desc
LIMIT 20







-------- cuanta gente en colmena son carga de un titular y pague la cotizacion. CUANTOS CASOS


SELECT "fld_emprut" , count("fld_emprut") AS cant FROM ISA.P_RDV_DST_LND_SYB.EMP
GROUP BY "fld_emprut"
ORDER BY cant desc
LIMIT 100


SELECT * FROM AFI.P_RDV_DST_LND_SYB.COB
LIMIT 10


---- isa.P_RDV_DST_LND_SYB.ISAPRETIPPAG glosas de pagos



SELECT 
	"fld_compensacion" ,*
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE "fld_cotrut" ='009868252-K'
LIMIT 10


SELECT 
	*
FROM AFI.P_RDV_DST_LND_SYB.BEN
WHERE "fld_cotrut" ='009868252-K'
LIMIT 10



SELECT * FROM  RCD.P_DDV_RCD.P_DDV_RCD_V_PAG 
WHERE "pag_cotrut" ='000946063-2' AND "pag_percot" ='2020/07'
LIMIT 10






SELECT DISTINCT "fld_periodoprod" FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE "fld_periodoprod" >='2020-01-01 00:00:00'
ORDER BY "fld_periodoprod" 
--LIMIT 10


SELECT 
	*
FROM isa.P_RDV_DST_LND_SYB.FUN 
WHERE  "fld_cotrut"='012037705-1' --"fld_funfolio" =10548926 AND "fld_funcorrel" = 12
ORDER BY "fld_periodoprod" 
--LIMIT 10

SELECT * FROM  AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_cotrut"='013435395-3' --"fld_funfolio" =10548926 AND "fld_funcorrel" =12
--LIMIT 10
ORDER BY "fld_periodoprod" 


SELECT 	periodo,
		count(DISTINCT rut_cot) AS cantidad 
FROM EST.P_DDV_EST.ca_pagos_empleadores
WHERE chek_pag_emp ='NO' AND compensa NOT IN (0)	--rut_cot = '010160188-9'
 		--AND periodo=202108
GROUP BY periodo
ORDER BY periodo
LIMIT 1000

SELECT * FROM EST.P_DDV_EST.ca_pagos_empleadores
WHERE 	rut_cot = '007294357-0'	
		AND 
		periodo=202108  
LIMIT 1000


--------------- ordena los empleadores de los cotizantes
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.temp_emp AS ( 
SELECT DISTINCT 
	"fld_cotrut" , --as rut_cotizante
	"fld_emprut", --AS rut_empleador	
	"fld_funfolio", --AS  contrato,
	"fld_funcorrel" --AS correlativo,	
FROM AFI.P_RDV_DST_LND_SYB.LFC
order by "fld_cotrut" 
)
;
----- replicando empleadores
CREATE OR REPLACE TEMPORARY TABLE est.P_DDV_EST.ca_emp AS (
WITH empleadores AS (
SELECT
	"fld_cotrut",
	"fld_emprut",
	"fld_funfolio",
	"fld_funcorrel",
	ROW_NUMBER () OVER (PARTITION BY "fld_funfolio","fld_funcorrel" ORDER BY "fld_funfolio","fld_funcorrel") AS n_veces
FROM EST.P_DDV_EST.temp_emp)
SELECT fld_cotrut,fld_funfolio,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4
FROM empleadores 
pivot(max("fld_emprut") FOR n_veces IN (1,2,3,4))
AS P (fld_cotrut,fld_funfolio,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4))
;
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.ca_pagos_empleadores AS ( 
WITH pagos AS ( 
SELECT 
	"pag_pagper",
	"pag_tipdoc" ,
	"pag_funfol", 
	"pag_funcor",
	"pag_cotrut" ,
	"pag_rutemp" ,
	"pag_rutpag" ,
	"pag_pagval" 
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_PAG
WHERE 	--"pag_cotrut" ='013767357-6' AND 
		--"pag_percot" ='2021/08' AND
		"pag_tipdoc" =1	AND 
		"pag_pagper" >= '2020-01-01 00:00:00'
--LIMIT 10
)
,empleador AS ( 
SELECT DISTINCT 
	EST.P_DDV_EST.FECHA_PERIODO(a."pag_pagper") AS periodo,
	a."pag_funfol" folio ,
	a."pag_funcor" correl,
	f."fld_compensacion" AS compensa ,
	f."fld_carganum" AS cargas,
	a."pag_cotrut" AS rut_cot,
	a."pag_rutemp" AS rut_emple,
	b."fld_emprazsoc" AS nombre_emple,
	a."pag_rutpag" AS rut_emp_paga,
	c."fld_emprazsoc" AS nombe_paga,
	CASE 
		WHEN a."pag_rutpag" = rut_emple 
		THEN 'OK'
		ELSE 'NO'
	END AS chek_pag_emp,
	e.empleador_1,
	e.empleador_2,
	e.empleador_3,
	e.empleador_4,
	CASE 
		WHEN a."pag_rutpag" = e.empleador_1 OR 
		 	 a."pag_rutpag" = e.empleador_2 OR 
		 	 a."pag_rutpag" = e.empleador_3 OR  
		 	 a."pag_rutpag" = e.empleador_4 
		THEN 'OK'
		ELSE 'NO'
	END AS chek_tab_emp,
	--c."fld_cotrut" AS rut_cot,
	a."pag_tipdoc",
	--b."fld_bencorrel",
	a."pag_pagval"
FROM pagos  a
--LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN b ON (a."pag_funfol" = b."fld_funfolio" AND a."pag_funcor"= b."fld_funcorrel" AND a."pag_cotrut" = b."fld_benrut" )-- AND 202110 BETWEEN b.FLD_BENVIGREALDESDE AND b.FLD_BENVIGREALHASTA)
--LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN c ON (a."pag_funfol" = c."fld_funfolio" AND a."pag_funcor"= c."fld_funcorrel" )-- AND 202110 BETWEEN c.FLD_BENVIGREALDESDE AND c.FLD_BENVIGREALHASTA)
LEFT JOIN EST.P_DDV_EST.CA_EMP e 	  ON (a."pag_funfol" = e.fld_funfolio AND a."pag_funcor"= e.fld_funcorrel)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.EMP b ON (a."pag_rutemp" = b."fld_emprut")
LEFT JOIN ISA.P_RDV_DST_LND_SYB.EMP c ON (a."pag_rutpag" = c."fld_emprut")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT f ON (a."pag_funfol" = f."fld_funfolio" AND a."pag_funcor"= f."fld_funcorrel")
--WHERE b."fld_bencorrel" IS NULL
)
----- contador de casos
SELECT 
	* 
FROM empleador
WHERE (chek_pag_emp IN ('NO') OR chek_tab_emp IN ('NO')) --AND rut_cotizante ='000946063-2'
ORDER BY rut_cot, periodo
)



SELECT 
	*  
FROM EST.P_DDV_EST.ca_pagos_empleadores
WHERE rut_cot = '018515994-9'
LIMIT 10


SELECT 
	periodo,
	chek_pag_emp,
	count(DISTINCT rut_cot) AS cant
FROM EST.P_DDV_EST.ca_pagos_empleadores
WHERE chek_pag_emp IN ('NO') AND compensa IN (0)
GROUP BY 	periodo,
			chek_pag_emp
ORDER BY periodo
LIMIT 10








	b.periodo,
	excesos,
	excedentes,
	recaudado,
	ingresos,
	hospitalario,
	ambulatorio,
	ges,
	licencia,
	pharma


SELECT
	*
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN
WHERE "lin_acreditado_rut" in ('006444055-1')
ORDER BY pla_fecha_pago , registro_id , periodo
LIMIT 1000





----------------- extracion real
WITH bene_aportes AS ( 
SELECT DISTINCT 
	b."lin_pagado_rut" 
   ,EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago") AS PERIODO_PAGO--'pla_fecha_pago' --= convert (varchar(10),RCD_PLANILLA.pla_fecha_pago,103)
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_TIPO_EMPLEADOR e ON (a."epa_idn"=e."epa_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_OFICINA_RECAUDADORA f ON (c."ofr_idn"=f."ofr_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_CUENTA g ON (b."cta_autorizacion_idn"=g."cta_idn")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN  ben ON (ben."fld_benrut" = b."lin_pagado_rut" and a."pla_periodo_remuneracion" between ben.FLD_BENVIGREALDESDE and ben.FLD_BENVIGREALHASTA)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (cnt."fld_cotrut" = b."lin_acreditado_rut"  and a."pla_periodo_remuneracion"  between cnt.FLD_VIGREALDESDE and cnt.FLD_VIGREALHASTA)
WHERE 	   a."pla_fecha_pago" BETWEEN '2021-11-01 00:00:00' AND '2021-11-30 00:00:00' 
	  AND  b."npa_idn" = 10 --- marca de los beneficiarios que aportan
ORDER BY b."lin_pagado_rut" 
)
, vigente AS ( 
SELECT DISTINCT 
	"fld_benrut" 
FROM AFI.P_RDV_DST_LND_SYB.BEN 
WHERE FLD_BENVIGREALDESDE <= 202111 AND FLD_BENVIGREALHASTA >= 202111
)
, uni AS ( 
SELECT DISTINCT 
	  b.*
FROM vigente a 
INNER  JOIN bene_aportes b ON (a."fld_benrut"=b."lin_pagado_rut")
--WHERE b."lin_pagado_rut" IS NOT NULL
)
, pagos_movil AS ( 
SELECT DISTINCT 
	b."lin_pagado_rut" AS ben_movil
   ,EST.P_DDV_EST.FECHA_PERIODO(a."pla_fecha_pago") AS PERIODO_PAGO_MOVIL--'pla_fecha_pago' --= convert (varchar(10),RCD_PLANILLA.pla_fecha_pago,103)
FROM RCD.P_RDV_DST_LND_SYB.RCD_PLANILLA a
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_LINEA b ON (a."pla_idn"=b."pla_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_REMESA c ON (a."rem_idn" = c."rem_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_TIPO_EMPLEADOR e ON (a."epa_idn"=e."epa_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_OFICINA_RECAUDADORA f ON (c."ofr_idn"=f."ofr_idn")
LEFT JOIN RCD.P_RDV_DST_LND_SYB.RCD_CUENTA g ON (b."cta_autorizacion_idn"=g."cta_idn")
LEFT JOIN AFI.P_RDV_DST_LND_SYB.BEN  ben ON (ben."fld_benrut" = b."lin_pagado_rut" and a."pla_periodo_remuneracion" between ben.FLD_BENVIGREALDESDE and ben.FLD_BENVIGREALHASTA)
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (cnt."fld_cotrut" = b."lin_acreditado_rut"  and a."pla_periodo_remuneracion"  between cnt.FLD_VIGREALDESDE and cnt.FLD_VIGREALHASTA)
WHERE 	   a."pla_fecha_pago" BETWEEN '2020-12-01 00:00:00' AND '2021-11-30 00:00:00' 
	  AND  b."npa_idn" = 10 --- marca de los beneficiarios que aportan
	  --AND  b."lin_pagado_rut" in ( 	'018188986-1')
ORDER BY b."lin_pagado_rut" 
)
, contador AS ( 
SELECT
	"lin_pagado_rut",
	count(DISTINCT periodo_pago_movil) AS meses
FROM uni a 
INNER  JOIN pagos_movil b ON (a."lin_pagado_rut"=b.ben_movil)
GROUP BY "lin_pagado_rut"
)
SELECT 
	meses,
	count(DISTINCT "lin_pagado_rut") AS cant_ben
FROM contador
GROUP BY meses
ORDER BY meses
LIMIT 12
---------------- sacar la informacion en base a los pagos de noviembre 2021




SELECT 
	PERIODO_VIGENCIA ,
	count(DISTINCT RUT_TITULAR) AS titulares,
	sum(CNT_CARFAM) AS cargas,
	SUM(PAGADO_TOTAL_PESOS),
	sum(PAGADO_TOTAL_UF),
	SUM(COSTO_TOTAL_PESOS) ,
	sum(COSTO_TOTAL_UF)
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO_VIGENCIA >=202101
GROUP BY PERIODO_VIGENCIA 
ORDER BY PERIODO_VIGENCIA 
LIMIT 20




------- EXTRACCION POR BENEFICIARIO
------------------------------------------------------
SELECT 
	* 
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN
WHERE "fld_benrut" = '018495677-2'
---LIMIT 10









SELECT count (1) FROM AFI.P_RDV_DST_LND_SYB.BEN
WHERE FLD_BENVIGREALDESDE <= 202112 AND FLD_BENVIGREALHASTA >= 202112 AND "fld_bencorrel" NOT IN (0) AND "fld_benedad" >= 24 
LIMIT 10

--- tipo parentesco
--- tipo carga
--- tabla con las glosas


SELECT * FROM ISA.P_RDV_DST_LND_SYB.ISAPRETIPOCA                       LIMIT 10

SELECT * FROM ISA.P_RDV_DST_LND_SYB.ISAPRECODRELAC LIMIT 10 




WITH contrato_aporte AS ( 
SELECT 	DISTINCT
	ap."fld_funfolio" ,
	ap."fld_funcorrel" ,
	cnt."fld_carganum" 
FROM EST.P_DDV_EST.CA_BENEFI_APORTE_BEN_CONTRATO ap
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (ap."fld_funfolio" = cnt."fld_funfolio" AND ap."fld_funcorrel"=cnt."fld_funcorrel")
)
SELECT 
	count(1) AS cantidad_contratos,
	sum("fld_carganum") AS cantidad_cargas
FROM contrato_aporte 






LIMIT 100


---
, agrupar AS ( 
SELECT
	"lin_pagado_rut",
	count(periodo_pago) AS cant_meses
FROM bene_aportes
GROUP BY "lin_pagado_rut"
ORDER BY cant_meses DESC 
--LIMIT 100
)











SELECT count(DISTINCT "lin_pagado_rut") 
FROM  uni a 
INNER  JOIN pagos_movil b ON (a."lin_pagado_rut"=b.ben_movil)



SELECT
	periodo_pago,
	count(DISTINCT "lin_pagado_rut") AS cant_ben
FROM uni
GROUP BY periodo_pago
ORDER BY periodo_pago
LIMIT 12








SELECT 
	periodo_pago,
	count(DISTINCT "lin_pagado_rut") AS cant
FROM bene_aportes
GROUP BY periodo_pago
ORDER BY periodo_pago



, agrupar AS ( 
SELECT
	"lin_pagado_rut",
	count(periodo_pago) AS cant_meses
FROM bene_aportes
GROUP BY "lin_pagado_rut"
ORDER BY cant_meses DESC 
--LIMIT 100
)
SELECT 
	cant_meses,
	count(DISTINCT "lin_pagado_rut") AS cant_ben
FROM agrupar 
GROUP BY cant_meses
ORDER BY cant_meses desc
LIMIT 20








SELECT * FROM bene_aportes
WHERE "lin_pagado_rut" ='018934067-2'
ORDER BY periodo_pago






SELECT *  FROM EST.P_STG_EST.CA_SINIESTRALIDAD LIMIT 10


From RCD_PLANILLA,RCD_LINEA (index IND_LIN_PLA),RCD_REMESA,RCD_TIPO_EMPLEADOR,RCD_OFICINA_RECAUDADORA,RCD_CUENTA
Where RCD_PLANILLA.pla_fecha_pago between @x_fecha_ini And @x_fecha_fin
    --and RCD_PLANILLA.pla_idn = RCD_LINEA.pla_idn
    --and RCD_LINEA.npa_idn    = 10 --BENEF. CON APORTE
    --and RCD_PLANILLA.rem_idn = RCD_REMESA.rem_idn
    --and RCD_PLANILLA.epa_idn = RCD_TIPO_EMPLEADOR.epa_idn
    --and RCD_REMESA.ofr_idn = RCD_OFICINA_RECAUDADORA.ofr_idn
    --and RCD_LINEA.cta_autorizacion_idn *= RCD_CUENTA.cta_idn
    /*and RCD_LINEA.lin_pagado_rut in (
    '011997366-K',
    '019809182-0',
    '015215257-4',
    '023998634-K',
    '020327921-3',
    '018524051-7',
    '016526547-5',
    '019323381-3')*/
    
    
    

