SELECT * FROM ISA.P_DDV_ISA.P_DDV_ISA_PRM_AGRUPA_PLANES LIMIT 1000

SELECT MIN(COD_DESACA), max(COD_DESACA) FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES LIMIT 100


DROP TABLE EST.P_DDV_EST.PRM_AGRUPA_PLANES 


SELECT * FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES --WHERE COD_DESACA = 9153 
ORDER BY COD_DESACA DESC 
LIMIT 1000

SELECT * FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX WHERE COD_DESACA = 9153 LIMIT 10


SELECT count(1) FROM ISA.P_DDV_ISA.P_DDV_ISA_PRM_AGRUPA_PLANES LIMIT 10


SELECT count(1) FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES 


SELECT * FROM ISA.P_DDV_ISA.P_DDV_ISA_PRM_AGRUPA_PLANES 
ORDER BY "cod_categoria" DESC 
LIMIT 100


SELECT * FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES
ORDER BY COD_DESACA DESC 
LIMIT 100



CREATE OR REPLACE TABLE EST.P_DDV_EST.PRM_AGRUPA_PLANES AS (
SELECT  
	isa.COD_DESACA ,
	isa.VAL_CATEG ,
	isa.COD_TFR ,
	isa.GLS_DESLARPDF ,
	isa.VIG_INICIO ,
	isa.VIG_FIN ,
	isa.GLS_DESACA ,
	isa.PLANES_EN_VENTA ,
	IFNULL(case
	             when sca."grupo" in (1,5,14) then 'HOMBRES'
	             when sca."grupo" = 11 and isa.COD_DESACA in (1461,1466,4658,4661,4666) then 'HOMBRES' 
	             when sca."grupo" in (2,6) then 'MUJERES'
	             when sca."grupo" = 11 and isa.COD_DESACA in (4659,4662,5502,5503) then 'MUJERES'
	             when sca."grupo" in (4,8,15) and sca."familia" <> 75 then 'FAMILIAR COB. REDUCIDA'
	             when sca."grupo" in (4,8) and sca."familia" = 75  then 'FAMILIAR COB. REDUCIDA'
	             when sca."grupo" in (3,7) and sca."familia" = 75  then 'FAMILIAR CON PARTO'
	             when sca."grupo" = 11 and isa.COD_DESACA in (1465,4660,4663,4667,5504,5505,5506,5507) then 'FAMILIAR COB. REDUCIDA'
	             else  'FAMILIAR CON PARTO'                           
	--            when b.grupo not in (1,5,2,6,4,8,14,15) then  "FAMILIAR CON PARTO"                           
            end,'FAMILIAR CON PARTO') AS SERIE,
	IFNULL(case
            when gp."codigo"  in (1,2,3,4,13,14,15) then 'INDIVIDUAL'
            else 'COLECTIVO'
          end,'SIN CLASIFICACION'
          ) AS TIPO_PLAN, 
	isa.COD_TFR AS TRF ,
    IFNULL(case 
               when isa.FLD_PREF = 0 then 'LIBRE ELECCION'
               --else GLS_PREF
               ELSE 'PREFERENTE'
               end,'NO ENCONTRADO'
            ) AS TIPO_PRODUCTO,
	--'' AS VENTA_EN , ------ DATO MANUAL
	CASE 
		WHEN  fp."venta_en" = 'S' THEN 'SANTIAGO'
		WHEN  fp."venta_en" ='R' THEN 'REGIONES'
		ELSE 'SANTIAGO'
	END AS VENTAPLAN_EN,   --- NULL SANTIAGO 
	--'' AS DETALLE_PRODUCTO ,  ----- SOLO LINEA PREFERENTE (DE DONDE EXTRAR)  -ISAPREPREF GLOSA_PREF
    CASE 
    	WHEN ipr.GLS_PREF='NO PREFERENTE' THEN 'L.E.' 
    	ELSE ipr.GLS_PREF 
    END AS DETALLE_PRODUCTO ,
	CASE 
		WHEN fp."es_family" ='N' THEN 'NO'
		WHEN fp."es_family" ='S' THEN 'SI'
		ELSE ''
	END AS TIPO_FAMILY ,  ---- 
    --'' AS TIPO_FAMILY , ------ GRUPLAN MARCA NO-SI 
    CASE 
    	WHEN TIPO_PLAN = 'INDIVIDUAL' THEN 'IN'
    	WHEN TIPO_PLAN = 'COLECTIVO' AND isa.GLS_DESACA LIKE'BU%' THEN 'BU'
    	WHEN TIPO_PLAN = 'COLECTIVO' AND isa.GLS_DESACA LIKE'AC%' THEN 'AC' 
    	WHEN TIPO_PLAN = 'COLECTIVO' AND isa.GLS_DESACA LIKE'AD%' THEN 'AD'
    	ELSE 'CO'
    END AS TIPO_COLECTIVO ,    	
	--'' AS TIPO_COLECTIVO , ----------- CONFIRMAR EXTRACCION (MANUAL O CASE) 
    aux.LINEA_PLAN ,
    --'' AS LINEA_PLAN , ------- DATO MANUAL
    aux.LINEA_PLAN_2 ,
    --'' AS LINEA_PLAN_2 , -------- DATO MANUAL
    isa.GLS_DESLAR ,
    aux.COD_TARIFICACION ,
    --'' AS COD_TARIFICACION ,  ----- DATO MANUAL
    aux.GLS_TARIFICACION ,
    --'' AS GLS_TARIFICACION ,  ----- DATO MANUAL     
	IFNULL(case
             when sca."grupo" in (1,5,14) then 'SIN MATERNIDAD'
             when sca."grupo" = 11 and isa.COD_DESACA in (1461,1466,4658,4661,4666) then 'SIN MATERNIDAD' 
             when sca."grupo" in (2,6) then 'SIN MATERNIDAD'
             when sca."grupo" = 11 and isa.COD_DESACA in (4659,4662,5502,5503) then 'SIN MATERNIDAD'
             when sca."grupo" in (4,8,15) and sca."familia" <> 75 then 'SIN MATERNIDAD'
             when sca."grupo" in (4,8) and sca."familia" = 75  then 'SIN MATERNIDAD'
             when sca."grupo" in (3,7) and sca."familia" = 75  then 'CON MATERNIDAD'
             when sca."grupo" = 11 and isa.COD_DESACA in (1465,4660,4663,4667,5504,5505,5506,5507) then 'SIN MATERNIDAD'
             else  'CON MATERNIDAD'                           
--             when b.grupo not in (1,5,2,6,4,8,14,15) then  "FAMILIAR CON PARTO"                           
           end,'CON MATERNIDAD'
          ) AS SERIE_DOS ,	    
    --isa.PLANES_EN_VENTA ,
    isa.FLD_PREF AS COD_PREFERENTE,
    aux.GRUPO_FAMILIA ,
	--'' AS GRUPO_FAMILIA, --- DATO MANUAL PODRIA SER GRUPLAN
    aux.SEGMENTO ,
    --'' AS SEGMENTO , ------ CLUSTER (NO LO ENCUENTRO) MANUAL
    cl."CLUSTER" ,
    aux.CLUSTER_ID ,
    --'' AS CLUSTER_ID, ------- CLUSTER (NO LO ENCUENTRO) MANTENER VACIO
    GREATEST (cl.TIPO_1_GRUPO_2, cl.TIPO_1_GRUPO_3, cl.TIPO_1_GRUPO_4, cl.TIPO_1_GRUPO_5, cl.TIPO_1_GRUPO_6) AS MAX_COB_PF_P1,
    --'' AS MAX_COB_PF_P1 , ----- CLUSTER (NO LO ENCUENTRO)  
  	GREATEST (cl.TIPO_7_GRUPO_2 , cl.TIPO_7_GRUPO_3, cl.TIPO_7_GRUPO_4, cl.TIPO_7_GRUPO_5, cl.TIPO_7_GRUPO_6) AS MAX_COB_PF_P7  ------CLUSTER (NO LO ENCUENTRO)
FROM AFI.P_RDV_DST_LND_SYB.ISAPREDESACA isa
INNER JOIN ISA.P_RDV_DST_LND_SYB.SERIES_CATEG sca ON (isa.COD_DESACA = sca."codigo")
--INNER JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm ON (isa.COD_DESACA = prm.COD_DESACA)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.GRUPLAN       gp  ON (sca."grupo" = gp."codigo")   ----  b.grupo *= d.codigo
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREPREF    ipr ON (isa.FLD_PREF = ipr.COD_PREF)
LEFT JOIN TARI.P_DDV_TARI.JKAUX_AG_K5         cl  ON (isa.COD_DESACA = cl.COD_CATEGORIA)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.FAMPLAN 	  fp  ON (sca."familia" = fp."codigo")
LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX aux ON (isa.COD_DESACA = aux.COD_DESACA)
WHERE isa.COD_DESACA NOT IN (9990,9991)
--LIMIT 2000
)	


SELECT * FROM TARI.P_DDV_TARI.JKAUX_AG_K5 LIMIT 10




SELECT * FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX 
ORDER BY COD_DESACA DESC 
LIMIT 100


-------- revisando tabla auxiliar
SELECT 
	isa.COD_DESACA 
FROM AFI.P_RDV_DST_LND_SYB.ISAPREDESACA isa 
LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX aux ON (isa.COD_DESACA = aux.COD_DESACA) 
WHERE aux.COD_DESACA IS null


-------- revisando tabla prm_agrupa_planes
SELECT 
	isa.COD_DESACA 
FROM AFI.P_RDV_DST_LND_SYB.ISAPREDESACA isa 
LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm ON (isa.COD_DESACA = prm.COD_DESACA) 
WHERE prm.COD_DESACA IS null




SELECT * FROM ISA.P_RDV_DST_LND_SYB.GRUPLAN 
--LIMIT 10



SELECT * FROM ISA.P_RDV_DST_LND_SYB.SERIES_CATEG 
WHERE "grupo" = 11
--LIMIT 100



SELECT * FROM ISA.P_RDV_DST_LND_SYB.FAMPLAN 
WHERE "codigo" in(136
137
138
139
140
142
230
303
326
327
329)
LIMIT 100


SELECT 
	* 
FROM AFI.P_RDV_DST_LND_SYB.ISAPREDESACA
WHERE COD_DESACA BETWEEBN 
LIMIT 10


SELECT
	*
FROM ISA.P_RDV_DST_LND_SYB.SERIES_CATEG 
WHERE "codigo" =9137
LIMIT 10


SELECT 
	* 
FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES_v2
--WHERE COD_DESACA =9137


SELECT * FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES 
WHERE COD_DESACA =9137
LIMIT 10


SELECT * FROM TARI.P_DDV_TARI.UPDATE_LIC_2022 


create or replace TABLE TARI.P_DDV_TARI.UPDATE_LIC_2022 (
	YEAR NUMBER(38,0),
	CANTIDAD NUMBER(38,0),
	TOTAL FLOAT,
	TOTAL_VIGENTES FLOAT,
	MEAN_F FLOAT,
	DELTA_F FLOAT,
	UPDATE_F FLOAT,
	MEAN_V FLOAT,
	DELTA_V FLOAT,
	UPDATE_V FLOAT
)



SELECT * FROM TARI.P_DDV_TARI.UPDATE_LIC_2022

CREATE OR REPLACE TABLE TARI.P_EDV_TARI.UPDATE_LIC_2022 AS 
( 
SELECT
	ANIO AS YEAR ,
	CANTIDAD ,
	TOTAL ,
	TOTAL_VIGENTES ,
	MEAN_F ,
	DELTA_F ,
	UPDATE_F ,
	MEAN_V ,
	DELTA_V ,
	UPDATE_V 
FROM EST.P_DDV_EST.UPDATE_LIC_2022
ORDER BY ANIO
)
	
SELECT * FROM TARI.P_EDV_TARI.UPDATE_LIC_2022

	

SELECT * FROM TARI.P_DDV_TARI.UPDATE_LIC_2022


-------- INSERTA LOS NUEVOS REGISTROS
INSERT INTO EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX
--- COLUMNAS (COD_DESACA ,LINEA_PLAN , LINEA_PLAN_2, COD_TARIFICACION, GLS_TARIFICACION, GRUPO_FAMILIA, SEGMENTO, CLUSTER_ID)
	VALUES 
		
		(14  ,'PREMIUM','MASTER',1 ,'TARIFICACION HASTA  20213' ,'IND U.F','bajo' , ''),
		(15  ,'PREMIUM','MASTER',1 ,'TARIFICACION HASTA  20213' ,'IND U.F','alto' , ''),
		(16  ,'PREMIUM','MASTER',1 ,'TARIFICACION HASTA  20213' ,'IND U.F','bajo' , ''),
		(17  ,'PREMIUM','MASTER',1 ,'TARIFICACION HASTA  20213' ,'IND U.F','bajo' , '')

		
INSERT INTO EST.P_DDV_EST.COBERTURA_IGZ_LE 
	VALUES
		(1  ,'IGZ1122',6,5.6),
		(2  ,'IGZ2122',8,7.5),
		(3  ,'IGZ3122',10,9.4),
		(4  ,'IGZ4122',12,11.3),
		(5  ,'IGZ5122',14,13.2),
		(6  ,'IGZ6122',16,15),
		(7  ,'IGZ7122',18,16.9),
		(8  ,'IGZ8122',20,18.8)
		

SELECT * FROM EST.P_DDV_EST.COBERTURA_IGZ_LE LIMIT 10







		
		
INSERT INTO EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX (COD_DESACA ,LINEA_PLAN , LINEA_PLAN_2, COD_TARIFICACION, GLS_TARIFICACION, GRUPO_FAMILIA, SEGMENTO, CLUSTER_ID)
	SELECT 
		COD_DESACA ,LINEA_PLAN , LINEA_PLAN_2, COD_TARIFICACION, GLS_TARIFICACION, GRUPO_FAMILIA, SEGMENTO, CLUSTER_ID	
	FROM EST.P_DDV_EST.PARA_AGREGAR_PRM_PLANES;
	
		
		
SELECT DISTINCT COD_TARIFICACION  FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX 
LIMIT 100

SELECT COD_TARIFICACION  FROM EST.P_DDV_EST.PARA_AGREGAR_PRM_PLANES LIMIT 10



SELECT COD_TARIFICACION  FROM EST.P_DDV_EST.PARA_AGREGAR_PRM_PLANES  LIMIT 10

DROP TABLE EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX
	
SELECT * FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX 
	
DROP TABLE 	EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX
	
	




------ aqui creare la tabla auxiliar manual	
CREATE OR REPLACE TABLE EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX AS ( 
SELECT 
	COD_DESACA ,
	LINEA_PLAN ,
	LINEA_PLAN_2 ,
	COD_TARIFICACION ,
	GLS_TARIFICACION ,
	GRUPO_FAMILIA ,
	SEGMENTO ,
	CLUSTER_ID 
FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES
--LIMIT 10
)
	



SELECT * FROM TARI.P_DDV_TARI.JKAUX_AG_K5 LIMIT 10


-------- REVISION CODIGOS
SELECT * FROM  TARI.P_DDV_TARI.JKAUX_AG_K5  ---EST.P_DDV_EST.PRM_AGRUPA_PLANES_AUX  --EST.P_DDV_EST.PRM_AGRUPA_PLANES 
WHERE cod_categoria  IN (9153,
8691,
8877,
8878,
8879,
8880,
8881,
8882,
8883,
8884,
8885,
8886,
8887,
8888,
8889,
8890,
8891,
8892,
8893,
8894,
8895,
8896,
8897,
8898,
8899,
8900,
8901,
8902,
8903,
8904,
8905,
8906,
8907,
8908,
8909,
8910,
8911,
8912,
8913,
8914,
8915,
8916,
8917,
8918,
8919,
8920,
8921,
8922,
8923,
8924,
8925,
8926,
8927,
8928,
8929,
8930,
8931,
8932,
8933,
8934,
8935,
8936,
8937,
8938,
8939,
8940,
8941,
8942,
8975,
8976,
8977,
8978,
8979,
8980,
8981,
8982,
8983,
8984,
8985,
8986,
8987,
8988,
8989,
8990,
8991,
8992,
8993,
8994,
8995,
8996,
8997,
8998,
8999,
9000,
9001,
9002,
9003,
9004,
9005,
9006,
9007,
9008,
9009,
9010,
9011,
9012,
9013,
9014,
9015,
9016,
9017,
9018,
9019,
9020,
9021,
9022,
9023,
9024,
9025,
9026,
9027,
9028,
9029,
9030,
9031,
9032,
9033,
9034,
9035,
9036,
9037,
9038,
9039,
9040,
9041,
9042,
9043,
9044,
9045,
9046,
9047,
9048,
9049,
9050,
9051,
9052,
9053,
9054,
9055,
9056,
9057,
9058,
9059,
9060,
9061,
9062,
9063,
9064,
9065,
9066,
9108,
9109)
LIMIT 500

SELECT * FROM ISA.P_DDV_ISA.P_DDV_ISA_PRM_AGRUPA_PLANES  LIMIT 100

SELECT DISTINCT  DETALLE_PRODUCTO  FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES
--WHERE DETALLE_PRODUCTO ='PRF MULTIRED'
 LIMIT 100

select * from "AFI"."P_RDV_DST_LND_SYB"."ISAPREDESACA" LIMIT 100

select * from "ISA"."P_RDV_DST_LND_SYB"."ISAPREPREF" LIMIT 100

select * FROM "ISA"."P_RDV_DST_LND_SYB"."GRUPLAN" LIMIT 100

select * from "ISA"."P_RDV_DST_LND_SYB"."SERIES_CATEG" LIMIT 100

SELECT * FROM ISA.P_RDV_DST_LND_SYB.SERIES LIMIT 10

select * FROM ISA.P_RDV_DST_LND_SYB.FAMPLAN LIMIT 100 --- VENTA EN  FAMALIA = CODIGO SERIES 

SELECT DISTINCT  "venta_en"  FROM ISA.P_RDV_DST_LND_SYB.FAMPLAN

SELECT * FROM ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_CATEGORIA  LIMIT 10

---- maximo de varias columnas GREATEST 
---- minimo de varias columnas LEAST

SELECT DISTINCT *  from "TARI"."P_DDV_TARI"."JKAUX_AG_K5"
--ORDER BY COD_CATEGORIA ASC 
LIMIT 100



SELECT DISTINCT "fld_cntcateg" , "fld_planvig" ,SUBSTRING("fld_cntantcate",0,1) AS gls_tipo_trabajado
FROM AFI.P_RDV_DST_LND_SYB.CNT
ORDER BY "fld_cntcateg"
LIMIT 10



	
	
	
	USE isapre
go
IF OBJECT_ID('dbo.IQ_PRM_CATEGORIA') IS NOT NULL
BEGIN
    DROP VIEW dbo.IQ_PRM_CATEGORIA
    IF OBJECT_ID('dbo.IQ_PRM_CATEGORIA') IS NOT NULL
        PRINT '<<< FAILED DROPPING VIEW dbo.IQ_PRM_CATEGORIA >>>'
    ELSE
        PRINT '<<< DROPPED VIEW dbo.IQ_PRM_CATEGORIA >>>'
END
go
CREATE VIEW dbo.IQ_PRM_CATEGORIA
AS
  select 
cod_categoria = COD_DESACA,
categoria_gls = GLS_DESACA,
codgls_categ = right('0000'+convert(varchar(4),COD_DESACA),4) + ' ' +GLS_DESACA,
precio = VAL_CATEG,
nomreal = GLS_DESLAR,
moneda=COD_MONEDA,
triptico = COD_TFR,
aquiensevende = PLANES_EN_VENTA,
stgodesc1 = STGO_DESC1ANO, 
stgodesc2 = STGO_DESC2ANO,
stgodesc3 = STGO_DESC3ANO,
regdesc1 = REG_DESC1ANO,
regdesc2 = REG_DESC2ANO,
regdesc3 = REG_DESC3ANO,
sevendeen  = VENTAPLAN_EN,
cnvtramo = FLD_TRAMO,
red=' ',
segcatas = FLD_MEDICO,
cobertura = 0,
tipo_plan = isnull(case
             when b.grupo in (1,5,14) then "HOMBRES"
             when b.grupo = 11 and COD_DESACA in (1461,1466,4658,4661,4666) then "HOMBRES" 
             when b.grupo in (2,6) then "MUJERES"
             when b.grupo = 11 and COD_DESACA in (4659,4662,5502,5503) then "MUJERES"
             when b.grupo in (4,8,15) and b.familia <> 75 then "FAMILIAR COB. REDUCIDA"
             when b.grupo in (4,8) and b.familia = 75  then "SIN MATERNIDAD"
             when b.grupo in (3,7) and b.familia = 75  then "CON MATERNIDAD"
             when b.grupo = 11 and COD_DESACA in (1465,4660,4663,4667,5504,5505,5506,5507) then "FAMILIAR COB. REDUCIDA"
             else  "FAMILIAR CON PARTO"                           
--             when b.grupo not in (1,5,2,6,4,8,14,15) then  "FAMILIAR CON PARTO"                           
            end,'FAMILIAR CON PARTO'), 
serie_nueva = isnull(case
             when b.grupo in (1,5,14) then "SIN MATERNIDAD"
             when b.grupo = 11 and COD_DESACA in (1461,1466,4658,4661,4666) then "SIN MATERNIDAD" 
             when b.grupo in (2,6) then "SIN MATERNIDAD"
             when b.grupo = 11 and COD_DESACA in (4659,4662,5502,5503) then "SIN MATERNIDAD"
             when b.grupo in (4,8,15) and b.familia <> 75 then "SIN MATERNIDAD"
             when b.grupo in (4,8) and b.familia = 75  then "SIN MATERNIDAD"
             when b.grupo in (3,7) and b.familia = 75  then "CON MATERNIDAD"
             when b.grupo = 11 and COD_DESACA in (1465,4660,4663,4667,5504,5505,5506,5507) then "SIN MATERNIDAD"
             else  "CON MATERNIDAD"                           
--             when b.grupo not in (1,5,2,6,4,8,14,15) then  "FAMILIAR CON PARTO"                           
            end,'CON MATERNIDAD'),             
serie_tres = isnull(case
             when b.grupo in (1,5,14) then "HOMBRES"
             when b.grupo = 11 and COD_DESACA in (1461,1466,4658,4661,4666) then "HOMBRES" 
             when b.grupo in (2,6) then "MUJERES"
             when b.grupo = 11 and COD_DESACA in (4659,4662,5502,5503) then "MUJERES"
             when b.grupo in (4,8,15) and b.familia <> 75 then "FAMILIAR COB. REDUCIDA"
             when b.grupo in (4,8) and b.familia = 75  then "FAMILIAR COB. REDUCIDA"
             when b.grupo in (3,7) and b.familia = 75  then "FAMILIAR CON PARTO"
             when b.grupo = 11 and COD_DESACA in (1465,4660,4663,4667,5504,5505,5506,5507) then "FAMILIAR COB. REDUCIDA"
             else  "FAMILIAR CON PARTO"                           
--             when b.grupo not in (1,5,2,6,4,8,14,15) then  "FAMILIAR CON PARTO"                           
            end,'FAMILIAR CON PARTO'),            
grupos=isnull(b.grupo,0),
glosa_grupo = d.glosa,
cod_preferente= FLD_PREF,
preferente =isnull(
                  case 
                    when FLD_PREF = 0 then 'LIBRE ELECCION'
                    else GLS_PREF
                   end,'NO ENCONTRADO'),
familia = isnull(case
            when d.codigo in (1,2,3,4,13,14,15) then 'INDIVIDUAL'
            else 'COLECTIVO'
          end,'SIN CLASIFICACION'),
grupo_familia = isnull(case
            when d.codigo in (1,2,3,4,13,14,15) then 'IND U.F'
            when d.codigo in (5,6,7,8,12) then 'PLAN U.F'
            when d.codigo in (9) then 'DENTAL'
            when d.codigo in (10) then 'MEDICO'
            when d.codigo in (11) then 'PLAN %'
          end,'SIN CLASIFICACION'),     
fecha_creacion = convert(char(8),VIG_INICIO,112)      
                  
from 
--ISAPREDESACA
temporal..CATEGORIAS_III a, SERIES_CATEG b,ISAPREPREF c,  GRUPLAN d
where COD_DESACA *= b.codigo and
GLS_DESACA <> ' ' and
COD_DESACA not in (9990,9991) and
FLD_PREF *= convert(int,COD_PREF) and
  b.grupo *= d.codigo
go
IF OBJECT_ID('dbo.IQ_PRM_CATEGORIA') IS NOT NULL
    PRINT '<<< CREATED VIEW dbo.IQ_PRM_CATEGORIA >>>'
ELSE
    PRINT '<<< FAILED CREATING VIEW dbo.IQ_PRM_CATEGORIA >>>'
go
REVOKE SELECT ON dbo.IQ_PRM_CATEGORIA FROM bquery
go
GRANT SELECT ON dbo.IQ_PRM_CATEGORIA TO bquery
GO




    IFNULL(case
             when sca."grupo" in (1,5,14) then 'HOMBRES'
             when sca."grupo" = 11 and isa.COD_DESACA in (1461,1466,4658,4661,4666) then 'HOMBRES' 
             when sca."grupo" in (2,6) then 'MUJERES'
             when sca."grupo" = 11 and isa.COD_DESACA in (4659,4662,5502,5503) then 'MUJERES'
             when sca."grupo" in (4,8,15) and sca."familia"  <> 75 then 'FAMILIAR COB. REDUCIDA'
             when sca."grupo" in (4,8) and sca."familia" = 75  then 'SIN MATERNIDAD'
             when sca."grupo" in (3,7) and sca."familia" = 75  then 'CON MATERNIDAD'
             when sca."grupo" = 11 and isa.COD_DESACA in (1465,4660,4663,4667,5504,5505,5506,5507) then 'FAMILIAR COB. REDUCIDA'
             else  'FAMILIAR CON PARTO'                           
--             when b.grupo not in (1,5,2,6,4,8,14,15) then  "FAMILIAR CON PARTO"                           
            end,'FAMILIAR CON PARTO') AS tipo_plan
