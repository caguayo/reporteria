--- muestra id 31203941 rut real 006883279-9

SELECT 
*
FROM AFI.P_RDV_DST_LND_SYB.COB 
LIMIT 10 

SELECT
	"fld_funfolio" ,
	"fld_funcorrel" ,
	"fld_cotrut" ,
	"fld_concostototal" ,
	"fld_valorbase"  
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE "fld_funfolio" = 3250238 AND "fld_funcorrel" =1339  --AND 202201 BETWEEN FLD_VIGREALDESDE  AND FLD_VIGREALHASTA  ---  "fld_cotrut" = '006883279-9'
LIMIT 10 

SELECT
*
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE "fld_funfolio" = 3250238 AND "fld_funcorrel" =1339  --AND 202201 BETWEEN FLD_VIGREALDESDE  AND FLD_VIGREALHASTA  ---  "fld_cotrut" = '006883279-9'
LIMIT 10 


SELECT 
* 
FROM AFI.P_RDV_DST_LND_SYB.COB 
WHERE "fld_funnotiffun" =1251042 AND "fld_funcorrel" =2427
LIMIT 100


SELECT 
	*
FROM RCD.P_RDV_DST_LND_SYB.RCD_CTA_PAG 
WHERE "pag_funfol" =11474280 AND "pag_funcor" =2 AND "pag_periodo_cotizacion" =202201
LIMIT 10



------ reemplazar la lfc por la cob

--- casos fun con maria isabel espinoza 

----------EMPLEADORES UNICOS POR RUT
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.temp_emp AS ( 
SELECT DISTINCT 
	cnt."fld_cotrut",
	cob."fld_emprut" ,
	cob."fld_funnotiffun" ,
	cob."fld_funcorrel" 
FROM AFI.P_RDV_DST_LND_SYB.COB cob
LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (cob."fld_funnotiffun"=cnt."fld_funfolio" AND cob."fld_funcorrel"=cnt."fld_funcorrel")
order by cnt."fld_cotrut"  
)

----- replicando empleadores
CREATE OR REPLACE TEMPORARY TABLE est.P_DDV_EST.ca_emp AS (
WITH empleadores AS (
SELECT
"fld_cotrut",
"fld_emprut",
"fld_funnotiffun",
"fld_funcorrel",
ROW_NUMBER () OVER (PARTITION BY "fld_funnotiffun","fld_funcorrel" ORDER BY "fld_funnotiffun","fld_funcorrel") AS n_veces
FROM EST.P_DDV_EST.temp_emp)
SELECT fld_cotrut,fld_funnotiffun,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4,empleador_5,empleador_6
FROM empleadores 
pivot(max("fld_emprut") FOR n_veces IN (1,2,3,4,5,6))
AS P (fld_cotrut,fld_funnotiffun,fld_funcorrel,empleador_1,empleador_2,empleador_3,empleador_4,empleador_5,empleador_6))



CREATE OR REPLACE TEMPORARY TABLE est.P_DDV_EST.ca_bad AS (
WITH bad_temporal AS (
SELECT DISTINCT 
bad."bad_cotrut" ,
bad."bad_funfolio" ,
bad."bad_funcorrel" ,
--bad."bad_producto" ,
cpw."bad_nombre" ,
row_number() over (partition by "bad_cotrut" order by "bad_cotrut") AS n_veces ---contar
FROM afi.P_RDV_DST_LND_SYB.BAD bad
LEFT JOIN ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL  cpw ON (bad."bad_producto"=cpw."bad_idn")
WHERE  "bad_vigdes" <= 202312 AND "bad_vighas" >= 202202 )
SELECT bad_cotrut,bad_funfolio,bad_funcorrel,bad_1,bad_2,bad_3,bad_4,bad_5,bad_6
FROM bad_temporal
pivot(max("bad_nombre") FOR n_veces IN (1,2,3,4,5,6))
AS P (bad_cotrut,bad_funfolio,bad_funcorrel,bad_1,bad_2,bad_3,bad_4,bad_5,bad_6 ))
--LIMIT 10


SELECT COUNT(1) FROM EST.P_DDV_EST.ca_bad LIMIT 10 

WITH vn_vig AS (     ---- cartera
/*
SELECT 
	cnt."fld_funfolio" ,
	cnt."fld_funcorrel" ,
	cnt."fld_cotrut" ,
	cnt."fld_concostototal" ,
	cnt."fld_renunexced" ,
	emp.empleador_1 ,
	emp.empleador_2 ,
	emp.empleador_3 ,
	emp.empleador_4 ,
	emp.empleador_5 ,
	emp.empleador_6 
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt 
LEFT JOIN est.P_DDV_EST.ca_emp emp ON (cnt."fld_funfolio" = emp.fld_funnotiffun AND cnt."fld_funcorrel"= emp.fld_funcorrel)
WHERE 202202 BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA
*/
	SELECT DISTINCT 
		lfc."fld_periodoprod" ,
		lfc."fld_funfolio",
	    lfc."fld_funcorrel",
		lfc."fld_cotrut",
		cnt."fld_concostototal",
		cnt."fld_renunexced" ,
		emp.empleador_1 ,
		emp.empleador_2 ,
		emp.empleador_3 ,
		emp.empleador_4 ,
		emp.empleador_5 ,
		emp.empleador_6 
	FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
	LEFT JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio" = cnt."fld_funfolio" and lfc."fld_funcorrel" = cnt."fld_funcorrel")
	LEFT JOIN est.P_DDV_EST.ca_emp emp ON (cnt."fld_funfolio" = emp.fld_funnotiffun AND cnt."fld_funcorrel"= emp.fld_funcorrel)
		WHERE lfc."fld_periodoprod" BETWEEN '2022-01-01 00:00:00' AND '2022-03-31 23:59:59'
			  AND lfc."fld_funnotificacod"  = 1 
			  AND lfc."fld_tpotransac" IN('VN','CT')
)
,
coti_emp AS (
SELECT DISTINCT 
	"fld_funnotiffun" ,
	"fld_funcorrel" ,
	"fld_emprut" ,
	sum("fld_funmodmonto")::float AS total_cotizacion 
FROM AFI.P_RDV_DST_LND_SYB.COB 
--WHERE 202201 BETWEEN FLD_COBVIGREALDESDE AND FLD_COBVIGREALHASTA AND "fld_funnotiffun" =10916228 AND "fld_funcorrel" =6 -- "pag_funfol" =11262821 AND "pag_funcor" =1 --AND MAX("pag_periodo_cotizacion")--AND 
GROUP BY "fld_funnotiffun" , "fld_funcorrel" , "fld_emprut" 
ORDER BY "fld_funnotiffun" , "fld_funcorrel" DESC
--LIMIT 10
)
SELECT
	/*ca."fld_funfolio" AS folio,
	ca."fld_funcorrel" AS correl,
	CASE 
		WHEN ca."fld_renunexced"= 1 THEN 'SI'
		ELSE 'NO'
	END AS renun_exced,*/
	--ca."fld_cotrut" AS cotrut,
	"fld_periodoprod" ,
	SUBSTRING(ca."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	ca."fld_concostototal"::float AS pactado,
	--ca.empleador_1 ,
	IFNULL(emp1.total_cotizacion,0)::float AS pactado_emp1,
	--ca.empleador_2 ,
	IFNULL(emp2.total_cotizacion,0)::float AS pactado_emp2,
	--ca.empleador_3 ,
	IFNULL(emp3.total_cotizacion,0)::float AS pactado_emp3,
	--ca.empleador_4 ,
	IFNULL(emp4.total_cotizacion,0)::float AS pactado_emp4,
	--ca.empleador_5 ,
	IFNULL(emp4.total_cotizacion,0)::float AS pactado_emp5,
	--ca.empleador_6 ,
	IFNULL(emp4.total_cotizacion,0)::float AS pactado_emp6,
	(pactado_emp1+pactado_emp2+pactado_emp3+pactado_emp4+pactado_emp5+pactado_emp6) AS total_pac_emp,
	bad.bad_1 ,
	bad.bad_2 ,
	bad.bad_3 ,
	bad.bad_4 ,
	CASE 
		WHEN total_pac_emp > pactado THEN 'genera excedente'
		ELSE 'no genera'
	END AS check_exced
FROM vn_vig ca
LEFT JOIN coti_emp emp1 ON (ca."fld_funfolio" = emp1."fld_funnotiffun" AND ca."fld_funcorrel"=emp1."fld_funcorrel" AND ca.empleador_1=emp1."fld_emprut")
LEFT JOIN coti_emp emp2 ON (ca."fld_funfolio" = emp2."fld_funnotiffun" AND ca."fld_funcorrel"=emp2."fld_funcorrel" AND ca.empleador_2=emp2."fld_emprut")
LEFT JOIN coti_emp emp3 ON (ca."fld_funfolio" = emp3."fld_funnotiffun" AND ca."fld_funcorrel"=emp3."fld_funcorrel" AND ca.empleador_3=emp3."fld_emprut")
LEFT JOIN coti_emp emp4 ON (ca."fld_funfolio" = emp4."fld_funnotiffun" AND ca."fld_funcorrel"=emp4."fld_funcorrel" AND ca.empleador_4=emp4."fld_emprut")
LEFT JOIN coti_emp emp5 ON (ca."fld_funfolio" = emp5."fld_funnotiffun" AND ca."fld_funcorrel"=emp5."fld_funcorrel" AND ca.empleador_5=emp5."fld_emprut")
LEFT JOIN coti_emp emp6 ON (ca."fld_funfolio" = emp6."fld_funnotiffun" AND ca."fld_funcorrel"=emp6."fld_funcorrel" AND ca.empleador_6=emp6."fld_emprut")
LEFT JOIN est.P_DDV_EST.ca_bad bad ON (ca."fld_funfolio"=bad.bad_funfolio AND ca."fld_funcorrel"=bad.bad_funcorrel)
--WHERE BAD_2 IS NOT NULL  ---ca."fld_cotrut"='005646060-8'
--ORDER BY folio, correl 
--LIMIT 100
------------- fin extraccion






----------- revision excedentes 
SELECT 
	*
FROM RCD.P_RDV_DST_LND_SYB.EXC_CTA 
WHERE "cta_rut" ='005646060-8'
LIMIT 10 

SELECT 
	* 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE "fld_cotrut" ='018459135-9' AND  202201 BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA
LIMIT 10

SELECT 
		"pag_funfol" ,
		"pag_funcor" 
FROM RCD.P_RDV_DST_LND_SYB.RCD_CTA_PAG 
WHERE "pag_cotrut" ='018459135-9' AND "pag_periodo_cotizacion" =202201
LIMIT 10


SELECT 
	* 
FROM est.P_DDV_EST.ca_emp
WHERE fld_cotrut= '018459135-9'
LIMIT 10

SELECT 
	* 
FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE "fld_cotrut" ='018459135-9'
LIMIT 10







SELECT
	--*
	COUNT(DISTINCT "pag_funfol") AS cant 
FROM pagos_emp  
WHERE correlativo =1
LIMIT 500



SELECT * FROM ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL  LIMIT 10

SELECT 
	* 
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO 
LIMIT 10

SELECT * FROM est.P_DDV_EST.ca_emp LIMIT 10




SELECT DISTINCT ID_TITULAR , RUT_TITULAR  FROM EST.P_STG_EST.CA_SINIESTRALIDAD --LIMIT 10


SELECT 
	* 
FROM ISA.P_RDV_DST_LND_SYB.EMP 
LIMIT 10



SELECT 
	"fld_funfolio" ,
	"fld_correlativo" 
FROM ISA.P_RDV_DST_LND_SYB.EMP_COB 
WHE ---"fld_funfolio" =11262821 AND "fld_correlativo" =1
LIMIT 10











SELECT 
	count(DISTINCT "fld_emprut")
FROM AFI.P_RDV_DST_LND_SYB.COB 
WHERE 202203 BETWEEN FLD_COBVIGREALDESDE AND FLD_COBVIGREALHASTA AND "fld_funmodcotizacod" =4  --- "fld_funnotiffun" =11493069 AND "fld_funcorrel" =1
LIMIT 10 



SELECT 
	* 
FROM est.P_DDV_EST.ca_emp 
WHERE fld_cotrut='006883279-9'
limit 10 





SELECT 
	*
FROM RCD.P_RDV_DST_LND_SYB.EXC_MCC 
WHERE "cta_rut" ='007392178-3'
LIMIT 10







