--------COMPARACION DATAMART - INFORMACION_VS_CARTERA_GASTOS
WITH gastos_datamart AS (
SELECT 
	PERIODO, 
	--T1.RUT_TITULAR, 
	--t1.cod_beneficiario, 
	IFF(ORIGEN = 'HOSPITALARIO' and TIPO_BONO<>3,sum(BONIFICADO),0 ) AS DM_HOSPITALARIO,
	IFF(ORIGEN = 'AMBULATORIO'and TIPO_BONO<>3 ,sum(BONIFICADO),0 ) AS DM_AMBULATORIO,
	IFF(ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES,
	IFF(ORIGEN = 'HOSPITALARIO' and TIPO_BONO=3,sum(BONIFICADO),0 ) AS DM_CAEC_HOSPITALARIO,
	IFF(ORIGEN = 'AMBULATORIO' and TIPO_BONO=3,sum(BONIFICADO),0 ) AS DM_CAEC_AMBULATORIO,
	sum(MONTO_CAEC) AS DM_CAEC,
	IFF(ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
	SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
	SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE PERIODO BETWEEN 202101 AND 202103
GROUP BY 
	PERIODO,ORIGEN, tipo_bono, tipo_licencia
	--PERIODO-- , DM_HOSPITALARIO, DM_AMBULATORIO, DM_GES, DM_CAEC_HOSPITALARIO, DM_CAEC_AMBULATORIO, DM_CAEC, DM_LICENCIA
ORDER BY 
	PERIODO)
SELECT 
PERIODO, 
SUM(DM_HOSPITALARIO) AS HOSPITALARIO,
SUM(DM_CAEC_HOSPITALARIO) AS CAEC_HOSPITALARIO,
HOSPITALARIO + CAEC_HOSPITALARIO AS SUMA_HOSPITALARIOS,
SUM(DM_AMBULATORIO) AS AMBULATORIO,
SUM(DM_CAEC) AS CAEC
FROM GASTOS_DATAMART 
GROUP BY PERIODO 
ORDER BY PERIODO


WITH gastos AS (
SELECT PERIODO, ORIGEN , sum(bonificado) AS total  FROM GTO.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM
WHERE periodo BETWEEN 202101 AND 202103
GROUP BY PERIODO, ORIGEN
ORDER BY PERIODO)
SELECT top 10 * FROM gastos




SELECT top 1 "fld_ingreso",datediff(ww,dateadd(dd,'day'(getdate())*-1,getdate())+1,getdate())+1 AS test1, * FROM AFI.P_RDV_DST_LND_SYB.CNT 



SELECT * 
FROM ISA.P_RDV_DST_LND_SYB.PREC_FECHA 
WHERE "fecha" BETWEEN '2020-01-01' AND '2030-12-31' 


SELECT MAX("fecha") FROM ISA.P_RDV_DST_LND_SYB.PREC_FECHA 





SELECT datediff('weak',dateadd('day'(getdate())*-1,getdate())+1,getdate())+1 FROM ISA.P_RDV_DST_LND_SYB.PREC_FECHA 
WHERE "fecha" BETWEEN '2020-01-01' AND '2020-02-29' 


	

SELECT PERIODO, SUM(GASTO_HOSPITALARIOS) AS HOSPITALARIO, SUM(CAEC_HOSPITALARIO) AS CAEC_HOSPI, HOSPITALARIO+CAEC_HOSPI AS SUMA_HOSPI,  SUM(GASTO_AMBULATORIOS) AS AMBULATORIO, SUM(INGRESOS) AS INGRESO 
FROM EST.P_STG_EST.CARTERA_BEN
WHERE PERIODO BETWEEN 202101 AND 202103
GROUP BY PERIODO
ORDER BY PERIODO 
	
	
	
SELECT DISTINCT PERIODO FROM EST.P_STG_EST.CARTERA_BEN
ORDER BY PERIODO
	
SELECT TOP 1 * FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO


SELECT top 1 * FROM  GTO.P_DDV_GTO.P_DDV_GTO_MATRIZ_MARGEN 

SELECT 
periodo_procesamiento,
periodo,
CASE 
	WHEN periodo_procesamiento = periodo THEN 'actualizacion_normal'
	ELSE 'otra_actualizacion'
END AS cheq,
SUM(total_ingreso) AS total_ingreso,
SUM(ingreso) AS ingreso,
SUM(gasto_hospitalario) AS hospitalario,
SUM(gasto_ambulatorio) AS ambulatorio,
SUM(gasto_ges) AS ges,
SUM(gasto_caec) AS caec,
sum(gasto_lcc) AS licencias
FROM GTO.P_DDV_GTO.P_DDV_GTO_MATRIZ_MARGEN 
WHERE periodo BETWEEN 202101 AND 202103 AND periodo_procesamiento AND periodo_procesamiento = 202103--cheq ='actualizacion_normal'
GROUP BY periodo_procesamiento,periodo
ORDER BY PERIODO 


WITH vn AS (
select 
    --lfc."fld_cotrut" as rut_afiliado,
    SUBSTRING("fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
    "fld_fechafun" as fecha_fun,
    "fld_funfolio" as contrato,
    "fld_funcorrel" as correlativo,
    "fld_folio" as folio,
	"fld_emprut" as rut_empleador,
    "fld_fectraspaso" as fecha_traspaso,
    "fld_fechareal" as fecha_real,
	"fld_periodoprod" as fecha_produccion,
	"tipo_plan" AS tipo_plan
from AFI.P_RDV_DST_LND_SYB.LFC
WHERE "fld_periodoprod" between '2018-01-01 00:00:00' and '2020-12-31 23:59:59'
and "fld_funnotificacod"  = 1 
and "fld_tpotransac" IN('VN','CT')
)
SELECT 
--EST.P_DDV_EST.FECHA_PERIODO(fecha_produccion) AS periodo,
YEAR(fecha_produccion) AS anio,
CASE 
	WHEN tipo_plan IN ('I', 'E6') THEN 'INDIVIDUAL'
	ELSE 'COLECTIVO'
END AS tip_plan,
count(DISTINCT id_rut_afiliado) AS cantidad 
FROM vn
GROUP BY anio, tip_plan 
ORDER BY anio 





SELECT top 1 * FROM  AFI.P_RDV_DST_LND_SYB.LFC


----------
WITH desafi AS (
select 
    lfc."fld_cotrut" as rut_afiliado,
    SUBSTRING(lfc."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
    lfc."fld_funfolio" as contrato,
    lfc."fld_funcorrel" as correlativo,
    lfc."fld_periodoprod"	as fecha,
    case (lfc."fld_funnotificacod")
	    when 2 then 'Desafiliacion'
		when 333 then 'renuncia voluntaria'
    end as motivo, 
    cnt."fld_ingreso" as fecha_ingreso
from AFI.P_RDV_DST_LND_SYB.LFC lfc
	inner join AFI.P_RDV_DST_LND_SYB.CNT cnt
	on lfc."fld_funfolio" = cnt."fld_funfolio"
	and lfc."fld_funcorrel" = cnt."fld_funcorrel"
	and "fld_periodoprod" between '2018-01-01 00:00:00' and '2018-12-31 23:59:59'
	and ("fld_funnotificacod"=2 or "fld_funnotificacod"=333))
SELECT motivo, COUNT(DISTINCT rut_afiliado) FROM desafi
GROUP BY motivo

