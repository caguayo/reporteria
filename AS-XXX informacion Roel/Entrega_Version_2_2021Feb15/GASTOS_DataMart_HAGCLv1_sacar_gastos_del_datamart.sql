CREATE OR REPLACE TABLE EST.P_STG_EST.GASTOS_DM_HAGCL AS 
SELECT  T1.PERIODO, 
	T1.RUT_TITULAR, 
	t1.cod_beneficiario, 	
        SUM(DM_HOSPITALARIO) AS DM_HOSPITALARIO, 
	SUM(DM_AMBULATORIO) AS DM_AMBULATORIO, 
	SUM(DM_GES) AS DM_GES, 
	SUM(DM_CAEC) AS DM_CAEC, 
	SUM(DM_LICENCIA) AS DM_LICENCIA 
FROM 
	(		SELECT 
			T1.PERIODO, 
			T1.RUT_TITULAR, 
			t1.cod_beneficiario, 
			IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO<>3,sum(BONIFICADO),0 ) AS DM_HOSPITALARIO,
			IFF(T1.ORIGEN = 'AMBULATORIO'and T1.TIPO_BONO<>3 ,sum(BONIFICADO),0 ) AS DM_AMBULATORIO,
			IFF(T1.ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES,
			sum(MONTO_CAEC) AS DM_CAEC,
			IFF(T1.ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
                                           SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
                                           SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
			from "DTM"."P_STG_DTM_GTO"."P_STG_DTM_GTO_HECHOS_GTO" T1
			where T1.PERIODO between 201901 and 202101
			GROUP BY T1.PERIODO,
                 T1.ORIGEN, t1.tipo_bono, T1.RUT_TITULAR, t1.cod_beneficiario, tipo_licencia
	) t1
group by  T1.PERIODO, T1.RUT_TITULAR, t1.cod_beneficiario
