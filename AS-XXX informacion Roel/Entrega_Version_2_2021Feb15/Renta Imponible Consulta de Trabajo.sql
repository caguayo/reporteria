-- Consulta para validar la renta imponible de un RUT
-- Se debe poner el rut_titular dentro del parentesis: Ejemplo  ('004976757-9') y el periodo 202002
-- Si es mas de un rut se puede poner ',' entre cada RUT

-- Optimizacion
SELECT top 10 t10.periodo, t10.rut_titular, t10.id, sum(t10."fld_emprut") NUM_EMPLEADORES, sum(t10."fld_ultrenta") RENTA_IMPUESTA
from  (
  SELECT 
     s1.periodo,
     s1.rut_titular, 
     s1.id,   
     s3."fld_emprut",
     s3."fld_ultrenta" 
  FROM 
      "DTM"."P_STG_DTM_GTO"."P_STG_DTM_GTO_HECHOS_GTO" s1 
      LEFT OUTER JOIN "AFI"."P_RDV_DST_LND_SYB"."CNT" s2 
      ON (
          s1.RUT_TITULAR = s2."fld_cotrut" 
          AND s2.FLD_VIGREALDESDE <= s1.periodo 
          AND s2.FLD_VIGREALHASTA >= s1.periodo
      )   
      LEFT OUTER JOIN (  
              SELECT "fld_funnotiffun", "fld_funcorrel", 
                      count("fld_emprut") "fld_emprut", 
                      FLD_COBVIGREALDESDE, 
                      FLD_COBVIGREALHASTA, 
                      sum("fld_ultrenta") "fld_ultrenta"
              FROM  "AFI"."P_RDV_DST_LND_SYB"."COB" 
              GROUP  BY  "fld_funnotiffun", "fld_funcorrel", FLD_COBVIGREALDESDE, FLD_COBVIGREALHASTA
      ) s3 
      ON (
                s2."fld_funfolio"= s3."fld_funnotiffun" 
               AND s2."fld_funcorrel" = s3."fld_funcorrel"
               and s3.FLD_COBVIGREALDESDE <= s1.periodo AND s3.FLD_COBVIGREALHASTA >= s1.periodo
      )
      WHERE s1.periodo BETWEEN 202002 AND 202002
     -- and s1.rut_titular in ('004976757-9')
) t10    
group by  t10.periodo, t10.rut_titular, t10.id