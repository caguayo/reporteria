create or replace function "EST"."P_STG_EST".CONTRATO_MAX_PERIDO()
  returns date
  AS
  $$
  select to_date(concat(max(t1.periodo_vigencia),'01'),'YYYYMMDD') from "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t1
  $$
  ;