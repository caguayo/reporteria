CREATE OR REPLACE TABLE EST.P_STG_EST.CARTERA_BEN AS 
    SELECT 
        t1.RUT_TITULAR,
        to_number(SUBSTR(t1.rut_titular,1,9))*5 - 3265112 + 52658 AS ID_TITULAR,
        t4."fld_benrut" AS RUT_BENEFICIARIO,
        t4."fld_bencorrel" AS COD_BENEFICIARIO,
        IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,'TITULAR','BENEFICIARIO') AS TIPO_RUT,
        t1.periodo_vigencia  AS PERIODO,
        SUBSTR(t1.periodo_vigencia,1,4) AS ANNO,
        to_number(SUBSTR(t1.periodo_vigencia,5,2)) AS MES,
        (to_varchar(t1.FECHA_INGRESO,'YYYY-MM-DD')) AS FECHA_INGRESO,
        t1.ANTIGUEDAD,
        	CASE T4."fld_bensexo" 
                WHEN 2 THEN 'F'
                ELSE 'M' 
            END AS SEXO,
        row_number() over (partition by t1.periodo_vigencia, t1.rut_titular ORDER BY t4."fld_benrut" asc) REGISTRO_ID,
        IFF(REGISTRO_ID=1,t1.CNT_CARFAM,0) as NUM_CARGAS,
        t1.NUM_CONTRATO AS CONTRATO,
        t1.NUM_CORRELATIVO AS CORRELATIVO,
        t1.COD_REGION,
        t8.GLS_REGION AS REGION_GLS,
        t1.COD_COMUNA,
        t7.GLS_COMUNA AS COMUNA_GLS,
        t11.UBICACION,
        t1.COD_SUCURSAL,
        t2.GLS_SUCUR AS SUCURSAL_GLS,
        t11.COD_CENTROCOSTOS AS COD_CENTRO_COSTOS,
        t11.CENTROCOSTOS AS CENTRO_COSTOS_GLS,
        TO_DATE(to_varchar(t4."fld_bennacfec",'YYYY-MM-DD')) AS FECHA_NACIMIENTO,
        t4."fld_benedad" AS EDAD,
        IFF(REGISTRO_ID=1,t10."fld_emprut",0) AS NUM_EMPLEADORES,
        t1.COD_ACTIVIDAD	AS ACTIVIDAD,
        t1.TIPO_TRABAJADOR	AS TIPO_TRABAJADOR,
        t1.TIPO_TRANSACCION	AS TIPO_TRANSC,
        t1.COD_CATEGORIA	AS CATEGORIA_COD,
        t3.CATEGORIA_GLS	AS CATEGORIA_GLS,
        t5.SERIE,
        t5.TIPO_PLAN,
        t5.TIPO_PRODUCTO,
        t5.DETALLE_PRODUCTO,
        t5.LINEA_PLAN,
        t6."fld_segurohasta" AS SEG_MUERTE,       
        IFF(REGISTRO_ID=1,t1.COSTO_FINAL_UF,0) AS COSTO_FINAL,
        IFF(REGISTRO_ID=1,t6."fld_valorbase",0) AS PRECIO_BASE,
        IFF(REGISTRO_ID=1,t4."fld_factor",0) AS FACTOR_RIESGO,
        IFF(REGISTRO_ID=1,t6."fld_costototal",0) AS COSTO_TOTAL,
        IFF(REGISTRO_ID=1,t6."fld_benefadic",0) AS COSTO_BENEF_ADIC,
        IFF(REGISTRO_ID=1,t1.PAGADO_TOTAL_PESOS,0) AS PAGADO,
        IFF(REGISTRO_ID=1,t1.MONTO_EXCESO,0) AS EXCESOS,
        IFF(REGISTRO_ID=1,t1.MONTO_EXCEDENTE,0) AS EXCEDENTES,
        NVL(t14.DM_AMBULATORIO,0) AS GASTO_AMBULATORIOS,
        NVL(t14.DM_HOSPITALARIO,0) AS GASTO_HOSPITALARIOS,       
        NVL(t14.DM_GES,0) AS GASTO_GES,
        NVL(t14.DM_CAEC_HOSPITALARIO,0) AS CAEC_HOSPITALARIO,
        NVL(t14.DM_CAEC_AMBULATORIO,0) AS CAEC_AMBULATORIO,
        NVL(t14.DM_CAEC,0) AS GASTO_CAEC,
        IFF(REGISTRO_ID=1,t1.GASTO_PHARMA_PESOS,0) AS GASTO_PHARMA,
        IFF(REGISTRO_ID=1,NVL(t16.RECUPERACION_GASTO,0),0) AS RECUPERACION_GASTO,       
        IFF(REGISTRO_ID=1,NVL(t15.IVA_RECUPERADO,0),0) AS IVA_RECUPERADO,
        NVL(t14.DM_LICENCIA,0) AS GASTO_LICENCIAS,
        IFF(REGISTRO_ID=1,NVL(t10."fld_ultrenta",0),0) AS RENTA_IMPONIBLE,
        CASE	
        WHEN RENTA_IMPONIBLE > 1 AND RENTA_IMPONIBLE <=500000	
            THEN '>1 <= 500'
        WHEN RENTA_IMPONIBLE > 500001 AND RENTA_IMPONIBLE <= 1000000	
            THEN '>500 y <= 1000'	
        WHEN RENTA_IMPONIBLE > 1000001 AND RENTA_IMPONIBLE <=1500000
            THEN '>1000 y <= 1500'	
        WHEN RENTA_IMPONIBLE > 1500001 AND RENTA_IMPONIBLE <= 2300000
            THEN '>1500 y <= 2300'	
        WHEN RENTA_IMPONIBLE > 2300001
            THEN '> 2300'
        ELSE '' 
    END AS  RANGO_RENTA_IMPONIBLE,
        t9.VAL_MON AS VALOR_UF,
        t1.COD_AGENCIA AS AGENCIA_VN,
        t1.COD_AGENTE AS AGENTE_VN,
        t1.PERIODO_ANUALIDAD AS PERIODO_ANUAL,
        t1.CLASIFICACION AS CLASIF_RIESGO,
        t1.COD_MOROSIDAD AS CLASIF_MOROSIDAD,  
        CASE 
            WHEN  t4."fld_benedad" BETWEEN 0 AND 19 THEN '< 20 A�os'
            WHEN  t4."fld_benedad" BETWEEN 20 AND 24 THEN '20 a 24 A�os'
            WHEN  t4."fld_benedad" BETWEEN 25 AND 29 THEN '25 a 29 A�os'
            WHEN  t4."fld_benedad" BETWEEN 30 AND 34 THEN '30 a 34 A�os'
            WHEN  t4."fld_benedad" BETWEEN 35 AND 39 THEN '35 a 39 A�os'
            WHEN  t4."fld_benedad" BETWEEN 40 AND 44 THEN '40 a 44 A�os'
            WHEN  t4."fld_benedad" BETWEEN 45 AND 49 THEN '45 a 49 A�os'                  
            WHEN  t4."fld_benedad" BETWEEN 50 AND 54 THEN '50 a 54 A�os'
            WHEN  t4."fld_benedad" BETWEEN 55 AND 60 THEN '55 a 60 A�os'        
            WHEN  t4."fld_benedad" > 60 then '> 60 y mas A�os'
            ELSE ''
        END AS RANGO_EDAD,
         IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,FIRST_VALUE( t4."fld_benedad") over (partition by t1.periodo_vigencia, t1.RUT_TITULAR order by  t4."fld_bencorrel"),null) as EDAD_TITULAR,  
         CASE 
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 0 and 19 then '< 20 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 20 and 24 then '20 a 24 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 25 and 29 then '25 a 29 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 30 and 34 then '30 a 34 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 35 and 39 then '35 a 39 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 40 and 44 then '40 a 44 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 45 and 49 then '45 a 49 A�os'                  
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 50 and 54 then '50 a 54 A�os'
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR between 55 and 60 then '55 a 60 A�os'        
            WHEN  (t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and EDAD_TITULAR > 60 then '> 60 y mas A�os'
            ELSE ''
        END AS RANGO_EDAD_TITULAR,   
        CASE	
          WHEN t1.COSTO_FINAL_UF > 0 AND t1.COSTO_FINAL_UF <=2	
          THEN '>0 <= 2.0 UF'
          WHEN t1.COSTO_FINAL_UF > 2 AND t1.COSTO_FINAL_UF <=3	
          THEN '>2.0 <= 3.0 UF'	
          WHEN t1.COSTO_FINAL_UF > 3 AND t1.COSTO_FINAL_UF <=4	
          THEN '>3.0 <= 4.0 UF'	
          WHEN t1.COSTO_FINAL_UF > 4 AND t1.COSTO_FINAL_UF <=5	
          THEN '>4.0 <= 5.0 UF'	
          WHEN t1.COSTO_FINAL_UF > 5	
          THEN '>5.0 U'
          ELSE ''
        END AS RANGO_COTIZACION,
        CASE
          WHEN t1.antiguedad <=12
          THEN '00 a 12 meses'
          WHEN t1.antiguedad >12 AND t1.antiguedad <=24
          THEN '>12 a 24 meses'
          WHEN t1.antiguedad >24 AND t1.antiguedad <=36
          THEN '>24 a 36 meses'
          WHEN t1.antiguedad >36 AND t1.antiguedad <=48
          THEN '>36 a 48 meses'
          WHEN t1.antiguedad >48 AND t1.antiguedad <=60
          THEN '>48 a 60 meses'
          WHEN t1.antiguedad >60
          THEN '>60' 
          ELSE ''        
        END AS RANGO_ANTIGUEDAD,
        CASE
          WHEN t6."fld_valorbase" =0
          THEN 'igual a 0'
          WHEN t6."fld_valorbase" > 0.01 AND t6."fld_valorbase" <= 1.50
          THEN '0.01 - 1.50'
          WHEN t6."fld_valorbase" >1.51 AND t6."fld_valorbase" <= 2
          THEN '1.51 - 2.00'
          WHEN t6."fld_valorbase" >2.01 AND t6."fld_valorbase" <=3
          THEN '2.01 - 3.00'
          WHEN t6."fld_valorbase" >3.01 AND t6."fld_valorbase" <=4
          THEN '3.01 a 4.00'
          WHEN t6."fld_valorbase" >4.01 AND t6."fld_valorbase" <=5
          THEN '4.01 a 5.00'
          WHEN t6."fld_valorbase" >5.01 
          THEN '5.01 y más'
          ELSE ''
        END AS RANGO_PRECIO,
        t4."fld_bennombre" AS FLD_BENNOMBRE,
        t4."fld_benapepa" AS FLD_BENAPEPA, 
        t4."fld_benapema" AS FLD_BENAPEMA,
        t6."fld_funfolio" AS FLD_FUNFOLIO,
        t6."fld_funcorrel" AS FLD_FUNCORREL,
        IFF(REGISTRO_ID=1, t12.Valor_Iva,0) AS IVA,
        t1.COD_NEGOCIO,
        t13.GLS_MODCOTZ AS TIPO_COTIZACION,
        PAGADO - EXCESOS - EXCEDENTES AS RECAUDADO,
        PAGADO - EXCESOS - EXCEDENTES - IVA AS INGRESOS
    FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO  t1
        -- Liga con Sucursal
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRESUCURSAL  t2 ON (t1.cod_sucursal = t2.COD_SUCUR)
        -- Liga con Categoria
        LEFT OUTER JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_CATEGORIA t3 ON (t1.cod_categoria = t3.cod_categoria)
        -- Liga para agregar Nombre y Genero
        -- Inicio BEN 
        LEFT OUTER JOIN AFI.P_RDV_DST_LND_SYB.BEN  t4 ON (t1.RUT_TITULAR = t4."fld_cotrut" AND t1.num_correlativo = t4."fld_funcorrel" AND t4.FLD_BENVIGREALDESDE <= t1.periodo_vigencia AND t4.FLD_BENVIGREALHASTA >= t1.periodo_vigencia)
        -- Fin BEN
        -- Liga para agregar tipo_plan, detalle_producto
        LEFT OUTER JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES t5 ON (t1.cod_categoria = t5.COD_DESACA) --"IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES" t5  
        -- Liga para agregar datos de CNT
        LEFT OUTER JOIN AFI.P_RDV_DST_LND_SYB.CNT t6  ON (t1.RUT_TITULAR = t6."fld_cotrut" and t6.FLD_VIGREALDESDE <= t1.periodo_vigencia AND t6.FLD_VIGREALHASTA >= t1.periodo_vigencia)
        -- Liga para agregar COMUNA
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRECOMUNAS t7 ON (t1.COD_COMUNA = t7.COD_COMUNA )
        -- Liga para agregar Region
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION t8 ON (t1.COD_REGION = t8.COD_REGION )
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA t9 ON (t1.periodo_vigencia = to_number(to_char(dateadd(month, 1, fec_val),'YYYYMM')))  
        -- Liga para obtener la Renta Imponible
        -- Inicio Renta Imponible       
        LEFT OUTER JOIN (
           SELECT 	s4.periodo_vigencia, 
           			s4.rut_titular, 
           			sum(s4."fld_emprut") as "fld_emprut", 
           			sum(s4."fld_ultrenta") as "fld_ultrenta"
           FROM  (
	                SELECT 
	                   s1.periodo_vigencia,
	                   s1.rut_titular,                 
	                   s3."fld_emprut",
	                   s3."fld_ultrenta" 
	                FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO s1 
                    	LEFT OUTER JOIN AFI.P_RDV_DST_LND_SYB.CNT s2 ON (s1.RUT_TITULAR = s2."fld_cotrut" AND s2.FLD_VIGREALDESDE <= s1.periodo_vigencia AND s2.FLD_VIGREALHASTA >= s1.periodo_vigencia)   
                    LEFT OUTER JOIN (  
	                            SELECT 	"fld_funnotiffun", 
	                            		"fld_funcorrel", 
	                                    count("fld_emprut") "fld_emprut", 
	                                    FLD_COBVIGREALDESDE, 
	                                    FLD_COBVIGREALHASTA, 
	                                    sum("fld_ultrenta") "fld_ultrenta"
	                            FROM  AFI.P_RDV_DST_LND_SYB.COB 
	                            GROUP  BY  
	                            		"fld_funnotiffun", 
	                            		"fld_funcorrel", 
	                            		FLD_COBVIGREALDESDE, 
	                            		FLD_COBVIGREALHASTA
                    ) s3  ON (   s2."fld_funfolio"= s3."fld_funnotiffun" AND s2."fld_funcorrel" = s3."fld_funcorrel" AND s3.FLD_COBVIGREALDESDE <= s1.periodo_vigencia AND s3.FLD_COBVIGREALHASTA >= s1.periodo_vigencia)
      	 WHERE s1.periodo_vigencia BETWEEN
                -- Parametros para filtrar la Renta Imponible, como base contrato para contar empleadores y sumar renta imponible
                -- PERIODO INICIAL y PERIODO FINAL, se obtiene el maximo periodo de contrato y se restan 23 meses
                --to_number(to_char(dateadd(month,-35,EST.P_STG_EST.CONTRATO_MAX_PERIDO()),'YYYYMM'))  AND 
                --to_number(to_char(EST.P_STG_EST.CONTRATO_MAX_PERIDO(),'YYYYMM'))
                EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-35))||' 23:59:59')) AND 
    			EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59'))
                  
              ) s4  
              GROUP BY  
              s4.periodo_vigencia, 
              s4.rut_titular) t10 ON (t1.periodo_vigencia = t10.periodo_vigencia AND t1.rut_titular = t10.rut_titular)            
        -- Fin para Renta Imponible
        -- Liga con Sucursal para obtener centro de costos y ubicacion
        LEFT OUTER JOIN ISA.P_DDV_ISA.P_DDV_ISA_V_PRM_SUCURSAL2 t11  ON (t1.cod_sucursal = t11.cod_sucursal)
        -- Liga para obtener el iva
        LEFT OUTER JOIN (select perpag_esperado, rut_cotizante, SUM( Valor_Iva) as  Valor_Iva
                        from RCD.P_DDV_RCD.P_DDV_RCD_V_LIBRO_VENTAS
                        where  Codigo_Pago = 0
                        group by perpag_esperado, rut_cotizante) t12 ON (t1.periodo_vigencia = t12.perpag_esperado AND t1.rut_titular = rut_cotizante )
        -- Liga para agregar GLS_MODCOTZ Tipo Cotizacion
        LEFT OUTER JOIN ISA.P_RDV_DST_LND_SYB.ISAPREMODCOTZ t13      ON (t6."fld_monedatipocod" = t13.COD_MODCOTZ)
        -- Liga para agregar Gasto directo del Datamart de Gastos - Hospitalario, Ambulatorio, Ges, CAEC y Licencias
        LEFT OUTER JOIN (
             SELECT T1.PERIODO, 
                T1.RUT_TITULAR, 
                t1.cod_beneficiario, 	
                SUM(DM_HOSPITALARIO) AS DM_HOSPITALARIO, 
                SUM(DM_AMBULATORIO) AS DM_AMBULATORIO, 
                SUM(DM_GES) AS DM_GES, 
                SUM(DM_CAEC_HOSPITALARIO) AS DM_CAEC_HOSPITALARIO,
                SUM(DM_CAEC_AMBULATORIO) AS DM_CAEC_AMBULATORIO,
                SUM(DM_CAEC) AS DM_CAEC, 
                SUM(DM_LICENCIA) AS DM_LICENCIA 
            FROM 
                (		SELECT 
                        T1.PERIODO, 
                        T1.RUT_TITULAR, 
                        t1.cod_beneficiario, 
                        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO<>3,sum(BONIFICADO),0 ) AS DM_HOSPITALARIO,
                        IFF(T1.ORIGEN = 'AMBULATORIO'and T1.TIPO_BONO<>3 ,sum(BONIFICADO),0 ) AS DM_AMBULATORIO,
                        IFF(T1.ORIGEN = 'GES',sum(BONIFICADO),0 ) AS DM_GES,
                        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO=3,sum(BONIFICADO),0 ) AS DM_CAEC_HOSPITALARIO,
                        IFF(T1.ORIGEN = 'AMBULATORIO' and T1.TIPO_BONO=3,sum(BONIFICADO),0 ) AS DM_CAEC_AMBULATORIO,
                        sum(MONTO_CAEC) AS DM_CAEC,
                        IFF(T1.ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(SUM(MONTO_COTIZACION) > SUM(MONTO_SALUD),
                                                       SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + SUM(monto_afp)  + SUM(monto_cotizacion),
                                                       SUM(monto_subsidio) + SUM(monto_caja) + SUM(monto_invalidez_sobr) + sum(monto_afp)  + sum(monto_salud) ),0 ) AS DM_LICENCIA
                        FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO T1
                        WHERE T1.PERIODO  BETWEEN 
                            -- Parametros para filtrar los gastos del Datamart
                            -- PERIODO INICIAL y PERIODO FINAL, se obtiene el maximo periodo de contrato y se restan 23 meses
                            --to_number(to_char(dateadd(month,-35,EST.P_STG_EST.CONTRATO_MAX_PERIDO()),'YYYYMM'))  AND 
                            --to_number(to_char(EST.P_STG_EST.CONTRATO_MAX_PERIDO(),'YYYYMM'))
                            EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-35))||' 23:59:59')) AND 
            				EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59'))
                        GROUP BY T1.PERIODO,
                             T1.ORIGEN, t1.tipo_bono, T1.RUT_TITULAR, t1.cod_beneficiario, tipo_licencia
                ) t1
            group by  T1.PERIODO, T1.RUT_TITULAR, t1.cod_beneficiario
          ) t14
            ON (t1.periodo_vigencia = t14.periodo and
                t1.rut_titular = t14.rut_titular  and
               NVL(t4."fld_bencorrel",0) = t14.cod_beneficiario)
        -- Liga para agregar Iva Recuperado
        LEFT OUTER JOIN (
                          SELECT t1.PERIODO,
                              t1.RUT AS RUT_TITULAR,
                              SUM(t1.monto_credito_fiscal) AS IVA_RECUPERADO
                          FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS t1                         
                          GROUP BY t1.periodo, t1.rut
                        )  t15
            ON (t1.periodo_vigencia = t15.periodo and
                t1.rut_titular = t15.rut_titular)
        -- Liga para agregar Recuperacion del Gasto
        LEFT OUTER JOIN (
            SELECT t1."periodo_vig" AS PERIODO,t1."rut_titular" AS RUT_TITULAR,SUM(t1."monto_recuperado") RECUPERACION_GASTO   
            FROM IQ.P_DDV_IQ.RECUPERACION_GASTO t1
            GROUP BY t1."periodo_vig", t1."rut_titular"
       ) t16
          ON (t1.periodo_vigencia = t16.periodo and
                t1.rut_titular = t16.rut_titular)
       WHERE  t1.periodo_vigencia 
            BETWEEN 
            -- Parametros para filtrar los periodos de la base Contrato para Cartera BEN
            -- Los parametros estan en 3 lugares en esta consulta, Renta Imponible t10, Datamart de Gastos t14 y la base Contrato t1
            -- PERIODO INICIAL y PERIODO FINAL, se obtiene el maximo periodo de contrato y se restan 23 meses, o 35 meses
            --to_number(to_char(dateadd(month,-35,EST.P_STG_EST.CONTRATO_MAX_PERIDO()),'YYYYMM'))  AND
            --to_number(to_char(EST.P_STG_EST".CONTRATO_MAX_PERIDO(),'YYYYMM'))
            EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-35))||' 23:59:59')) AND 
            EST.P_DDV_EST.FECHA_PERIODO(TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59'))
            
            
            
            
            

                         
            
            
            
            
            
            
 