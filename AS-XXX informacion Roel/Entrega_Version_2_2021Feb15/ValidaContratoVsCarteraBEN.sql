--===========================================================================
-- VALIDACION CONTRATOS vs CARTERA_BEN
-- Ingreso directo de CONTRATOS
--===========================================================================
SELECT  --t1.cod_categoria,
      t1.periodo_vigencia,
      t3.DETALLE_PRODUCTO, 
      (COUNT(DISTINCT(t1.rut_titular))) AS COTIZANTES,
      -- Cargas 
      (SUM(t1.cnt_carfam))  AS CARGAS,
      -- Total 
      ((SUM(t1.pagado_total_pesos))-(SUM(t1.monto_exceso))-(SUM(t1.monto_excedente))) AS TOTAL,
      -- pagado_total_pesos 
      (SUM(t1.pagado_total_pesos)) AS PAGADO,
      -- monto_exceso 
      (SUM(t1.monto_exceso))  AS EXCESOS,
      -- monto_excedente
      (SUM(t1.monto_excedente)) AS EXEDENTES,
      -- Cotizantes 
      (SUM(t1.costo_final_uf))  AS uf_costo_final
FROM "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t1
  LEFT OUTER JOIN "IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES"  t3
    ON (t1.COD_CATEGORIA = t3."cod_categoria")
where t1.periodo_vigencia BETWEEN 202001 AND 202001
-- and t3.DETALLE_PRODUCTO in ('PRF. STA. MARIA','PRF. PUC','PRF. MAGALLANES')
    -- and t1.cod_categoria = 47
group by --t1.cod_categoria,
t1.periodo_vigencia,
 t3.DETALLE_PRODUCTO
order by 1,2



--===========================================================================
-- VALIDACION CARTERA_BEN vs CONTATOS
-- Ingreso directo de CARTERA_BEN
--===========================================================================

SELECT  --t1.cod_categoria,
      t1.periodo,
      t1.DETALLE_PRODUCTO, 
      (COUNT(DISTINCT(t1.rut_titular))) AS COTIZANTES,
      -- Cargas 
      (SUM(t1.num_cargas))  AS CARGAS,
      -- Total 
      SUM(t1.recaudado) AS TOTAL,
      -- pagado_total_pesos 
      (SUM(t1.pagado)) AS PAGADO,
      -- monto_exceso 
      (SUM(t1.excesos))  AS EXCESOS,
      -- monto_excedente
      (SUM(t1.Excedentes)) AS EXEDENTES,
      -- Cotizantes 
      (SUM(t1.costo_final))  AS costo_final
FROM "EST"."P_STG_EST"."CARTERA_BEN" t1
where t1.periodo BETWEEN 202001 AND 202001
group by t1.periodo,
 t1.DETALLE_PRODUCTO
order by 1,2