-- Gastos BEN
-- ==========================================
SELECT  --t1.cod_categoria,
      t1.periodo,
    -- t1.DETALLE_PRODUCTO, 
      (COUNT(DISTINCT(t1.rut_titular))) AS COTIZANTES,
      -- Cargas 
      (SUM(t1.num_cargas))  AS CARGAS,
      -- Total 
      SUM(t1.recaudado) AS TOTAL,
      -- pagado_total_pesos 
      (SUM(t1.pagado)) AS PAGADO,
      -- monto_exceso 
      (SUM(t1.excesos))  AS EXCESOS,
      -- monto_excedente
      (SUM(t1.Excedentes)) AS EXEDENTES,
      -- Cotizantes 
      (SUM(t1.costo_final))  AS costo_final,
      -- Cobrado     
      SUM(COBRADO) AS COBRADO,
      -- Bonificado
      SUM(BONIFICADO) AS BONIFICADO
FROM "EST"."P_STG_EST"."GASTOS_BEN" t1
where t1.periodo BETWEEN 201901 AND 202101
group by t1.periodo --, t1.DETALLE_PRODUCTO
order by 1-- ,2


--  Consulta por mes al Datamart Gastos
-- ==================================================
SELECT  --t1.cod_categoria,
      t1.periodo,
    -- t1.DETALLE_PRODUCTO, 
      (COUNT(DISTINCT(t1.rut_titular))) AS COTIZANTES,
      -- Cobrado     
      SUM(COBRADO) AS COBRADO,
      -- Bonificado
      SUM(BONIFICADO) AS BONIFICADO
FROM "DTM"."P_STG_DTM_GTO"."P_STG_DTM_GTO_HECHOS_GTO" t1
where t1.periodo BETWEEN 202001 AND 202101
group by t1.periodo --, t1.DETALLE_PRODUCTO
order by 1-- ,2

