CREATE OR REPLACE TABLE EST.P_STG_EST.GASTOS_BEN AS  
  SELECT
        t1.RUT_TITULAR,
        to_number(substr(t1.rut_titular,1,9))*5 - 3265112 + 52658 AS ID_TITULAR,
        NVL(t4."fld_benrut",t1.RUT_BENEFICIARIO) AS RUT_BENEFICIARIO,
        t1.COD_BENEFICIARIO AS COD_BENEFICIARIO,
        IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,'TITULAR','BENEFICIARIO') AS TIPO_RUT,       
        t1.periodo  AS PERIODO,
        substr(t1.periodo,1,4) AS ANNO,
        to_number(substr(t1.periodo,5,2)) AS MES,
        to_date(to_varchar(t17.FECHA_INGRESO,'YYYY-MM-DD')) AS FECHA_INGRESO,
        NVL(t1.ANTIGUEDAD,nvl(t17.ANTIGUEDAD,0)) AS GASTOS_ANTIGUEDAD,
        CASE T4."fld_bensexo" 
                WHEN 2 THEN 'F'
                ELSE 'M' 
              END AS SEXO,
        first_value(t1.ID) over (partition by t1.periodo, t1.RUT_TITULAR order by  t1.ID) as FIRST_ID,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID ,t17.CNT_CARFAM,0) as NUM_CARGAS,      
        NVL(t17.NUM_CONTRATO,t1.CONTRATO) AS CONTRATO,
        IFF(t17.RUT_TITULAR is null,'NO EXISTE','SI EXISTE') AS EXISTE_EN_CONTRATO,
      --  t17.NUM_CORRELATIVO AS CORRELATIVO,
        t1.CORRELATIVO AS CORRELATIVO, 
        t17.COD_REGION,
        t8.GLS_REGION AS REGION_GLS,       
        t1.COMUNATRABAJA AS COD_COMUNA,
        t7.GLS_COMUNA AS COMUNA_GLS,
        t11.UBICACION,       
        t1.SUCURSAL_CONTRATO AS COD_SUCURSAL,
        t2.GLS_SUCUR AS SUCURSAL_GLS,
        t11.COD_CENTROCOSTOS AS COD_CENTRO_COSTOS,
        t11.CENTROCOSTOS AS CENTRO_COSTOS_GLS,        
        to_date(to_varchar(t4."fld_bennacfec",'YYYY-MM-DD')) AS FECHA_NACIMIENTO,
        t4."fld_benedad" AS EDAD,        
        IFF(t1.ID = FIRST_ID,t10.NUM_EMPLEADORES,0) AS NUM_EMPLEADORES,        
        t17.COD_ACTIVIDAD	AS ACTIVIDAD,
        t17.TIPO_TRABAJADOR	AS TIPO_TRABAJADOR,
        t17.TIPO_TRANSACCION	AS TIPO_TRANSC,        
        t1.COD_CATEGORIA	AS CATEGORIA_COD,
        t3.CATEGORIA_GLS	AS CATEGORIA_GLS,       
        t5.SERIE,
        t5.TIPO_PLAN,
        t5.TIPO_PRODUCTO,
        t5.DETALLE_PRODUCTO,
        t5.LINEA_PLAN,        
        t6."fld_segurohasta" AS SEG_MUERTE,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.COSTO_FINAL_UF,0) AS COSTO_FINAL,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t6."fld_valorbase",0) AS PRECIO_BASE,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t4."fld_factor",0) AS FACTOR_RIESGO,       
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t6."fld_costototal",0) AS COSTO_TOTAL,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t6."fld_benefadic",0) AS COSTO_BENEF_ADIC,       
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.PAGADO_TOTAL_PESOS,0) AS PAGADO,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.MONTO_EXCESO,0) AS EXCESOS,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.MONTO_EXCEDENTE,0) AS EXCEDENTES,
        -- Obtener el numero de registro para seleccionar el primer registro y poder sumar
        row_number() over (partition by t1.periodo, t1.rut_titular, t1.ID order by t1.ID asc) REGISTRO_ID,
        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO<>3 and REGISTRO_ID=1,t1.BONIFICADO,0 ) AS GASTO_HOSPITALARIOS,
        IFF(T1.ORIGEN = 'AMBULATORIO' and T1.TIPO_BONO<>3 and REGISTRO_ID=1,BONIFICADO,0 ) AS GASTO_AMBULATORIOS,
        IFF(T1.ORIGEN = 'GES' and REGISTRO_ID=1,BONIFICADO,0 ) AS GASTO_GES,
        IFF(T1.ORIGEN = 'HOSPITALARIO' and T1.TIPO_BONO=3,BONIFICADO,0 ) AS CAEC_HOSPITALARIO,
        IFF(T1.ORIGEN = 'AMBULATORIO' and T1.TIPO_BONO=3,BONIFICADO,0 ) AS CAEC_AMBULATORIO,        
        IFF(REGISTRO_ID=1,NVL(MONTO_CAEC,0),0) AS GASTO_CAEC,        
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t17.GASTO_PHARMA_PESOS,0) AS GASTO_PHARMA,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,NVL(t16.RECUPERACION_GASTO,0),0) AS RECUPERACION_GASTO,       
        'pendiente' AS IVA_COTIZACIONES,
         IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,NVL(t15.IVA_RECUPERADO,0),0) AS IVA_RECUPERADO,     
         IFF(T1.ORIGEN = 'LICENCIA' and (tipo_licencia in (1,7)),IFF(MONTO_COTIZACION > MONTO_SALUD,
                                           monto_subsidio + monto_caja + monto_invalidez_sobr + monto_afp  + monto_cotizacion,
                                           monto_subsidio + monto_caja + monto_invalidez_sobr + monto_afp  + monto_salud ),0 ) AS GASTO_LICENCIAS,
        IFF(t1.ID = FIRST_ID, t10.RENTA_IMPONIBLE,0) AS RENTA, 
        CASE	
            WHEN RENTA > 1 and RENTA <=500000	
                THEN '>1 <= 500'
            WHEN RENTA > 500001 and RENTA <= 1000000	
                THEN '>500 y <= 1000'	
            WHEN RENTA > 1000001 and RENTA <=1500000
                THEN '>1000 y <= 1500'	
            WHEN RENTA > 1500001 and RENTA <= 2300000
                THEN '>1500 y <= 2300'	
            WHEN RENTA > 2300001
                THEN '> 2300'
            ELSE '' 
        END AS  RANGO_RENTA,
        t9.VAL_MON AS VALOR_UF,        
        t17.COD_AGENCIA AS AGENCIA_VN,
        t17.COD_AGENTE AS AGENTE_VN,
        t17.PERIODO_ANUALIDAD AS PERIODO_ANUAL,
        t17.CLASIFICACION AS CLASIF_RIESGO,
        t17.COD_MOROSIDAD AS CLASIF_MOROSIDAD,       
        case 
            when  t4."fld_benedad" between 0 and 19 then '< 20 Años'
            when  t4."fld_benedad" between 20 and 24 then '20 a 24 años'
            when  t4."fld_benedad" between 25 and 29 then '25 a 29 años'
            when  t4."fld_benedad" between 30 and 34 then '30 a 34 años'
            when  t4."fld_benedad" between 35 and 39 then '35 a 39 años'
            when  t4."fld_benedad" between 40 and 44 then '40 a 44 años'
            when  t4."fld_benedad" between 45 and 49 then '45 a 49 años'                  
            when  t4."fld_benedad" between 50 and 54 then '50 a 54 años'
            when  t4."fld_benedad" between 55 and 60 then '55 a 60 años'        
            when  t4."fld_benedad" > 60 then '> 60 y mas años'
            else ''
        end AS RANGO_EDAD,        
         first_value( t4."fld_benedad") over (partition by t1.periodo, t1.RUT_TITULAR order by  t4."fld_bencorrel") as EDAD_TITULAR,
         case 
            when  EDAD_TITULAR between 0 and 19 then '< 20 Años'
            when  EDAD_TITULAR between 20 and 24 then '20 a 24 años'
            when  EDAD_TITULAR between 25 and 29 then '25 a 29 años'
            when  EDAD_TITULAR between 30 and 34 then '30 a 34 años'
            when  EDAD_TITULAR between 35 and 39 then '35 a 39 años'
            when  EDAD_TITULAR between 40 and 44 then '40 a 44 años'
            when  EDAD_TITULAR between 45 and 49 then '45 a 49 años'                  
            when  EDAD_TITULAR between 50 and 54 then '50 a 54 años'
            when  EDAD_TITULAR between 55 and 60 then '55 a 60 años'        
            when  EDAD_TITULAR > 60 then '> 60 y mas años'
            else ''
        end AS RANGO_EDAD_TITULAR,   
        CASE	
          WHEN t17.COSTO_FINAL_UF > 0 and t17.COSTO_FINAL_UF <=2	
          THEN '>0 <= 2.0 UF'
          WHEN t17.COSTO_FINAL_UF > 2 and t17.COSTO_FINAL_UF <=3	
          THEN '>2.0 <= 3.0 UF'	
          WHEN t17.COSTO_FINAL_UF > 3 and t17.COSTO_FINAL_UF <=4	
          THEN '>3.0 <= 4.0 UF'	
          WHEN t17.COSTO_FINAL_UF > 4 and t17.COSTO_FINAL_UF <=5	
          THEN '>4.0 <= 5.0 UF'	
          WHEN t17.COSTO_FINAL_UF > 5	
          THEN '>5.0 U'
          ELSE ''
        END AS RANGO_COTIZACION,
        CASE
          WHEN GASTOS_ANTIGUEDAD <=12
          THEN '00 a 12 meses'
          WHEN GASTOS_ANTIGUEDAD >12 AND GASTOS_ANTIGUEDAD <=24
          THEN '>12 a 24 meses'
          WHEN GASTOS_ANTIGUEDAD >24 AND GASTOS_ANTIGUEDAD <=36
          THEN '>24 a 36 meses'
          WHEN GASTOS_ANTIGUEDAD >36 AND GASTOS_ANTIGUEDAD <=48
          THEN '>36 a 48 meses'
          WHEN GASTOS_ANTIGUEDAD >48 AND GASTOS_ANTIGUEDAD <=60
          THEN '>48 a 60 meses'
          WHEN GASTOS_ANTIGUEDAD >60
          THEN '>60' 
          ELSE ''        
        END AS RANGO_ANTIGUEDAD,       
        CASE
          WHEN t6."fld_valorbase" =0
          THEN 'igual a 0'
          WHEN t6."fld_valorbase" > 0.01 AND t6."fld_valorbase" <= 1.50
          THEN '0.01 - 1.50'
          WHEN t6."fld_valorbase" >1.51 AND t6."fld_valorbase" <= 2
          THEN '1.51 - 2.00'
          WHEN t6."fld_valorbase" >2.01 AND t6."fld_valorbase" <=3
          THEN '2.01 - 3.00'
          WHEN t6."fld_valorbase" >3.01 AND t6."fld_valorbase" <=4
          THEN '3.01 a 4.00'
          WHEN t6."fld_valorbase" >4.01 AND t6."fld_valorbase" <=5
          THEN '4.01 a 5.00'
          WHEN t6."fld_valorbase" >5.01 
          THEN '5.01 y más'
          ELSE ''
        END AS RANGO_PRECIO,
        t4."fld_bennombre" AS FLD_BENNOMBRE,
        t4."fld_benapepa" AS FLD_BENAPEPA, 
        t4."fld_benapema" AS FLD_BENAPEMA,       
        t6."fld_funfolio" AS FLD_FUNFOLIO,
        t6."fld_funcorrel" AS FLD_FUNCORREL,
        IFF((t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null) and t1.ID = FIRST_ID,t12.Valor_Iva,0) AS IVA,
        t17.COD_NEGOCIO,        
        t13.GLS_MODCOTZ AS TIPO_COTIZACION,
        IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,PAGADO - EXCESOS - EXCEDENTES,0) AS RECAUDADO,
        IFF(t4."fld_bencorrel"=0 or t4."fld_bencorrel" is null,PAGADO - EXCESOS - EXCEDENTES - IVA,0) AS INGRESOS,
        t1.FECHA_EMISION_PAGO,
        t1.ID,
        t1.FUENTE_ORIGEN,
        t1.ORIGEN,
        t1.TIPO_BONO,
        t1.GLS_TIPO_BONO,
        t1.COD_GRUPO,
        t1.GRUPO,
        t1.COD_SUBGRUPO,
        t1.SUBGRUPO,
        t1.COD_CIE,
        t1.GLOSA_CIE,
        t1.GRUPO_CIE,  
        t1.HOLDING_PRESTADOR,
        t1.RUT_INSTITUCION,
        t1.NOMBRE_INSTITUCION,
         IFF(t1.rut_institucion=t18."rut",t18."cod_prestad",t1.NOMBRE_INSTITUCION) AS PRESTADOR,
        IFF(REGISTRO_ID=1,nvl(t1.COBRADO,0),0) COBRADO,
        IFF(REGISTRO_ID=1,nvl(t1.BONIFICADO,0),0) BONIFICADO,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_SUBSIDIO,0),0) MONTO_SUBSIDIO,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_CAJA,0),0) MONTO_CAJA,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_INVALIDEZ_SOBR,0),0) MONTO_INVALIDEZ_SOBR,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_AFP,0),0) MONTO_AFP,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_COTIZACION,0),0) MONTO_COTIZACION,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_SALUD,0),0) MONTO_SALUD,
        IFF(REGISTRO_ID=1,nvl(t1.MONTO_SEGURO,0),0) MONTO_SEGURO,       
        t19.PERIODO_VIGENCIA AS Z_PERIODO, 
        t19.NUM_CONTRATO  AS Z_NUM_CONTRATO, 
        t19.NUM_CORRELATIVO AS Z_NUM_CORRELATIVO, 
        t19.rut_titular AS Z_RUT_TITULAR,
        t19."fld_cotrut" AS Z_FLD_COTRUT, 
        t19."fld_bencorrel" AS Z_FLD_BENCORREL,
        CASE t19."fld_bensexo" 
          WHEN 2 THEN 'F'
          ELSE 'M' 
        END AS Z_SEXO,  
        to_date(to_varchar(t19."fld_bennacfec",'YYYY-MM-DD')) AS Z_FECHA_NACIMIENTO,
        t19."fld_benedad" AS Z_EDAD,
        t19.COSTO_FINAL_UF AS Z_COSTO_FINAL_UF,
        t19."fld_bennombre" AS Z_FLD_BENNOMBRE,
        t19."fld_benapepa" AS Z_FLD_BENAPEPA, 
        t19."fld_benapema" AS Z_FLD_BENAPEMA    
     FROM "DTM"."P_STG_DTM_GTO"."P_STG_DTM_GTO_HECHOS_GTO" t1
        LEFT OUTER JOIN "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" t17
        ON (t1.periodo = t17.periodo_vigencia and t1.rut_titular = t17.rut_titular)
        -- Liga con Sucursal
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPRESUCURSAL" t2 ON
         (t17.COD_SUCURSAL = t2.COD_SUCUR)
        -- Liga con Categoria
        LEFT OUTER JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_CATEGORIA" t3
        ON (t1.cod_categoria = t3.cod_categoria)
        -- INICIO BEN Liga para agregar Nombre y Genero
        LEFT OUTER JOIN  "AFI"."P_RDV_DST_LND_SYB"."BEN" t4
        on (t1.RUT_TITULAR = t4."fld_cotrut"
           -- and t1.correlativo = t4."fld_funcorrel"
           and t1.cod_beneficiario = t4."fld_bencorrel"
            and t4.FLD_BENVIGREALDESDE <= t1.periodo AND t4.FLD_BENVIGREALHASTA >= t1.periodo
            )
        -- FIN BEN        
        -- Liga para agregar tipo_plan, detalle_producto
        LEFT OUTER JOIN "IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES" t5
        ON (t1.cod_categoria = t5."cod_categoria")        
        -- Liga para agregar datos de CNT
        LEFT OUTER JOIN "AFI"."P_RDV_DST_LND_SYB"."CNT" t6
        ON (t1.RUT_TITULAR = t6."fld_cotrut" and
            t6.FLD_VIGREALDESDE <= t1.periodo AND t6.FLD_VIGREALHASTA >= t1.periodo
           )
        -- Liga para agregar COMUNA       
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPRECOMUNAS" t7
        on (t1.COMUNATRABAJA = t7.COD_COMUNA )
        -- Liga para agregar Region
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPREREGION" t8
        on (t17.COD_REGION = t8.COD_REGION )      
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPREMONEDA" t9
        on (t1.periodo = to_number(to_char(dateadd(month, 1, fec_val),'YYYYMM')))  
        -- Liga para obtener la renta_imponible
        -- Inicio Renta Imponible
          LEFT OUTER JOIN
          (
            SELECT s4.periodo, s4.rut_titular, s4.id, sum(s4."fld_emprut") NUM_EMPLEADORES, sum(s4."fld_ultrenta") RENTA_IMPONIBLE
            from  (
                  SELECT 
                     s1.periodo,
                     s1.rut_titular, 
                     s1.id,   
                     s3."fld_emprut",
                     s3."fld_ultrenta" 
                  FROM 
                      "DTM"."P_STG_DTM_GTO"."P_STG_DTM_GTO_HECHOS_GTO" s1 
                      LEFT OUTER JOIN "AFI"."P_RDV_DST_LND_SYB"."CNT" s2 
                      ON (
                          s1.RUT_TITULAR = s2."fld_cotrut" 
                          AND s2.FLD_VIGREALDESDE <= s1.periodo 
                          AND s2.FLD_VIGREALHASTA >= s1.periodo
                      )   
                      LEFT OUTER JOIN (  
                              SELECT "fld_funnotiffun", "fld_funcorrel", 
                                      count("fld_emprut") "fld_emprut", 
                                      FLD_COBVIGREALDESDE, 
                                      FLD_COBVIGREALHASTA, 
                                      sum("fld_ultrenta") "fld_ultrenta"
                              FROM  "AFI"."P_RDV_DST_LND_SYB"."COB" 
                              GROUP  BY  "fld_funnotiffun", "fld_funcorrel", FLD_COBVIGREALDESDE, FLD_COBVIGREALHASTA
                      ) s3 
                      ON (
                                s2."fld_funfolio"= s3."fld_funnotiffun" 
                               AND s2."fld_funcorrel" = s3."fld_funcorrel"
                               and s3.FLD_COBVIGREALDESDE <= s1.periodo AND s3.FLD_COBVIGREALHASTA >= s1.periodo
                      )
                      WHERE s1.periodo BETWEEN 202001 AND 202012
             ) s4    
            group by  s4.periodo, s4.rut_titular, s4.id
          ) t10
          on (t1.periodo = t10.periodo 
              and t1.rut_titular = t10.rut_titular
              and t1.id = t10.id)
        -- Fin Renta Imponible
        -- Liga con Sucursal para obtener centro de costos y ubicacion
        LEFT OUTER JOIN "ISA"."P_DDV_ISA"."P_DDV_ISA_V_PRM_SUCURSAL2" t11
            ON         (t1.SUCURSAL_CONTRATO = t11.cod_sucursal)
        -- Liga para obtener el iva   -- se agrega minuto tiempo corrida, al agregar la columna
        LEFT OUTER JOIN (select perpag_esperado, rut_cotizante, sum( Valor_Iva) as  Valor_Iva
                        from "RCD"."P_DDV_RCD"."P_DDV_RCD_V_LIBRO_VENTAS"
                        where  Codigo_Pago = 0
                        group by perpag_esperado, rut_cotizante) t12
            ON  (t1.periodo = t12.perpag_esperado 
                 and t1.rut_titular = rut_cotizante )                
        -- Liga para agregar GLS_MODCOTZ - Tipo de Cotizacion
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."ISAPREMODCOTZ" t13
            ON (t6."fld_monedatipocod" = t13.COD_MODCOTZ)            
        -- Liga para agregar Iva Recuperado
        LEFT OUTER JOIN (   
              SELECT t1.PERIODO,
                  t1.RUT AS RUT_TITULAR,
                  SUM(t1.monto_credito_fiscal) AS IVA_RECUPERADO
              FROM "CON"."P_DDV_CON"."P_DDV_CON_IVA_RECUPERADO_BONOS" t1                         
              GROUP BY t1.periodo, t1.rut          
        ) t15
            ON (t1.periodo = t15.periodo and
                t1.rut_titular = t15.rut_titular)  
        -- Liga para agregar recuperacion del gasto
        LEFT OUTER JOIN (
            SELECT t1."periodo_vig" AS PERIODO,t1."rut_titular" AS RUT_TITULAR,SUM(t1."monto_recuperado") RECUPERACION_GASTO   
            FROM "IQ"."P_DDV_IQ"."RECUPERACION_GASTO" t1
            GROUP BY t1."periodo_vig", t1."rut_titular"
       ) t16
          ON (t1.periodo = t16.periodo and
                t1.rut_titular = t16.rut_titular)  
        --Liga para agregar el Prestador
        LEFT OUTER JOIN "ISA"."P_RDV_DST_LND_SYB"."PRM_AGRUPA_PRESTADORES" t18
        on (t1.rut_institucion = t18."rut")       
        -- Ligar contrato para traer valores vacios de otros periodos
        -- Inicio para Contratos           
        LEFT OUTER JOIN (
          SELECT  s1.PERIODO_VIGENCIA, 
                s1.NUM_CONTRATO, 
                s1.NUM_CORRELATIVO, 
                s1.rut_titular,
                s4."fld_cotrut", 
                s4."fld_bencorrel",
                s4."fld_bensexo",  
                s4."fld_bennacfec",
                s4."fld_benedad",
                s1.COSTO_FINAL_UF,
                s4."fld_bennombre",
                s4."fld_benapepa", 
                s4."fld_benapema"    
        from (
              ( 
                 SELECT r1.PERIODO_VIGENCIA, r1.NUM_CONTRATO, r1.NUM_CORRELATIVO, r1.RUT_TITULAR,
                       r2.FECHA_INGRESO, r2.COD_REGION, r2.COD_ACTIVIDAD, r2.TIPO_TRABAJADOR, r2.TIPO_TRANSACCION, 
                       r2.COSTO_FINAL_UF, r2.PAGADO_TOTAL_PESOS, r2.MONTO_EXCESO,r2.MONTO_EXCEDENTE, r2.GASTO_PHARMA_PESOS, 
                       r2.COD_AGENCIA, r2.COD_AGENTE, r2.PERIODO_ANUALIDAD, r2.CLASIFICACION, r2.COD_MOROSIDAD, r2.COD_NEGOCIO
                FROM
                    ( SELECT  MAX(s1.PERIODO_VIGENCIA) AS PERIODO_VIGENCIA, s1.NUM_CONTRATO, s1.NUM_CORRELATIVO, s1.RUT_TITULAR
                        from "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" s1  
                        GROUP BY s1.NUM_CONTRATO, s1.NUM_CORRELATIVO, s1.RUT_TITULAR 
                    ) r1
                      LEFT OUTER JOIN "AFI"."P_DDV_AFI"."P_DDV_AFI_CONTRATO" r2
                      ON (r1.PERIODO_VIGENCIA = r2.PERIODO_VIGENCIA
                            AND r1.NUM_CONTRATO  = r2.NUM_CONTRATO
                            AND r1.NUM_CORRELATIVO = r2.NUM_CORRELATIVO
                            AND r1.RUT_TITULAR = r2.RUT_TITULAR)
               ) s1
                LEFT OUTER JOIN  "AFI"."P_RDV_DST_LND_SYB"."BEN" s4
                    --fld_bencorrel
                on (s1.RUT_TITULAR = s4."fld_cotrut"
                    and s1.num_correlativo = s4."fld_funcorrel"
                    and s4.FLD_BENVIGREALDESDE <= s1.periodo_vigencia AND s4.FLD_BENVIGREALHASTA >= s1.periodo_vigencia 
                    )
                ) 
        ) t19
        on (t1.rut_titular = t19.rut_titular
            and t1.correlativo = t19.NUM_CORRELATIVO
            and t1.cod_beneficiario = t19."fld_bencorrel"
            and t1.CONTRATO = t19.NUM_CONTRATO
           )            
        -- Fin para Contratos        
    WHERE    t1.periodo BETWEEN  
            to_number(to_char(dateadd(month,-12,"EST"."P_STG_EST".CONTRATO_MAX_PERIDO()),'YYYYMM'))  AND 
            to_number(to_char("EST"."P_STG_EST".CONTRATO_MAX_PERIDO(),'YYYYMM'))
   
  
