create or replace table EST.P_STG_EST.GASTOS_HAGC
as
SELECT to_number(t1.periodo) periodo,
       t1.DETALLE_PRODUCTO,
       t1.rutafi,
       t1.codben,
       sum(HOSPITALARIO) as HOSPITALARIO,
       sum(AMBULATORIO) AS AMBULATORIO,
       sum(GES) AS GES,
       sum(CAEC) AS CAEC
FROM 
  ( SELECT t1.periodo,
     --   t1.codcateg  AS cod_categoria,
        t3.DETALLE_PRODUCTO AS DETALLE_PRODUCTO, 
        -- MONTO_ 
        t1.rutafi,
        t1.codben,
         t1.tipo, 
         t1.gls_tipo,
         t1.tipoprestacion,      
        -- tipo_prestador 
        (CASE 
                WHEN t1.tipo = 1 and CONTAINS(t1.tipoprestacion,'A')
                THEN 'AMBULATORIO'
                WHEN  t1.tipo = 2 and (CONTAINS(t1.tipoprestacion, 'GM') OR CONTAINS (t1.tipoprestacion,'GD') OR CONTAINS(t1.tipoprestacion,'GF')) 
                THEN 'GES'
                WHEN t1.tipo= 3
                THEN 'CAEC'
                WHEN t1.tipo= 4 and CONTAINS(t1.tipoprestacion,'GC') 
                THEN 'CAEC'
                WHEN  t1.tipo=5 and CONTAINS ( t1.tipoprestacion,'A') 
                THEN 'AMBULATORIO'
                WHEN  t1.tipo=5 and CONTAINS ( t1.tipoprestacion,'GF') 
                THEN 'GES'              
                WHEN  t1.tipo=1 and (CONTAINS ( t1.tipoprestacion,'H') OR CONTAINS(t1.tipoprestacion,'M'))
                THEN 'HOSPITALARIO'
                ELSE ''
            END) AS tipo_prestador,
           SUM(t1.monto) AS MONTO,
           SUM(t1.mtobonificado) AS MTOBONIFICADO,
           SUM(t1.monto_caec) AS MONTO_CAEC,
           SUM(t1.bonificado_puro) AS BONIFICADO_PURO,
           IFF(tipo_prestador = 'HOSPITALARIO',sum(MTOBONIFICADO),0 ) HOSPITALARIO,
           IFF(tipo_prestador = 'AMBULATORIO',sum(MTOBONIFICADO),0 ) AMBULATORIO,
           IFF(tipo_prestador = 'GES',sum(MTOBONIFICADO),0 ) GES,
           IFF(tipo_prestador = 'CAEC',sum(MTOBONIFICADO),0 ) CAEC          
    FROM "GTO"."P_DDV_GTO"."P_DDV_GTO_VGASTOEMISIONBONOS" t1  
      LEFT OUTER JOIN "IQ"."P_DDV_IQ"."PRM_AGRUPA_PLANES"  t3
      ON (t1.CODCATEG = t3."cod_categoria")      
    WHERE t1.periodo BETWEEN 201901 AND 202101
    GROUP BY t1.PERIODO,
             --  t1.CODCATEG,
             t3.DETALLE_PRODUCTO,
             t1.rutafi,
             t1.codben,
             t1.tipo,
             t1.gls_tipo,
             t1.tipoprestacion,
               CASE 
               -- WHEN t1.tipoprestacion = 'A20'
               -- THEN 'HOSPITALARIO'
                WHEN t1.tipo = 1 and CONTAINS(t1.tipoprestacion,'A')
                THEN 'AMBULATORIO'
                WHEN  t1.tipo = 2 and (CONTAINS(t1.tipoprestacion, 'GM') OR CONTAINS (t1.tipoprestacion,'GD') OR CONTAINS(t1.tipoprestacion,'GF')) 
                THEN 'GES'
                WHEN t1.tipo= 3
                THEN 'CAEC'
                WHEN t1.tipo= 4 and CONTAINS(t1.tipoprestacion,'GC') 
                THEN 'CAEC'
                WHEN  t1.tipo=5 and CONTAINS ( t1.tipoprestacion,'A') 
                THEN 'AMBULATORIO'
                WHEN  t1.tipo=5 and CONTAINS ( t1.tipoprestacion,'GF') 
                THEN 'GES'              
                WHEN  t1.tipo=1 and (CONTAINS ( t1.tipoprestacion,'H') OR CONTAINS(t1.tipoprestacion,'M'))
                THEN 'HOSPITALARIO'
                ELSE ''
            END
   order by t1.tipo, t1.tipoprestacion
)  t1      
GROUP BY  t1.periodo,t1.detalle_producto, t1.rutafi,t1.codben --, t1.tipo_prestador

