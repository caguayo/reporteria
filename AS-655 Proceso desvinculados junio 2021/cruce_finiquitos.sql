
WITH cnt_vig AS (
	SELECT DISTINCT 
		"fld_cotrut" , 
		"fld_funfolio" ,
		"fld_funcorrel" 
	FROM afi.P_RDV_DST_LND_SYB.cnt
	WHERE  202103 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA 
), union_ AS ( 
	SELECT DISTINCT 
		*
	FROM est.P_DDV_EST.FINIQUITADOS_ABRIL fq
	LEFT JOIN  cnt_vig vig  ON (fq.RutTitular= vig."fld_cotrut")
), 
 ultimo_pago AS ( 
	SELECT 
		"pag_funfol", 
		"pag_rutemp", 
		"pag_funcor",
		"pag_rutpag",
		max("pag_percot") AS percot_maximo ,
		MAX("pag_pagfec")::datetime AS ultima_fecpag 
	FROM rcd.P_DDV_RCD.P_DDV_RCD_V_PAG 
	WHERE "pag_pagfec" >= '2020-11-01 00:00:00' AND "pag_rutpag" NOT IN ('000000000-0')
	--WHERE "pag_rutemp" = '078921690-8'
	--AND "pag_funfol" = 5241619 AND "pag_funcor" = 1496 
	GROUP BY "pag_funfol", "pag_rutemp", "pag_funcor", "pag_rutpag"
	--ORDER BY "pag_pagfec" desc´
)
SELECT DISTINCT 
	cnt."fld_funcorrel",
	cnt."fld_codneg" ,
	'202103' AS per_vig,
	cnt."fld_cotrut" ,
	cnt."fld_cotapepa" ,
	cnt."fld_cotapema" ,
	cnt."fld_cotnombre",
	cnt."fld_funfolio",
	cnt."fld_planvig" ,
	cnt."fld_glscateg",	
	uni."Rut avisa finiquito",
	uni."RAZON SOCIAL FINIQUITO",
	neg.AGRUPACION_GLS ,
	neg.AGRUPACION ,
	cnt."fld_agecod",
	cob.cant_empleadores,
	--est.P_DDV_EST.fecha_periodo(up.ultima_fecpag) AS per_pago,
	year(up.ultima_fecpag)*100+MONTH(up.ultima_fecpag)::int AS per_pago,
	--up.ultima_fecpag,
	up."pag_rutemp",
	emp_cob."fld_emprazsoc" AS razon_social,
	up."pag_rutpag",	
	emp_pag."fld_emprazsoc",
	CASE 
		WHEN cnt."fld_compensacion" = 0 THEN 'N'
		ELSE 'S'
	END AS compensados_gls,
	CASE 
		WHEN LENGTH(cnt."fld_funanufec") = 1 THEN CONCAT(cnt."fld_anoanualidad",'0',cnt."fld_funanufec")
		ELSE CONCAT(cnt."fld_anoanualidad", cnt."fld_funanufec")
	END AS anualidad,
	'202102' AS per_cot,
	lfc."fld_tpotransac",
	lfc."fld_funnotificacod",
	--est.P_DDV_EST.fecha_periodo(lfc."fld_periodoprod") AS per_produccion
	year(lfc."fld_periodoprod")*100+MONTH(lfc."fld_periodoprod")::int AS per_produccion
FROM union_ uni
	---- detalles de los contratos de la cnt
	INNER JOIN afi.P_RDV_DST_LND_SYB.cnt cnt ON (uni.RUTTITULAR = cnt."fld_cotrut" AND  uni."fld_funfolio" = cnt."fld_funfolio" AND uni."fld_funcorrel" = cnt."fld_funcorrel" AND 202105 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA )
	---- cantidad de empleadores
	INNER JOIN 
			(SELECT DISTINCT 
					"fld_funnotiffun",
					"fld_funcorrel" ,
					COUNT(DISTINCT "fld_emprut") AS cant_empleadores
			 FROM afi.P_RDV_DST_LND_SYB.cob
			 GROUP BY 
			 		"fld_funnotiffun",
					"fld_funcorrel" ) cob ON (uni."fld_funfolio" = cob."fld_funnotiffun" AND uni."fld_funcorrel" = cob."fld_funcorrel")
	----- detalle de los codigos de negocios
	LEFT JOIN isa.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO_TOTAL neg ON (cnt."fld_codneg"= neg.COD_NEGOCIO)
	----- detalle de los empledares pagadores
	LEFT JOIN ultimo_pago up ON (uni."fld_funfolio" = up."pag_funfol" AND uni."fld_funcorrel" = up."pag_funcor")
	----- detalle lfc
	LEFT JOIN afi.P_RDV_DST_LND_SYB.LFC lfc ON (uni."fld_funfolio" = lfc."fld_funfolio" AND uni."fld_funcorrel" = lfc."fld_funcorrel" AND uni."Rut avisa finiquito" = lfc."fld_emprut")
	----- empleador cobrado
	LEFT JOIN isa.P_RDV_DST_LND_SYB.EMP emp_cob ON (up."pag_rutemp" = emp_cob."fld_emprut")
	----- empleador pagador
	LEFT JOIN isa.P_RDV_DST_LND_SYB.EMP emp_pag ON (up."pag_rutpag" = emp_pag."fld_emprut")
WHERE uni."fld_cotrut" IS NOT NULL AND  CONTAINS (cnt."fld_planvig",'E1' ) --AND  cnt."fld_cotrut"  = '006068361-1'
ORDER BY "fld_cotrut" 






SELECT top 10 * FROM EST.P_DDV_EST.FINIQUITADOS_ABRIL 




SELECT top 10 * FROM GTO.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM 


SELECT top 10 * FROM 



SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.lfc
WHERE "fld_cotrut" = '006068361-1' AND  "fld_funcorrel" = 313
--"fld_funfolio" = 5241619 



SELECT top 10 * FROM isa.P_RDV_DST_LND_SYB.FUN
WHERE "fld_cotrut" = '017625947-7'




SELECT top 10 * FROM  rcd.P_DDV_RCD.P_DDV_RCD_V_PAG 
WHERE "pag_funfol" =5240949 AND "pag_funcor" =2184



SELECT top 10 "fld_codneg" ,"fld_agecod" , * FROM afi.P_RDV_DST_LND_SYB.cnt
WHERE  202106 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA AND "fld_cotrut" = '017625947-7'



SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.COB 
WHERE "fld_funnotiffun" IN (5241619) AND "fld_funcorrel" = 1496

"fld_emprut" IN ('096572360-9')



SELECT top 10 * FROM isa.P_RDV_DST_LND_SYB.EMP_COB 
WHERE "fld_emprut" ='076952550-5'



SELECT top 10 * FROM isa.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO_TOTAL
WHERE agrupacion = '097004000-5'



SELECT top 10 * FROM isa.P_DDV_ISA.P_DDV_ISA_PRM_NEGOCIO
WHERE agrupacion = '089862200-2'



SELECT top 10 * FROM isa.P_RDV_DST_LND_SYB.PSC
WHERE "fld_pscrut"  = '076952550-5'


SELECT top 10 * FROM isa.P_RDV_DST_LND_SYB.EMP
WHERE "fld_emprut" = '076952550-5'



SELECT top 10 * FROM emple



SELECT 
	CASE 
		WHEN ultima_vig IS NULL THEN 'nulo'
		ELSE 'ok'
	END AS revision,
	COUNT(1) AS contar 
	FROM union_
GROUP BY revision


SELECT * FROM union_


SELECT top 10 * FROM est.P_DDV_EST.FINIQUITADOS_JUNIO
WHERE raz "Raz�n Social" 

DROP TABLE est.P_DDV_EST.FINIQUITADOS_JUNIO



SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.COB 
WHERE "fld_emprut" IN ('096572360-9')




WITH ultimo_pago AS ( 
	SELECT 
		"pag_funfol", 
		"pag_rutemp", 
		"pag_funcor",
		"pag_rutpag",
		max("pag_percot") AS percot_maximo ,
		MAX("pag_pagfec") AS ultima_fecpag 
	FROM rcd.P_DDV_RCD.P_DDV_RCD_V_PAG 
	WHERE "pag_pagfec" >= '2021-01-01 00:00:00' AND "pag_rutpag" NOT IN ('000000000-0')
	--WHERE "pag_rutemp" = '078921690-8'
	--AND "pag_funfol" = 5241619 AND "pag_funcor" = 1496 
	GROUP BY "pag_funfol", "pag_rutemp", "pag_funcor", "pag_rutpag"
	--ORDER BY "pag_pagfec" desc´
)
SELECT  * FROM ultimo_pago
WHERE "pag_funfol" = 5241619-- AND "pag_funcor" = 1908
ORDER BY "pag_funcor"

