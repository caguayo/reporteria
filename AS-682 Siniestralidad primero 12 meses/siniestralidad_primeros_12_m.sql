WITH personas_1 AS ( 
SELECT DISTINCT 
	a.PERIODO_VIGENCIA AS PERIODO,
	a.RUT_TITULAR ,
	a.COD_CATEGORIA ,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO a
INNER JOIN (
			SELECT 
				"codigo" AS CODIGO
			FROM ISA.P_RDV_DST_LND_SYB.SERIES_CATEG
			WHERE "grupo" <> 11
			) b ON (a.COD_CATEGORIA=b.CODIGO) 
WHERE PERIODO BETWEEN 201808 AND 201809
)
--SELECT * FROM personas_1 LIMIT 10
, ingresos AS ( 
SELECT DISTINCT 
	PERIODO_VIGENCIA AS PERIODO,
	RUT_TITULAR ,
	--count(DISTINCT RUT_TITULAR) AS contar_rut,
	SUM(PAGADO_TOTAL_UF) AS PAGADO_TOTAL_UF ,
	SUM(PAGADO_TOTAL_PESOS) AS PAGADO_TOTAL_PESOS,
	SUM(MONTO_EXCESO) AS MONTO_EXCESO,
	SUM(MONTO_EXCEDENTE) AS MONTO_EXCEDENTE ,
	SUM(GASTO_PHARMA_PESOS) AS GASTO_PHARMA_PESOS,
	SUM(COSTO_FINAL_UF) AS COSTO_FINAL_UF,
	SUM(COSTO_FINAL_PESOS) AS COSTO_FINAL_PESOS 
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO BETWEEN  201808 AND 201809
GROUP BY PERIODO, RUT_TITULAR 
)
SELECT 
	a.PERIODO,
	COUNT(DISTINCT a.RUT_TITULAR) AS CANT_RUT,
	SUM(PAGADO_TOTAL_UF) AS PAGADO_TOTAL_UF ,
	SUM(PAGADO_TOTAL_PESOS) AS PAGADO_TOTAL_PESOS,
	SUM(MONTO_EXCESO) AS MONTO_EXCESO,
	SUM(MONTO_EXCEDENTE) AS MONTO_EXCEDENTE ,
	SUM(GASTO_PHARMA_PESOS) AS GASTO_PHARMA_PESOS,
	SUM(COSTO_FINAL_UF) AS COSTO_FINAL_UF,
	SUM(COSTO_FINAL_PESOS) AS COSTO_FINAL_PESOS 
FROM personas_1 a 
INNER JOIN ingresos b ON (a.RUT_TITULAR=b.RUT_TITULAR AND a.PERIODO=b.PERIODO)
GROUP BY a.PERIODO
LIMIT 10






SELECT * FROM EST.P_DDV_EST.SINIES_12_MESES LIMIT 10





SELECT 
	--ANIO,
	--ANNIO,
	--PERIODO_INGRESO,
	--PERIODO_MIN_VIG,
	--PERIODO_DOCE_MESES,
	PERIODO_MES,
	--RESTA_MES,
	VAL_MON::DOUBLE AS VAL_MON,
	COUNT(DISTINCT RUT_TITULAR) AS CUENTA_RUT,
	SUM(COSTO_FINAL_UF)::INT AS COSTO_FINAL_UF,
	SUM(PAGADO_TOTAL_PESOS)::INT AS PAGADO_TOTAL_PESOS,
	SUM(MONTO_EXCESO)::INT AS MONTO_EXCESO,
	SUM(MONTO_EXCEDENTE)::INT AS MONTO_EXCEDENTE,
	SUM(VALOR_IVA)::INT AS VALOR_IVA,
	SUM(GASTO_PHARMA_PESOS)::INT AS GASTO_PHARMA_PESOS,
	SUM(BONIFICADO)::INT AS BONIFICADO,
	SUM(MONTO_CAEC)::INT AS MONTO_CAEC,
	SUM(MONTO_CREDITO_FISCAL)::INT AS MONTO_CREDITO_FISCAL
FROM EST.P_DDV_EST.SINIES_12_MESES_TEST_02
--WHERE  ANIO IS NOT NULL 
GROUP BY 
		ANNIO,
		PERIODO_INGRESO,
		PERIODO_MIN_VIG,
		PERIODO_DOCE_MESES,
		PERIODO_MES,
		RESTA_MES,
		VAL_MON
ORDER BY PERIODO_INGRESO , PERIODO_DOCE_MESES, PERIODO_MES, RESTA_MES
--LIMIT 10



SELECT 
	--PERIODO,
	--VAL_MON::DOUBLE AS VAL_MON,
	COUNT(DISTINCT RUT_TITULAR) AS CUENTA_RUT,
	SUM(COSTO_FINAL_UF)::INT AS COSTO_FINAL_UF,
	SUM(PAGADO_TOTAL_PESOS)::INT AS PAGADO_TOTAL_PESOS,
	SUM(MONTO_EXCESO)::INT AS MONTO_EXCESO,
	SUM(MONTO_EXCEDENTE)::INT AS MONTO_EXCEDENTE,
	SUM(VALOR_IVA)::INT AS VALOR_IVA,
	SUM(GASTO_PHARMA_PESOS)::INT AS GASTO_PHARMA_PESOS,
	SUM(BONIFICADO)::INT AS BONIFICADO,
	SUM(MONTO_CAEC)::INT AS MONTO_CAEC,
	SUM(MONTO_CREDITO_FISCAL)::INT AS MONTO_CREDITO_FISCAL
FROM EST.P_DDV_EST.SINIES_12_MESES_TEST_02
WHERE  PERIODO BETWEEN 201901 AND 201912
--GROUP BY 	PERIODO
--ORDER BY PERIODO_INGRESO , PERIODO_DOCE_MESES, PERIODO_MES, RESTA_MES






SELECT 
	* 
FROM EST.P_DDV_EST.SINIES_12_MESES_TEST_02
LIMIT 10


-----------------------------------------
-----------------------------------------
-----------------------------------------

--- CAMBIANDO A QUERY MARIELA
---------------------------------------------------------------------------------
CREATE OR REPLACE  TABLE EST.P_DDV_EST.SINIES_12_MESES_TEST_02 AS (
WITH base_personas AS ( 
SELECT DISTINCT 
	PERIODO AS PERIODO ,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT AS RUT_TITULAR,
	CODCATEG AS COD_CATEGORIA,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS
WHERE PERIODO >= 201701
UNION 
SELECT DISTINCT 
	PERPAG_ESPERADO AS PERIODO,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT_COTIZANTE AS RUT_TITULAR,
	COD_CATEGORIA ,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_LIBRO_VENTAS
WHERE PERIODO >= 201701 AND CODIGO_PAGO = 0
UNION 
SELECT DISTINCT 
	PERIODO_VIGENCIA AS PERIODO,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT_TITULAR ,
	COD_CATEGORIA ,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO >= 201701
UNION 
SELECT DISTINCT 
	PERIODO AS PERIODO ,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT_TITULAR ,
	COD_CATEGORIA ,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO >= 201701
UNION 
SELECT DISTINCT 
	"periodo_vig" AS PERIODO,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	"rut_titular" AS RUT_TITULAR,
	"cod_categoria" AS COD_CATEGORIA,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM FIN.P_DDV_FIN.P_DDV_FIN_RECUPERACION_GASTO_NEW
WHERE PERIODO >= 201701
)
, personas_unicas AS ( 
SELECT DISTINCT 
	PERIODO AS PERIODO ,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT_TITULAR,
	ANIO
FROM base_personas
)
, gastos AS ( 
SELECT 
	PERIODO AS PERIODO ,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT_TITULAR ,
	sum(BONIFICADO) AS BONIFICADO,
	sum(MONTO_CAEC) AS MONTO_CAEC
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO >= 201701
GROUP BY PERIODO , RUT_TITULAR 
)
, iva_recu AS ( 
SELECT  
	PERIODO AS PERIODO ,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT AS RUT_TITULAR,
	sum(MONTO_CREDITO_FISCAL) AS MONTO_CREDITO_FISCAL
FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS
WHERE PERIODO >= 201701
GROUP BY PERIODO, RUT_TITULAR
)
, rebate AS ( 
SELECT DISTINCT 
	"periodo_vig" AS PERIODO,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	"rut_titular" AS RUT_TITULAR,
	sum("monto_recuperado") AS MONTO_RECUPERADO
FROM FIN.P_DDV_FIN.P_DDV_FIN_RECUPERACION_GASTO_NEW
WHERE PERIODO >= 201701
GROUP BY PERIODO, RUT_TITULAR
)
, ingresos AS ( 
SELECT DISTINCT 
	PERIODO_VIGENCIA AS PERIODO,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT_TITULAR ,
	SUM(PAGADO_TOTAL_UF) AS PAGADO_TOTAL_UF ,
	SUM(PAGADO_TOTAL_PESOS) AS PAGADO_TOTAL_PESOS,
	SUM(MONTO_EXCESO) AS MONTO_EXCESO,
	SUM(MONTO_EXCEDENTE) AS MONTO_EXCEDENTE ,
	SUM(GASTO_PHARMA_PESOS) AS GASTO_PHARMA_PESOS,
	SUM(COSTO_FINAL_UF) AS COSTO_FINAL_UF,
	SUM(COSTO_FINAL_PESOS) AS COSTO_FINAL_PESOS 
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO >= 201701
GROUP BY PERIODO, RUT_TITULAR 
)
, iva_recau AS ( 
SELECT DISTINCT 
	PERPAG_ESPERADO AS PERIODO,
	EST.P_DDV_EST.PERIODO_FECHA(PERIODO) AS FECHA_PERIODO,
	RUT_COTIZANTE AS RUT_TITULAR,
	sum(VALOR_IVA) AS VALOR_IVA
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_LIBRO_VENTAS
WHERE PERIODO >= 201701 AND CODIGO_PAGO = 0
GROUP BY PERIODO, RUT_TITULAR
)
, agrupa_todo AS ( 
SELECT  
	bp.ANIO,
	bp.PERIODO,
	bp.FECHA_PERIODO,
	bp.RUT_TITULAR,
	g.BONIFICADO,
	g.MONTO_CAEC,
	icu.MONTO_CREDITO_FISCAL,
	icau.VALOR_IVA,
	re.MONTO_RECUPERADO,
	ing.PAGADO_TOTAL_UF ,
	ing.PAGADO_TOTAL_PESOS,
	ing.MONTO_EXCESO,
	ing.MONTO_EXCEDENTE ,
	ing.GASTO_PHARMA_PESOS,
	ing.COSTO_FINAL_UF,
	ing.COSTO_FINAL_PESOS 
FROM personas_unicas bp 
LEFT JOIN gastos 	g 		ON (bp.RUT_TITULAR = g.RUT_TITULAR AND bp.PERIODO = g.PERIODO)
LEFT JOIN iva_recu  icu	ON (bp.RUT_TITULAR = icu.RUT_TITULAR AND bp.PERIODO = icu.PERIODO)
LEFT JOIN iva_recau icau 	ON (bp.RUT_TITULAR = icau.RUT_TITULAR AND bp.PERIODO = icau.PERIODO)
LEFT JOIN rebate 	re		ON (bp.RUT_TITULAR = re.RUT_TITULAR AND bp.PERIODO = re.PERIODO)
LEFT JOIN ingresos  ing 	ON (bp.RUT_TITULAR = ing.RUT_TITULAR AND bp.PERIODO = ing.PERIODO)
)
/*SELECT
	ANIO,
	PERIODO ,
	sum(PAGADO_TOTAL_PESOS),
	COUNT(DISTINCT RUT_TITULAR)
FROM agrupa_todo
WHERE PERIODO BETWEEN 201901 AND 219012 --ANIO = 2019 --
GROUP BY ANIO , PERIODO 
LIMIT 100
*/
SELECT DISTINCT 
	ag.ANIO,
	SUBSTRING(per.PERIODO_INGRESO,0,4) AS ANNIO , 
	ag.PERIODO::INT AS PERIODO ,
	--EST.P_DDV_EST.PERIODO_FECHA(ag.PERIODO) AS FECHA_PERIODO ,
	ag.FECHA_PERIODO,
	ag.RUT_TITULAR ,
	per.PERIODO_INGRESO::int AS PERIODO_INGRESO ,
	--EST.P_DDV_EST.PERIODO_FECHA(PERIODO_INGRESO::int) AS FECHA_PERIODO_INGRESO ,
	--EST.P_DDV_EST.PERIODO_FECHA(IFNULL(PERIODO_INGRESO,'1900-01-01')) AS FECHA_PERIODO_INGRESO,
	CASE 
		WHEN per.PERIODO_INGRESO IS NULL THEN '1900-01-01'
		ELSE EST.P_DDV_EST.PERIODO_FECHA(PERIODO_INGRESO::int)
	END FECHA_PERIODO_INGRESO,
	per.PERIODO_MIN_VIG::INT AS PERIODO_MIN_VIG ,
	per.PERIODO_DOCE_MESES::INT AS PERIODO_DOCE_MESES ,
	uf.VAL_MON::DOUBLE AS VAL_MON ,
	per.RESTA_MESES::INT AS RESTA_MES ,
	per.PERIODO_MES::INT AS PERIODO_MES ,
	IFNULL(ag.BONIFICADO,0)::INT AS BONIFICADO ,
	IFNULL(ag.MONTO_CAEC,0)::INT AS MONTO_CAEC ,
	IFNULL(ag.MONTO_CREDITO_FISCAL,0)::INT AS MONTO_CREDITO_FISCAL ,
	IFNULL(ag.VALOR_IVA,0)::DOUBLE AS VALOR_IVA ,
	IFNULL(ag.MONTO_RECUPERADO,0)::INT AS MONTO_RECUPERADO ,
	IFNULL(ag.PAGADO_TOTAL_UF,0)::DOUBLE AS PAGADO_TOTAL_UF ,
	IFNULL(ag.PAGADO_TOTAL_PESOS,0)::INT AS PAGADO_TOTAL_PESOS ,
	IFNULL(ag.MONTO_EXCESO,0)::INT AS MONTO_EXCESO ,
	IFNULL(ag.MONTO_EXCEDENTE,0)::INT AS MONTO_EXCEDENTE ,
	IFNULL(ag.GASTO_PHARMA_PESOS,0)::INT AS GASTO_PHARMA_PESOS ,
	IFNULL(ag.COSTO_FINAL_UF,0)::DOUBLE AS COSTO_FINAL_UF,
	IFNULL(ag.COSTO_FINAL_PESOS,0)::INT AS COSTO_FINAL_PESOS
FROM EST.P_DDV_EST.CA_PERMANENCIA_CARTERA per
FULL JOIN agrupa_todo ag ON (per."fld_cotrut" = ag.RUT_TITULAR AND per.PERIODO_MES = ag.PERIODO)
LEFT JOIN ( 
			SELECT 
				EST.P_DDV_EST.FECHA_PERIODO(FEC_VAL) AS PERIODO,
				VAL_MON
			FROM ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA
		  )	uf ON (per.PERIODO_MES = uf.PERIODO)
WHERE  ag.ANIO IS NOT NULL
--LIMIT 10 
)
------------------------------------------------ FIN SEGUNDA PARTE REVISADA 






--PRIMERA EXTRACCION
---------------------------------------------------------------------------------
CREATE OR REPLACE  TABLE EST.P_DDV_EST.SINIES_12_MESES AS (
WITH base_personas AS ( 
SELECT DISTINCT 
	PERIODO ,
	RUT AS RUT_TITULAR,
	CODCATEG AS COD_CATEGORIA,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS
WHERE PERIODO >= 201701
UNION 
SELECT DISTINCT 
	PERPAG_ESPERADO AS PERIODO,
	RUT_COTIZANTE AS RUT_TITULAR,
	COD_CATEGORIA ,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_LIBRO_VENTAS
WHERE PERIODO >= 201701 AND CODIGO_PAGO = 0
UNION 
SELECT DISTINCT 
	PERIODO_VIGENCIA AS PERIODO,
	RUT_TITULAR ,
	COD_CATEGORIA ,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO >= 201701
UNION 
SELECT DISTINCT 
	PERIODO ,
	RUT_TITULAR ,
	COD_CATEGORIA ,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO >= 201701
UNION 
SELECT DISTINCT 
	"periodo_vig" AS PERIODO,
	"rut_titular" AS RUT_TITULAR,
	"cod_categoria" AS COD_CATEGORIA,
	SUBSTRING(PERIODO,0,4) AS ANIO 
FROM FIN.P_DDV_FIN.P_DDV_FIN_RECUPERACION_GASTO_NEW
WHERE PERIODO >= 201701
)
, base_personas_2 AS ( 
SELECT 
	*
FROM base_personas a 
INNER JOIN (
			SELECT 
				"codigo" AS CODIGO
			FROM ISA.P_RDV_DST_LND_SYB.SERIES_CATEG
			WHERE "grupo" <> 11
			) b ON (a.COD_CATEGORIA=b.CODIGO) 
)
--SELECT * FROM base_personas_2 LIMIT 10
--SELECT DISTINCT 
--	PERIODO,
--	RUT_TITULAR,
--	COD_CATEGORIA
--FROM base_personas
--ORDER BY RUT_TITULAR, PERIODO 
--LIMIT 10
------------ gastos dtm
--WITH 
, gastos AS ( 
SELECT 
	PERIODO ,
	RUT_TITULAR ,
	sum(BONIFICADO) AS BONIFICADO,
	sum(MONTO_CAEC) AS MONTO_CAEC
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PERIODO >= 201701
GROUP BY PERIODO , RUT_TITULAR 
)
--SELECT 
--	*
--FROM gastos
--LIMIT 10
--------------- iva recu
--WITH 
, iva_recu AS ( 
SELECT  
	PERIODO ,
	RUT AS RUT_TITULAR,
	sum(MONTO_CREDITO_FISCAL) AS MONTO_CREDITO_FISCAL
FROM CON.P_DDV_CON.P_DDV_CON_IVA_RECUPERADO_BONOS
WHERE PERIODO >= 201701
GROUP BY PERIODO, RUT_TITULAR
)
--SELECT 
--	*
--FROM iva_recu
--LIMIT 10
--------------- rebate
--WITH 
, rebate AS ( 
SELECT DISTINCT 
	"periodo_vig" AS PERIODO,
	"rut_titular" AS RUT_TITULAR,
	sum("monto_recuperado") AS MONTO_RECUPERADO
FROM FIN.P_DDV_FIN.P_DDV_FIN_RECUPERACION_GASTO_NEW
WHERE PERIODO >= 201701
GROUP BY PERIODO, RUT_TITULAR
)
--SELECT 
--	*
--FROM rebate 
--LIMIT 10
--------------- ingresos
--WITH 
, ingresos AS ( 
SELECT DISTINCT 
	PERIODO_VIGENCIA AS PERIODO,
	RUT_TITULAR ,
	SUM(PAGADO_TOTAL_UF) AS PAGADO_TOTAL_UF ,
	SUM(PAGADO_TOTAL_PESOS) AS PAGADO_TOTAL_PESOS,
	SUM(MONTO_EXCESO) AS MONTO_EXCESO,
	SUM(MONTO_EXCEDENTE) AS MONTO_EXCEDENTE ,
	SUM(GASTO_PHARMA_PESOS) AS GASTO_PHARMA_PESOS,
	SUM(COSTO_FINAL_UF) AS COSTO_FINAL_UF,
	SUM(COSTO_FINAL_PESOS) AS COSTO_FINAL_PESOS 
FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE PERIODO >= 201701
GROUP BY PERIODO, RUT_TITULAR 
)
--SELECT 
--	*
--FROM ingresos
--LIMIT 10
------------- iva_recau
--WITH 
, iva_recau AS ( 
SELECT DISTINCT 
	PERPAG_ESPERADO AS PERIODO,
	RUT_COTIZANTE AS RUT_TITULAR,
	sum(VALOR_IVA) AS VALOR_IVA
FROM RCD.P_DDV_RCD.P_DDV_RCD_V_LIBRO_VENTAS
WHERE PERIODO >= 201701 AND CODIGO_PAGO = 0
GROUP BY PERIODO, RUT_TITULAR
)
, agrupa_todo AS ( 
--SELECT 
--	*
--FROM iva_recau
SELECT 
	bp.ANIO,
	bp.PERIODO,
	bp.RUT_TITULAR,
	g.BONIFICADO,
	g.MONTO_CAEC,
	icu.MONTO_CREDITO_FISCAL,
	icau.VALOR_IVA,
	re.MONTO_RECUPERADO,
	ing.PAGADO_TOTAL_UF ,
	ing.PAGADO_TOTAL_PESOS,
	ing.MONTO_EXCESO,
	ing.MONTO_EXCEDENTE ,
	ing.GASTO_PHARMA_PESOS,
	ing.COSTO_FINAL_UF,
	ing.COSTO_FINAL_PESOS 
FROM base_personas_2 bp 
LEFT JOIN gastos 	g 		ON (bp.RUT_TITULAR = g.RUT_TITULAR AND bp.PERIODO = g.PERIODO)
LEFT JOIN iva_recu  icu		ON (bp.RUT_TITULAR = icu.RUT_TITULAR AND bp.PERIODO = icu.PERIODO)
LEFT JOIN iva_recau icau 	ON (bp.RUT_TITULAR = icau.RUT_TITULAR AND bp.PERIODO = icau.PERIODO)
LEFT JOIN rebate 	re		ON (bp.RUT_TITULAR = re.RUT_TITULAR AND bp.PERIODO = re.PERIODO)
LEFT JOIN ingresos  ing 	ON (bp.RUT_TITULAR = ing.RUT_TITULAR AND bp.PERIODO = ing.PERIODO)
)
SELECT DISTINCT 
	--ag.ANIO,
	SUBSTRING(per.PERIODO_INGRESO,0,4) AS ANNIO, 
	ag.PERIODO::int AS PERIODO,
	ag.RUT_TITULAR,
	per.PERIODO_INGRESO::INT AS PERIODO_INGRESO ,
	per.PERIODO_MIN_VIG::INT AS PERIODO_MIN_VIG ,
	per.PERIODO_DOCE_MESES::INT AS PERIODO_DOCE_MESES ,
	uf.VAL_MON::DOUBLE AS VAL_MON ,
	per.RESTA_MESES::INT AS RESTA_MES ,
	per.PERIODO_MES::INT AS PERIODO_MES ,
	IFNULL(ag.BONIFICADO,0)::INT AS BONIFICADO,
	IFNULL(ag.MONTO_CAEC,0)::INT AS MONTO_CAEC,
	IFNULL(ag.MONTO_CREDITO_FISCAL,0)::INT AS MONTO_CREDITO_FISCAL ,
	IFNULL(ag.VALOR_IVA,0)::DOUBLE AS VALOR_IVA ,
	IFNULL(ag.MONTO_RECUPERADO,0)::INT AS MONTO_RECUPERADO,
	IFNULL(ag.PAGADO_TOTAL_UF,0)::DOUBLE AS PAGADO_TOTAL_UF ,
	IFNULL(ag.PAGADO_TOTAL_PESOS,0)::INT AS PAGADO_TOTAL_PESOS,
	IFNULL(ag.MONTO_EXCESO,0)::INT AS MONTO_EXCESO ,
	IFNULL(ag.MONTO_EXCEDENTE,0)::INT AS MONTO_EXCEDENTE ,
	IFNULL(ag.GASTO_PHARMA_PESOS,0)::INT AS GASTO_PHARMA_PESOS ,
	IFNULL(ag.COSTO_FINAL_UF,0)::DOUBLE AS COSTO_FINAL_UF,
	IFNULL(ag.COSTO_FINAL_PESOS,0)::INT AS COSTO_FINAL_PESOS
FROM EST.P_DDV_EST.CA_PERMANENCIA_CARTERA per
LEFT JOIN agrupa_todo ag ON (per."fld_cotrut" = ag.RUT_TITULAR AND per.PERIODO_MES = ag.PERIODO)
LEFT JOIN ( 
			SELECT 
				EST.P_DDV_EST.FECHA_PERIODO(FEC_VAL) AS PERIODO,
				VAL_MON
			FROM ISA.P_RDV_DST_LND_SYB.ISAPREMONEDA
		  )	uf ON (per.PERIODO_MES = uf.PERIODO)
WHERE per.PERIODO_INGRESO BETWEEN 201801 AND 202101 AND ag.ANIO IS NOT NULL
--LIMIT 10 
)
------------------------------------------------ FIN SEGUNDA PARTE REVISADA 