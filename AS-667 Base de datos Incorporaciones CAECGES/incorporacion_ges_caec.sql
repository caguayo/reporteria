------ activacion GES-CAEC

SELECT DISTINCT "estado" FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
LIMIT 10

SELECT * FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
LIMIT 10



SELECT * FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO 
WHERE "rutcot" = '010707190-3' AND "fecha_solges" ='2021-01-06'
LIMIT 10



SELECT DISTINCT "periodo_solges" FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO
WHERE "fecha_solges" >= '2021-01-01'
ORDER BY "periodo_solges" 


SELECT DISTINCT "periodo_red" FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
WHERE "periodo_red" >= 202101
ORDER BY "periodo_red" 



------ extraccion de los datos
----- activaciones caec
SELECT DISTINCT 
	"rutcot" AS rut_afiliado,
	"rutben" AS rut_beneficiario,
	"nombre_ben"  AS nombre_beneficiario,
	"periodo_red" AS fecha_activacion_caec
FROM GES.P_RDV_DST_LND_SYB.CAEC_ESTUDIO
WHERE "periodo_red" BETWEEN 202101 AND 202107
--LIMIT 10


------- activacion ges 
SELECT DISTINCT 
	"rutcot" as rut_afiliado,
	--SUBSTRING("rutcot",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
	"rutben" as rut_beneficiario,
	--SUBSTRING("rutben",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	"nombre_ben" as nombre_beneficiario,
	"periodo_solges" AS periodo_activacion_ges,
	"fecha_solges"::date as fecha_activacion_ges
	--"descrip_is" as tipo_patologia
	--"estado_caso" 
FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO  ges
WHERE "fecha_solges" BETWEEN '2021-01-01 00:00:00' AND '2021-07-31 23:59:59' 
	  AND "estado_caso" not in ('ANULADO', 'NO CURSADO')
--LIMIT 100






SELECT * FROM est.P_DDV_EST.CA_CIA_SEGURO_2020 LIMIT 100

