---------------------
'075764900-4',
'076188197-3',
'076600628-0',
'079943600-0',
'086359300-K',
'088566900-',
'089201400-0',
'090222000-3',
'091440000-7',
'093658000-9',
'095304000-K',
'096532330-9',
'096731890-6',
'096757710-3',
'096853150-6'
-------------------




------------- extraccion 
WITH cartera_vig AS ( 
SELECT DISTINCT 
		cnt."fld_cotrut" ,
		cnt."fld_cotapepa" ,
		cnt."fld_cotapema" ,
		cnt."fld_cotnombre",
		cnt."fld_funfolio",
		cnt."fld_funcorrel",
		cnt."fld_planvig" ,
		cnt."fld_cntcateg" ,
		cnt."fld_glscateg",	
		cnt."fld_agecod",
		cnt."fld_codneg" ,
		cnt."fld_concostototal",
		ben."fld_bensexo" ,
		ben."fld_bennacfec",
		acti.gls_activi
FROM AFI.P_RDV_DST_LND_SYB.cnt cnt
	INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio" = ben."fld_funfolio" AND 
												 cnt."fld_funcorrel" = ben."fld_funcorrel" AND 
												 ben."fld_bencorrel"= 0)
	INNER JOIN isa.P_RDV_DST_LND_SYB.ISAPREACTIVI acti ON (cnt."fld_conactivcod"= acti.cod_activi)
WHERE 202106 BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA
)
SELECT 
		vig."fld_cotrut" ,
		vig."fld_cotapepa" ,
		vig."fld_cotapema" ,
		vig."fld_cotnombre",
		CASE 
			WHEN vig."fld_bensexo" = 1 THEN 'Hombre'
			ELSE 'Mujer'
		END AS Genero,
		vig.gls_activi,
		'' AS rol,
		cob."fld_emprut" ,
		emp."fld_emprazsoc",
		emp."fld_empdir" ,
		vig."fld_bennacfec",
		'COLMENA GOLDEN' AS isapre,
		'' AS unidad,
		vig."fld_concostototal",
		vig."fld_cntcateg",
		vig."fld_glscateg"	
FROM cartera_vig vig 
	INNER JOIN 	(SELECT DISTINCT 
						"fld_funnotiffun",
						"fld_funcorrel" ,
						"fld_emprut"
				 FROM afi.P_RDV_DST_LND_SYB.cob
				 WHERE  "fld_emprut"   IN  ('075764900-4',
											'076188197-3',
											'076600628-0',
											'079943600-0',
											'086359300-K',
											'088566900-K',
											'089201400-0',
											'090222000-3',
											'091440000-7',
											'093658000-9',
											'095304000-K',
											'096532330-9',
											'096731890-6',
											'096757710-3',
											'096853150-6')
			) cob ON (vig."fld_funfolio" = cob."fld_funnotiffun" AND vig."fld_funcorrel" = cob."fld_funcorrel")
	LEFT JOIN isa.P_RDV_DST_LND_SYB.EMP emp ON (cob."fld_emprut"  = emp."fld_emprut")	
--------------------------

	
	
	
	
	
	
	
	
	

	

	
--------------------- revison codigo
-------------------------------------------------------

SELECT top 10 * FROM isa.P_RDV_DST_LND_SYB.ISAPREACTIVI

SELECT top 10 * FROM afi.P_RDV_DST_LND_SYB.cnt
WHERE ."fld_conactivcod" 


SELECT 
	"fld_emprut",
	"fld_emprazsoc",
	count(1) AS contador
FROM empleador
GROUP BY "fld_emprut",
		 "fld_emprazsoc"
ORDER BY "fld_emprut"


SELECT  * FROM isa.P_RDV_DST_LND_SYB.EMP
WHERE "fld_emprut" IN ( '075764900-4',
						'076188197-3',
						'076600628-0',
						'079943600-0',
						'086359300-K',
						'088566900-K',
						'089201400-0',
						'090222000-3',
						'091440000-7',
						'093658000-9',
						'095304000-K',
						'096532330-9',
						'096731890-6',
						'096757710-3',
						'096853150-6')



SELECT top 11 * FROM afi.P_RDV_DST_LND_SYB.cnt
WHERE  202106 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA AND 
INNER JOIN 
		(SELECT DISTINCT 
				"fld_funnotiffun",
				"fld_funcorrel" ,
				"fld_emprut" 
		 FROM afi.P_RDV_DST_LND_SYB.cob
		 WHERE "fld_emprut" IN ('075764900-4',
								'076188197-3',
								'076600628-0',
								'079943600-0',
								'086359300-K',
								'088566900-K',
								'089201400-0',
								'090222000-3',
								'091440000-7',
								'093658000-9',
								'095304000-K',
								'096532330-9',
								'096731890-6',
								'096757710-3',
								'096853150-6')
		 GROUP BY 
		 		"fld_funnotiffun",
				"fld_funcorrel" ) 
		cob ON (uni."fld_funfolio" = cob."fld_funnotiffun" AND uni."fld_funcorrel" = cob."fld_funcorrel")



 cobrados AS ( 
	SELECT 
		"pag_funfol", 
		"pag_funcor",
		"pag_rutemp", 
		"pag_rutpag",
		max("pag_percot") AS percot_maximo ,
		MAX("pag_pagfec")::datetime AS ultima_fecpag 
	FROM rcd.P_DDV_RCD.P_DDV_RCD_V_PAG 
	WHERE "pag_pagfec" >= '2020-11-01 00:00:00' AND 
	--"pag_rutpag" NOT IN ('000000000-0')
	"pag_rutemp" IN (	'075764900-4',
						'076188197-3',
						'076600628-0',
						'079943600-0',
						'086359300-K',
						'088566900-K',
						'089201400-0',
						'090222000-3',
						'091440000-7',
						'093658000-9',
						'095304000-K',
						'096532330-9',
						'096731890-6',
						'096757710-3',
						'096853150-6')	
	GROUP BY "pag_funfol", "pag_rutemp", "pag_funcor", "pag_rutpag"










