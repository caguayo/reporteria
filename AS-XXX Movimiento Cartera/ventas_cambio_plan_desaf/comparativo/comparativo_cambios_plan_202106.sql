--
-- Cambios de Plan al mismo dia habil
--
-- select 'Cambios de Plan'

declare @tope date

set nocount on

set @tope = convert(char(6), getdate(), 112) + '01'

select convert(date, f.fld_periodoprod) as 'periodo_prod',
       case f.fld_firmaweb when 1 then 'Remoto' else 'Presencial' end as 'canal',
       case when f.fld_cotsexo = 1 then 'Hombre' else 'Mujer' end as 'sexo',
       count(1) as 'cant'
  into #tmp
  from FUN f,
       LFC l,
       CNT c
 where f.fld_periodoprod    > '20181201'
   and f.fld_funfolio       = l.fld_folio
   and f.fld_periodoprod    < @tope
   and f.fld_contrato       = c.fld_funfolio
   and f.fld_correlativo    = c.fld_funcorrel
   and convert(int, f.fld_plancod) <> c.fld_cntcateg
   and ltrim(rtrim(f.fld_plancod)) is not null
   and convert(varchar, f.fld_funnotificacod) like '%9%'
   and (f.COD_PROCESO       = 0 or f.COD_PROCESO is null)
 group by
       convert(date, f.fld_periodoprod),
       case f.fld_firmaweb when 1 then 'Remoto' else 'Presencial' end,
       case when f.fld_cotsexo = 1 then 'Hombre' else 'Mujer' end
 order by
       1, 2

select convert(char(10), periodo_prod, 105) + ';' +
       canal         + ';' +
       sexo          + ';' +
       convert(varchar, cant)
  from #tmp
