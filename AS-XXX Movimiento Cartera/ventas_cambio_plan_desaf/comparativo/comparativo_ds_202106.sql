--
-- ds acumuladas dia habil
--
-- select 'DS'

declare @tope date

set nocount on

set @tope = convert(char(6), getdate(), 112) + '01'

select -- convert(char(6), d.ds_fechasolicitud, 112) as 'mes',
       convert(char(6), d.ds_fechaingreso, 112) as 'mes',
       case when p.per_sexo = 'M' then 'Hombre' else 'Mujer' end as 'sexo',
       convert(date, convert(char(6), d.ds_fechaingreso, 112) + '01') as 'hasta',
       count(1) as 'cant'
  into #tmp
  from DS_PERSONA p,
       DS d
 where d.ds_fechaingreso >= '20190101'
   and d.ds_fechaingreso  < @tope
   and d.per_idn          = p.per_idn
   and d.tpo_idn          = 1 -- venta nueva
 group by
--       convert(char(6), d.ds_fechasolicitud, 112),
       convert(char(6), d.ds_fechaingreso, 112),
       case when p.per_sexo = 'M' then 'Hombre' else 'Mujer' end,
       convert(date, convert(char(6), d.ds_fechaingreso, 112) + '01')
 order by
       1, 2
--at isolation read uncommitted

select mes   + ';' +
       sexo  + ';' +
       convert(char(10), hasta, 105) + ';' +
       convert(varchar, cant)
  from #tmp