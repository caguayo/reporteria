--
-- cotizaciones acumuladas dia calendario
--

-- select 'Cotizaciones'

declare @tope date

set nocount on

set @tope = convert(char(6), getdate(), 112) + '01'

select convert(char(6), c.cot_fecha, 112) as 'mes',
       case when b.ben_sexo = 'M' then 'Hombre' else 'Mujer' end as 'sexo',
       convert(date, convert(char(6), c.cot_fecha, 112) + '01') as 'hasta',
       count(1) as 'cant'
  into #tmp
  from CPW_COTIZACION c,
       CPW_BENEFICIARIO b
 where c.cot_fecha       >= '20190101'
   and c.cot_fecha        < @tope
   and c.cot_idn          = b.cot_idn
   and b.tbe_idn          = 1
 group by
       convert(char(6), c.cot_fecha, 112),
       case when b.ben_sexo = 'M' then 'Hombre' else 'Mujer' end,
       convert(date, convert(char(6), c.cot_fecha, 112) + '01')
 order by
       3, 2
--at isolation read uncommitted

select mes           + ';' +
       sexo          + ';' +
       convert(char(10), hasta, 105) + ';' +
       convert(varchar, cant)
  from #tmp
