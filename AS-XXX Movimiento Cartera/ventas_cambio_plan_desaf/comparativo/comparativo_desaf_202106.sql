--
-- Desafiliaciones
--
-- select 'Desafiliaciones'

declare @tope date

set nocount on

set @tope = convert(char(6), getdate(), 112) + '01'

select convert(date, convert(char(6), car.car_fecha_recep, 112) + '01') as 'fecha_recep',
       case when b.fld_bensexo = 1 then 'Hombre' else 'Mujer' end as 'sexo',
       count(distinct car.car_rut_afil) as 'cant'
  into #tmp
  from dsf_carta car,       
       CNT c,
       BEN b
 where car.car_fecha_recep  >= '20190101'
   and car.car_fecha_recep   < @tope
   and car.car_rut_afil      = c.fld_cotrut
   and car.car_fecha_recep between c.fld_inivigfec and c.fld_finvigfec
   and car.car_estado_carta  = 2
   and c.fld_funfolio        = b.fld_funfolio
   and c.fld_funcorrel       = b.fld_funcorrel
   and c.fld_cotrut          = b.fld_benrut
 group by
       convert(date, convert(char(6), car.car_fecha_recep, 112) + '01'),
       case when b.fld_bensexo = 1 then 'Hombre' else 'Mujer' end
 order by
       1, 2, 3

select convert(char(10), fecha_recep, 105) + ';' +
       sexo          + ';' +       
       convert(varchar, cant)
  from #tmp
