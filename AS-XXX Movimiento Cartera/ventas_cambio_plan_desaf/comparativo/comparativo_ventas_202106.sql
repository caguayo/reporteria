--
-- Ventas
--
-- select 'Ventas'

declare @tope date

set nocount on

set @tope = convert(char(6), getdate(), 112) + '01'

select convert(date, f.fld_periodoprod) as 'PeriodoProd',
       t.grupo as 'Canal',
       case when f.fld_cotsexo = 1 then 'Hombre' else 'Mujer' end as 'sexo',
       i.GLS_ISAPRE as isapre_origen,
       case
         when i.COD_ISAPRE in (0, 1) then 'Fonasa'
         else 'Otra Isapre'
       end as origen,
       isnull(reg.GLS_WEB_REGION, 'DESCONOCIDA') as 'region',
       count(1) as 'cant'
  into #tmp
  from FUN f,
       LFC l,
       ISAPREISAPRE i,
       PREC_TIPFUN t,
       ISAPRESEXO s,
       ISAPREREGION reg
 where f.fld_periodoprod     > '20181201'
   and f.fld_periodoprod     < @tope
   and f.fld_funfolio        = l.fld_folio
   and f.fld_funnotificacod  = 1
   and f.fld_cotsexo         = s.COD_SEXO
   and f.fld_isapreantcod   *= i.COD_ISAPRE
   and f.fld_tipfun          = t.cod
   and f.fld_region         *= reg.COD_REGION
 group by
       convert(date, f.fld_periodoprod),
       t.grupo,
       case when f.fld_cotsexo = 1 then 'Hombre' else 'Mujer' end,
       i.GLS_ISAPRE,
       case
         when i.COD_ISAPRE in (0, 1) then 'Fonasa'
         else 'Otra Isapre'
       end,
       isnull(reg.GLS_WEB_REGION, 'DESCONOCIDA')
 order by
       1, 2

select convert(char(10), PeriodoProd, 105) + ';' +
       Canal         + ';' +
       sexo          + ';' +
       isapre_origen + ';' +
       origen        + ';' +
       region        + ';' +
       convert(varchar, cant)
  from #tmp
