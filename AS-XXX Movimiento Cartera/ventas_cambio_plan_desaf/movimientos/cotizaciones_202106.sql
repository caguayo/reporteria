--
-- cotizaciones mes actual hasta ayer
--
declare @desde      date,
        @hora_tope  datetime

set nocount on

set @desde = convert(char(6), dateadd(month, -1, getdate()), 112) + '01'

exec FunW_ObtenerUltimoDiaMes @desde, @hora_tope out

set @hora_tope = convert(char(8), @hora_tope, 112) + ' ' + '23:59:59.999'

-- -- para validar cantidad de datos que deben aparecer en el resultado
-- select count(distinct c.cot_idn) as 'cantidad esperada'
--   from CPW_COTIZACION c,
--        CPW_BENEFICIARIO b
--  where c.cot_fecha >= @desde
--    and c.cot_idn    = b.cot_idn
--    and b.tbe_idn    = 1
--    and c.cot_fecha <= @hora_tope
-- at isolation read uncommitted
-- -- go

-- sp_help CPW_BENEFICIARIO

-- datos
select convert(char(6), c.cot_fecha, 112) as 'Per_produccion',
       convert(int, (datepart(day, c.cot_fecha) / cua.largo_semana)) + 1 as 'cuarto',
       case when b.ben_sexo = 'M' then 'Hombre' else 'Mujer' end as 'sexo',
       c.cot_rut as 'Rut',
       datediff(month, b.ben_fecha_nacim, c.cot_fecha) / 12 as 'Edad',
--        tram.RAN_INI as 'etario_desde',
--        tram.RAN_FIN as 'etario_hasta',
       0 as 'etario_desde',
       0 as 'etario_hasta',
       isnull(m.mon_renta_imp, 0) as 'Renta_ingresada',
       (select count(1) - 1 from CPW_BENEFICIARIO b2 where b2.cot_idn = c.cot_idn ) as 'cargas'
  into #tmp
  from CPW_COTIZACION c,
       CPW_BENEFICIARIO b (index idx_beneficiario_cot_idn),
       CPW_MONTOS m,
       PREC_MES_JMM cua/*,
       PLANESFACTRAM tram*/
 where c.cot_fecha >= @desde
   and c.cot_idn    = b.cot_idn
   and b.tbe_idn    = 1
   and b.ben_idn   *= m.ben_idn
   and datepart(month, c.cot_fecha) = cua.mes
   and c.cot_fecha <= @hora_tope
--    and (datediff(month, b.ben_fecha_nacim, c.cot_fecha) / 12) between tram.RAN_INI and tram.RAN_FIN
--    and tram.TIP_FACTRAM = 23
 order by
       1, 2, 3, 4
-- at isolation read uncommitted

update #tmp
   set t.etario_desde = tram.RAN_INI,
       t.etario_hasta = tram.RAN_FIN
  from #tmp t,
       PLANESFACTRAM tram
 where t.Edad between tram.RAN_INI and tram.RAN_FIN
   and tram.TIP_FACTRAM     = 23

select Per_produccion             + ';' +
       convert(varchar, cuarto)   + ';' +
       sexo                       + ';' +
       Rut                        + ';' +
       convert(varchar, Edad)            + ';' +
       convert(varchar, etario_desde)    + ';' +
       convert(varchar, etario_hasta)    + ';' +
       convert(varchar, Renta_ingresada) + ';' +
       convert(varchar, cargas)
  from #tmp
