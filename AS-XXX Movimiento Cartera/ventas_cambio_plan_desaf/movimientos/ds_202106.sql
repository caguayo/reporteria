--
-- DS desde este mes en adelante
--
-- declare @desde      date,
--         @hora_tope  datetime

-- set @desde     = convert(char(6), getdate(), 112) + '01',
--     @hora_tope = convert(char(8), dateadd(day, -1, getdate()), 112) + ' ' + '23:59:59.999'

-- select @desde as '@desde', @hora_tope as '@hora_tope'

-- select count(1) as 'cantidad esperada'
--   from DS d, DS_PERSONA p
--  where d.ds_fechasolicitud >= @desde
--    and d.ds_fechasolicitud <= @hora_tope
--    and d.tpo_idn            = 1 -- venta nueva
--    and d.per_idn            = p.per_idn


-- select convert(char(6), d.ds_fechasolicitud, 112) as 'Per_produccion',
--        convert(int, (datepart(day, d.ds_fechasolicitud) / cua.largo_semana)) + 1 as 'cuarto',
--        case when p.per_sexo = 'M' then 'Hombre' else 'Mujer' end as 'sexo',
--        d.ds_idn,
--        p.per_rut as 'Rut',
--        datediff(month, p.per_fechanac, d.ds_fechasolicitud) / 12 as 'Edad',
--        i.GLS_ISAPRE   as 'Isapre_origen',
--        (select count(1) - 1 from DS_CARGA c, DS d2 where c.ds_idn =* d2.ds_idn and d2.ds_idn = d.ds_idn ) as 'cargas' /*,
--        d.ds_fechasolicitud as 'fecha_solicitud',
--        d.ds_fechaingreso */
--   from DS d,
--        DS_PERSONA p,
--        ISAPREISAPRE i,
--        PREC_MES_JMM cua
--  where d.ds_fechasolicitud >= @desde
--    and d.per_idn            = p.per_idn
--    and d.COD_ISAPRE        *= i.COD_ISAPRE
--    and datepart(month, d.ds_fechasolicitud) = cua.mes
--    and d.tpo_idn            = 1 -- venta nueva
--    and d.ds_fechasolicitud <= @hora_tope
--  order by
--        1, 2, 3, 4, 5, 6



-- por fecha de ingreso. Creo que esta fecha es mas relevante en las DS
--
-- DS desde este mes en adelante
--
declare @desde      date,
        @hora_tope  datetime

set nocount on

set @desde = convert(char(6), dateadd(month, -1, getdate()), 112) + '01'

exec FunW_ObtenerUltimoDiaMes @desde, @hora_tope out

set @hora_tope = convert(char(8), @hora_tope, 112) + ' ' + '23:59:59.999'

-- select @desde as '@desde', @hora_tope as '@hora_tope'

select convert(char(6), d.ds_fechaingreso, 112) as 'Per_produccion',
       convert(int, (datepart(day, d.ds_fechaingreso) / cua.largo_semana)) + 1 as 'cuarto',
       case when p.per_sexo = 'M' then 'Hombre' else 'Mujer' end as 'sexo',
       d.ds_idn,
       p.per_rut as 'Rut',
       datediff(month, p.per_fechanac, d.ds_fechaingreso) / 12 as 'Edad',
       i.GLS_ISAPRE   as 'Isapre_origen',
       (select count(1) - 1 from DS_CARGA c, DS d2 where c.ds_idn =* d2.ds_idn and d2.ds_idn = d.ds_idn ) as 'cargas' /*,
       d.ds_fechasolicitud as 'fecha_solicitud',
       d.ds_fechaingreso */
  into #tmp
  from DS d,
       DS_PERSONA p,
       ISAPREISAPRE i,
       PREC_MES_JMM cua
 where d.ds_fechaingreso    >= @desde
   and d.per_idn            = p.per_idn
   and d.COD_ISAPRE        *= i.COD_ISAPRE
   and datepart(month, d.ds_fechaingreso) = cua.mes
   and d.tpo_idn            = 1 -- venta nueva
   and d.ds_fechaingreso   <= @hora_tope
 order by
       d.ds_fechaingreso

select Per_produccion           + ';' +
       convert(varchar, cuarto) + ';' +
       sexo                     + ';' +
       convert(varchar, ds_idn) + ';' +
       Rut                      + ';' +
       convert(varchar, Edad)   + ';' +
       Isapre_origen            + ';' +
       convert(varchar, cargas)
  from #tmp