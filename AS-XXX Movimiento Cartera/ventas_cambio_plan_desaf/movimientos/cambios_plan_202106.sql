--
-- cambios de plan mes anterior
--
declare @periodo_prod date

set nocount on

set @periodo_prod = convert(char(6), dateadd(month, -1, getdate()), 112) + '01'

-- datos
select convert(char(6), f.fld_funnotiffec, 112) as 'Per_produccion',
       convert(int, (datepart(day, f.fld_funnotiffec) / cua.largo_semana)) + 1 as 'cuarto',
       case when f.fld_cotsexo = 1 then 'Hombre' else 'Mujer' end as 'sexo',
       f.fld_cotrut         as 'Rut',
       f.fld_funfolio       as 'FolioFun',      
       c.fld_cntcateg       as 'Cate_origen',
       str_replace(convert(varchar, c.fld_concostototal), '.', ',') as 'costo_origen',
       f.fld_plancod        as 'Cate_destino',
       e.fld_concostofinal  as 'Costo_EmpCob',
       (datediff(month, f.fld_cotnacfec, f.fld_funnotiffec) / 12)        as 'Edad',
       case
         when f.fld_region = 13 then 'Santiago'
         else 'Regiones'
       end as 'Ubicacion',
       '' as 'segmento_orig',
       '' as 'segmento_dest',
       '' as 'cambio_segmento',
       0 as 'etario_desde',
       0 as 'etario_hasta',
       c.fld_carganum   as 'cargas_orig',
       f.fld_totben - 1 as 'cargas_dest',
       f.fld_funagencianum as 'agencia_rut',
       convert(varchar(35), '') as 'agencia_sup',
       c.fld_clasifica     as 'clasif'
  into #tmp
  from FUN f,
       EMP_COB e,
       CNT c,
       PREC_MES_JMM cua,
       LFC l
 where f.fld_periodoprod    = @periodo_prod
   and f.fld_funfolio       = l.fld_folio
   and f.fld_funfolio       = e.fld_funfolioorig
   and f.fld_contrato       = c.fld_funfolio
   and f.fld_correlativo    = c.fld_funcorrel
   and convert(int, f.fld_plancod) <> c.fld_cntcateg
   and f.fld_plancod      <> ''
   and (f.COD_PROCESO = 0 or f.COD_PROCESO is null)
   and datepart(month, f.fld_funnotiffec) = cua.mes
 order by
       1, 2, 3

update #tmp
   set t.etario_desde = tram.RAN_INI,
       t.etario_hasta = tram.RAN_FIN
  from #tmp t,
       PLANESFACTRAM tram
 where t.Edad between tram.RAN_INI and tram.RAN_FIN
   and tram.TIP_FACTRAM     = 23

update #tmp
   set t.agencia_sup = age.FLD_NOMBRE
  from #tmp t,
       FUNAGV age
 where t.agencia_rut    = age.FLD_AGENCIARUT
   and age.FLD_AGECOD   = 0

select Per_produccion,
       cuarto,
       sexo,
       Rut,
       FolioFun,
       Cate_origen,
       costo_origen,
       Cate_destino,
       sum(Costo_EmpCob) as 'costo_destino',
       Edad,
       Ubicacion,
       segmento_orig,
       segmento_dest,
       cambio_segmento,
       etario_desde,
       etario_hasta,
       cargas_orig,
       cargas_dest,
       agencia_rut,
       agencia_sup,
       clasif
  into #salida
  from #tmp
 group by
       Per_produccion,
       cuarto,
       sexo,
       Rut,
       FolioFun,
       Cate_origen,
       costo_origen,
       Cate_destino,
       Edad,
       Ubicacion,
       segmento_orig,
       segmento_dest,
       cambio_segmento,
       etario_desde,
       etario_hasta,
       cargas_orig,
       cargas_dest,
       agencia_rut,
       agencia_sup,
       clasif

select Per_produccion             + ';' +
       convert(varchar, cuarto)   + ';' +
       sexo                       + ';' +
       Rut                        + ';' +
       convert(varchar, FolioFun) + ';' +
       convert(varchar, Cate_origen)  + ';' +
       costo_origen                   + ';' +
       convert(varchar, Cate_destino) + ';' +
       str_replace(convert(varchar, costo_destino), '.', ',') + ';' +
       convert(varchar, Edad)         + ';' +
       Ubicacion                      + ';' +
       segmento_orig                  + ';' +
       segmento_dest                  + ';' +
       cambio_segmento                + ';' +
       convert(varchar, etario_desde) + ';' +
       convert(varchar, etario_hasta) + ';' +
       convert(varchar, cargas_orig)  + ';' +
       convert(varchar, cargas_dest)  + ';' +
       agencia_rut                    + ';' +
       agencia_sup                    + ';' +
       clasif
  from #salida