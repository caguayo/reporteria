--
-- desafiliaciones mes actual hasta ayer
--
declare @desde      date,
        @hora_tope  datetime

set nocount on

set @desde = convert(char(6), dateadd(month, -1, getdate()), 112) + '01'

exec FunW_ObtenerUltimoDiaMes @desde, @hora_tope out

set @hora_tope = convert(char(8), @hora_tope, 112) + ' ' + '23:59:59.999'

-- -- para validar la cantidad de registros que deberian aparecer en el resultado
-- select count(distinct car.car_folio_desaf )
--   from dsf_carta car
--  where car.car_fecha_recep  >= @desde
--    and car.car_estado_carta  = 2
--    and car.car_fecha_recep  <= @hora_tope
-- at isolation read uncommitted

-- datos
select distinct
       convert(char(6), car.car_fecha_recep, 112)                 as 'Per_produccion',
       convert(int, (datepart(day, car.car_fecha_recep) / cua.largo_semana)) + 1 as 'cuarto',
       case when b.fld_bensexo = 1 then 'Hombre' else 'Mujer' end as 'sexo',
       car.car_rut_afil as 'Rut',
       c.fld_cntcateg   as 'Cate_origen',
       str_replace(convert(varchar, c.fld_concostototal), '.', ',') as 'costo_origen',
       dest.GLS_ISAPRE  as 'Isapre_Destino',
       datediff(month, b.fld_bennacfec, car.car_fecha_recep) / 12 as 'Edad',
--        tram.RAN_INI     as 'etario_desde',
--        tram.RAN_FIN     as 'etario_hasta',
       0 as 'etario_desde',
       0 as 'etario_hasta',
       /* cat.Segmento */ ''    as 'segmento', -- segmento lo agregara Hans
       c.fld_carganum   as 'cargas',
       c.fld_clasifica
  into #tmp
  from dsf_carta car,
       CNT c,
       BEN b,
       PREC_MES_JMM cua,
       ISAPREISAPRE dest /*,
       PREC_CATEG_JMM cat, -- segmento lo agregara Hans
       PLANESFACTRAM tram*/
 where car.car_fecha_recep  >= @desde
   and car.car_rut_afil      = c.fld_cotrut
   and car.car_fecha_recep between c.fld_inivigfec and c.fld_finvigfec
   and c.fld_funfolio        = b.fld_funfolio
   and c.fld_funcorrel       = b.fld_funcorrel
   and b.fld_bencorrel       = 0
   and c.fld_cotrut          = b.fld_benrut
   and car.car_estado_carta  = 2
   and car.car_isapre_dstno *= dest.COD_ISAPRE
   and datepart(month, car.car_fecha_recep) = cua.mes
   and car.car_fecha_recep  <= @hora_tope

--   and c.fld_cntcateg        = cat.COD_DESACA -- segmento lo agregara Hans

--    and (datediff(month, b.fld_bennacfec, car.car_fecha_recep) / 12) between tram.RAN_INI and tram.RAN_FIN
--    and tram.TIP_FACTRAM      = 23
 order by
       1, 2, 3, 4
-- at isolation read uncommitted

update #tmp
   set t.etario_desde = tram.RAN_INI,
       t.etario_hasta = tram.RAN_FIN
  from #tmp t,
       PLANESFACTRAM tram
 where t.Edad between tram.RAN_INI and tram.RAN_FIN
   and tram.TIP_FACTRAM     = 23

select Per_produccion             + ';' +
       convert(varchar, cuarto)   + ';' +
       sexo                       + ';' +
       Rut                        + ';' +
       convert(varchar, Cate_origen)  + ';' +
       costo_origen                   + ';' +
       Isapre_Destino                 + ';' +
       convert(varchar, Edad)         + ';' +
       convert(varchar, etario_desde) + ';' +
       convert(varchar, etario_hasta) + ';' +
       segmento                       + ';' +
       convert(varchar, cargas)       + ';' +
       fld_clasifica
  from #tmp
