--
-- ventas mes actual hasta ayer
--
declare @periodo_prod date

set nocount on

set @periodo_prod = convert(char(6), dateadd(month, -1, getdate()), 112) + '01'

-- datos
select convert(char(6), f.fld_periodoprod, 112) as 'Per_produccion',
       convert(int, (datepart(day, f.fld_funnotiffec) / cua.largo_semana)) + 1 as 'cuarto',
       case when f.fld_cotsexo = 1 then 'Hombre' else 'Mujer' end as 'sexo',
       f.fld_cotrut   as 'Rut',
       f.fld_funfolio as 'FolioFun',
       f.fld_ttracod  as 'Transac',
       t.grupo        as 'Canal',
       f.fld_plancod  as 'Categoria',
       e.fld_concostofinal as 'Costo_EmpCob',
       i.GLS_ISAPRE   as 'Isapre_origen',
       (datediff(month, f.fld_cotnacfec, f.fld_funnotiffec) / 12) as 'Edad',
       case
         when f.fld_region = 13 then 'Santiago'
         else 'Regiones'
       end as 'Ubicacion',
       case
         when i.COD_ISAPRE in (0, 1) then 'Fonasa'
         else 'Otra Isapre'
       end as 'origen',
       '' as 'Segmento',
       datediff(month, f.fld_cotnacfec, f.fld_funnotiffec) / 12 as 'edad',
       0 as 'etario_desde',
       0 as 'etario_hasta',
       f.fld_totben - 1 as 'cargas',
       isnull(reg.GLS_WEB_REGION, 'DESCONOCIDA') as 'region'
  into #tmp
  from FUN f,
       EMP_COB e,
       PREC_MES_JMM cua,
       ISAPREISAPRE i,
       PREC_TIPFUN t,
       ISAPREREGION reg,
       LFC l
 where f.fld_periodoprod    = @periodo_prod
   and f.fld_funfolio       = l.fld_folio
   and f.fld_funnotificacod = 1
   and f.fld_funfolio       = e.fld_funfolioorig
   and f.fld_isapreantcod  *= i.COD_ISAPRE
   and f.fld_tipfun         = t.cod
   and datepart(month, f.fld_funnotiffec) = cua.mes
   and f.fld_region        *= reg.COD_REGION
 order by
       1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12

update #tmp
   set t.etario_desde = tram.RAN_INI,
       t.etario_hasta = tram.RAN_FIN
  from #tmp t,
       PLANESFACTRAM tram
 where t.edad between tram.RAN_INI and tram.RAN_FIN
   and tram.TIP_FACTRAM     = 23

select Per_produccion,
       cuarto,
       sexo,
       Rut,
       FolioFun,
       Transac,
       Canal,
       Categoria,
       str_replace(convert(varchar, sum(Costo_EmpCob)), '.', ',') as 'costo_final',
       Isapre_origen,
       Edad,
       Ubicacion,
       origen,
       Segmento,
       edad,
       etario_desde,
       etario_hasta,
       cargas,
       region
  into #salida
  from #tmp t
 group by
       Per_produccion,
       cuarto,
       sexo,
       Rut,
       FolioFun,
       Transac,
       Canal,
       Categoria,
       Isapre_origen,
       Edad,
       Ubicacion,
       origen,
       Segmento,
       edad,
       etario_desde,
       etario_hasta,
       cargas,
       region

select Per_produccion             + ';' +
       convert(varchar, cuarto)   + ';' +
       sexo                       + ';' +
       Rut                        + ';' +
       convert(varchar, FolioFun) + ';' +
       Transac                    + ';' +
       Canal                      + ';' +
       Categoria                  + ';' +
       costo_final                + ';' +
       Isapre_origen              + ';' +
       convert(varchar, Edad)     + ';' +
       Ubicacion                  + ';' +
       origen                     + ';' +
       Segmento                   + ';' +
       convert(varchar, edad)     + ';' +
       convert(varchar, etario_desde) + ';' +
       convert(varchar, etario_hasta) + ';' +
       convert(varchar, cargas)       + ';' +
       region
 from #salida