
SELECT * FROM EST.P_DDV_EST.CA_VENTAS_MES_MOVIMIENTO_TEMP LIMIT 100

SELECT min("fld_periodoprod"), MAX("fld_periodoprod") FROM ISA.P_RDV_DST_LND_SYB.FUN LIMIT 10

SELECT * FROM  ISA.P_RDV_DST_LND_SYB.EMP_COB LIMIT 10

SELECT * FROM  ISA.P_RDV_DST_LND_SYB.PREC_MES_JMM LIMIT 10

SELECT * FROM  ISA.P_RDV_DST_LND_SYB.PREC_TIPFUN LIMIT 10
       
SELECT * FROM  EST.P_DDV_EST.PRM_AGRUPA_PLANES LIMIT 10
       
SELECT * FROM  ISA.P_RDV_DST_LND_SYB.ISAPREREGION LIMIT 10

SELECT * FROM  ISA.P_RDV_DST_LND_SYB.ISAPREISAPRE LIMIT 10

SELECT min("fld_periodoprod"), MAX("fld_periodoprod") FROM  AFI.P_RDV_DST_LND_SYB.LFC LIMIT 10
       
       
       

------------ VENTAS NUEVAS
------------ EJECUCION DE LA TABLA
CREATE OR REPLACE TEMPORARY TABLE  EST.P_DDV_EST.CA_VENTAS_MES_MOVIMIENTO_TEMP AS (
SELECT 'venta' AS tip_movimiento,
	   est.P_DDV_EST.FECHA_PERIODO(f."fld_funnotiffec")::varchar AS Per_produccion,
	   (DATE_PART(day, f."fld_funnotiffec") / cua."largo_semana")::INT + 1 AS cuarto,
	   CASE 
       		WHEN f."fld_cotsexo" = 1 
       		THEN 'Hombre' 
       		ELSE 'Mujer' 
       END              AS sexo,
	   f."fld_cotrut"   AS Rut,       
       --(SUBSTRING(f."fld_cotrut",1,9)::int*5-3265112+52658)::varchar  AS id_rut_afiliado,
       f."fld_funfolio" as FolioFun,
       f."fld_ttracod"  as Transac,
       t."grupo"        as Canal,
       f."fld_plancod"::INT AS Categoria,
       e."fld_concostofinal"::double AS Costo_EmpCob,
       i.GLS_ISAPRE   as Isapre_origen,
       (datediff(MONTH, f."fld_cotnacfec", f."fld_funnotiffec") / 12)::INT AS Edad,
       CASE  when f."fld_region" = 13 then 'Santiago' else 'Regiones' end as Ubicacion,
       CASE  when i."COD_ISAPRE" in (0, 1) then 'Fonasa' else 'Otra Isapre'   end as origen,
       cat.Segmento,
       (datediff(month, f."fld_cotnacfec", f."fld_funnotiffec") / 12)::int  as edad2,
       0::int AS etario_desde,
       0::int AS etario_hasta,
       --        tram.RAN_INI as 'etario_desde',
       --        tram.RAN_FIN as 'etario_hasta',
       f."fld_totben" - 1 as cargas,
       CASE 
       		WHEN reg."GLS_WEB_REGION" IS NULL 
       		THEN 'DESCONOCIDA'
       		ELSE reg."GLS_WEB_REGION"
       END              as region,
       cat.tipo_plan,
       CASE
       		WHEN cat.SEGMENTO = 'Bajo+'  THEN 'Bajo'
			WHEN cat.SEGMENTO = 'Medio+' THEN 'Medio'
			WHEN cat.SEGMENTO = 'Alto+'  THEN 'Alto'
			WHEN cat.SEGMENTO = 'L.E. Reg+' THEN 'L.E. Reg'
			ELSE cat.SEGMENTO
	   END AS segmento_unico,
	   --CONCAT(sexo,', ',TIP_MOVIMIENTO) AS sexo_tipmovi
	   CONCAT(sexo,', ',origen) AS sexo_tipmovi
	   --f."fld_ciudad"   AS cuidad
FROM ISA.P_RDV_DST_LND_SYB.FUN f
  	   LEFT JOIN ISA.P_RDV_DST_LND_SYB.EMP_COB e        ON (f."fld_funfolio"  = e."fld_funfolioorig")
       LEFT JOIN ISA.P_RDV_DST_LND_SYB.PREC_MES_JMM cua ON (date_part(month, f."fld_funnotiffec") = cua."mes")
       LEFT JOIN ISA.P_RDV_DST_LND_SYB.PREC_TIPFUN t    ON (f."fld_tipfun" = t."cod")
       LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES cat    ON (CASE WHEN f."fld_plancod" =' ' THEN '-10'
   		    												     WHEN f."fld_plancod" =''  THEN '-10'
   		    												     ELSE f."fld_plancod" 
   	   													     END::int = cat.COD_DESACA)	
       LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION reg ON (f."fld_region" = reg.COD_REGION) 
       LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREISAPRE i   ON (f."fld_isapreantcod"  = i.COD_ISAPRE)
       INNER JOIN AFI.P_RDV_DST_LND_SYB.LFC          l   ON (f."fld_funfolio" = l."fld_folio")
 WHERE f."fld_periodoprod" BETWEEN '2019-01-01 00:00:00'  AND  add_months(last_day(getdate()),-1)::DATETIME  -->= @desde
   AND f."fld_guiarem"        = 1
   AND f."fld_funnotificacod" = 1   
   AND f."fld_emifec"        <= TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59') ---termino_mes_anterior  ----4@hora_tope	   
 ORDER BY
       1, 2, 3, 4, 5, 6, 7, 8, 9--,10, 11, 12
)
; 
update EST.P_DDV_EST.CA_VENTAS_MES_MOVIMIENTO_TEMP a
   set a.ETARIO_DESDE = tram.RAN_INI,
       a.ETARIO_HASTA = tram.RAN_FIN
  from AFI.P_RDV_DST_LND_SYB.PLANESFACTRAM tram
 where a.Edad between tram.RAN_INI and tram.RAN_FIN
   and tram.TIP_FACTRAM     = 23    
;
create or replace TABLE EST.P_DDV_EST.CA_VENTAS_MES_MOVIMIENTO AS ( 
SELECT 
	TIP_MOVIMIENTO ,
	PER_PRODUCCION ,
	CUARTO ,
	SEXO ,
	RUT ,
	FOLIOFUN ,
	TRANSAC ,
	CANAL ,
	CATEGORIA::INT AS CATEGORIA ,
	--COSTO_FINAL ,
	sum(Costo_EmpCob)::double as costo_final,
	ISAPRE_ORIGEN ,
	EDAD ,
	UBICACION ,
	ORIGEN ,
	SEGMENTO ,
	EDAD2 ,
	ETARIO_DESDE ,
	ETARIO_HASTA ,
	CARGAS ,
	REGION  ,
	TIPO_PLAN ,
	SEGMENTO_UNICO ,
	SEXO_TIPMOVI ,
	0 AS CARGAS_ORIG ,
	0 AS CATE_ORIGEN ,
	0.0::FLOAT AS COSTO_ORIGEN ,
	0 AS CATE_DESTINO ,
	0.0::FLOAT AS COSTO_DESTINO,
	'' AS SEGMENTO_ORIG ,
	'' AS SEGMENTO_DEST ,
	'' AS CAMBIO_SEGMENTO ,
	0  AS CARGAS_DEST ,
	'' AS AGENCIA_RUT ,
	'' AS AGENCIA_SUP ,
	'' AS CLASIF ,
	'' AS TIPO_PLAN_ORIG ,
	'' AS TIPO_PLAN_DEST 
	FROM EST.P_DDV_EST.CA_VENTAS_MES_MOVIMIENTO_TEMP
	GROUP BY 
		TIP_MOVIMIENTO ,
	PER_PRODUCCION ,
	CUARTO ,
	SEXO ,
	RUT ,
	FOLIOFUN ,
	TRANSAC ,
	CANAL ,
	CATEGORIA ,
	ISAPRE_ORIGEN ,
	EDAD ,
	UBICACION ,
	ORIGEN ,
	SEGMENTO ,
	EDAD2 ,
	ETARIO_DESDE ,
	ETARIO_HASTA ,
	CARGAS ,
	REGION  ,
	TIPO_PLAN ,
	SEGMENTO_UNICO ,
	SEXO_TIPMOVI ,
	CARGAS_ORIG ,
	CATE_ORIGEN ,
	COSTO_ORIGEN ,
	CATE_DESTINO ,
	COSTO_DESTINO,
	SEGMENTO_ORIG ,
	SEGMENTO_DEST ,
	CAMBIO_SEGMENTO ,
	CARGAS_DEST ,
	AGENCIA_RUT ,
	AGENCIA_SUP ,
	CLASIF ,
	TIPO_PLAN_ORIG ,
	TIPO_PLAN_DEST 
	)
;
-------------------------------- FIN VENTAS NUEVAS




-------------------------------------------------------------
--------------CAMBIOS DE PLAN---------------------------------
------------- MENSUAL/MOVIMIENTOS MES ANTERIOR/CAMBIOS DE PLAN
-- cambios de plan mes actual hasta ayer
-------- ESTE DECLARE DEBE QUEDAR MONTADO EN TABLEU
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_CAMB_PLAN_MES_MOVIMIENTO_TEMP AS (
SELECT 
	   'cambio' AS tip_movimiento,
	   est.P_DDV_EST.FECHA_PERIODO(f."fld_funnotiffec")::string as Per_produccion,
	   case f."fld_firmaweb" when 1 
       		then 'Remoto' 
       		else 'Presencial' 
       end as canal,
       (date_part(day, f."fld_funnotiffec") / cua."largo_semana")::int + 1 as cuarto, --chequear columnas  jorge montero
       case 
       		when f."fld_cotsexo" = 1 
       		then 'Hombre' 
       		else 'Mujer' 
       end as sexo,
       f."fld_cotrut"         as Rut,
       f."fld_funfolio"       as FolioFun,      
       c."fld_cntcateg"       as Cate_origen,
       c."fld_concostototal"::double as costo_origen,
       f."fld_plancod" ::INT      as Cate_destino,
       e."fld_concostofinal"  as Costo_EmpCob,
       (datediff(month, f."fld_cotnacfec", f."fld_funnotiffec") / 12)  as Edad,
       case
       		when f."fld_region" = 13 then 'Santiago'
       		else 'Regiones'
       end as Ubicacion,
       orig.SEGMENTO    as segmento_orig,
       dest.SEGMENTO    as segmento_dest,
       case 
       	   when orig.SEGMENTO = dest.SEGMENTO
           then 'NO' 
           else 'SI'  
       end as cambio_segmento,
       --tram.RAN_INI     as etario_desde,
       --tram.RAN_FIN     as etario_hasta,
       0::int AS etario_desde,
       0::int AS etario_hasta,
       c."fld_carganum"   as cargas_orig,
       f."fld_totben" - 1 as cargas_dest,
       f."fld_funagencianum" as agencia_rut,
       age.FLD_NOMBRE     as agencia_sup,
       --''::varchar(35) AS agencia_sup,
       c."fld_clasifica"     as clasif,
       ORIG.TIPO_PLAN AS tipo_plan_orig,
       DEST.TIPO_PLAN AS tipo_plan_dest,
       CASE
       		WHEN dest.SEGMENTO = 'Bajo+'  THEN 'Bajo'
			WHEN dest.SEGMENTO = 'Medio+' THEN 'Medio'
			WHEN dest.SEGMENTO = 'Alto+'  THEN 'Alto'
			WHEN dest.SEGMENTO = 'L.E. Reg+' THEN 'L.E. Reg'
			ELSE dest.SEGMENTO
	   END AS segmento_unico,
	   CONCAT(sexo,', ',TIP_MOVIMIENTO) AS sexo_tipmovi
from 	ISA.P_RDV_DST_LND_SYB.FUN f
	    INNER JOIN ISA.P_RDV_DST_LND_SYB.EMP_COB e ON (f."fld_funfolio" = e."fld_funfolioorig")
	    INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT c ON (f."fld_contrato" = c."fld_funfolio" and f."fld_correlativo" = c."fld_funcorrel" and f."fld_plancod"::int <> c."fld_cntcateg")
	    INNER JOIN AFI.P_RDV_DST_LND_SYB.LFC l ON (f."fld_funfolio" = l."fld_folio")
	    INNER JOIN ISA.P_RDV_DST_LND_SYB.PREC_MES_JMM cua ON (date_part(month, f."fld_funnotiffec") = cua."mes")
	    --INNER JOIN AFI.P_RDV_DST_LND_SYB.PLANESFACTRAM tram ON ((datediff(month, f."fld_cotnacfec", f."fld_funnotiffec")/ 12) between tram.RAN_INI and tram.RAN_FIN AND tram.TIP_FACTRAM = 23)
	    INNER JOIN ISA.P_RDV_DST_LND_SYB.FUNAGV age ON (f."fld_funagencianum" = age.FLD_AGENCIARUT and age.FLD_AGECOD = 0)
	    LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES orig ON (c."fld_cntcateg" = orig."COD_DESACA")
	    LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES dest ON (CASE  WHEN f."fld_plancod" =' ' THEN '-10'
	   															 WHEN f."fld_plancod" =''  THEN '-10'
	   		 													 ELSE f."fld_plancod" 
	   		 											    END::int = dest.COD_DESACA)	   		 											   
WHERE  f."fld_periodoprod" BETWEEN '2019-01-01 00:00:00' and  add_months(last_day(getdate()),-1)::DATETIME---inicio 1 meses atras ---@desde
	   and f."fld_plancod"   NOT in( '',' ')
	   and (f."COD_PROCESO"      = 0 or f."COD_PROCESO" is null)	   
 order by
       1, 2, 3 )
 -------------------------       
; 
update EST.P_DDV_EST.CA_CAMB_PLAN_MES_MOVIMIENTO_TEMP a
   set a.ETARIO_DESDE = tram.RAN_INI,
       a.ETARIO_HASTA = tram.RAN_FIN
  from AFI.P_RDV_DST_LND_SYB.PLANESFACTRAM tram
 where a.Edad between tram.RAN_INI and tram.RAN_FIN
   and tram.TIP_FACTRAM     = 23    
;   
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_CAMB_PLAN_MES_MOVIMIENTO AS (
select tip_movimiento,
	   Per_produccion,
	   canal,
       cuarto,
       sexo,
       Rut,
       FolioFun,
       Cate_origen,
       costo_origen AS COSTO_ORIGEN,
       Cate_destino::INT AS CATE_DESTINO,
       sum(Costo_EmpCob)::double as costo_destino,
       Edad::int AS edad,
       Ubicacion,
       segmento_orig,
       segmento_dest,
       cambio_segmento,
       etario_desde,
       etario_hasta,
       cargas_orig,
       cargas_dest,
       agencia_rut,
       agencia_sup,
       clasif,
       tipo_plan_orig,
       tipo_plan_dest,
       segmento_unico,
       sexo_tipmovi,
       '' AS TRANSAC ,
	   0::INT  AS CATEGORIA ,
	   0::DOUBLE AS COSTO_FINAL ,
	   '' AS ISAPRE_ORIGEN ,
	   '' AS ORIGEN ,
	   '' AS SEGMENTO ,
	   0 AS EDAD2 ,
	   0 AS CARGAS,
	   '' AS REGION,
	   '' AS TIPO_PLAN
 FROM EST.P_DDV_EST.CA_CAMB_PLAN_MES_MOVIMIENTO_TEMP
 GROUP BY
 	   tip_movimiento,
       Per_produccion,
       canal,
       cuarto,
       sexo,
       Rut,
       FolioFun,
       Cate_origen,
       costo_origen,
       Cate_destino,
       Edad,
       Ubicacion,
       segmento_orig,
       segmento_dest,
       cambio_segmento,
       etario_desde,
       etario_hasta,
       cargas_orig,
       cargas_dest,
       agencia_rut,
       agencia_sup,
       clasif,
       tipo_plan_orig,
       tipo_plan_dest,
       segmento_unico,
       sexo_tipmovi,
       TRANSAC ,
	   CATEGORIA ,
	   COSTO_FINAL ,
	   ISAPRE_ORIGEN ,
	   ORIGEN ,
	   SEGMENTO ,
	   EDAD2 ,
	   CARGAS,
	   REGION,
	   TIPO_PLAN
)
------------------------------------ FIN CAMBIOS DE PLAN


SELECT DISTINCT SEXO_TIPMOVI FROM EST.P_DDV_EST.CA_CAMB_PLAN_MES_MOVIMIENTO



SELECT PER_PRODUCCION , count(DISTINCT RUT)
FROM EST.P_DDV_EST.CA_DESAFI_MES_MOVIMIENTO 
WHERE PER_PRODUCCION >= 202101
GROUP BY PER_PRODUCCION 
ORDER BY PER_PRODUCCION 
--LIMIT 10

---------------------DESAFILICIONES
--------- QUERY CON LA EXTRACCION DE DESAFILIACIONES
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_DESAFI_MES_MOVIMIENTO AS ( 
select DISTINCT
	   'desafiliacion'                                                              AS tip_movimiento,
       est.P_DDV_EST.FECHA_PERIODO(car."car_fecha_recep")::string                   as Per_produccion,
       (date_part(day, car."car_fecha_recep") / cua."largo_semana")::int + 1        as cuarto,
       case when b."fld_bensexo" = 1 then 'Hombre' else 'Mujer' end 			    as sexo,
       car."car_rut_afil" 															as Rut,
       c."fld_cntcateg"   															as Cate_origen,
       replace(c."fld_concostototal"::double, '.', ',') 							as costo_origen,
       dest.GLS_ISAPRE  															as Isapre_Destino,
       (datediff(month, b."fld_bennacfec", car."car_fecha_recep") / 12)::int	    as Edad,
--        tram.RAN_INI     as 'etario_desde',
--        tram.RAN_FIN     as 'etario_hasta',
       0 																			as etario_desde,
       0 																			as etario_hasta,
       cat.Segmento            														as segmento, -- segmento lo agregara Hans
       c."fld_carganum"   															as cargas,
       c."fld_clasifica",
       car."car_plan" 
  from ISA.P_RDV_DST_LND_SYB."dsf_carta" car 
       inner JOIN AFI.P_RDV_DST_LND_SYB.CNT c 				ON (car."car_rut_afil" = c."fld_cotrut")
       inner JOIN AFI.P_RDV_DST_LND_SYB.BEN b				ON (c."fld_funfolio"   = b."fld_funfolio" AND c."fld_funcorrel" = b."fld_funcorrel" AND c."fld_cotrut" = b."fld_benrut" AND b."fld_bencorrel" = 0 )
       INNER JOIN ISA.P_RDV_DST_LND_SYB.PREC_MES_JMM cua 	ON (date_part(month, car."car_fecha_recep") = cua."mes" )
       inner JOIN ISA.P_RDV_DST_LND_SYB.ISAPREISAPRE dest    ON (car."car_isapre_dstno"  = dest.COD_ISAPRE) ---- revisar en caso de tener diferencias 
       inner JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES cat        ON (c."fld_cntcateg"= cat.COD_DESACA)      
       													--			CASE  WHEN c."fld_cntcateg" =' ' THEN '-10'
	   													--		      WHEN c."fld_cntcateg" =''  THEN '-10'
	   		 											--		      ELSE c."fld_cntcateg" 
	   		 											 --       END::int )
       --PREC_CATEG_JMM cat, -- segmento lo agregara Hans
       --PLANESFACTRAM tram*/
 where car."car_fecha_recep" between '2019-01-01 00:00:00' and add_months(last_day(getdate()),-1)::DATETIME---inicio 1
   and car."car_estado_carta"  = 2
   AND  car."car_fecha_recep" between c."fld_inivigfec" and c."fld_finvigfec"
   ---and car."car_fecha_recep"  <= @hora_tope
--   and c.fld_cntcateg        = cat.COD_DESACA -- segmento lo agregara Hans
--    and (datediff(month, b.fld_bennacfec, car.car_fecha_recep) / 12) between tram.RAN_INI and tram.RAN_FIN
--    and tram.TIP_FACTRAM      = 23
 order by
       1, 2, 3, 4
-- at isolation read uncommitted
);
--------------------------------------------
------------------ revisar si es necesario agragar esta parte a las desafiliaciones
--------------------
/*
SELECT * FROM EST.P_DDV_EST.ca_desfi_test LIMIT 10

SELECT PER_PRODUCCION , count(1) FROM EST.P_DDV_EST.CA_DESAFI_MES_MOVIMIENTO 
GROUP BY per_produccion   
ORDER BY per_produccion LIMIT 10


update #tmp
   set t.etario_desde = tram.RAN_INI,
       t.etario_hasta = tram.RAN_FIN
  from #tmp t,
       PLANESFACTRAM tram
 where t.Edad between tram.RAN_INI and tram.RAN_FIN
   and tram.TIP_FACTRAM     = 23

select Per_produccion             + ';' +
       convert(varchar, cuarto)   + ';' +
       sexo                       + ';' +
       Rut                        + ';' +
       convert(varchar, Cate_origen)  + ';' +
       costo_origen                   + ';' +
       Isapre_Destino                 + ';' +
       convert(varchar, Edad)         + ';' +
       convert(varchar, etario_desde) + ';' +
       convert(varchar, etario_hasta) + ';' +
       segmento                       + ';' +
       convert(varchar, cargas)       + ';' +
       fld_clasifica
  from #tmp
  */
------------------------------------------
---------------------- FIN DESAFILIACIONES


 
 SELECT DISTINCT TIP_MOVIMIENTO , SEXO , SEXO_TIPMOVI FROM EST.P_DDV_EST.CA_MIX_MES_MOVIMIENTO LIMIT 10
 
 SELECT 
 	PER_PRODUCCION ,
 	count(1)
 FROM EST.P_DDV_EST.CA_MIX_MES_MOVIMIENTO 
 GROUP BY PER_PRODUCCION 
 ORDER BY PER_PRODUCCION desc
 --LIMIT 10


-----------------UNION TABLA VENTA Y CAMBIO DE PLAN
  CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_MIX_MES_MOVIMIENTO AS ( 
  SELECT 
    TIP_MOVIMIENTO ,
	PER_PRODUCCION ,
	CANAL ,
	CUARTO ,
	SEXO ,
	RUT ,
	FOLIOFUN ,
	CATE_ORIGEN ,
	COSTO_ORIGEN,
	CATE_DESTINO ,
	COSTO_DESTINO,
	EDAD ,
	UBICACION ,
	SEGMENTO_ORIG ,
	SEGMENTO_DEST ,
	CAMBIO_SEGMENTO ,
	ETARIO_DESDE ,
	ETARIO_HASTA ,
	CARGAS_ORIG ,
	CARGAS_DEST ,
	AGENCIA_RUT ,
	AGENCIA_SUP ,
	CLASIF ,
	TIPO_PLAN_ORIG ,
	TIPO_PLAN_DEST ,
	SEGMENTO_UNICO ,
	SEXO_TIPMOVI ,
	TRANSAC ,
	CATEGORIA ,
	COSTO_FINAL ,
	ISAPRE_ORIGEN ,
	ORIGEN ,
	SEGMENTO ,
	EDAD2 ,
	CARGAS ,
	REGION ,
	TIPO_PLAN 
    FROM EST.P_DDV_EST.CA_VENTAS_MES_MOVIMIENTO 
   	UNION  ALL 
    SELECT 
    TIP_MOVIMIENTO ,
	PER_PRODUCCION ,
	CANAL ,
	CUARTO ,
	SEXO ,
	RUT ,
	FOLIOFUN ,
	CATE_ORIGEN ,
	COSTO_ORIGEN,
	CATE_DESTINO ,
	COSTO_DESTINO,
	EDAD ,
	UBICACION ,
	SEGMENTO_ORIG ,
	SEGMENTO_DEST ,
	CAMBIO_SEGMENTO ,
	ETARIO_DESDE ,
	ETARIO_HASTA ,
	CARGAS_ORIG ,
	CARGAS_DEST ,
	AGENCIA_RUT ,
	AGENCIA_SUP ,
	CLASIF ,
	TIPO_PLAN_ORIG ,
	TIPO_PLAN_DEST ,
	SEGMENTO_UNICO ,
	SEXO_TIPMOVI ,
	TRANSAC ,
	CATEGORIA ,
	COSTO_FINAL ,
	ISAPRE_ORIGEN ,
	ORIGEN ,
	SEGMENTO ,
	EDAD2 ,
	CARGAS ,
	REGION ,
	TIPO_PLAN 
    FROM EST.P_DDV_EST.CA_CAMB_PLAN_MES_MOVIMIENTO 
)

------------------UNION DE LA TABLA VENTA NUEVA Y CAMBIOS DE PLAN 







SELECT 
	* 
FROM ISA.P_RDV_DST_LND_SYB.PRM_AGRUPA_PRESTADORES
WHERE "codigo"=28
LIMIT 10

SELECT 
	* 
FROM RcdInfBenefConAporte
















