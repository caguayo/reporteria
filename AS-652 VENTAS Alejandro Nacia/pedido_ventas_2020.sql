------------ EJECUCION DE LA TABLA
CREATE OR REPLACE  TABLE est.P_DDV_EST.CA_VENTAS_MES_MOVIM_ALEJ_NAV AS (
SELECT 
	   est.P_DDV_EST.FECHA_PERIODO(f."fld_funnotiffec")::varchar AS Per_produccion,
	   f."fld_cotrut" AS Rut,
	   (SUBSTRING(f."fld_cotrut",1,9)::int*5-3265112+52658)::varchar  AS id_rut_afiliado,
       CASE 
       		WHEN f."fld_cotsexo" = 1 
       		THEN 'Hombre' 
       		ELSE 'Mujer' 
       END AS sexo,
       f."fld_plancod"  AS Categoria,
       f."fld_ciudad" AS cuidad,
       CASE 
       		WHEN reg."GLS_WEB_REGION" IS NULL 
       		THEN 'DESCONOCIDA'
       		ELSE reg."GLS_WEB_REGION"
       END as region,
       (datediff(MONTH, f."fld_cotnacfec", f."fld_funnotiffec") / 12)::INT AS Edad,
       e."fld_concostofinal"::double AS Costo_EmpCob             
     /*f."fld_funfolio"       as FolioFun,
       (DATE_PART(day, f."fld_funnotiffec") / cua."largo_semana")::INT + 1 AS cuarto,  
       f."fld_ttracod"  as Transac,
       t."grupo"        as Canal,
       i.GLS_ISAPRE   as Isapre_origen,
       CASE  when f."fld_region" = 13 then 'Santiago' else 'Regiones' end as Ubicacion,
       CASE  when i."COD_ISAPRE" in (0, 1) then 'Fonasa' else 'Otra Isapre'   end as origen,
       cat.Segmento,
       (datediff(month, f."fld_cotnacfec", f."fld_funnotiffec") / 12)::int  as edad2,
       0::int AS etario_desde,
       0::int AS etario_hasta,
--        tram.RAN_INI as 'etario_desde',
--        tram.RAN_FIN as 'etario_hasta',
       f."fld_totben" - 1 as cargas,
       */     
  FROM ISA.P_RDV_DST_LND_SYB.FUN f
  	   LEFT JOIN ISA.P_RDV_DST_LND_SYB.EMP_COB e ON (f."fld_funfolio"  = e."fld_funfolioorig")
       LEFT JOIN ISA.P_RDV_DST_LND_SYB.PREC_MES_JMM cua ON (date_part(month, f."fld_funnotiffec") = cua."mes")
       LEFT JOIN ISA.P_RDV_DST_LND_SYB.PREC_TIPFUN t ON (f."fld_tipfun" = t."cod")
       LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES cat ON (CASE WHEN f."fld_plancod" =' ' THEN '-10'
   		    												  WHEN f."fld_plancod" =''  THEN '-10'
   		    												  ELSE f."fld_plancod" 
   	   													 END::int = cat.COD_DESACA)	
       LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION reg ON (f."fld_region" = reg.COD_REGION) 
       LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPREISAPRE i   ON (f."fld_isapreantcod"  = i.COD_ISAPRE)               
 WHERE f."fld_periodoprod" BETWEEN dateadd('month',-12, time_slice(current_date(), 1, 'month', 'start'))::DATETIME AND  add_months(last_day(getdate()),-1)::DATETIME  -->= @desde
   AND f."fld_guiarem"        = 1
   AND f."fld_funnotificacod" = 1   
   AND f."fld_emifec"        <= TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59') ---termino_mes_anterior  ----4@hora_tope	   
 ORDER BY
       1, 2, 3, 4, 5, 6, 7, 8, 9--,10, 11, 12
)


SELECT top 1
dateadd('month',-1, time_slice(current_date(), 1, 'month', 'start'))::DATETIME AS ini_PERIODO,
add_months(last_day(getdate()),-1)::DATETIME AS fin_periodo,
add_months(last_day(getdate()),-1)::DATETIME AS hoy,
TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),-1))||' 23:59:59') AS test,
ISAPRE_ORIGEN 
FROM EST.P_DDV_EST.CA_AFILIACIONES 


SELECT Per_produccion,count(1) FROM est.P_DDV_EST.CA_VENTAS_MES_MOVIM_ALEJ_NAV
GROUP BY Per_produccion
ORDER BY Per_produccion


SELECT top 10 * FROM est.P_DDV_EST.CA_VENTAS_MES_MOVIM_ALEJ_NAV


SELECT top 10 * FROM ISA.P_RDV_DST_LND_SYB.FUN
WHERE "fld_cotrut" ='027029250-K'


SELECT 
"fld_cotrut" , 
"fld_periodoprod" , 
"fld_funfolio", 
"fld_funnotiffec", 
"fld_plancod", 
"fld_emifec",
"fld_guiarem" ,
"fld_funnotificacod", 
"fld_tipfun" 
FROM ISA.P_RDV_DST_LND_SYB.FUN
WHERE "fld_cotrut" = '014389850-4'
ORDER BY "fld_periodoprod" 
