
---- extraccion
SELECT 
	--"fld_funfolio", 
	--"fld_funcorrel" ,
	--"fld_cotrut" , 
	--"fld_benrut" , 
	--"fld_bencorrel" , 
	--"fld_bennacfec" ,
	--"fld_bensexo"  
	CASE WHEN 
	CASE WHEN ben."fld_bencorrel" = 0 THEN 'Cotizante' ELSE 'Carga' END AS tipo_beneficiario,
	(datediff(month, ben."fld_bennacfec" , getdate()) / 12)::int  as Edad,
	case WHEN ben."fld_bensexo" = 1 then 'Hombre' else 'Mujer' end as sexo,
	COUNT(DISTINCT(ben."fld_benrut")) AS cantidad_beneficiarios
FROM AFI.P_RDV_DST_LND_SYB.BEN ben
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (ben."fld_funfolio"= cnt."fld_funfolio" AND ben."fld_funcorrel"=cnt."fld_funcorrel") 
WHERE 202107 BETWEEN FLD_BENVIGREALDESDE  AND FLD_BENVIGREALHASTA --AND edad=33
GROUP BY 
	tipo_beneficiario,
	Edad,
	sexo
ORDER BY 
	edad,
	tipo_beneficiario
--LIMIT 100

	
	
WITH cartera AS ( 
SELECT 
	cnt."fld_cotrut" AS rut_cotizante,
	ben2."fld_bensexo" AS sexo_cotizante,
	ben."fld_benrut" AS rut_beneficiario,
	ben."fld_bensexo" AS sexo_beneficiario,
	CASE WHEN ben."fld_bencorrel" = 0 THEN 'Cotizante' ELSE 'Carga' END AS tipo_beneficiario,
	(datediff(month, ben."fld_bennacfec" , getdate()) / 12)::int  as Edad_beneficiario
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
--------extraccion por beneficiario
INNER JOIN AFI.P_RDV_DST_LND_SYB.BEN ben ON (cnt."fld_funfolio" = ben."fld_funfolio" AND cnt."fld_funcorrel"=ben."fld_funcorrel")
-----extraccion por cotizante
INNER JOIN ( SELECT 
				* 
			 FROM AFI.P_RDV_DST_LND_SYB.BEN
			 WHERE 202107 BETWEEN FLD_BENVIGREALDESDE  AND FLD_BENVIGREALHASTA AND 
			 	   "fld_bencorrel" =0
		   ) ben2 ON (cnt."fld_funfolio" = ben2."fld_funfolio" AND cnt."fld_funcorrel"=ben2."fld_funcorrel" )
WHERE 202107 BETWEEN cnt.FLD_VIGREALDESDE AND cnt.FLD_VIGREALHASTA
)
SELECT 
	case WHEN sexo_cotizante = 1 then 'Hombre' else 'Mujer' end as sexo_coti,
	--case WHEN sexo_beneficiario = 1 then 'Hombre' else 'Mujer' end as sexo_benefi,
	--count(DISTINCT(rut_cotizante)) AS cant_cotizantes,
	count(DISTINCT(rut_beneficiario)) AS cant_benefi
	--count(DISTINCT(rut_beneficiario)) AS cant_beneficiarios
FROM cartera
	--WHERE tipo_beneficiario = 'Carga' --AND Edad_beneficiario >= 18
GROUP BY 
	sexo_coti
ORDER BY 
	sexo_coti
	
	

SELECT
	tipo_beneficiario,
	rut_cotizante,
	case WHEN sexo_cotizante = 1 then 'Hombre' else 'Mujer' end as sexo_coti,
	rut_beneficiario,
	case WHEN sexo_beneficiario = 1 then 'Hombre' else 'Mujer' end as sexo_benef,
	Edad_beneficiario
FROM cartera 
ORDER BY 
	 rut_cotizante, rut_beneficiario




SELECT 
	--sexo_cotizante,
	case WHEN sexo_cotizante = 1 then 'Hombre' else 'Mujer' end as sexo_coti,
	count(DISTINCT(rut_cotizante)) AS cant_cotizantes,
	--sexo_beneficiario,
	case WHEN sexo_beneficiario = 1 then 'Hombre' else 'Mujer' end as sexo_benef,
	count(DISTINCT(rut_beneficiario)) AS cant_beneficiario,
	Edad_beneficiario
FROM cartera 
GROUP BY 
	sexo_coti,
	sexo_benef,
	Edad_beneficiario
	
	
SELECT * FROM isa.P_RDV_DST_LND_SYB.SERIES   LIMIT 10

----- codigo revision

SELECT COUNT(DISTINCT("fld_cotrut")) AS cotizantes, COUNT(DISTINCT("fld_benrut")) AS beneficiarios FROM AFI.P_RDV_DST_LND_SYB.BEN
WHERE 202107 BETWEEN FLD_BENVIGREALDESDE  AND FLD_BENVIGREALHASTA



SELECT * FROM AFI.P_RDV_DST_LND_SYB.BEN LIMIT 10


SELECT * FROM AFI.P_RDV_DST_LND_SYB.CNT LIMIT 10


