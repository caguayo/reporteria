Quería pedirte la siguiente información,
-	Base de dato de las personas que en los últimos 2 años hayan tenido horas al kinesiólogo. Prestaciones tipo 11 o codate 60XXXX
-	Base de dato de las personas que en 1 año hayan ido a lo menos 1 vez al traumatólogo. Consulta traumatólogo 30147
-	Base de datos de las personas que en los últimos dos años tuvieron urgencias dentales. Urgencia dental es solo GES PS 46 o 47?
-	Base de datos de las personas que en el último año hayan tenido ingresos a urgencias. Consulta a urgencia? O ingreso a hospitalización.
-	Base de datos de los afiliados los cuales sus cargan menores a 18 años tuvieron más de 1 ingreso a urgencia en los últimos 2 años. Misma duda de arriba.
