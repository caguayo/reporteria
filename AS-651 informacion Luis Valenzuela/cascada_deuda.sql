
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_DETALLE_DEUDA_OCT_SEPT_PART_4 AS (
SELECT "periodo_cotizacion" , "rut_afiliado" , "rut_empleador_paga" ,"monto_deuda" FROM COB.P_DDV_COB.P_DDV_COB_DEUDA_INICIAL
WHERE "periodo_cotizacion" BETWEEN 202001 AND 202009 
ORDER BY "periodo_cotizacion" 
)


SELECT "periodo_cotizacion", COUNT(1) FROM EST.P_DDV_EST.CA_DETALLE_DEUDA_SEPT_AGOS_PART_4
GROUP BY "periodo_cotizacion"
ORDER BY "periodo_cotizacion"