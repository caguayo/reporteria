----------- CARTERA Y BENEFICIARIOS NEONATAL / PRE-ESCOLAR / ESCOLAR
SELECT 
	* 
FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE 202108 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA 
LIMIT 10


-fecha inicio
-fecha fin 
-fecha nacimiento
-genero 
-id 



------ extraccion
----neonatal
WITH neonatal AS ( 
SELECT
	'Neonatal' AS segmento,
	--"fld_funfolio" ,
	--"fld_funcorrel" ,
	SUBSTRING("fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_titular,
	SUBSTRING("fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	/*CASE 
		WHEN "fld_bencorrel" = 0 THEN 'Titular'
		ELSE 'Carga'
	END AS identificardor,*/
	"fld_beninivigfec" AS inicio_vigenci,
	"fld_benfinvigfec" AS fin_vigencia,
	"fld_bennacfec" AS fecha_nacimiento,
	CASE 
		WHEN "fld_bensexo" = 1 THEN 'HOMBRE'
		ELSE 'MUJER'
	END AS sexo
	--(datediff(day, "fld_bennacfec" , getdate()))::int AS dias_nacimiento
	--(datediff(month, "fld_bennacfec" , getdate()) / 12)::int AS edad
FROM AFI.P_RDV_DST_LND_SYB.BEN 
WHERE 202109 BETWEEN FLD_BENVIGREALDESDE AND FLD_BENVIGREALHASTA 
HAVING (datediff(day, "fld_bennacfec" , getdate()))::int  <=29
)
SELECT * FROM neonatal;
-----pre escolar
WITH preescolar AS ( 
SELECT
	'Pre Escolar' AS tipo,
	--"fld_funfolio" ,
	--"fld_funcorrel" ,
	SUBSTRING("fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_titular,
	SUBSTRING("fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	/*CASE 
		WHEN "fld_bencorrel" = 0 THEN 'Titular'
		ELSE 'Carga'
	END AS identificardor,*/
	"fld_beninivigfec" AS inicio_vigenci,
	"fld_benfinvigfec" AS fin_vigencia,
	"fld_bennacfec" AS fecha_nacimiento,
	CASE 
		WHEN "fld_bensexo" = 1 THEN 'HOMBRE'
		ELSE 'MUJER'
	END AS sexo
	--(datediff(day, "fld_bennacfec" , getdate()))::int AS dias_nacimiento
	--(datediff(month, "fld_bennacfec" , getdate()) / 12)::int AS edad
FROM AFI.P_RDV_DST_LND_SYB.BEN 
WHERE 202109 BETWEEN FLD_BENVIGREALDESDE AND FLD_BENVIGREALHASTA AND (datediff(month, "fld_bennacfec" , getdate()) / 12)::int <= 5
HAVING (datediff(day, "fld_bennacfec" , getdate()))::int >29
)
SELECT * FROM preescolar 
;
------- escolar
WITH escolar AS ( 
SELECT
	'Escolar' AS tipo,
	--"fld_funfolio" ,
	--"fld_funcorrel" ,
	SUBSTRING("fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_titular,
	SUBSTRING("fld_benrut",1,9)::int*5-3265112+52658  AS id_rut_beneficiario,
	/*CASE 
		WHEN "fld_bencorrel" = 0 THEN 'Titular'
		ELSE 'Carga'
	END AS identificardor,*/
	"fld_beninivigfec" AS inicio_vigenci,
	"fld_benfinvigfec" AS fin_vigencia,
	"fld_bennacfec" AS fecha_nacimiento,
	CASE 
		WHEN "fld_bensexo" = 1 THEN 'HOMBRE'
		ELSE 'MUJER'
	END AS sexo
	--(datediff(day, "fld_bennacfec" , getdate()))::int AS dias_nacimiento
	--(datediff(month, "fld_bennacfec" , getdate()) / 12)::int AS edad
FROM AFI.P_RDV_DST_LND_SYB.BEN 
WHERE 202109 BETWEEN FLD_BENVIGREALDESDE AND FLD_BENVIGREALHASTA
	  AND (datediff(month, "fld_bennacfec" , getdate()) / 12)::int BETWEEN 6 AND 18
)
SELECT * FROM escolar
;






----------- extraccion prestacion 
-----partos ultimos 3 a�os
WITH partos_mujeres AS ( 
SELECT DISTINCT
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_TRAZADORA , 
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (2004003,2004004,2004005,2004006,2004013) --AND PRESTACION IN (1)
)
SELECT * FROM partos_mujeres LIMIT 100
------- fin partos de mujeres



------ NEO NATAL 
---------- 1
SELECT DISTINCT
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE CODATE_TRAZADORA IN (30135, 30123) --AND PRESTACION IN (1)
LIMIT 100
----------



---------- 2
SELECT DISTINCT
	 periodo,
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE codate_trazadora IN (404122)--AND PRESTACION IN (1)
LIMIT 100
------------


----------- 3
SELECT DISTINCT
	 periodo,
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE cod_cie LIKE 'O%' AND cod_cie NOT LIKE 'O8%' AND FUENTE_ORIGEN NOT IN ('LIQ')
LIMIT 100 
-------------


------------ 4
SELECT DISTINCT
	 periodo,
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE fuente_origen IN ('HMQ','EAH') AND codate_trazadora IN (101007) --AND PRESTACION IN (1)
LIMIT 100
-------------


SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_EAH
WHERE "eah_rutcot" ='016124690-5' AND "eah_conefe" > '2021-01-01 00:00:00' LIMIT 10



SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_DPREFA
WHERE 	--"dpr_codrub" = 30 AND 
		"dpr_codidn" IN (9971417, 9971376,9971377,9971379,9971382,9971380,9971383,9971378) LIMIT 100 ---"dpr_nroepr" = 2889465
		

						
--------------- 5 incubadora
SELECT 
	epref."epr_nrostr" ,
	dpref."dpr_codrub" ,
	dpref."dpr_nroepr" ,
	eah."eah_rutcot" ,
	eah."sol_benrut" ,
	eah."eah_conefe" ,
	eah."eah_idn" ,
	eah."eah_cobtot"::int AS cobrado ,
	eah."eah_valbon"::int AS  bonificado ,
	eah."fld_gescaec"::int AS ges_caec 
FROM HOS.P_RDV_DST_LND_SYB.SH_EAH eah
LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EPREFA epref ON (eah."sol_nrosol" = epref."epr_nrostr")
LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_dPREFA dpref ON (epref."epr_codidn" = dpref."dpr_nroepr") -- AND dpref."dpr_codrub" = 30)
WHERE "eah_conefe" BETWEEN '2021-01-01 00:00:00' AND '2021-08-30 23:59:59' AND  dpref."dpr_codrub" = 30 --AND eah."eah_rutcot" ='025957469-2'
-----------------



------------ 6 al universo de las embarazadas hay que sacar las prestaciones tipo 13
SELECT DISTINCT
	 PERIODO,
	 FOLIO,
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PRESTACION IN (13)
ORDER BY PERIODO
LIMIT 100
---------------






-------------- PRE ESCOLAR
------- cobertura pre escolar accidentes, quemaduras, etc.... CIE TODOS LOS QUE COMIENZAN CON S Y T
SELECT DISTINCT
	 PERIODO,
	 FOLIO,
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE  	(cod_cie LIKE 'S%' OR cod_cie LIKE 'T%' )
		AND FUENTE_ORIGEN NOT IN ('LIQ')
ORDER BY PERIODO
LIMIT 100




------------ TRASLADO (AEREO O TERRESTRE)
SELECT DISTINCT
	 PERIODO,
	 FOLIO,
	 RUT_TITULAR ,
	 RUT_BENEFICIARIO ,
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_DETALLE,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PRESTACION IN (12)  AND FUENTE_ORIGEN NOT IN ('LIQ') --AND RUT_TITULAR ='012846834-K' --AND CODATE_TRAZADORA IS NOT NULL
ORDER BY PERIODO 
LIMIT 100
---------------




----------ESCOLAR
------- cobertura pre escolar accidentes, quemaduras, etc.... CIE TODOS LOS QUE COMIENZAN CON S Y T
SELECT DISTINCT
	 PERIODO,
	 FOLIO,
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE cod_cie LIKE 'S%' AND cod_cie LIKE 'T%' AND FUENTE_ORIGEN NOT IN ('LIQ')
ORDER BY PERIODO
LIMIT 100




------------ TRASLADO (AEREO O TERRESTRE)
SELECT DISTINCT
	 PERIODO,
	 FOLIO,
	 RUT_TITULAR ,
	 RUT_BENEFICIARIO ,
	 ORIGEN ,
	 FUENTE_ORIGEN ,
	 PRESTACION ,
	 CODATE_DETALLE,
	 CODATE_TRAZADORA , 
	 glosa_codate_trazadora,
	 CODATE_TRAZADORA_2 , 
	 CODATE_TRAZADORA_3 , 
	 COD_CIE ,
	 GLOSA_CIE ,
	 PROBLEMA_SALUD ,
	 GLS_PROBLEMA_SALUD ,
	 ORIGEN 
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE PRESTACION IN (12)  AND FUENTE_ORIGEN NOT IN ('LIQ') --AND RUT_TITULAR ='012846834-K' --AND CODATE_TRAZADORA IS NOT NULL
ORDER BY PERIODO 
LIMIT 100
---------------





--------- EXTRAER A LOS 3 A�OS 
SELECT * FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
LIMIT 10









------------------------- revision codigo
SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_EPREFA
WHERE "epr_codidn" =2786789

SELECT 
	* 
FROM HOS.P_RDV_DST_LND_SYB.SH_DPREFA
WHERE "dpr_codidn" = 2786789 LIMIT 10---unir min(epr_codidn) drp_cobrub = 30

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SISHOSPRUBRO

SELECT * FROM eah

-----------------------------------------











