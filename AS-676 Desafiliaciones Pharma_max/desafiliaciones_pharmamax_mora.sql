SELECT 
	*
FROM AFI.P_RDV_DST_LND_SYB.LFC 
LIMIT 10

SELECT 
      /* bad_pharma */
    (case
    	when "fld_tipopharma" = 5 then 'Pharma Max'
    else 'NO'
    end) AS bad_pharma, 
	*
FROM AFI.P_RDV_DST_LND_SYB.CNT 
LIMIT 10


SELECT 
*
FROM ISA.P_RDV_DST_LND_SYB."dsf_historia" 
LIMIT 10




WITH desafi_pharma AS (  
SELECT
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS periodo,
	(case
    	when cnt."fld_tipopharma" = 5 then 'Pharma Max'
    	else 'NO'
    end) AS bad_pharma, 
    lfc."fld_cotrut" as rut_afiliado,
    SUBSTRING(lfc."fld_cotrut",1,9)::int*5-3265112+52658  AS id_rut_afiliado,
    lfc."fld_funfolio" as contrato,
    lfc."fld_funcorrel" as correlativo,
    --lfc."fld_periodoprod"	as fecha,
    case (lfc."fld_funnotificacod")
	    when 2 then 'DESAFILIACION ISAPRE'
		when 333 then 'RENUNCIA VOLUNTARIA'
    end as tipo_desaf, 
    CASE 
    	WHEN "fld_motivodesaf" IN (1,2,87) THEN 'MORA'
		--WHEN "fld_motivodesaf"= 2 THEN 'MORA RESP.AFILIADO'  
		--WHEN "fld_motivodesaf"= 87 THEN 'MORA TRAB. DEPEND.'
    	ELSE 'OTRO MOTIVO'
	END AS motivo,
	    CASE 
    	WHEN "fld_motivodesaf"= 1 THEN 'MORA VOLUNT/INDEP'
		WHEN "fld_motivodesaf"= 2 THEN 'MORA RESP.AFILIADO'  
		WHEN "fld_motivodesaf"= 87 THEN 'MORA TRAB. DEPEND.'
    	ELSE 'OTRO MOTIVO'
	END AS motivo2
    --cnt."fld_ingreso" as fecha_ingreso
from AFI.P_RDV_DST_LND_SYB.LFC lfc
	inner join AFI.P_RDV_DST_LND_SYB.CNT cnt on lfc."fld_funfolio" = cnt."fld_funfolio"	and lfc."fld_funcorrel" = cnt."fld_funcorrel" and "fld_periodoprod" between '2021-01-01 00:00:00' and '2021-12-31 23:59:59'
	and ("fld_funnotificacod"=2 or "fld_funnotificacod"=333)
ORDER BY periodo
)
SELECT * FROM desafi_pharma --LIMIT 10


SELECT
	periodo,
	tipo_desaf,
	COUNT(DISTINCT id_rut_afiliado) 
FROM desafi_pharma
GROUP BY 	periodo,
			tipo_desaf
ORDER BY periodo
--LIMIT 100





SELECT 
	*
FROM ISA.P_RDV_DST_LND_SYB.DS_ESTADO 
LIMIT 10


WITH alemana AS (
select distinct mpl.COD_DESACA as CATEGORIA , p."agrupacion" AS CLINICA
FROM "AFI"."P_RDV_DST_LND_SYB"."PLA_MPL" mpl
inner join "AFI"."P_RDV_DST_LND_SYB"."PLA_DPL" dpl on (mpl."mpl_idn"=dpl."mpl_idn")
inner join "AFI"."P_RDV_DST_LND_SYB"."PLA_EPL" epl on (dpl."dpl_idn"=epl."dpl_idn")
inner JOIN "ISA"."P_RDV_DST_LND_SYB"."PRM_AGRUPA_PRESTADORES" p on (p."rut"=epl."fld_medrut")
where p."codigo" in (2,11,28)
and epl."fld_bonporc">50
and mpl."fld_plantipocod" ='I'
), 
desafi_alemana as ( 
SELECT 
	*
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
	INNER JOIN  AFI.P_RDV_DST_LND_SYB.CNT cnt on (lfc."fld_funfolio" = cnt."fld_funfolio" and lfc."fld_funcorrel" = cnt."fld_funcorrel")
	INNER JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES prm ON (cnt."fld_cntcateg"=prm.COD_DESACA)
	INNER JOIN alemana ale ON (cnt."fld_cntcateg" = ale.categoria)
	WHERE  "fld_periodoprod" between '2021-01-01 00:00:00' and '2021-12-31 23:59:59'
	and (lfc."fld_funnotificacod"=333)
)
SELECT 
	est.P_DDV_EST.fecha_periodo("fld_periodoprod") AS periodo,
	count(1)
--ORDER BY periodo
FROM desafi_alemana
GROUP BY periodo
ORDER BY periodo
LIMIT 12



SELECT 
	*
FROM EST.P_DDV_EST.PRM_AGRUPA_PLANES LIMIT 100


SELECT 
	*
FROM ISA.P_RDV_DST_LND_SYB."dsf_carta"
WHERE "car_fecha_recep" between '2021-01-01 00:00:00' AND '2021-12-31 23:59:59' AND "car_estado_carta" =2 --AND "car_motivo_desaf" IN (1,2,87)
LIMIT 1000


SELECT 
	*
FROM ISA.P_RDV_DST_LND_SYB."dsf_desaf" 
WHERE  CONTAINS("des_gls_motivo",'MORA') OR CONTAINS("des_gls_motivo",'MORO') 
LIMIT 100

SELECT 

---DTI_MTO_DIF_CTO