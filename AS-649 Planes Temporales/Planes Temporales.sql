-----BASE MONITOREO PLAN TEMPORAL
-----EXTRACCION PLANES TEMPORALES
CREATE OR REPLACE TEMPORARY TABLE AFI.P_DDV_AFI.P_DDV_AFI_CA_PLAN_TEMP AS(
-------todos los planes temporales
WITH RECURSIVE 
BASE_TEMP AS (
	SELECT 
	lfc."fld_periodoprod" AS periodo_produccion,
	lfc."fld_funfolio" AS funfolio,
	lfc."fld_funcorrel" AS funcorrel, 
	lfc."fld_cotrut" AS rut_cot,
	cnt."fld_cotnombre" AS nombre_afiliado,
	cnt."fld_cotapepa" AS apellido_paterno,
	cnt."fld_cotapema" AS apellido_materno,
	lfc."fld_emprut" AS rut_empleador, 
	lfc."edad_cotiz" AS edad_cot,
	CASE 
		 WHEN ben."fld_bensexo" = 1 
		 THEN 'Hombre' 
		 ELSE 'Mujer'
	END AS sexo,
	cnt."fld_region" AS cod_region,
	reg.GLS_REGION AS gls_region,
	cnt."fld_carganum" AS cant_cargas,
	cnt."fld_mail" AS mail,
	cnt."fld_telefono" AS telefono,
	cnt."fld_comunacod" AS cod_comuna,
	comu.GLS_COMUNA AS gls_comuna,
	lfc."fld_cntcateg" AS cod_categoria,
	isa.GLS_DESACA AS glosa_categoria,
	cnt."fld_clasifica" AS clasi_riesgo,
	cnt."fld_concostototal"::double costo_total,
	--cnt."fld_ingreso" AS fecha_ingreso_isa,
	lfc."fld_fechafun" AS fecha_ing_pl_temp,
	cnt."fld_inivigfec" AS fecha_vig_pl_temp,
	datediff(month, cnt."fld_inivigfec", getdate()) AS permanencia_plan_temp, 
	cnt."fld_funfolioant" funfolio_anterior,
	cnt."fld_funantcorrel" funcorrel_anterior,
	lfc."fld_cntcategant"::int AS cod_categoria_anterior,
	isados.GLS_DESACA AS glosa_categoria_anterior
	FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
	INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio" = cnt."fld_funfolio" AND lfc."fld_funcorrel" = cnt."fld_funcorrel")
	INNER JOIN (SELECT * FROM AFI.P_RDV_DST_LND_SYB.BEN WHERE "fld_bencorrel" =0) ben ON (lfc."fld_funfolio" =ben."fld_funfolio" AND lfc."fld_funcorrel"=ben."fld_funcorrel")
	INNER JOIN AFI.P_RDV_DST_LND_SYB.ISAPREDESACA isa ON (lfc."fld_cntcateg" = isa.COD_DESACA)
	INNER JOIN AFI.P_RDV_DST_LND_SYB.ISAPREDESACA isados ON (lfc."fld_cntcategant" = isados.COD_DESACA)
	INNER JOIN ISA.P_RDV_DST_LND_SYB.ISAPREREGION reg ON (cnt."fld_region" = reg.COD_REGION)
	INNER JOIN ISA.P_RDV_DST_LND_SYB.ISAPRECOMUNAS comu ON (cnt."fld_comunacod"= comu.COD_COMUNA)
	WHERE lfc."fld_cntcateg" = 8477)
,
BASE_TEMP_2 AS (--- agrega el costo_total del contrato anterior
	SELECT 
	pt.* ,
	cnt."fld_inivigfec" AS fecha_vig_plan_anterior,
	datediff(month, cnt."fld_inivigfec", getdate()) AS permanencia_plan_anterior, 
	cnt."fld_clasifica" AS clasi_riesgo_anterior,
	cnt."fld_concostototal"::double AS costo_total_anterior
	FROM BASE_TEMP pt 
	INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (pt.FUNFOLIO_ANTERIOR = cnt."fld_funfolio" AND pt.FUNCORREL_ANTERIOR = cnt."fld_funcorrel"))
, 
max_vig AS (
	SELECT  DISTINCT 
	max("fld_ingreso") AS ingreso, 
	"fld_cotrut"   
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY "fld_cotrut")
,
BASE_ULTI_INGRE AS (
SELECT  DISTINCT 
	cnt."fld_ingreso", 
	cnt."fld_cotrut" ,  
	cnt."fld_inivigfec" ,
	datediff(month, cnt."fld_ingreso"  , getdate()) AS permanencia_isapre,
	datediff(month, cnt."fld_inivigfec", getdate()) AS antiguedad_isapre,
ROW_NUMBER() OVER(PARTITION BY cnt."fld_cotrut"ORDER BY cnt."fld_inivigfec" asc) AS n_veces
FROM AFI.P_RDV_DST_LND_SYB.CNT 
INNER JOIN MAX_VIG vig ON(cnt."fld_cotrut"=vig."fld_cotrut" AND cnt."fld_ingreso" = vig.INGRESO)
ORDER BY n_veces, cnt."fld_cotrut")
SELECT DISTINCT 
inge."fld_ingreso" AS fecha_ingreso_isa,
inge."fld_inivigfec" AS fecha_vigencia_isa,
* 
FROM BASE_TEMP_2 tempo
INNER JOIN (SELECT * FROM BASE_ULTI_INGRE WHERE n_veces = 1) inge ON (tempo.rut_cot = inge."fld_cotrut")
)
----FIN EXTRACCION TODOS LOS PLANES TEMPORALES
;
----PRIMERA BASE
CREATE OR REPLACE TABLE AFI.P_DDV_AFI.P_DDV_AFI_PLAN_TEMP_BASE AS (
SELECT
RUT_COT AS RUT_AFILIADO,
RUT_EMPLEADOR AS RUT_PAGADOR,
EDAD_COT AS EDAD,
GLS_REGION,
SEXO,
CANT_CARGAS AS CANT_CARGAS,
COD_CATEGORIA_ANTERIOR AS PLAN_ANTERIOR,
GLOSA_CATEGORIA_ANTERIOR AS GLS_PLAN_ANTERIOR,
COSTO_TOTAL AS PACTADO_PLAN_TEMPORAL,
COSTO_TOTAL_ANTERIOR  AS PACTADO_PLAN_ANTERIOR,
FECHA_INGRESO_ISA AS FECHA_INGRESO_ISAPRE,
antiguedad_isapre,
FECHA_VIG_PL_TEMP AS FECHA_VIG_PLAN_TEMPORAL,
CASE 
	WHEN datediff(month, FECHA_VIG_PL_TEMP, getdate()) < 0 
	THEN 0 
	ELSE datediff(month, FECHA_VIG_PL_TEMP, getdate()) 
	END AS antiguedad_plan_temporal,
CLASI_RIESGO_ANTERIOR AS CLASI_RIESGO_ANTES_PLAN_TEMP,
CLASI_RIESGO AS CLASI_RIESGO_ACTUAL
FROM AFI.P_DDV_AFI.P_DDV_AFI_CA_PLAN_TEMP  
)
---- FIN PRIMERA BASE
;
-----SEGUNDA BASE
-----personas que salen del plan temporal
CREATE OR REPLACE TABLE AFI.P_DDV_AFI.P_DDV_AFI_SALEN_PLAN_TEMP AS (
SELECT
RUT_COT AS RUT_AFILIADO,
RUT_EMPLEADOR AS RUT_PAGADOR,
EDAD_COT AS EDAD,
GLS_REGION,
SEXO,
CANT_CARGAS AS CANT_CARGAS,
COD_CATEGORIA_ANTERIOR AS PLAN_ANTERIOR,
GLOSA_CATEGORIA_ANTERIOR AS GLS_PLAN_ANTERIOR,
COSTO_TOTAL AS PACTADO_PLAN_TEMPORAL,
COSTO_TOTAL_ANTERIOR  AS PACTADO_PLAN_ANTERIOR,
FECHA_INGRESO_ISA AS FECHA_INGRESO_ISAPRE,
antiguedad_isapre,
FECHA_VIG_PL_TEMP AS FECHA_VIG_PL_TEMP,
CASE 
	WHEN datediff(month, FECHA_VIG_PL_TEMP, getdate()) < 0 
	THEN 0 
	ELSE datediff(month, FECHA_VIG_PL_TEMP, getdate()) 
	END AS antiguedad_plan_temporal,
CLASI_RIESGO_ANTERIOR AS CLASI_RIESGO_ANTES_PLAN_TEMP,
CLASI_RIESGO AS CLASI_RIESGO_ACTUAL
FROM AFI.P_DDV_AFI.P_DDV_AFI_CA_PLAN_TEMP 
WHERE RUT_COT != RUT_EMPLEADOR 
)
-------
---- FIN SEGUNDA BASE
;
---TERCERA BASE 
------ total gastos
CREATE OR REPLACE TABLE AFI.P_DDV_AFI.P_DDV_AFI_GASTO_PLAN_TEMP AS (
SELECT  
ORIGEN , 
SUM(MONTO_CAEC)::INT AS MONTO_CAEC , 
SUM(COBRADO)::INT AS MONTO_COBRADO , 
SUM(BONIFICADO)::INT MONTO_BONIFICADO,
0 ::INT AS MONTO_LICENCIAS_MEDICAS
FROM  GTO.P_DDV_GTO.P_DDV_GTO_GASTO_POR_ITEM
WHERE CODIGO_CATEG =8477 ---agregar el gasto por el cod 8477
GROUP BY ORIGEN)
-----------------------------
;
---- PROTOCOLO SALIDA PLAN TEMPORAL
CREATE OR REPLACE TABLE AFI.P_DDV_AFI.P_DDV_AFI_SALEN_PLAN_TEMP_SB AS (
SELECT
RUT_COT AS RUT_AFILIADO,
RUT_EMPLEADOR AS RUT_PAGADOR,
NOMBRE_AFILIADO,
APELLIDO_PATERNO,
APELLIDO_MATERNO,
EDAD_COT AS EDAD,
MAIL,
TELEFONO,
GLS_REGION,
GLS_COMUNA,
SEXO,
CANT_CARGAS AS CANT_CARGAS,
COD_CATEGORIA_ANTERIOR AS PLAN_ANTERIOR,
GLOSA_CATEGORIA_ANTERIOR AS GLS_PLAN_ANTERIOR,
COSTO_TOTAL AS PACTADO_PLAN_TEMPORAL,
COSTO_TOTAL_ANTERIOR  AS PACTADO_PLAN_ANTERIOR
FROM AFI.P_DDV_AFI.P_DDV_AFI_CA_PLAN_TEMP 
WHERE RUT_COT != RUT_EMPLEADOR 
)
----FIN PROTOCOLO SALIDA PLAN TEMPORAL

SELECT * FROM AFI.P_DDV_AFI.P_DDV_AFI_SALEN_PLAN_TEMP_SB


SELECT * FROM AFI.P_DDV_AFI.P_DDV_AFI_CA_PLAN_TEMP


SELECT * FROM ISA.P_RDV_DST_LND_SYB.ISAPREISAPRE LIMIT 10
