
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_PLANES AS (
SELECT 
a."fld_cotrut" ,
a."fld_cntcateg",
a."fld_glscateg",
b.TIPO_PLAN 
FROM AFI.P_RDV_DST_LND_SYB.CNT a 
left JOIN IQ.P_DDV_IQ.PRM_AGRUPA_PLANES b ON (a."fld_cntcateg"=b.COD_CATEGORIA) 
WHERE FLD_VIGREALDESDE <= 202010 AND FLD_VIGREALHASTA >= 202010)

WHERE TIPO_PLAN ='COLECTIVOS' 
SELECT "fld_cntcateg" ,"fld_glscateg", COUNT(DISTINCT("fld_cotrut")) AS cantidad_afiliados FROM EST.P_DDV_EST.CA_PLANES
GROUP BY 
"fld_cntcateg" ,"fld_glscateg"


SELECT top 10 * FROM AFI.P_RDV_DST_LND_SYB.CNT


SELECT top 10 * FROM contrato

SELECT top 10 * FROM IQ.P_DDV_IQ.PRM_AGRUPA_PLANES
WHERE COD_CATEGORIA IN (8493,
8492,
8491,
8490,
8489,
8488,
8487,
8486,
8485,
8484,
8483,
8482,
8481,
8480,
8479,
8478
)


create or replace view "EST"."P_STG_EST".periods as (
  with 
    nums(i) as (
        select 0
        union all select i + 1 from nums where i <=12
    )
  select dateadd(month, i, '2019-01-01 00:00:00.000'::timestamp) as period from nums
)
with _sample as (
    select "fld_funfolio" as contrato,
           "fld_funcorrel" as correlativo,
           "fld_inivigfec" as inivig, 
           "fld_finvigfec" as finvig
    from "AFI"."P_RDV_DST_LND_SYB"."CNT"
    where "fld_inivigfec" <= '2019-12-31 23:59:59.000'::timestamp
      and "fld_finvigfec" >= '2019-01-01 00:00:00.000'::timestamp
)
select m.period,
       sum(
       case 
         when cnt.inivig <= m.period and cnt.finvig >= m.period then 1
         else 0
       end) as c
from _sample as cnt
full join "EST"."P_STG_EST".periods as m
group by m.period
order by m.period
