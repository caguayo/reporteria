CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_CAEC_COVID AS (
SELECT DISTINCT
	sol."sol_nrosol",
	sol."sol_fecint",
	sol."cso_idn",
	sol."sol_espmed",
	sol."sol_diali1",
	sol."sol_descri",
	sol."sol_obscm",
	sol."sol_ginter",
	sol."sol_benrut",
	sol."sol_cdt1"
FROM HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol
INNER JOIN	(SELECT * 
			FROM HOS.P_RDV_DST_LND_SYB.SH_CIEST
		    WHERE "cie_codcie" IN ('U07','B97.2','B34.2','B32.2','J12.8','J12.9','J18.9') ) ciest 
		    ON (sol."sol_nrosol" = ciest."cie_nrostr")
WHERE sol."sol_fecint" >= '2020-03-01 00:00:00')


---------------------------------------------------------
SELECT top 3 * FROM GES.P_RDV_DST_LND_SYB.GES_ESTUDIO


SELECT top 3 * FROM EST.P_DDV_EST.CA_RUT_CAEC_COVID_CSV 


SELECT top 3 * FROM HOS.P_RDV_DST_LND_SYB.SH_SOLTRA

SELECT top 3 * FROM HOS.P_RDV_DST_LND_SYB.SH_CIEST

SELECT top 3 * FROM HOS.P_RDV_DST_LND_SYB.SISHOSPCIE


SELECT top 3 * FROM AFI.P_RDV_DST_LND_SYB.CNT

 SELECT top 3 * FROM  HOS.P_RDV_DST_LND_SYB.SH_EAH 