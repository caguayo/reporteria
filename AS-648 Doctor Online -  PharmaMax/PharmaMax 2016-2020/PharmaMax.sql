
------------------REVISION POR TABLA CONTRATO----------
---- metodo antiguo que se entrego
------------------ ventas nuevas
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_TEST01 AS (
SELECT DISTINCT 
EST.P_DDV_EST.FECHA_PERIODO(FECHA_INGRESO_ISAPRE) AS VN ,
--PERIODO_VIGENCIA AS VN,
RUT_TITULAR 
FROM afi.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE 
--FECHA_INGRESO >= '2015-01-01'
--PERIODO_VIGENCIA >= 1995
TIPO_TRANSACCION ='VN'
)
;
-------- personas que tienen pharma
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_VIG_PHARMA AS(
SELECT DISTINCT 
A.PERIODO_VIGENCIA ,
b.VN,
A.RUT_TITULAR  
FROM afi.P_DDV_AFI.P_DDV_AFI_CONTRATO A
INNER JOIN EST.P_DDV_EST.CA_TEST01 B ON (A.RUT_TITULAR=B.RUT_TITULAR)
WHERE  FECHA_PHARMA NOT IN ('1900-01-01 00:00:00') 
AND A.PERIODO_VIGENCIA >= 201601 ---201601
)
;

CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_PHARMA AS (
SELECT 
PERIODO_VIGENCIA, 
VN , 
COUNT(RUT_TITULAR) AS CANT FROM  EST.P_DDV_EST.CA_VIG_PHARMA
--WHERE PERIODO_VIGENCIA = 201810
GROUP BY PERIODO_VIGENCIA, VN
ORDER BY PERIODO_VIGENCIA ,VN )
--- fin metodo antiguo









-------- extraccion de pharma max
---- nuevo metodo que NO se envio
create or replace view EST.P_DDV_EST.periods as (
with nums(i) as (
				select 0 union all select i + 1 from nums where i <=80  ---ajustar para el fin de los meses
			    )    
	select 
		est.P_DDV_EST.fecha_periodo(dateadd(month, i, '2015-01-01 00:00:00.000'::timestamp)) as period 
	from nums
);
WITH pharma AS ( 
SELECT
	"fld_cotrut" ,
	est.P_DDV_EST.fecha_periodo("fld_ingreso") AS periodo_vn ,
	FLD_VIGREALDESDE,
	FLD_VIGREALHASTA
FROM AFI.P_RDV_DST_LND_SYB.CNT 
--INNER JOIN est.P_DDV_EST.CA_VN b ON (a."fld_cotrut"= b."fld_cotrut") 
WHERE "fld_ingreso" >= '2015-01-01 00:00:00'
--a.FLD_VIGREALDESDE <= 201910 AND a.FLD_VIGREALHASTA >= 201910 
AND "fld_tipopharma" = 5 --AND "fld_finpharma" > 0
)
, uni_pharma_periodo AS ( 
SELECT 
	* 
FROM pharma a 
	LEFT JOIN EST.P_DDV_EST.periods b ON (b.period BETWEEN a.FLD_VIGREALDESDE AND FLD_VIGREALHASTA )
)
SELECT 
 periodo_vn,
 period AS periodo_vigencia,
 count(DISTINCT "fld_cotrut") AS cantidad
FROM uni_pharma_periodo
WHERE periodo_vn = '017590550-2'
GROUP BY 
		periodo_vn,
		periodo_vigencia
ORDER BY 
		periodo_vn,
		periodo_vigencia
--LIMIT 100
----------- fin 
















--------- nueva extraccion 
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_TEST AS (
SELECT
a."fld_cotrut" 
FROM AFI.P_RDV_DST_LND_SYB.CNT a
INNER JOIN est.P_DDV_EST.CA_VN b ON (a."fld_cotrut"= b."fld_cotrut") 
WHERE a.FLD_VIGREALDESDE <= 201910 AND a.FLD_VIGREALHASTA >= 201910 
AND a."fld_tipopharma" = 5 AND "fld_finpharma" > 0
)





SELECT count (1) FROM EST.P_DDV_EST.CA_TEST

SELECT top 10 * FROM EST.P_DDV_EST.CA_TEST




SELECT count(1) FROM EST.P_DDV_EST.CA_VN


SELECT 
sum(PHARMA_MAX_PESOS) AS pesos, 
sum(PHARMA_MAX_UF) AS uf, 
SUM(PAGADO_PHARMA_PESOS) AS pagado, 
SUM(PAGADO_PHARMA_UF) AS pago_uf 
FROM afi.P_DDV_AFI.P_DDV_AFI_CONTRATO 
WHERE TIPO_TRANSACCION ='VN' 
AND PERIODO_VIGENCIA =201810


SELECT MAX(PERIODO_VIGENCIA) AS maximo,MIN(PERIODO_VIGENCIA) AS maximo,  FROM afi.P_DDV_AFI.P_DDV_AFI_CONTRATO


SELECT * FROM EST.P_DDV_EST.CA_TEST01


SELECT * FROM EST.P_DDV_EST.CA_VN  LIMIT 10


CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_VN AS (
SELECT "fld_cotrut" FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_funnotificacod" = 1 
AND "fld_tpotransac" IN('VN','CT') 
AND "fld_periodoprod" BETWEEN '2018-10-01 00:00:00' AND '2018-10-31 23:59:59'
)





SELECT * FROM EST.P_DDV_EST.CA_VIG_PHARMA 
WHERE periodo_vigencia >= 202101
LIMIT 101

SELECT COUNT(1) FROM EST.P_DDV_EST.CA_TEST01 

SELECT * FROM EST.P_DDV_EST.CA_PHARMA 
ORDER BY periodo_vigencia , vn 
LIMIT 200


SELECT TOP 10 FECHA_INGRESO , FECHA_INGRESO_ISAPRE FROM afi.P_DDV_AFI.P_DDV_AFI_CONTRATO

SELECT top 10 * FROM afi.P_DDV_AFI.P_DDV_AFI_CONTRATO 




--------------------------------------------------------------------------------------------------------------
SELECT count (1) FROM AFI.P_RDV_DST_LND_SYB.LFC 
WHERE "fld_funnotificacod" = 1 
AND "fld_tpotransac" IN('VN','CT') 
AND "fld_periodoprod" BETWEEN '2017-08-01 00:00:00' AND '2017-08-31 23:59:59'

SELECT count (1) FROM est.P_DDV_EST.CA_VN 

SELECT TOP 3 * FROM est.P_DDV_EST.CA_VN

SELECT top 10 "fld_cotrut" , "fld_tipopharma", "fld_finpharma" ,"fld_tpotransac" ,FLD_VIGREALDESDE ,FLD_VIGREALHASTA 
FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE "fld_cotrut" ='025798634-9'











