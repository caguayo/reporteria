
------------ extraccion final 
create or replace view EST.P_DDV_EST.periods as (
with nums(i) as (
				select 0 union all select i + 1 from nums where i <=80  ---ajustar para el fin de los meses
			    )    
	select 
		est.P_DDV_EST.fecha_periodo(dateadd(month, i, '2015-01-01 00:00:00.000'::timestamp)) as period 
	from nums
	    );
WITH doctor_online AS ( 
SELECT 
	"fld_cotrut" ,
	est.P_DDV_EST.fecha_periodo("fld_ingreso") AS periodo_vn ,
	FLD_VIGREALDESDE ,
	FLD_VIGREALHASTA 
FROM afi.P_RDV_DST_LND_SYB.CNT a
INNER JOIN AFI.P_RDV_DST_LND_SYB.BAD b ON (a."fld_funfolio"=b."bad_funfolio" AND a."fld_funcorrel"=b."bad_funcorrel")
WHERE 		a."fld_ingreso" >= '2015-01-01 00:00:00'
	  AND 	b."bad_producto" IN (22,23)
)
, uni_dr_periodo AS ( 
SELECT 
	* 
FROM doctor_online a
	LEFT JOIN EST.P_DDV_EST.periods b ON (b.period BETWEEN a.FLD_VIGREALDESDE AND FLD_VIGREALHASTA )
)
SELECT 
	periodo_vn,
	period AS vigencia,
	COUNT(DISTINCT "fld_cotrut") AS cantidad
FROM uni_dr_periodo
--WHERE "fld_cotrut" ='016457152-1'
GROUP BY periodo_vn,
		 vigencia
ORDER BY 
		--"fld_cotrut", 
		periodo_vn,
		vigencia
--LIMIT 500
------- fin




		
		
		
		
		








SELECT top 10 * FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE "fld_cotrut" ='013693110-5' AND ("fld_tpotransac" ='VN' OR "fld_tpotransac" ='CT')

CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_VN_PARA_BAD AS (
SELECT top 10 CONCAT(substring("fld_periodovn",0,4),substring("fld_periodovn",6,2)) AS periodo, "fld_cotrut"  FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE "fld_tpotransac" IN ('VN', 'CT') AND "fld_funnotificacod" =1 AND "fld_periodoprod" >= '2015-01-01 00:00:00'
)

SELECT *, EST.P_DDV_EST.PERIODO_FECHA(peri)::timestamp FROM EST.P_DDV_EST.PRUEBA


SELECT 

SELECT top 3 * FROM AFI.P_RDV_DST_LND_SYB.BAD
WHERE "bad_producto" IN (22,23) AND "bad_cotrut" ='013693110-5'




SELECT * FROM ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL



SELECT TOP 3 * FROM ISAPRE


SELECT TOP 10 * FROM EST.P_DDV_EST.periods

SELECT top 10 * FROM est.P_DDV_EST.test

--------- loop por mes

----- ingresa los meses 

----- ventas nuevas
CREATE OR REPLACE TEMPORARY TABLE EST.P_DDV_EST.CA_VN_PARA_BAD AS (
SELECT 
	CONCAT(substring("fld_periodovn",0,4),substring("fld_periodovn",6,2)) AS periodo, 
	"fld_cotrut"  
	FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE 	"fld_tpotransac" IN ('VN', 'CT') AND 
		"fld_funnotificacod" =1 AND 
		"fld_periodoprod" >= '2015-01-01 00:00:00'
)

SELECT * FROM EST.P_DDV_EST.CA_VN_PARA_BAD LIMIT 10


SELECT * FROM EST.P_DDV_EST.CA_VN_PARA_BAD LIMIT 10


SELECT top 3 * FROM EST.P_DDV_EST.CA_VN_PARA_BAD

SELECT count(1) FROM EST.P_DDV_EST.CA_VN_PARA_BAD


SELECT max(PERIOD) FROM EST.P_DDV_EST.periods LIMIT 10

------- bucle para extraer los bad
create or replace view EST.P_DDV_EST.periods as (
with nums(i) as (
				select 0 union all select i + 1 from nums where i <=80  ---ajustar para el fin de los meses
			    )    
	select 
		dateadd(month, i, '2015-01-01 00:00:00.000'::timestamp) as period 
	from nums
	    )
;	  
-------- extraccion de las VN para doctor_online 
--------   
CREATE OR REPLACE TEMPORARY TABLE est.P_DDV_EST.test AS ( 
with samples as (
			    select 
			     "bad_cotrut",
			     EST.P_DDV_EST.PERIODO_FECHA("bad_vigdes")::timestamp AS inivig,
			     EST.P_DDV_EST.PERIODO_FECHA("bad_vighas")::timestamp AS finvig
			    FROM AFI.P_RDV_DST_LND_SYB.BAD
				WHERE "bad_producto" IN (22,23) 
			    AND "bad_vigdes" <= EST.P_DDV_EST.FECHA_PERIODO('2025-12-31 23:59:59.000'::timestamp)
			    AND "bad_vighas" >= EST.P_DDV_EST.FECHA_PERIODO('2000-01-01 00:00:00.000'::timestamp)
                )  
select m.period,
       "bad_cotrut" 
from samples as cnt
full join EST.P_DDV_EST.periods as m
)


SELECT * FROM est.P_DDV_EST.test  LIMIT 10


create or replace view EST.P_DDV_EST.periods as (
with nums(i) as (
				select 0 union all select i + 1 from nums where i <=80  ---ajustar para el fin de los meses
			    )    
	select 
		est.P_DDV_EST.fecha_periodo(dateadd(month, i, '2015-01-01 00:00:00.000'::timestamp)) as period 
	from nums
	    )
;	

SELECT MIN(PERIOD), MAX(PERIOD) FROM EST.P_DDV_EST.periods LIMIT 10




SELECT * FROM afi.P_RDV_DST_LND_SYB.BAD LIMIT 10



SELECT * FROM est.P_DDV_EST.test 
--WHERE 
LIMIT 10



SELECT 
	b."bad_nombre" ,
	a.* 
FROM afi.P_RDV_DST_LND_SYB.BAD a
LEFT JOIN  ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL b ON ( a."bad_producto"=b."bad_idn")
LIMIT 10



SELECT *   FROM afi.P_DDV_AFI.P_DDV_AFI_CONTRATO LIMIT 10



SELECT * FROM isa.P_RDV_DST_LND_SYB.CPW_BENADICIONAL 
WHERE --"bad_idn" IN (22,23)
	  CONTAINS ("bad_nombre",'Dr.')
LIMIT 10


SELECT max(PERIOD) FROM est.P_DDV_EST.test


CREATE OR REPLACE TEMPORARY TABLE est.P_DDV_EST.union_doc_vn_vig AS (
SELECT DISTINCT 
EST.P_DDV_EST.FECHA_PERIODO(a.PERIOD)::string AS VIGENCIA, 
b.PERIODO AS VN,
a."bad_cotrut" AS RUT_TITULAR
FROM est.P_DDV_EST.test a
INNER JOIN EST.P_DDV_EST.CA_VN_PARA_BAD b ON (a."bad_cotrut"=b."fld_cotrut")
)

SELECT * FROM est.P_DDV_EST.union_doc_vn_vig
WHERE vn = 202105
--LIMIT 

SELECT top 10 * FROM est.P_DDV_EST.union_doc_vn_vig
    
CREATE OR REPLACE TABLE est.P_DDV_EST.CA_doctor_online AS (
SELECT
VIGENCIA,
VN,
count(RUT_TITULAR) AS CANT
FROM est.P_DDV_EST.union_doc_vn_vig
GROUP BY VIGENCIA, VN
ORDER BY VIGENCIA, VN
)

SELECT * FROM est.P_DDV_EST.CA_doctor_online
ORDER BY vigencia, vn 
LIMIT 1000


SELECT MAX(VIGENCIA), MAX(VN) FROM est.P_DDV_EST.CA_doctor_online
---------------------------------------------------------------






    CREATE OR REPLACE PROCEDURE "EST"."P_DDV_EST".JC_base_antiguedad()
  RETURNS FLOAT
  LANGUAGE JAVASCRIPT
  AS $$
?
// Inicializo algunas variables.
    var periodo_min = 0;
    var periodo_base = 0; 
    
// ============ Busco el �ltimo periodo que se encuentra cargado en la tabla.
?
    var q1 = 'SELECT MIN("periodo"), MAX("periodo") FROM "EST"."P_DDV_EST"."JC_antiguedad" ;' ;
    var statement1 = snowflake.createStatement({sqlText: q1}) ;
    var resultado = statement1.execute();
   
    if(resultado.next()){
        periodo_min = resultado.getColumnValue(1);
        periodo_base = resultado.getColumnValue(2);
    } 
    
//    Chequeo si regreso null, debiese ocurrir solamente cuando la tabla esta vac�a
?
    if(periodo_base !== null) {
        // NADA continue;
        }
    else{
        periodo_min = 198904;           //inicio de vigencia del primer contrato en CNT
        periodo_base = periodo_min;
        }
    
?
//  Arbitrariamente se decide borrar los �ltimos 3 meses y volver a calcularlo por si hubo alguna modificaci�n en los contratos.
//  Le resto 3 meses al periodo base y reviso que no sea menor al periodo m�nimo de la tabla ( importante para la primera carga solamente)
   
    var q2 = `SELECT "EST"."P_DDV_EST".PERIODOADD(:1, -3),
                     "EST"."P_DDV_EST".PERIODOADD(:1, -4),
                     "EST"."P_DDV_EST".PERIODOADD(:2, -1) ;` ;
    
    var stmt_q2 = snowflake.createStatement( {sqlText: q2, binds: [periodo_base, periodo_min]});
    var res2 = stmt_q2.execute();
    
    if(res2.next()){
        var temp = res2.getColumnValue(1);
        if( temp <= periodo_min){
            periodo_base = periodo_min;
            periodo_anterior = res2.getColumnValue(3);
        }
        else {
            periodo_base = temp;
            periodo_anterior = res2.getColumnValue(2);
        }
    }
    
// ============ borro los periodos
?
    var del = 'DELETE FROM "EST"."P_DDV_EST"."JC_antiguedad" WHERE "periodo" >= :1 ;' ;
    snowflake.execute({sqlText: del, binds: [periodo_base]}); 
    
// ============ calculo el n�mero de vueltas para el loop
?
    var q3 = 'SELECT DATEDIFF(month, "EST"."P_DDV_EST".PERIODO_FECHA(:1), CURRENT_DATE()) ;' ;
    var res3 = snowflake.createStatement({sqlText: q3, binds: [periodo_base]}).execute();
    if(res3.next()) { meses = res3.getColumnValue(1); }
?
    var periodo_cal = periodo_base;
    
    var i;
    for (i = 1; i<=meses; i++){
        var tabla_aux = `CREATE OR REPLACE TEMPORARY TABLE "EST"."P_DDV_EST"."AUX_ANT" AS 
                         SELECT A.* FROM "EST"."P_DDV_EST"."JC_antiguedad" A
                         WHERE A."periodo" = :1 ;` ;
                         
        var stmt_aux = snowflake.createStatement({sqlText: tabla_aux, binds: [periodo_anterior]});
        
        var res_aux   = stmt_aux.execute();
        
// ============ C�lculo la antig�edad y la inserto a la tabla
?
        var tabla_anti = `INSERT INTO "EST"."P_DDV_EST"."JC_antiguedad"
                          SELECT    :1                          AS "periodo",
                                    A."fld_cotrut"              AS "fld_cotrut",
                                    IFNULL(B."antiguedad"+1, 1) AS "antiguedad",
                                    IFNULL(B."ini_vig", :1)     AS "ini_vig"
                                    
                                FROM "AFI"."P_RDV_DST_LND_SYB"."CNT" A
                                    LEFT JOIN "EST"."P_DDV_EST"."AUX_ANT" B ON (A."fld_cotrut" = B."fld_cotrut")
                                    
                           WHERE    A."FLD_VIGREALDESDE" <= :1
                                AND A."FLD_VIGREALHASTA" >= :1; ` ;
                                
         var stmt_anti = snowflake.createStatement({sqlText: tabla_anti, binds: [periodo_cal]}) ;
         stmt_anti.execute();
         
         periodo_anterior = periodo_cal;
         
         var cambio_per = snowflake.execute({sqlText: 'SELECT "EST"."P_DDV_EST".PERIODOADD(:1, 1) ;', binds: [periodo_cal]});
         cambio_per.next();
         periodo_cal = cambio_per.getColumnValue(1) ;
         
        
    }
    return meses + 0;
    
  $$
  ;
    
    
    
    
    
    
    
