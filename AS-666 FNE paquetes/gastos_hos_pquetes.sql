SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_EAH   --- honorarios medicos
LIMIT 10


SELECT "pqt_idn" , * FROM HOS.P_RDV_DST_LND_SYB.SH_PROCED 
WHERE  "pqt_idn" NOT IN (0)   ---"proc_codidn" = 102474835 
LIMIT 10



---- cuentas clinicas 
SELECT 
	eam."eam_idn" ,
	eam."sol_nrosol" ,
	eam."eam_rutcot" ,
	eam."eam_fecemi" ,
	eam."eam_pagtip" ,
	eam."eam_valcob" ,
	eam."eam_valbon" ,
	pro."pqt_idn" 
FROM HOS.P_RDV_DST_LND_SYB.SH_EAM eam
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_PROCED  pro ON (eam."sol_nrosol"= pro."proc_nrostr" )
--LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EPREFA  epr ON (eam."sol_nrosol"=epr."epr_nrostr") 
WHERE "eam_fecemi" BETWEEN '2021-01-01 00:00:00' AND '2021-02-28 00:00:00'
LIMIT 10


SH_EPREFA, 
SH_VISITA,  
SH_PROCED, 
SH_HONMED, 
SH_HOSGES  ---- detalle de la eprefa (epr_codidn)


SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_PROCED LIMIT 10  ---"pqt_idn"  -- proc_nrostr

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_HONMED LIMIT 10  ---"pqt_idn"  -- hon_nrostr 

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_VISITA LIMIT 10  ---"pqt_idn"  -- vis_nrostr

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_EPREFA LIMIT 10	---"pqt_idn"  

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_HOSGES LIMIT 10  ---"pqt_idn"






------ honorarios medicos  EXTRACCION MARIELA
SELECT 
	eah."eah_idn" ,
	eah."sol_nrosol" ,
	eah."eah_rutcot" ,
	eah."eah_rutptd" ,
	epr."pqt_idn" AS pqt_epr,
	pqt."pqt_codate",
	eah."eah_fecemi" ,
	eah."eah_pagtip" ,
	eah."eah_valcop" ,
	eah."eah_valbon" 	
FROM HOS.P_RDV_DST_LND_SYB.SH_EAH eah
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EPREFA 		epr ON (eah."sol_nrosol" = epr."epr_nrostr" AND eah."eah_rutptd" = epr."epr_rutptd")
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_DPREFA 		dpr ON (epr."epr_codidn" = dpr."dpr_nroepr")  ---(eah."sol_nrosol"= dpr."dpr_nrostr" AND eah."eah_rutptd"=dpr."dpr_rutptd")  ----- por que NO tiene la misma UNION que la epr????
	LEFT JOIN CME.P_RDV_DST_LND_SYB.CM_PQTN			pqt ON (epr."pqt_idn" = pqt."pqt_idn" )
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SISHOSPRUBRO 	sis ON (dpr."dpr_codrub" = sis.COD_RUBRO)
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_HOSGES 		hos ON (dpr."dpr_codidn" = hos."dpr_codidn" AND dpr."dpr_nroepr" = hos."epr_codidn" AND epr."epr_codidn" = hos."epr_codidn")
WHERE eah."eah_fecemi" BETWEEN '2017-01-01 00:00:00' AND '2019-12-28 00:00:00' AND  eah."eah_idn"=83697594 ---AND"sol_nrosol" = 3648922 
LIMIT 100


SELECT * FROM CME.P_RDV_DST_LND_SYB.CM_PQTN 		LIMIT 10 ---

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_DPREFA 		LIMIT 10 ---

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SISHOSPRUBRO  	LIMIT 10 ---

SELECT * FROM CM.P_RDV_DST_LND_SYB.CM_PQTN 			LIMIT 10 ---PAQUETE



    SELECT  eah."sol_nrosol",
            eah."eah_rutcot",
            eah."eah_cobtot",
            dah."dah_cobtot",
            dah."dah_codrub"     
        FROM "HOS"."P_RDV_DST_LND_SYB"."SH_EAH"           eah
            INNER JOIN "HOS"."P_RDV_DST_LND_SYB"."SH_DAH" dah ON (eah."eah_idn" = dah."eah_idn")
            LIMIT 10






WITH eah AS (
    SELECT  eah."sol_nrosol",
            eah."eah_rutcot",
            eah."eah_cobtot",
            dah."dah_cobtot",
            dah."dah_codrub"     
        FROM "HOS"."P_RDV_DST_LND_SYB"."SH_EAH"           eah
            INNER JOIN "HOS"."P_RDV_DST_LND_SYB"."SH_DAH" dah ON (eah."eah_idn" = dah."eah_idn")
            WHERE  eah."eah_conefe" BETWEEN '2021-01-01 00:00:00' and '2021-01-30 23:59:59' --AND eah."eah_estado" not in (3,4)
    UNION 
    SELECT
    		eam."sol_nrosol" ,
    		eam."eam_rutcot" ,
    		eam."eam_valcob" AS eah_cobtot ,
    		dah."dah_cobtot" ,
    		dah."dah_codrub" 
    	FROM HOS.P_RDV_DST_LND_SYB.SH_EAM eam
    		INNER JOIN "HOS"."P_RDV_DST_LND_SYB"."SH_DAH" dah ON (eam."eam_idn" = dah."eah_idn")
    		WHERE eam."eam_conefe" between '2021-01-01 00:00:00' and '2021-01-30 23:59:59' --and eam."eam_estado" not in (3,4)
    		)            
            SELECT * FROM eah LIMIT  10
--,datos AS (
            
            
            


------------ PREP
-------------- agregar codigo pqt a prestaciones hospitalarias
WITH eah AS (
    SELECT  eah."sol_nrosol",
            eah."eah_rutcot",
            eah."eah_cobtot",
            dah."dah_cobtot",
            dah."dah_codrub"     
        FROM "HOS"."P_RDV_DST_LND_SYB"."SH_EAH"           eah
            INNER JOIN "HOS"."P_RDV_DST_LND_SYB"."SH_DAH" dah ON (eah."eah_idn" = dah."eah_idn")
            WHERE  eah."eah_conefe" BETWEEN '2015-01-01 00:00:00' and '2021-01-30 23:59:59' --AND eah."eah_estado" not in (3,4)
    UNION 
    SELECT
    		eam."sol_nrosol" ,
    		eam."eam_rutcot" ,
    		eam."eam_valcob" AS eah_cobtot ,
    		dah."dah_cobtot" ,
    		dah."dah_codrub" 
    	FROM HOS.P_RDV_DST_LND_SYB.SH_EAM eam
    		INNER JOIN "HOS"."P_RDV_DST_LND_SYB"."SH_DAH" dah ON (eam."eam_idn" = dah."eah_idn")
    		WHERE eam."eam_conefe" between '2015-01-01 00:00:00' and '2021-01-30 23:59:59' --and eam."eam_estado" not in (3,4)
    		)            
SELECT  'eprefa'::varchar(14)       					AS "origen",
        epr."epr_codidn" ,
        epr."epr_nrostr" ,
        epr."epr_rutptd" ,
        epr."pqt_idn" ,
        epr."epr_monexe"            					AS "cobrado"  ,
        epr."epr_scoexe"            					AS "descuento",   -- debiese ser el cobrado por la cl�nica
        COALESCE(hos."hos_codate", eah."dah_codrub")    AS "id",
        --pqt.*,
        hos."hos_mtoori", --eah."dah_cobtot",
        COALESCE(cdt."fld_prestades", act.GLS_RUBRO) 	AS "item",
        epr."epr_urgencia" ,
        hos."hos_horario" ,
        hos."hos_objetado" ,
        hos."hos_sinconv" ,
        hos."hos_codmotivo" ,
        IFF(hos."hos_cantid"= 0, hos."hos_canhor", hos."hos_cantid" )   AS "hos_cantidad",  -- para d�as camas queda raro el valo unitario
        hos."hos_valuni",
        epr."epr_rutptd"                                AS "rutmed" ,
        epr."epr_rutptd"                                AS "rutemi"
FROM   HOS.P_RDV_DST_LND_SYB.SH_EPREFA epr
        --INNER JOIN 	EST.P_STG_EST.JC_DPQT             		 pqt ON (epr."epr_nrostr"  = pqt."sol_nrosol")  ---- sacar esta tabla 
        INNER JOIN 	eah                          	 		     ON (epr."epr_nrostr" = eah."sol_nrosol" AND epr."epr_monexe" = eah."eah_cobtot")   ----pqt."sol_rutafi" = eah."eah_rutcot" AND    
        LEFT JOIN 	HOS.P_RDV_DST_LND_SYB.SISHOSPRUBRO 		 act ON (eah."dah_codrub" = act.COD_RUBRO)
        LEFT JOIN 	HOS.P_RDV_DST_LND_SYB.SH_HOSGES    		 hos ON (epr."epr_codidn" = hos."epr_codidn" AND eah."dah_codrub" = hos."hos_codrub")                          
        LEFT JOIN  	ISA.P_RDV_DST_LND_SYB.CDTCOMP_HISTORICA  cdt ON (hos."hos_codate" = cdt."fld_prestacod") --AND pqt."sol_fecint" BETWEEN cdt."fld_inivigfec" AND cdt."fld_finvigfec")
--WHERE epr."epr_nrostr" = $st
UNION ALL
SELECT  'honorarios'            AS "origen",
        hon."hon_codidn",
        hon."hon_nrostr",
        hon."hon_rutptd",
        hon."pqt_idn",
        hon."hon_valcob",
        0 ,
        hon."hon_codpro"        AS "id",
        --pqt.*,
        hon."hon_valcob"        AS "cob_preest",
        act.GLS_ACTOR ,
        hon."hon_urgencia" ,
        'no existe',
        hon."hon_objetado" ,
        '-',
        hon."hon_motivosc",
        1                       AS "cantidad",
        hon."hon_valcob"        AS "costo",    --- necesarios para mantener la estructura
        hon."hon_rutptd" ,
        hon."hon_rutemi"       
FROM HOS.P_RDV_DST_LND_SYB.SH_HONMED  hon
            --INNER JOIN EST.P_STG_EST.JC_DPQT              pqt ON (hon."hon_nrostr" = pqt."sol_nrosol")
            LEFT JOIN  HOS.P_RDV_DST_LND_SYB.SISHOSPACTOR act ON (hon."hon_codpro" = act.COD_ACTOR)
--WHERE hon."hon_nrostr" = $st
UNION ALL
SELECT  'visitas'               AS "origen",
        vis."vis_codidn",
        vis."vis_nrostr",
        vis."vis_rutemi",
        vis."pqt_idn",
        vis."vis_valtot",
        0,
        vis."vis_codesp" ,
        --pqt.*,
        vis."vis_valtot"        AS "cob_prest",
        esp.GLS_ESPE,
        vis."vis_urgencia" ,
        vis."vis_horario" ,
        vis."vis_objetado" ,
        vis."vis_convenio" ,
        vis."vis_codmotivo",
        vis."vis_cantid" ,
        vis."vis_valuni" ,
        vis."vis_rutmed" ,
        vis."vis_rutemi" 
        FROM HOS.P_RDV_DST_LND_SYB.SH_VISITA    	vis
       	--INNER JOIN EST.P_STG_EST.JC_DPQT    		pqt ON (vis."vis_nrostr" = pqt."sol_nrosol")
        LEFT JOIN  LCC.P_RDV_DST_LND_SYB.MEDICOESPE esp ON (vis."vis_codesp" = esp.COD_ESPE)
--WHERE vis."vis_nrostr" = $st
UNION ALL
SELECT  'procedimientos'            AS "origen",
        proc."proc_codidn",
        proc."proc_nrostr",
        proc."proc_rutptd",
        proc."pqt_idn",
        proc."proc_valtot",
        0,
        proc."proc_codproc" ,
        --pqt.*,
        proc."proc_valtot"          AS "cob_prest",
        cdt."fld_prestades"         AS "item",
        proc."proc_urgencia" ,
        proc."proc_horario" ,
        proc."proc_objetado" ,
        proc."proc_tipconv" ,
        proc."proc_motcod",
        proc."proc_cantid" ,
        proc."proc_valuni" ,
        proc."proc_rutptd" ,
        proc."proc_rutemi" 
        FROM HOS.P_RDV_DST_LND_SYB.SH_PROCED                    proc
            --INNER JOIN EST.P_STG_EST.JC_DPQT                    pqt		ON (proc."proc_nrostr" = pqt."sol_nrosol")
            LEFT JOIN  ISA.P_RDV_DST_LND_SYB.CDTCOMP_HISTORICA  cdt 	ON (proc."proc_codproc" = cdt."fld_prestacod" )--AND pqt."sol_fecint" BETWEEN cdt."fld_inivigfec" AND cdt."fld_finvigfec")
    --WHERE   epr.epr_nrostr = 3233574 --proc."proc_nrostr" = $st
LIMIT 10;

------------ FIN PREP
------------------------------------------

SELECT ,* FROM hos.P_RDV_DST_LND_SYB.SH_EAH
WHERE "sol_nrosol" = 3233574
LIMIT 10

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_EAM 
WHERE "sol_nrosol" = 3233574
LIMIT 10

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_SOLTRA
WHERE "sol_nrosol" = 3233574 LIMIT 10






------- JOSE CANALES           
WITH hospitalario AS ( 
----- eah 
select distinct    
	eah."eah_idn" as idn_bono,/*idn prestacion*/
	eah."sol_nrosol" AS st,
	eah."eah_rutcot" as rut_afiliado,/*rut afiliado*/
	eah."eah_codben" as bencorrel,/*codigo beneficiario*/
	eah."eah_nrocon" as contrato, /*contrato*/
	eah."eah_nrocor" as correlativo, /*correlativo*/
	eah."eah_conefe"::date as fecha, /*fecha contable*/
	est.P_DDV_EST.FECHA_PERIODO(eah."eah_conefe") AS periodo,/*periodo_contable*/
	--year(datepart(eah.eah_conefe))*100 + month(datepart(eah.eah_conefe)) as periodo,/*periodo contable*/
	sol."sol_insrut" as rut_prestador, /*rut prestador*/
	case
	    when eah."der_idn" > 0 and eah."cso_idn" = 0 then 'GES'
		when eah."cso_idn" > 0 and eah."der_idn" = 0 then 'CAEC'
		when eah."der_idn" > 0 and eah."cso_idn" > 0 then 'GES-CAEC'
		else 'OTRO' 
	end as activacion, /*prestacion ges-caec, ges o caec*/
	eah."eah_catego" AS categoria ,
	0 AS codate,
	dah."dah_codrub" AS rubro,
	eah."eah_urgencia" as urgencia, /*es urgencia*/	
	eah."eah_cobtot"::int as cobrado, /*cobrado*/
	eah."eah_valbon"::int + eah."fld_gescaec"::int as bonificado /*bonificado*/
from HOS.P_RDV_DST_LND_SYB.SH_EAH  eah 
	LEFT  JOIN  HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol on (eah."sol_nrosol" = sol."sol_nrosol" and eah."eah_rutcot" = sol."sol_rutafi" and eah."eah_codben" = sol."sol_codben")
	INNER JOIN HOS.P_RDV_DST_LND_SYB.SH_DAH dah ON (eah."eah_idn" = dah."eah_idn")
	WHERE  eah."eah_conefe" BETWEEN '2015-01-01 00:00:00' and '2022-12-31 23:59:59' AND eah."eah_estado" not in (3,4)
---fin eah
UNION
----eam 
select  
	eam."eam_idn"  as idn_bono,
	eam."sol_nrosol" AS st,
	eam."eam_rutcot"  as rut_afiliado,
	eam."eam_codben"  as bencorrel,
	eam."eam_nrocon" as contrato,
	eam."eam_nrocor"  as correlativo,
	eam."eam_conefe"::date as fecha,
	est.P_DDV_EST.fecha_periodo(eam."eam_conefe") AS periodo,
	sol."sol_insrut"  as rut_prestador,
	 /*prestacion ges-caec, ges o caec*/
	case
	    when eam."der_idn">0 and eam."cso_idn" = 0 then 'GES'
		when eam."cso_idn">0 and eam."der_idn" = 0 then 'CAEC'
		when eam."der_idn">0 and eam."cso_idn">0   then 'GES-CAEC'
		else 'OTRO' 
	end  as activacion,
	eam."eam_catego" AS categoria,
	dam."dam_codate" AS codate,
	0 AS rubro,
	/*es urgencia*/	eam."eam_urgencia" as urgencia,
	/*cobrado*/eam."eam_valcob"::int  as cobrado,
 	/*bonificado*/ eam."eam_valbon"::int + eam."fld_gescaec"::int as bonificado
from HOS.P_RDV_DST_LND_SYB.SH_EAM eam 
    	    LEFT  JOIN HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol on (eam."sol_nrosol" = sol."sol_nrosol" and eam."eam_rutcot" = sol."sol_rutafi" and eam."eam_codben" = sol."sol_codben")
    	    INNER JOIN HOS.P_RDV_DST_LND_SYB.SH_DAM dam ON (eam."eam_idn" = dam."dam_idn") ---no aplica el rubro desde la eam
WHERE eam."eam_conefe" between '2015-01-01 00:00:00' and '2022-12-31 23:59:59' and eam."eam_estado" not in (3,4)
---fin eam
), paquetes AS ( 
--SELECT * FROM hospitalario 
--WHERE st = 3604444
SELECT   
	--epr."pqt_idn" AS pqt_epr,
	--pro."pqt_idn" AS pqt_proced ,
	--vis."pqt_idn" AS pqt_vis,
	--hon."pqt_idn" AS pqt_hon,
	--hos.idn,
	--hos.st,
	--COALESCE (epr."pqt_idn", pro."pqt_idn", vis."pqt_idn" , hon."pqt_idn") AS pqt_idn,
	--pqt."pqt_tecnica" ,
	--pqt."pqt_codate",
    --epr."epr_nrostr" ,
	hos.*,
	hges."hos_valcob" AS cobrado_hos, 
	hges."hos_valbonsis" AS bonificado_hos,
	hges."hos_codate"  ,
	hges."epr_codidn" 
	--cat.TIPO_PRODUCTO 
FROM hospitalario hos
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_EPREFA  		epr ON  (hos.st = epr."epr_nrostr" AND hos.cobrado = epr."epr_monexe") ----
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_PROCED  		pro ON  (hos.st = pro."proc_nrostr" AND hos.cobrado = pro."proc_valtot") ----
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_VISITA  		vis ON  (hos.st = vis."vis_nrostr" AND hos.cobrado = vis."vis_valtot" ) ----
	LEFT JOIN HOS.P_RDV_DST_LND_SYB.SH_HONMED  		hon ON  (hos.st = hon."hon_nrostr" AND  hos.cobrado = hon."pqt_monto" ) -----
	LEFT JOIN CME.P_RDV_DST_LND_SYB.CM_PQTN         pqt ON  (COALESCE (epr."pqt_idn", pro."pqt_idn", vis."pqt_idn" , hon."pqt_idn") = pqt."pqt_idn") -----
	--LEFT JOIN EST.P_DDV_EST.PRM_AGRUPA_PLANES       cat ON  (hos.categoria = cat.COD_DESACA) -----
	FULL JOIN HOS.P_RDV_DST_LND_SYB.SH_HOSGES       hges ON (epr."epr_codidn" = hges."epr_codidn" AND rubro = hges."hos_codrub") ----- 
--WHERE st = 3233574
--ORDER BY st 
--LIMIT 5000
)
, presta_hos AS (
SELECT
	idn_bono,
	st,
	rut_afiliado ,
	bencorrel ,
	contrato ,
	correlativo ,
	fecha ,
	periodo ,
	rut_prestador ,
	activacion ,
	categoria ,
	"epr_codidn" ,
	"hos_codate" ,
	rubro ,
	urgencia ,
	cobrado_hos::INT AS cobrado ,
	bonificado_hos::INT AS bonificado
FROM paquetes 
WHERE idn_bono =109656756 --periodo =201806 AND  --AND --rubro =7--PQT_IDN != 0 --AND codate != 0  --codate = 2004003--
ORDER BY periodo , idn_bono , rubro 
--LIMIT 1000
)
SELECT 
 	*
FROM presta_hos 



SELECT 
	rut_afiliado ,
	idn_bono ,
	periodo ,
	rubro ,
	--count(DISTINCT idn_bono) AS cant_bonos,
	sum(cobrado) AS cob,
	sum(bonificado) AS bon
FROM presta_hos
GROUP BY rut_afiliado ,periodo ,rubro , idn_bono 
ORDER BY rubro 
LIMIT 100





SELECT 
	*
FROM presta_hos
ORDER BY periodo , idn_bono
LIMIT 100




---------- FIN




SELECT 
	PERIODO ,
	ORIGEN ,
	FUENTE_ORIGEN ,
	FOLIO ,
	sum(BONIFICADO) AS bon ,
	sum(COBRADO) AS cob
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO 
WHERE  PERIODO BETWEEN 201806 AND 201806
GROUP BY PERIODO ,ORIGEN ,FUENTE_ORIGEN , folio 
LIMIT 10



SELECT 
	PERIODO ,
	FOLIO ,
	RUT_TITULAR ,
	RUBRO ,
	sum(COBRADO) AS cob,
	sum(BONIFICADO) AS bon,
	COB-BON AS COPAGO
FROM DTM.P_STG_DTM_GTO.P_STG_DTM_GTO_HECHOS_GTO
WHERE FOLIO =97704394
GROUP BY PERIODO , folio, RUT_TITULAR ,RUBRO 
ORDER BY RUBRO 
LIMIT 100



SELECT 
	 PERIODO_PRESTACION ,
	FOLIO_BONO ,
	ID_RUT_TITULAR ,
	RUT_TITULAR ,
	RUBRO ,
	COBRADO ,
	BONIFICADO ,
	COPAGO 
FROM EST.P_DDV_EST.ca_cia_seguro_2015_rev A
LEFT JOIN (SELECT DISTINCT 
			RUT_TITULAR ,
			ID_TITULAR 
			FROM EST.P_STG_EST.CA_SINIESTRALIDAD ) B ON (A.ID_RUT_TITULAR=B.ID_TITULAR)
WHERE copago <0
LIMIT 50



SELECT 
	count(1) 
FROM EST.P_DDV_EST.ca_cia_seguro_2020_rev 
LIMIT 10


SELECT 
	count(1) 
FROM EST.P_DDV_EST.ca_cia_seguro_2020 
LIMIT 10



SELECT 
	* 
FROM HOS.P_RDV_DST_LND_SYB.SISHOSPRUBRO  
LIMIT 100


SELECT * FROM 

SELECT "hos_valcob" , "hos_valbonsis" , "hos_codrub" , * FROM HOS.P_RDV_DST_LND_SYB.SH_HOSGES 
WHERE "epr_codidn" =2214931 AND "hos_codrub" =7
--LIMIT 10




SELECT DISTINCT 
	RUT_TITULAR ,
	ID_TITULAR 
FROM EST.P_STG_EST.CA_SINIESTRALIDAD 
LIMIT 10

WITH hospitalario AS ( 
----- eah 
select distinct    
	eah."eah_idn" as idn,/*idn prestacion*/
	eah."sol_nrosol" AS st,
	eah."eah_rutcot" as rut_afiliado,/*rut afiliado*/
	eah."eah_codben" as bencorrel,/*codigo beneficiario*/
	eah."eah_nrocon" as contrato, /*contrato*/
	eah."eah_nrocor" as correlativo, /*correlativo*/
	eah."eah_conefe"::date as fecha, /*fecha contable*/
	est.P_DDV_EST.FECHA_PERIODO(eah."eah_conefe") AS periodo,/*periodo_contable*/
	--year(datepart(eah.eah_conefe))*100 + month(datepart(eah.eah_conefe)) as periodo,/*periodo contable*/
	sol."sol_insrut" as rut_prestador, /*rut prestador*/
	case
	    when eah."der_idn" > 0 and eah."cso_idn" = 0 then 'GES'
		when eah."cso_idn" > 0 and eah."der_idn" = 0 then 'CAEC'
		when eah."der_idn" > 0 and eah."cso_idn" > 0 then 'GES-CAEC'
		else 'OTRO' 
	end as activacion, /*prestacion ges-caec, ges o caec*/
	eah."eah_catego" AS categoria ,
	0 AS codate,
	dah."dah_codrub" AS rubro,
	eah."eah_urgencia" as urgencia, /*es urgencia*/	
	eah."eah_cobtot"::int as cobrado, /*cobrado*/
	eah."eah_valbon"::int + eah."fld_gescaec"::int as bonificado /*bonificado*/
from HOS.P_RDV_DST_LND_SYB.SH_EAH  eah 
	LEFT  JOIN  HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol on (eah."sol_nrosol" = sol."sol_nrosol" and eah."eah_rutcot" = sol."sol_rutafi" and eah."eah_codben" = sol."sol_codben")
	INNER JOIN HOS.P_RDV_DST_LND_SYB.SH_DAH dah ON (eah."eah_idn" = dah."eah_idn")
	WHERE  eah."eah_conefe" BETWEEN '2015-01-01 00:00:00' and '2021-01-30 23:59:59' AND eah."eah_estado" not in (3,4)
---fin eah
UNION
----eam 
select distinct 
	eam."eam_idn"  as idn,
	eam."sol_nrosol" AS st,
	eam."eam_rutcot"  as rut_afiliado,
	eam."eam_codben"  as bencorrel,
	eam."eam_nrocon" as contrato,
	eam."eam_nrocor"  as correlativo,
	eam."eam_conefe"::date as fecha,
	est.P_DDV_EST.fecha_periodo(eam."eam_conefe") AS periodo,
	sol."sol_insrut"  as rut_prestador,
	 /*prestacion ges-caec, ges o caec*/
	case
	    when eam."der_idn">0 and eam."cso_idn" = 0 then 'GES'
		when eam."cso_idn">0 and eam."der_idn" = 0 then 'CAEC'
		when eam."der_idn">0 and eam."cso_idn">0   then 'GES-CAEC'
		else 'OTRO' 
	end  as activacion,
	eam."eam_catego" AS categoria,
	dam."dam_codate" AS codate,
	0 AS rubro,
	/*es urgencia*/	eam."eam_urgencia" as urgencia,
	/*cobrado*/eam."eam_valcob"::int  as cobrado,
 	/*bonificado*/ eam."eam_valbon"::int + eam."fld_gescaec"::int as bonificado
from HOS.P_RDV_DST_LND_SYB.SH_EAM eam 
    	    LEFT  JOIN HOS.P_RDV_DST_LND_SYB.SH_SOLTRA sol on (eam."sol_nrosol" = sol."sol_nrosol" and eam."eam_rutcot" = sol."sol_rutafi" and eam."eam_codben" = sol."sol_codben")
    	    INNER JOIN HOS.P_RDV_DST_LND_SYB.SH_DAM dam ON (eam."eam_idn" = dam."dam_idn") ---no aplica el rubro desde la eam
WHERE eam."eam_conefe" between '2015-01-01 00:00:00' and '2021-01-30 23:59:59' and eam."eam_estado" not in (3,4)
---fin eam
)
SELECT 
	*
FROM hospitalario
WHERE st = 109656756--idn=109656756



SELECT * FROM CME.P_RDV_DST_LND_SYB.CM_ADP WHERE "adp_codate" =1202056 --AND "adp_rut" = '071457900-2'   ---006853760-6

SELECT * FROM cme.P_RDV_DST_LND_SYB.CM_DDP LIMIT 100

SELECT * FROM CME.P_RDV_DST_LND_SYB.CM_PQTN LIMIT 10

SELECT * FROM ISA.P_RDV_DST_LND_SYB.CDTCOMP LIMIT 10

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_EAH LIMIT 10

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_DAH LIMIT 10

SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_DAM LIMIT 10

SELECT * FROM hos.P_RDV_DST_LND_SYB.SH_SOLTRA LIMIT 10



SELECT * FROM  HOS.P_RDV_DST_LND_SYB.SH_EPREFA 
WHERE "epr_nrostr" = 3604444
LIMIT 10

SELECT DISTINCT 
	"pqt_tecnica" 
FROM paquetes
LIMIT 500

SELECT * FROM CME.P_RDV_DST_LND_SYB.CM_ADP 
WHERE "adp_codate" = 2004006
LIMIT 10


SELECT "pqt_idn" ,* FROM HOS.P_RDV_DST_LND_SYB.SH_PROCED 
WHERE "proc_nrostr" =3164693



SELECT "pqt_idn" ,* FROM HOS.P_RDV_DST_LND_SYB.SH_VISITA
WHERE "vis_nrostr" = 3233574--"pqt_idn" IS NOT NULL AND "pqt_idn" NOT IN (0)
ORDER BY "vis_nrostr" 
LIMIT 5000


SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_HONMED  
WHERE "hon_nrostr" = 3233574
LIMIT 10


SELECT * FROM CME.P_RDV_DST_LND_SYB.CM_PQTN 
WHERE "pqt_idn" != 0 LIMIT 10  ---- cual es la marca tecnica?

SELECT * FROM cme.P_RDV_DST_LND_SYB.CM_PQTOCN LIMIT 10

SELECT * FROM cme.P_RDV_DST_LND_SYB.CM_PQTDETPREST LIMIT 10 

SELECT * FROM cme.P_RDV_DST_LND_SYB.CM_PQTHONN LIMIT 10




SELECT * FROM "HOS"."P_RDV_DST_LND_SYB"."SH_DAM" LIMIT 10


SELECT * FROM hospitalario
WHERE idn = 97574216 LIMIT 10


SELECT * FROM HOS.P_RDV_DST_LND_SYB.SH_DAH LIMIT 10






------------------------------ otro codigo
create or replace table "EST"."P_STG_EST"."TARIF_HOSPITALARIO" as (
    with recursive
        -- SAMPLE
        _sample as (
            select rut_afiliado
            from "TARI"."P_STG_DPR_DST_TARI"."SAMPLE_FINAL"
            where per1 >= 201601
        ), 

        -- CODATE Y CANTIDADES
        _aranst as ( 
          select
            'idn_' || row_number() over (partition by ara."ara_nrostr" order by ara."ara_nrostr")  
                              as idn,
            ara."ara_nrostr"  as solicitud,
            ara."ara_codara"  as codate,
            ara."ara_numvec"  as cantidad
          from "HOS"."P_RDV_DST_LND_SYB"."SH_ARANST" as ara
        ),

        _codate as (
            select solicitud, codate_1, codate_2, codate_3
            from (
              select idn, solicitud, codate
              from _aranst
            ) 
              pivot(max(codate) for idn in ('idn_1', 'idn_2', 'idn_3'))
                as p (solicitud, codate_1, codate_2, codate_3)
            order by solicitud
        ),

        _cantidad as (
            select solicitud, cantidad_1, cantidad_2, cantidad_3
            from (
              select idn, solicitud, cantidad
              from _aranst
            ) 
              pivot(max(cantidad) for idn in ('idn_1', 'idn_2', 'idn_3'))
                as p (solicitud, cantidad_1, cantidad_2, cantidad_3)
            order by solicitud
        ),

        _sh_codate as (
            select cod.solicitud, cod.codate_1, cod.codate_2, cod.codate_3,
              can.cantidad_1, can.cantidad_2, can.cantidad_3
            from _codate as cod
            inner join _cantidad can
              on cod.solicitud = can.solicitud
        ),

        --CIES
        _sh_cie as (
            select 
              row_number() over (partition by cie."cie_nrostr" order by cie."cie_nrostr")  
                                as idn,  
              cie."cie_nrostr"  as solicitud,
              cie."cie_codcie"  as cod_cie

            from "HOS"."P_RDV_DST_LND_SYB"."SH_CIEST" as cie
            qualify idn = 1 
        )
    

    select distinct
      eah."eah_idn"            as idn,                -- Identificador del bono/ree (puede repetirse)
      eah."sol_nrosol"         as solicitud,          -- Solicitud Hospitalaria
      eah."eah_rutcot"         as rut_afiliado,       -- Rut del titular
      eah."eah_nrocon"         as contrato,           -- Numero de contrato
      eah."eah_nrocor"         as correlativo,        -- Correlativo del contrato
      eah."eah_codben"         as bencorrel,          -- Correlativo del beneficiario
      eah."eah_conefe"::date   as fecha,              -- Fecha contable
      "EST"."P_STG_EST".ts2per(eah."eah_conefe") 
                               as periodo,            -- Periodo
      date_part(year, eah."eah_conefe"::date) 
                               as year,               -- A�o de la prestacion
      sol."sol_insrut"         as rut_prestador,      -- Rut del prestador
      case 
        when eah."der_idn" > 0 and eah."cso_idn" = 0 then 'GES'
        when eah."cso_idn" > 0 and eah."der_idn" = 0 then 'CAEC'
        when eah."der_idn" > 0 and eah."cso_idn" > 0 then 'GES-CAEC'
        else 'OTRO'
      end                      as activacion,         -- GES, CAEC o plan complementario
      dpr."dpr_codrub"         as rubro,              -- Rubro hospitalario
      cie.cod_cie              as cod_cie,            -- Codigo cie (problema de salud)
      hos."hos_codate"         as codigo_atencion,  -- Codate
      hos."hos_cantid"         as cantidad,           -- Cantidad 
      dah."dah_pretip"         as tipo_prestacion,    -- Tipo de la prestacion 1-27
      dah."dah_presub"         as subtipo_prestacion, -- Subtipo A-G
      dah."dah_pretip" || dah."dah_presub"         
                               as tipo_subtipo,       -- Tipo + Subtipo
      hos."hos_valcob"         as cobrado,            -- Cobrado
      hos."hos_valbon"         as bonificado,         -- Bonificado 
      hos."hos_idn"            as idn_detalle,        -- Identificador unico del detalle de la prestacion
      'EAH'                    as origen 


    from "HOS"."P_RDV_DST_LND_SYB"."SH_EAH" as eah
      inner join _sample 
        on  eah."eah_rutcot" = _sample.rut_afiliado
        and eah."eah_conefe" between '2016-01-01 00:00:00.000'::timestamp 
                                 and '2019-12-31 23:59:59.000'::timestamp
        and eah."eah_estado" not in (3,4)
      left join "HOS"."P_RDV_DST_LND_SYB"."SH_SOLTRA" as sol 
        on  eah."sol_nrosol" = sol."sol_nrosol"
        and eah."eah_rutcot" = sol."sol_rutafi"
        and eah."eah_codben" = sol."sol_codben"
      left join "HOS"."P_RDV_DST_LND_SYB"."SH_EPREFA" as epr
        on eah."eah_idn" = epr."epr_nrobon"
      left join "HOS"."P_RDV_DST_LND_SYB"."SH_DPREFA" as dpr
        on epr."epr_codidn" = dpr."dpr_nroepr"
      left join "HOS"."P_RDV_DST_LND_SYB"."SH_HOSGES" as hos
        on dpr."dpr_codidn" = hos."dpr_codidn"
      left join "HOS"."P_RDV_DST_LND_SYB"."SH_DAH" as dah
        on  eah."eah_idn" = dah."eah_idn"
        and dpr."dpr_codrub" = dah."dah_codrub"
      left join _sh_cie as cie
        on eah."sol_nrosol" = cie.solicitud

    union

    select distinct
      eam."eam_idn"            as idn,                -- Identificador del bono/ree (puede repetirse)
      eam."sol_nrosol"         as solicitud,          -- Solicitud Hospitalaria
      eam."eam_rutcot"         as rut_afiliado,       -- Rut del titular
      eam."eam_nrocon"         as contrato,           -- Numero de contrato
      eam."eam_nrocor"         as correlativo,        -- Correlativo del contrato
      eam."eam_codben"         as bencorrel,          -- Correlativo del beneficiario
      eam."eam_conefe"::date   as fecha,              -- Fecha contable
      "EST"."P_STG_EST".ts2per(eam."eam_conefe") 
                               as periodo,            -- Periodo
      date_part(year, eam."eam_conefe"::date) 
                               as year,               -- A�o de la prestacion
      sol."sol_insrut"         as rut_prestador,      -- Rut del prestador
      case 
        when eam."der_idn" > 0 and eam."cso_idn" = 0 then 'GES'
        when eam."cso_idn" > 0 and eam."der_idn" = 0 then 'CAEC'
        when eam."der_idn" > 0 and eam."cso_idn" > 0 then 'GES-CAEC'
        else 'OTRO'
      end                      as activacion,         -- GES, CAEC o plan complementario
      case 
        when eam."eam_cobori" = 1 
            or (dam."dam_pretip" = 15 and dam."dam_presub" = 'A') 
            or (dam."dam_pretip" = 5 and dam."dam_presub" = 'A') then 991
        when eam."eam_cobori" = 2 
            and dam."dam_pretip" = 4
            and dam."dam_presub" in ('A','C','D') then 992
        when eam."eam_cobori" = 2 
            and dam."dam_pretip" = 9
            and dam."dam_presub" = 'B' then 993
        when eam."eam_cobori" = 2 
            and dam."dam_pretip" = 12
            and dam."dam_presub" = 'A' then 994
        when eam."eam_cobori" = 3 
            and  dam."dam_tipvis" = 'V' then 995
        when eam."eam_cobori" = 3 
            and  dam."dam_tipvis" = 'I' then 996
        when dam."dam_pretip" = 1 and dam."dam_presub" = 'D' then 997 
        when dam."dam_pretip" = 4 and dam."dam_presub" = 'E' then 998 
      end                      as rubro,       -- Codigo rubro (para tarificacion)
      cie.cod_cie              as cod_cie,            -- Codigo Cie
      cod.codate_1             as codigo_atencion,    -- Codate
      cod.cantidad_1           as cantidad,           -- Cantidad del codate
      dam."dam_pretip"         as tipo_prestacion,    -- Tipo prestacion
      dam."dam_presub"         as subtipo_prestacion, -- Subtipo
      dam."dam_pretip" || dam."dam_presub"
                               as tipo_subtipo,       -- Tipo + Subtipo
      dam."dam_valcob"         as cobrado,            -- Cobrado
      dam."dam_valbon"         as bonificado,         -- bonificado
      dam."dam_idn"            as idn_detalle,        -- Detalle de la prestacion (unico)
      'EAM'                    as origen

    from "HOS"."P_RDV_DST_LND_SYB"."SH_EAM" as eam
      inner join _sample 
        on  eam."eam_rutcot" = _sample.rut_afiliado
        and eam."eam_conefe" between '2016-01-01 00:00:00.000'::timestamp 
                                 and '2019-12-31 23:59:59.000'::timestamp
        and eam."eam_estado" not in (3,4)
      left join "HOS"."P_RDV_DST_LND_SYB"."SH_SOLTRA" as sol 
        on  eam."sol_nrosol" = sol."sol_nrosol"
        and eam."eam_rutcot" = sol."sol_rutafi"
        and eam."eam_codben" = sol."sol_codben"
      left join "HOS"."P_RDV_DST_LND_SYB"."SH_DAM" as dam
        on eam."eam_idn" = dam."eam_idn"
      left join _sh_cie as cie
        on eam."sol_nrosol" = cie.solicitud 
      left join _sh_codate as cod
        on eam."sol_nrosol" = cod.solicitud
  
    order by fecha, idn, idn_detalle
)

