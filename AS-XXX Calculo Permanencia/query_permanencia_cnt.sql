

--------------- MARCANDO A LA CARTERA VIGENTE CON SU CANTIDAD DE PERMANENCIA
--SET periodo_ini = dateadd(MONTH ,-47,getdate())::timestamp;
--dateadd('month',-12, time_slice(current_date(), 1, 'month', 'start'));--'2020-01-01 00:00:00.000'::timestamp;

SET periodo_ini ='2018-01-01'

SET periodo_fin = getdate()::timestamp;

--TO_TIMESTAMP(TO_CHAR(add_months(last_day(getdate()),0))||' 23:59:59')::timestamp;
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_PERMANENCIA_CARTERA AS ( 
WITH nums(i) AS (
SELECT 
	0
	UNION ALL SELECT i + 1
FROM nums 
WHERE i <= datediff(MONTH, $periodo_ini, $periodo_fin)-1
)
,  periods AS 
(
 SELECT 
 	EST.P_DDV_EST.FECHA_PERIODO(dateadd(month, i::int, $periodo_ini)) AS periodo
 FROM nums
 )
 --SELECT min(periodo),max(periodo) FROM periods LIMIT 10 
, 
minima_vig_rank AS (
SELECT DISTINCT 
	"fld_cotrut" , 
	"fld_ingreso" ,
	"fld_cntcateg",
	MIN(FLD_VIGREALDESDE) AS minima_vigencia ,
row_number() over (partition by "fld_cotrut", "fld_ingreso" order by "fld_cotrut", minima_vigencia) AS n_veces 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
	"fld_cotrut",
	"fld_ingreso",
	"fld_cntcateg"
ORDER BY "fld_cotrut" , minima_vigencia 
)
--SELECT * FROM minima_vig_rank
--WHERE "fld_cotrut" = '012073836-4'
, minima_vig AS (
SELECT * FROM minima_vig_rank
WHERE n_veces =1
)
--SELECT * FROM minima_vig 
--WHERE "fld_cotrut" = '012073836-4'
--LIMIT 100 
--WITH
, cartera_vigente AS ( 
SELECT DISTINCT 
	"fld_cotrut" ,
	FLD_VIGREALDESDE,
	FLD_VIGREALHASTA,
	"fld_ingreso",
	"fld_cntcateg"
FROM AFI.P_RDV_DST_LND_SYB.CNT 
WHERE FLD_VIGREALDESDE <= EST.P_DDV_EST.FECHA_PERIODO($periodo_fin)  AND  FLD_VIGREALHASTA >= EST.P_DDV_EST.FECHA_PERIODO($periodo_ini)
)
, union_bases AS ( 
SELECT 
	a."fld_cotrut",
	c.periodo AS mes,
	b."fld_ingreso",
	b."fld_cntcateg",
	b.minima_vigencia ,
	last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig,
	a.FLD_VIGREALDESDE ,
	a.FLD_VIGREALHASTA 
FROM cartera_vigente  a
LEFT JOIN minima_vig b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
INNER JOIN periods  c ON (c.periodo BETWEEN a.FLD_VIGREALDESDE AND a.FLD_VIGREALHASTA)
)
SELECT 
	"fld_cotrut",
	"fld_ingreso",
	"fld_cntcateg",
	minima_vig ,
	dateadd(MONTH,11,minima_vig) doce_meses,
	EST.P_DDV_EST.PERIODO_FECHA(mes) AS mes,
	EST.P_DDV_EST.FECHA_PERIODO("fld_ingreso") AS PERIODO_INGRESO,
	EST.P_DDV_EST.FECHA_PERIODO(minima_vig) AS PERIODO_MIN_VIG,
	EST.P_DDV_EST.FECHA_PERIODO(doce_meses) AS PERIODO_DOCE_MESES,
	mes AS PERIODO_MES,
	DATEDIFF(month, minima_vig, EST.P_DDV_EST.PERIODO_FECHA(mes))+1 AS resta_meses
	--FLD_VIGREALDESDE ,
	--FLD_VIGREALHASTA 
FROM union_bases
--WHERE  "fld_cotrut" = '012073836-4'
ORDER BY "fld_cotrut" , mes
--LIMIT 100
)
------------ TERMINO CODIGO 




SELECT * FROM ISA.P_RDV_DST_LND_SYB.CPAY_AUTORIZACION LIMIT 1000

SELECT * FROM ISA.P_RDV_DST_LND_SYB.cpay


SELECT 
	count(1)
FROM EST.P_DDV_EST.CA_PERMANENCIA_CARTERA 
LIMIT 10




WITH vigencia AS (
SELECT DISTINCT 
"fld_cotrut" , 
"fld_ingreso" ,
MIN(FLD_VIGREALDESDE) AS minima_vigencia 
FROM AFI.P_RDV_DST_LND_SYB.CNT 
GROUP BY 
"fld_cotrut",
"fld_ingreso"
ORDER BY "fld_cotrut"  
), a_b AS ( 
SELECT DISTINCT 
a."fld_cotrut" , 
b."fld_ingreso",
last_day(EST.P_DDV_EST.PERIODO_FECHA(minima_vigencia)) AS minima_vig
FROM AFI.P_RDV_DST_LND_SYB.CNT a
LEFT JOIN vigencia b ON (a."fld_cotrut"=b."fld_cotrut" AND a."fld_ingreso" = b."fld_ingreso")
WHERE  202203 BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA 
	  --FLD_VIGREALDESDE <= 202103 AND 
	  --FLD_VIGREALHASTA >= 201801
)
, a_b_c AS (
SELECT 
*,
'2022-03-31' AS periodo_resta,
DATEDIFF(month, minima_vig, '2022-03-31')+1 AS resta_meses
FROM a_b
)
SELECT 
	"fld_cotrut" ,
	"fld_ingreso" ,
	minima_vig ,
	dateadd(MONTH,11,minima_vig) doce_meses,
	to_date(periodo_resta) AS mes_resta,
	EST.P_DDV_EST.FECHA_PERIODO("fld_ingreso") AS PERIODO_INGRESO,
	EST.P_DDV_EST.FECHA_PERIODO(minima_vig) AS PERIODO_MIN_VIG,
	EST.P_DDV_EST.FECHA_PERIODO(doce_meses) AS PERIODO_DOCE_MESES,
	EST.P_DDV_EST.FECHA_PERIODO(periodo_resta) AS PERIODO_RESTA,
	resta_meses 
FROM a_b_c
LIMIT 10





SELECT 
count(DISTINCT "fld_cotrut") AS cant_titulares,
sum(resta_meses) AS total_meses,
TOTAL_MESES/cant_titulares AS div_meses_titu,
div_meses_titu/12 AS div_final
FROM a_b_c


SELECT
*
FROM a_b_c LIMIT 10
--WHERE "fld_cotrut" ='010628175-0'

/*
SELECT 
count(DISTINCT "fld_cotrut") AS cant_titulares,
sum(resta_meses) AS total_meses,
TOTAL_MESES/cant_titulares AS div_meses_titu,
div_meses_titu/12 AS div_final
FROM a_b_c
*/

SELECT
	CASE 
		--WHEN (dateadd(MONTH ,2,"fld_ingreso") = minima_vig  OR (dateadd(MONTH ,1,"fld_ingreso")) = minima_vig) THEN 'ok'
		WHEN (PERIODO_INGRESO +2 = PERIODO_MIN_VIG  OR PERIODO_INGRESO +1 = PERIODO_MIN_VIG) THEN 'ok'
		ELSE 'no'
	END AS chek_vigencia,
	*
FROM EST.P_DDV_EST.CA_PERMANENCIA_CARTERA  
WHERE chek_vigencia ='no' AND PERIODO_MIN_VIG < 199501--"fld_cotrut" = '012073836-4'
LIMIT 1000


SELECT min(PERIODO_MES), max(PERIODO_MES) FROM EST.P_DDV_EST.CA_PERMANENCIA_CARTERA LIMIT 10










SELECT "fld_vigdesde"  FROM AFI.P_RDV_DST_LND_SYB.CNT LIMIT 10


, a_b_c AS (
SELECT 
*,
'2022-03-31' AS periodo_resta,
DATEDIFF(month, minima_vig, '2022-03-31')+1 AS resta_meses
FROM a_b
)
SELECT 
	"fld_cotrut" ,
	"fld_ingreso" ,
	minima_vig ,
	dateadd(MONTH,11,minima_vig) doce_meses,
	to_date(periodo_resta) AS mes_resta,
	EST.P_DDV_EST.FECHA_PERIODO("fld_ingreso") AS PERIODO_INGRESO,
	EST.P_DDV_EST.FECHA_PERIODO(minima_vig) AS PERIODO_MIN_VIG,
	EST.P_DDV_EST.FECHA_PERIODO(doce_meses) AS PERIODO_DOCE_MESES,
	EST.P_DDV_EST.FECHA_PERIODO(periodo_resta) AS PERIODO_RESTA,
	resta_meses 
FROM a_b_c
LIMIT 10

)
 

SELECT COUNT(1) , COUNT(DISTINCT "fld_cotrut") FROM EST.P_DDV_EST.CA_PERMANENCIA_CARTERA  LIMIT 10


SELECT * FROM EST.P_DDV_EST.CA_PERMANENCIA_CARTERA LIMIT 100






----------------- VENTAS NUEVAS HISTORIAS Y QUE  SE ENCUENTRAN VIGENTES AL 202105
WITH vn AS (
SELECT DISTINCT 
est.P_DDV_EST.fecha_periodo("fld_periodoprod") AS periodo,
"fld_cotrut" 
from AFI.P_RDV_DST_LND_SYB.LFC 
	WHERE "fld_periodoprod" >= '2011-01-01 00:00:00'
	and "fld_funnotificacod"  = 1 
	and "fld_tpotransac" IN('VN','CT')
), vigentes AS (
SELECT DISTINCT "fld_cotrut" , "fld_ingreso" AS incorporacion FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE FLD_VIGREALDESDE <= 202105 AND FLD_VIGREALHASTA >= 202105
), union_ AS (
SELECT DISTINCT 
t1.periodo,
t1."fld_cotrut",
CASE 
	WHEN t2."fld_cotrut" IS NULL THEN 'no vigente'
	ELSE 'vigente'
END AS status_vig
FROM vn t1
LEFT JOIN vigentes t2 ON (t1."fld_cotrut" = t2."fld_cotrut")
)
SELECT "fld_cotrut"
FROM union_
WHERE status_vig = 'vigente'






WITH asd AS (
SELECT  
--est.P_DDV_EST.fecha_periodo(lfc."fld_periodoprod") AS periodo,
YEAR("fld_periodoprod"),
lfc."fld_cotrut" 
from AFI.P_RDV_DST_LND_SYB.LFC lfc
	WHERE  lfc."fld_periodoprod" >= '2011-01-01 00:00:00'
	and lfc."fld_funnotificacod"  = 1 
	and lfc."fld_tpotransac" IN('VN','CT')
)
SELECT count(DISTINCT "fld_cotrut") FROM asd
	
	
SELECT "fld_cotrut" , "fld_ingreso" AS incorporacion FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE FLD_VIGREALDESDE <= 202105 AND FLD_VIGREALHASTA >= 202105
