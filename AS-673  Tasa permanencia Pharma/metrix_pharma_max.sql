	
------ UNIVERSO VENTAS NUEVAS CON PHARMA_MAX
-----------------------------------------------------------
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_PHARMA_MAX_VN AS ( 			
SELECT 
	lfc."fld_cotrut" AS fld_cotrut_vn ,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN, 
	CASE 
		WHEN 	cnt."fld_tipopharma" = 5 THEN 'PHARMA_MAX'
		ELSE 'NO'
	END AS CHEK_PHARMA	
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )
WHERE  lfc."fld_funnotificacod" = 1  AND 
	   lfc."fld_periodoprod" BETWEEN '2017-01-01 00:00:00' AND '2017-01-31 23:59:59'  
UNION 
SELECT 
	lfc."fld_cotrut" AS fld_cotrut_vn,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN, 
	CASE 
		WHEN 	cnt."fld_tipopharma" = 5 THEN 'PHARMA_MAX'
		ELSE 'NO'
	END AS CHEK_PHARMA
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )
WHERE  lfc."fld_funnotificacod" = 1  AND 
	   lfc."fld_periodoprod" BETWEEN '2018-01-01 00:00:00' AND '2018-01-31 23:59:59'
UNION
SELECT 
	lfc."fld_cotrut" AS fld_cotrut_vn,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN, 
	CASE 
		WHEN 	cnt."fld_tipopharma" = 5 THEN 'PHARMA_MAX'
		ELSE 'NO'
	END AS CHEK_PHARMA
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )
WHERE  lfc."fld_funnotificacod" = 1  AND 
	   lfc."fld_periodoprod" BETWEEN '2019-01-01 00:00:00' AND '2019-01-31 23:59:59'
UNION
SELECT 
	lfc."fld_cotrut" AS fld_cotrut_vn ,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN, 
	CASE 
		WHEN 	cnt."fld_tipopharma" = 5 THEN 'PHARMA_MAX'
		ELSE 'NO'
	END AS CHEK_PHARMA
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )
WHERE  lfc."fld_funnotificacod" = 1  AND 
	   lfc."fld_periodoprod" BETWEEN '2020-01-01 00:00:00' AND '2020-01-31 23:59:59'
UNION
SELECT 
	lfc."fld_cotrut" AS fld_cotrut_vn ,
	EST.P_DDV_EST.FECHA_PERIODO(lfc."fld_periodoprod") AS PERIODO_VN, 
	CASE 
		WHEN 	cnt."fld_tipopharma" = 5 THEN 'PHARMA_MAX'
		ELSE 'NO'
	END AS CHEK_PHARMA
FROM AFI.P_RDV_DST_LND_SYB.LFC lfc
INNER JOIN AFI.P_RDV_DST_LND_SYB.CNT cnt ON (lfc."fld_funfolio"=cnt."fld_funfolio" AND lfc."fld_funcorrel"=cnt."fld_funcorrel" )
WHERE  lfc."fld_funnotificacod" = 1  AND 
	   lfc."fld_periodoprod" BETWEEN '2021-01-01 00:00:00' AND '2021-01-31 23:59:59'  
)
------- FIN VN PHARMA MAX





----------- UNIVERSO QUIENES MANTIENEN PHARMA MAX
create or replace view EST.P_DDV_EST.periods as (
with nums(i) as (
				select 0 union all select i + 1 from nums where i <=90  ---ajustar para el fin de los meses
			    )    
	select 
		est.P_DDV_EST.fecha_periodo(dateadd(month, i, '2015-01-01 00:00:00.000'::timestamp)) as period 
	from nums
);
CREATE OR REPLACE TABLE EST.P_DDV_EST.CA_PHARMA_MAX_COMPLETO AS ( 
WITH pharma AS ( 
SELECT
	pd.period,
	"fld_cotrut" AS fld_cotrut_mensual,
	"fld_funfolio",
	"fld_funcorrel",
	--est.P_DDV_EST.fecha_periodo("fld_ingreso") AS periodo_vn ,
	--FLD_VIGREALDESDE,
	--FLD_VIGREALHASTA,
	CASE
		WHEN "fld_tipopharma" NOT IN (5) THEN 'renuncia pharma_max'
		ELSE 'pharma_max'
	END AS fld_tpotransac ,
	CASE
		WHEN "fld_tipopharma" NOT IN (5) THEN 'renuncia pharma_max'
		ELSE 'pharma_max'
	END AS fld_tipopharma 
FROM AFI.P_RDV_DST_LND_SYB.CNT cnt
INNER JOIN EST.P_DDV_EST.periods pd ON (pd.period BETWEEN FLD_VIGREALDESDE AND FLD_VIGREALHASTA)
--INNER JOIN est.P_DDV_EST.CA_VN b ON (a."fld_cotrut"= b."fld_cotrut") 
WHERE period >=201701
--a.FLD_VIGREALDESDE <= 201910 AND a.FLD_VIGREALHASTA >= 201910 
--AND "fld_tipopharma" = 5 --AND "fld_finpharma" > 0
--ORDER BY FLD_VIGREALDESDE
UNION
---------- DESAFILIACIONES
SELECT
	EST.P_DDV_EST.FECHA_PERIODO("fld_periodoprod") as PERIOD,
    "fld_cotrut" AS fld_cotrut_mensual,
    "fld_funfolio" ,
    "fld_funcorrel" ,
    --"fld_succod" ,
    case ("fld_funnotificacod")
	    when 2 then 'Desafiliacion isapre'
		when 333 then 'desafiliacion voluntaria'
    end as "fld_tpotransac",
    'renuncia pharma_max' AS fld_tipopharma 
from AFI.P_RDV_DST_LND_SYB.LFC 
	WHERE  "fld_periodoprod" >= '2017-01-01 00:00:00'
	and ("fld_funnotificacod"=2 or "fld_funnotificacod"=333)
)
SELECT  
	period,
	fld_cotrut_mensual,
	ROW_NUMBER () OVER (PARTITION BY fld_cotrut_mensual ORDER BY period) AS n_veces,
	fld_tpotransac ,
	fld_tipopharma 
	FROM pharma
WHERE fld_tpotransac NOT IN ('pharma_max') 
	  --"fld_cotrut" ='015485668-4' 
ORDER BY period , fld_cotrut_mensual 
---LIMIT 1000
)
------- FIN MANTIENEN PHARMA MAX


----- desafilicacion de personas que no contrataron pharma_max
WITH uni AS ( 
SELECT 
	*
FROM EST.P_DDV_EST.CA_PHARMA_MAX_VN a
LEFT JOIN EST.P_DDV_EST.CA_PHARMA_MAX_COMPLETO b ON (a.fld_cotrut_vn=b.fld_cotrut_mensual)
)
, orden_2 AS ( 
SELECT
	PERIODO_VN ,
	period,
	n_veces,
	fld_cotrut_vn,
	ROW_NUMBER () OVER (PARTITION BY fld_cotrut_vn ORDER BY period) AS n_veces_2
	--count(DISTINCT fld_cotrut_vn)
FROM uni
WHERE --PERIODO_VN = 201701 AND 
	  CHEK_PHARMA ='NO' AND 
	  fld_tpotransac ='desafiliacion voluntaria'
	  --AND fld_cotrut_vn = '016607644-7'
	  --AND n_veces = 1
--GROUP BY PERIODO_VN ,period
ORDER BY fld_cotrut_vn
)
SELECT 
	PERIODO_VN ,
	PERIOD,
	COUNT(DISTINCT fld_cotrut_vn) 
FROM orden_2
WHERE n_veces_2 =1
GROUP BY PERIODO_VN,period 
ORDER BY PERIODO_VN , PERIOD
--LIMIT 10



----- datos de quienes renunciaron a pharma_max
WITH uni AS ( 
SELECT 
	*
FROM EST.P_DDV_EST.CA_PHARMA_MAX_VN a
LEFT JOIN EST.P_DDV_EST.CA_PHARMA_MAX_COMPLETO b ON (a.fld_cotrut_vn=b.fld_cotrut_mensual)
)
, renuncia_pm AS ( 
SELECT
	est.P_DDV_EST.fecha_periodo(DATEADD(MONTH, -1, est.P_DDV_EST.periodo_fecha(period))) AS mes_menos,
	*
FROM uni
WHERE CHEK_PHARMA='PHARMA_MAX' AND N_VECES = 1 AND fld_tpotransac='renuncia pharma_max'
)
SELECT 
	a.PERIODO_VN ,
	a.period AS periodo_renuncia,
	a.fld_cotrut_vn,
	to_number(SUBSTR(fld_cotrut_vn,1,9))*5 - 3265112 + 52658 AS ID_TITULAR,
	e."fld_benedad" ,
	b."fld_concostototal",
	CASE 
		WHEN "fld_succod" =16 THEN 'web'
		ELSE 'presencial'
	END AS canal,
	b."fld_comunacod" AS codigo_comuna,
	comu.gls_comuna AS comuna,
	b."fld_region" AS codigo_region,
	b."fld_ciudad" AS cuidad,
	b."fld_succod" AS codigo_sucursal	,
	sucu.GLS_SUCUR AS sucursal,
	b."fld_agecod" AS codigo_agente ,
	b."fld_agencianum" AS codigo_agencia,
	c.MONTO_EXCEDENTE 
FROM renuncia_pm a 
left JOIN AFI.P_RDV_DST_LND_SYB.CNT b ON (a.fld_cotrut_vn = b."fld_cotrut" AND a.mes_menos BETWEEN fld_vigrealdesde AND fld_vigrealhasta)
left JOIN AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO c ON (a.fld_cotrut_vn=c.rut_titular AND a.mes_menos = c.periodo_vigencia)
left JOIN AFI.P_RDV_DST_LND_SYB.BEN e ON (a.fld_cotrut_vn = e."fld_cotrut" AND a.mes_menos BETWEEN e.FLD_BENVIGREALDESDE AND e.FLD_BENVIGREALHASTA AND e."fld_bencorrel"=0)
left JOIN ISA.P_RDV_DST_LND_SYB.ISAPRECOMUNAS comu ON (b."fld_comunacod"= comu.COD_COMUNA)
LEFT JOIN ISA.P_RDV_DST_LND_SYB.ISAPRESUCURSAL sucu ON (b."fld_succod" = sucu.COD_SUCUR)
WHERE PERIODO_VN <= periodo_renuncia ---AND PERIODO_VN =201901
ORDER BY PERIODO_VN , periodo_renuncia
--LIMIT 100





SELECT * FROM agencia





SELECT * FROM RCD.P_RDV_DST_LND_SYB.EXC_CTA --e ON (a."fld_cotrut"=e."cta_rut" AND e."cta_estado"=0)
WHERE "cta_estado" =0 AND 
	  "cta_rut"='025086820-0'
LIMIT 10




SELECT * FROM AFI.P_DDV_AFI.P_DDV_AFI_CONTRATO
WHERE RUT_TITULAR ='025086820-0' AND PERIODO_VIGENCIA ='201807'
LIMIT 10


SELECT age FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE "fld_cotrut" ='011844505-8'
LIMIT 10



SELECT
	PERIODO_VN ,
	period,
	COUNT(DISTINCT fld_cotrut_vn) 
FROM uni
WHERE --PERIODO_VN = 201701 AND 
	  CHEK_PHARMA ='NO' AND 
	  fld_tpotransac ='desafiliacion voluntaria'
GROUP BY PERIODO_VN ,period
ORDER BY periodo_vn ,period
--LIMIT 10



SELECT 
	PERIODO_VN ,
	CHEK_PHARMA ,
	count(DISTINCT fld_cotrut_vn)
FROM EST.P_DDV_EST.CA_PHARMA_MAX_VN
GROUP BY PERIODO_VN , CHEK_PHARMA 
ORDER BY PERIODO_VN 
LIMIT 10



SELECT
	*
FROM EST.P_DDV_EST.CA_PHARMA_MAX_VN a 
INNER JOIN EST.P_DDV_EST.CA_PHARMA_MAX_VN ()





SELECT * FROM EST.P_DDV_EST.CA_PHARMA_MAX_VN 
WHERE "fld_cotrut" ='006870461-8'
LIMIT 10


SELECT 
	* 
FROM EST.P_DDV_EST.CA_PHARMA_MAX_COMPLETO
--WHERE "fld_cotrut" ='016359478-1'
ORDER BY PERIOD 
LIMIT 10


SELECT
	*
FROM AFI.P_RDV_DST_LND_SYB.LFC
LIMIT 10


SELECT DISTINCT 
	 "fld_funnotificacod" 
FROM AFI.P_RDV_DST_LND_SYB.LFC
ORDER BY "fld_funnotificacod" 
LIMIT 100



------------------------ revision codigos


---------- DESAFILIACIONES
SELECT
	EST.P_DDV_EST.FECHA_PERIODO("fld_periodoprod") as PERIOD,
    "fld_cotrut",
    case ("fld_funnotificacod")
	    when 2 then 'Desafiliacion'
		when 333 then 'renuncia voluntaria'
    end as "fld_tpotransac",
    0 AS fld_tipopharma 
from AFI.P_RDV_DST_LND_SYB.LFC 
	WHERE  "fld_periodoprod" >= '2017-01-01 00:00:00'
	and ("fld_funnotificacod"=2 or "fld_funnotificacod"=333)
LIMIT 100







SELECT "fld_tipopharma" ,* FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE "fld_cotrut" ='007716043-4' 
LIMIT 10


SELECT * FROM AFI.P_RDV_DST_LND_SYB.LFC
WHERE "fld_cotrut" ='007716043-4'

SELECT * FROM EST.P_DDV_EST.CA_PHARMA_MAX_COMPLETO
WHERE "fld_cotrut"='006779731-0'
ORDER BY period
LIMIT 100

SELECT * FROM isa.P_RDV_DST_LND_SYB.PREC_TIPFUN LIMIT 10



SELECT * FROM 

LIMIT 10


SELECT "fld_funfolio" ,
		"fld_funcorrel" ,
		"fld_cotrut" ,
		FLD_VIGREALDESDE ,
		FLD_VIGREALHASTA ,
		"fld_tipopharma" ,
		"fld_ingreso" 
FROM AFI.P_RDV_DST_LND_SYB.CNT
WHERE "fld_cotrut" ='015485668-4'
ORDER BY FLD_VIGREALDESDE 	 
LIMIT 10



SELECT * FROM AFI.P_RDV_DST_LND_SYB.BAD
WHERE "bad_cotrut" ='018466161-6'
LIMIT 10
		
		
SELECT 
	cpw."bad_nombre",
	bad.* 
FROM AFI.P_RDV_DST_LND_SYB.BAD bad
INNER JOIN ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL cpw ON (bad."bad_producto" = cpw."bad_idn")
WHERE 
	--bad."bad_producto" IN (65,66) 
	--AND 
	bad."bad_cotrut" ='018466161-6'
LIMIT 10 

SELECT * FROM ISA.P_RDV_DST_LND_SYB.CPW_BENADICIONAL


SELECT * FROM "LCC"."P_DDV_LCC"."P_DDV_LCC_HOJA_DEVIDA"
LIMIT 10


select * from est.P_DDV_EST.ca_cia_seguro_2020
limit 10

